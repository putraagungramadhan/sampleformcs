﻿using AutoMapper;
using NP_API.Common.Resolver;
using NP_API.Dto;
using NP_API.Dto.Custom;
using NP_API.Dto.Relation;
using NP_API.Dto.Request.Company;
using NP_API.Dto.Request.CompanyBranch;
using NP_API.Dto.Request.CompanyClass;
using NP_API.Dto.Request.CompanyDocument;
using NP_API.Dto.Request.CompanyGrade;
using NP_API.Dto.Request.CompanyGroup;
using NP_API.Dto.Request.CompanyGroupEmployee;
using NP_API.Dto.Request.CompanyLevel;
using NP_API.Dto.Request.CompanyOrganization;
using NP_API.Dto.Request.CompanyTitle;
using NP_API.Dto.Request.Feedback;
using NP_API.Dto.Request.News;
using NP_API.Dto.Request.NewsCategory;
using NP_API.Dto.Request.User;
using NP_API.Dto.Request.UserPermission;
using NP_API.Models;
using NP_API.Dto.Request.SetupConfigurations;

namespace NP_API.Common
{
    public class MapperProfile : Profile
    {
        public MapperProfile()
        {
            CreateMap<CompanyGrade, CompanyGradeResultDto>();
            CreateMap<Company, CompanyResultDto>().ForMember(dest => dest.LogoDecode, opt => opt.MapFrom<LogoDecodeValueResolver>());
            CreateMap<Company, CompanyResultOnGradeDto>();
            CreateMap<Company, CompanyRegisterDto>();
            CreateMap<Company, UpdateCompanyDto>();
            CreateMap<UpdateCompanyDto, Company>();
            CreateMap<Company, CompanyResultOnRelationDto>().ForMember(dest => dest.LogoDecode, opt => opt.MapFrom<LogoDecodeValueCompanyRelationResolver>());
            CreateMap<CompanyRegisterDto, Company>();

            CreateMap<CompanyOrganization, CompanyOrganizationDto>();
            CreateMap<CompanyOrganization, CompanyOrganizationResultDto>();
            CreateMap<CompanyOrganization, CompanyOrganizationResultOnRelationDto>();
            CreateMap<CompanyOrganizationDto, CompanyOrganization>();
            CreateMap<UpdateOrganizationDto, CompanyOrganization>();

            CreateMap<CompanyLevel, CompanyLevelDto>();
            CreateMap<CompanyLevel, CompanyLevelResultDto>();
            CreateMap<CompanyLevel, CompanyLevelResultOnRelationDto>();
            CreateMap<CompanyLevelDto, CompanyLevel>();
            CreateMap<UpdateLevelDto, CompanyLevel>();

            CreateMap<CompanyDocumentDto, CompanyDocument>();
            CreateMap<CompanyDocument, CompanyDocumentDto>();
            CreateMap<CompanyDocument, CompanyDocumentResultDto>().ForMember(d => d.file, Op => Op.MapFrom<FileDecodeCompanyDocumentRelationResolver>());
            CreateMap<NewCompanyDocumentDto, CompanyDocument>();
            CreateMap<UpdateCompanyDocumentDto, CompanyDocument>();
            CreateMap<CompanyDocument, UpdateCompanyDocumentDto > ();

            CreateMap<CompanyTitle, CompanyTitleDto>();
            CreateMap<CompanyTitle, CompanyTitleResultDto>();
            CreateMap<CompanyTitle, CompanyTitleResultOnRelationDto>();
            CreateMap<CompanyTitleDto, CompanyTitle>();
            CreateMap<CompanyTitleResultDto, CompanyTitle>();
            CreateMap<UpdateCompanyTitleDto, CompanyTitle>();

            CreateMap<CompanyGrade, CompanyGradeDto>();
            CreateMap<CompanyGrade, CompanyGradeResultDto>();
            CreateMap<CompanyGrade, CompanyGradeResultOnRelationDto>();
            CreateMap<CompanyGradeDto, CompanyGrade>();
            CreateMap<CompanyGradeResultDto, CompanyGrade>();
            CreateMap<UpdateGradeDto, CompanyGrade>();

            CreateMap<CompanyClass, CompanyClassDto>();
            CreateMap<CompanyClass, CompanyClassResultDto>();
            CreateMap<CompanyClass, CompanyClassResultOnRelationDto>();
            CreateMap<CompanyClassDto, CompanyClass>();
            CreateMap<UpdateClassDto, CompanyClass>();

            CreateMap<CompanyGroupEmployee, CompanyGroupEmployeeOnRelationDto>();
            CreateMap<CompanyGroupEmployee, CompanyGroupEmployeeDto>();
            CreateMap<CompanyGroupEmployee, CompanyGroupEmployeeResultDto>();
            CreateMap<CompanyGroupEmployeeResultDto, CompanyGroupEmployee>();
            CreateMap<CompanyGroupEmployeeDto, CompanyGroupEmployee>();
            CreateMap<UpdateGroupEmployeeDto, CompanyGroupEmployee>();
            CreateMap<UpdateGroupEmployeeDto, CompanyGroupEmployeeDto>();

            CreateMap<User, UserResultOnRelationDto>();
            CreateMap<User, AdminRegisterDto>();
            CreateMap<User, NewUserDto>();
            CreateMap<AdminRegisterDto, User>();
            CreateMap<User, UserLoginDto>();
            CreateMap<User, UserResultDto>();
            CreateMap<NewUserDto, User>();
            CreateMap<UserLoginDto, User>();
     
            CreateMap<NewsCategoryDto, NewsCategory>();
            CreateMap<NewsCategory, NewsCategoryResultDto>();
            CreateMap<NewsCategory, NewsCategoryDto>();
            CreateMap<NewNewsCategoryDto, NewsCategory>();
            CreateMap<UpdateNewsCategoryDto, NewsCategory>();
            CreateMap<NewsCategory, UpdateNewsCategoryDto>();

            CreateMap<NewsDto, News>();
            CreateMap<News, NewsResultDto>();
            CreateMap<News, NewsDto>();
            CreateMap<NewNewsDto, News>();
            CreateMap<UpdateNewsDto, News>();
            CreateMap<News, UpdateNewsDto>();

            CreateMap<NewSetupConfigurationDto, SetupConfigurations>();
            CreateMap<UpdateSetupConfigurationDto, SetupConfigurations>();
            CreateMap<SetupConfigurationResultDto, SetupConfigurations>();
            CreateMap<SetupConfigurations, UpdateSetupConfigurationDto>();
            CreateMap<SetupConfigurations, NewSetupConfigurationDto>();
            CreateMap<SetupConfigurations, SetupConfigurationResultDto>();

            CreateMap<FeedbackDto, Feedback>();
            CreateMap<Feedback, FeedbackResultDto>();
            CreateMap<Feedback, FeedbackDto>();
            CreateMap<NewFeedbackDto, Feedback>();
            CreateMap<UpdateFeedbackDto, Feedback>();
            CreateMap<Feedback, UpdateFeedbackDto>();
            CreateMap<CompanyBranch, CompanyBranchResultOnRelationDto>();
            CreateMap<CompanyBranch, CompanyBranchResultDto>();
            CreateMap<CompanyBranchResultDto, CompanyBranch>();
            CreateMap<CompanyBranchDto, CompanyBranch>();
            CreateMap<UpdateCompanyBranchDto, CompanyBranch>();
            CreateMap<Module, ModuleOnRelationDto>();

            CreateMap<UserPermission, UserPermissionResultDto>();
            CreateMap<UserPermissionResultDto, UserPermission>();
            CreateMap<UserPermissionDto, UserPermission>();
            CreateMap<UpdateUserPermissionDto, UserPermission>();

            CreateMap<CompanyGroup, CompanyGroupOnRelationDto>();
            CreateMap<CompanyGroup, CompanyGroupResultDto>();
            CreateMap<CompanyGroupResultDto, CompanyGroup>();
            CreateMap<CompanyGroupDto, CompanyGroup>();
            CreateMap<UpdateCompanyGroupDto, CompanyGroup>();
        }
    }
}
