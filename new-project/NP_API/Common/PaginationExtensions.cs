﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;
using System.Linq.Expressions;
using static StackExchange.Redis.Role;

namespace NP_API.Common
{
    public static class PaginationExtensions
    {
        public static async Task<PagedList<TDestination>> PaginateAsync<T, TDestination>(
        this IQueryable<T> query,
        IMapper _mapper,
        int page,
        int pageSize,
        Expression<Func<T, bool>>[] wherePredicates = null,
        string orderBy = null,
        bool ascending = true) where TDestination : class
        {
            if (wherePredicates.Length > 0)
            {
                var combinedPredicate = CombinePredicatesWithAnd(wherePredicates);
                query = query.Where(combinedPredicate);
            }

            if (!string.IsNullOrEmpty(orderBy))
            {
                // Create an Expression<Func<T, object>> predicate based on the orderBy parameter
                var parameter = Expression.Parameter(typeof(T));
                var property = Expression.PropertyOrField(parameter, orderBy);
                var lambda = Expression.Lambda<Func<T, object>>(Expression.Convert(property, typeof(object)), parameter);

                // Apply ordering based on the dynamically created predicate
                query = ascending
                    ? query.OrderBy(lambda)
                    : query.OrderByDescending(lambda);
            }
            var rawSql = query.ToQueryString();
            Console.WriteLine("Raw SQL Query: " + rawSql);
            var totalItems = await query.CountAsync();
            var items = await query
                .Skip((page - 1) * pageSize)
                .Take(pageSize)
                .ToListAsync();
            var mappedItems = _mapper.Map<List<TDestination>>(items);

            return new PagedList<TDestination>(mappedItems, totalItems, page, pageSize);
        }
        private static Expression<Func<T, bool>> CombinePredicatesWithAnd<T>(
        Expression<Func<T, bool>>[] predicates)
        {
            if (predicates == null || predicates.Length == 0)
            {
                return null;
            }

            var parameter = Expression.Parameter(typeof(T));
            Expression combinedPredicate = Expression.Constant(true); // Start with true to create "AND" conditions

            foreach (var predicate in predicates)
            {
                var body = new RebindParameterVisitor(
                    predicate.Parameters[0], parameter)
                    .Visit(predicate.Body);
                combinedPredicate = Expression.AndAlso(combinedPredicate, body);
            }

            return Expression.Lambda<Func<T, bool>>(combinedPredicate, parameter);
        }
    }
    public class PagedList<T>
    {
        public List<T> Items { get; }
        public int TotalItems { get; }
        public int Page { get; }
        public int PageSize { get; }
        public int LastPage { get; }

        public PagedList(List<T> items, int totalItems, int page, int pageSize)
        {
            Items = items;
            TotalItems = totalItems;
            Page = page;
            PageSize = pageSize;
            LastPage = (int)Math.Ceiling((double)totalItems / pageSize);
        }
    }

    public class RebindParameterVisitor : ExpressionVisitor
    {
        private readonly ParameterExpression _oldParameter;
        private readonly ParameterExpression _newParameter;

        public RebindParameterVisitor(
            ParameterExpression oldParameter,
            ParameterExpression newParameter)
        {
            _oldParameter = oldParameter;
            _newParameter = newParameter;
        }

        protected override Expression VisitParameter(ParameterExpression node)
        {
            return node == _oldParameter ? _newParameter : base.VisitParameter(node);
        }
    }
}
