using NP_API.Dto;

namespace NP_API.Common
{
    public class RequestHandlerAsync<T>
  {
    public async Task<ResultDto> getResultAsync(Func<Task<T>> processRequest, string message = "")
    {
        ResultDto result = new ResultDto();
        T obj = await Task.Run<T>(() => processRequest());
        if (!obj.Equals(null))
        {
            result.data = obj;
            result.title = message;
            result.isSuccess = true;
            result.error = null;
        }
        else
        {
            result.data = null;
            result.title = string.Empty;
            result.isSuccess = false;
            result.error = "Request failed";
        }
        
        return result;
    }
  }
}