﻿using AutoMapper;
using NP_API.Config;
using NP_API.Dto.Relation;
using NP_API.Dto.Request.CompanyDocument;
using NP_API.Models;
using Microsoft.Extensions.Options;

namespace NP_API.Common.Resolver
{
    public class FileDecodeCompanyDocumentRelationResolver : IValueResolver<CompanyDocument, CompanyDocumentResultDto,string>
    {

        private readonly S3Config _config;

        public FileDecodeCompanyDocumentRelationResolver(IOptionsMonitor<S3Config> optionsMonitor)
        {
            this._config = optionsMonitor.CurrentValue;
        }
        public string Resolve(CompanyDocument source, CompanyDocumentResultDto destination, string destMember, ResolutionContext context)
        {
            var filePath = "https://" + _config.Endpoint + "/" + _config.BucketName + "/company/document/" + source.Guid.ToString() + "/file/" + source.file ;
            return filePath;
        }
    }
}
