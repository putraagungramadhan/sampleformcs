﻿using AutoMapper;
using NP_API.Config;
using NP_API.Dto.Relation;
using NP_API.Dto.Request.Company;
using NP_API.Models;
using Microsoft.Extensions.Options;

namespace NP_API.Common.Resolver
{
    public class LogoDecodeValueCompanyRelationResolver : IValueResolver<Company, CompanyResultOnRelationDto, string>
    {
        private readonly S3Config _config;

        public LogoDecodeValueCompanyRelationResolver(IOptionsMonitor<S3Config> optionsMonitor)
        {
            this._config = optionsMonitor.CurrentValue;
        }
        public string Resolve(Company source, CompanyResultOnRelationDto destination, string destMember, ResolutionContext context)
        {
            var logoPath = "https://" + _config.Endpoint + "/" + _config.BucketName + "/logo/" + source.Id.ToString() + "/" + source.Logo;
            return logoPath;
        }
    }
}
