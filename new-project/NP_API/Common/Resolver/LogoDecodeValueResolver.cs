﻿using AutoMapper;
using NP_API.Config;
using NP_API.Dto.Request.Company;
using NP_API.Models;
using Microsoft.Extensions.Options;
using Minio;

namespace NP_API.Common.Resolver
{
    public class LogoDecodeValueResolver : IValueResolver<Company, CompanyResultDto, string>
    {
        private readonly S3Config _config;

        public LogoDecodeValueResolver(IOptionsMonitor<S3Config> optionsMonitor)
        {
            this._config = optionsMonitor.CurrentValue;
        }
        public string Resolve(Company source, CompanyResultDto destination, string destMember, ResolutionContext context)
        {
            var logoPath = "https://"+_config.Endpoint + "/" + _config.BucketName + "/logo/" + source.Id.ToString() + "/" + source.Logo;
            return logoPath;
        }
    }
}
