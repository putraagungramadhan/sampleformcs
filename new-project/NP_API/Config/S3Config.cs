﻿namespace NP_API.Config
{
    public class S3Config
    {
        public string BucketName { get; set; }
        public string Region { get; set; }
        public string AccessKey { get; set; }
        public string SecretAccessKey { get; set; }
        public string Endpoint { get; set; }
    }
}
