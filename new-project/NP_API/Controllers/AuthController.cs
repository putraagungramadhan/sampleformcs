﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Net.Http.Headers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using NP_API.Service.Interface;
using NP_API.Service;
using NP_API.Common;
using NP_API.Dto;
using Microsoft.AspNetCore.Http;
using NP_API.Dto.Custom;
using NP_API.Exceptions;
using NP_API.Validation.Company;
using System.Text.Json;
using NP_API.Validation.Auth;

namespace NP_API.Controllers
{
    [ApiController]
    [Route("api/v1/auth/")]
    public class AuthController : ControllerBase
    {
        private readonly IAuthService _authService;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly string? _token;
        public AuthController(IAuthService authService, IHttpContextAccessor httpContextAccessor)
        {
            this._authService = authService;
            this._httpContextAccessor = httpContextAccessor;
            this._token = _httpContextAccessor.HttpContext.Request.Headers["authorization"];
        }

        [HttpPost("login")]
        public async Task<ResultDto> login([FromBody] LoginDto dto)
        {
            LoginValidator validator = new LoginValidator();
            var validationResult = validator.Validate(dto);

            if (!validationResult.IsValid)
            {
                throw new BadRequestException(JsonSerializer.Serialize(validationResult.Errors, new JsonSerializerOptions
                {
                    WriteIndented = true
                }));
            }

            RequestHandlerAsync<UserLoginDto> requestHandler = new RequestHandlerAsync<UserLoginDto>();
            return await requestHandler.getResultAsync(() => _authService.login(dto));
        }
        [HttpPost("logout"), Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<ResultDto> logout()
        {
            RequestHandlerAsync<ResultDto> requestHandler = new RequestHandlerAsync<ResultDto>();
            return await requestHandler.getResultAsync(() => _authService.logout(_token));
        }
    }
}
