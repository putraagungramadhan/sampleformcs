﻿using NP_API.Common;
using NP_API.Dto;
using NP_API.Service.Interface;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NP_API.Dto.Request.CompanyBranch;
using NP_API.Exceptions;
using NP_API.Service;
using System.Text.Json;
using NP_API.Validation.CompanyBranch;
using FluentValidation;
using NP_API.Dto.Custom;
using NP_API.Validation;

namespace NP_API.Controllers
{
    [ApiController]
    [Route("api/v1/company/branch"), Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class CompanyBranchController : ControllerBase
    {
        private readonly ICompanyBranchService _companyBranchService;
        public CompanyBranchController(ICompanyBranchService companyBranchService)
        {
            _companyBranchService = companyBranchService;
        }
        [HttpGet("")]
        public async Task<ResultDto> Get([FromQuery] GetDto dto)
        {
            RequestHandlerAsync<PagedList<CompanyBranchResultDto>> requestHandler = new RequestHandlerAsync<PagedList<CompanyBranchResultDto>>();
            return await requestHandler.getResultAsync(() => _companyBranchService.GetData(dto), "Berhasil ambil data");
        }
        [HttpPost("create")]
        public async Task<ResultDto> Create([FromBody] CompanyBranchDto dto)
        {
            CreateValidator validator = new CreateValidator();
            var validationResult = validator.Validate(dto);

            if (!validationResult.IsValid)
            {
                throw new BadRequestException(JsonSerializer.Serialize(validationResult.Errors, new JsonSerializerOptions
                {
                    WriteIndented = true
                }));
            }

            RequestHandlerAsync<CompanyBranchResultDto> requestHandler = new RequestHandlerAsync<CompanyBranchResultDto>();
            return await requestHandler.getResultAsync(() => _companyBranchService.Save(dto), "Berhasil simpan data");
        }
        [HttpPost("update")]
        public async Task<ResultDto> Update([FromBody] UpdateCompanyBranchDto dto)
        {
            UpdateValidator validator = new UpdateValidator();
            var validationResult = validator.Validate(dto);

            if (!validationResult.IsValid)
            {
                throw new BadRequestException(JsonSerializer.Serialize(validationResult.Errors, new JsonSerializerOptions
                {
                    WriteIndented = true
                }));
            }

            RequestHandlerAsync<CompanyBranchResultDto> requestHandler = new RequestHandlerAsync<CompanyBranchResultDto>();
            return await requestHandler.getResultAsync(() => _companyBranchService.Update(dto), "Berhasil update data");
        }
        [HttpPost("Delete")]
        public async Task<ResultDto> Delete([FromBody] DeleteDto dto)
        {
            DeleteValidator validator = new DeleteValidator();
            var validationResult = validator.Validate(dto);

            if (!validationResult.IsValid)
            {
                throw new BadRequestException(JsonSerializer.Serialize(validationResult.Errors, new JsonSerializerOptions
                {
                    WriteIndented = true
                }));
            }

            RequestHandlerAsync<bool> requestHandler = new RequestHandlerAsync<bool>();
            return await requestHandler.getResultAsync(() => _companyBranchService.Delete(dto), "Berhasil hapus data");
        }
    }
}
