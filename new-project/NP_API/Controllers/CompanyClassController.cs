﻿using NP_API.Common;
using NP_API.Dto.Request.CompanyClass;
using NP_API.Dto;
using NP_API.Service.Interface;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using NP_API.Exceptions;
using NP_API.Models;
using NP_API.Validation.CompanyClass;
using System.Text.Json;
using NP_API.Dto.Custom;
using NP_API.Validation;

namespace NP_API.Controllers
{
    [ApiController]
    [Route("api/v1/company/class"), Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class CompanyClassController : ControllerBase
    {
        private readonly ICompanyClassService _classService;
        public CompanyClassController(ICompanyClassService classService)
        {
            this._classService = classService;
        }
        [HttpGet("")]
        public async Task<ResultDto> Get([FromQuery] GetDto dto)
        {
            RequestHandlerAsync<PagedList<CompanyClassResultDto>> requestHandler = new RequestHandlerAsync<PagedList<CompanyClassResultDto>>();
            return await requestHandler.getResultAsync(() => _classService.GetData(dto));
        }
        [HttpPost("create")]
        public async Task<ResultDto> Create(CompanyClassDto dto)
        {
            CreateValidator validator = new CreateValidator();
            var validationResult = validator.Validate(dto);

            if (!validationResult.IsValid)
            {
                throw new BadRequestException(JsonSerializer.Serialize(validationResult.Errors, new JsonSerializerOptions
                {
                    WriteIndented = true
                }));
            }

            RequestHandlerAsync<CompanyClass> requestHandler = new RequestHandlerAsync<CompanyClass>();
            return await requestHandler.getResultAsync(() => _classService.Create(dto));
        }
        [HttpPost("update")]
        public async Task<ResultDto> Update(UpdateClassDto dto)
        {
            UpdateValidator validator = new UpdateValidator();
            var validationResult = validator.Validate(dto);

            if (!validationResult.IsValid)
            {
                throw new BadRequestException(JsonSerializer.Serialize(validationResult.Errors, new JsonSerializerOptions
                {
                    WriteIndented = true
                }));
            }

            RequestHandlerAsync<CompanyClass> requestHandler = new RequestHandlerAsync<CompanyClass>();
            return await requestHandler.getResultAsync(() => _classService.Update(dto));
        }
        [HttpPost("delete")]
        public async Task<ResultDto> Delete(DeleteDto dto)
        {
            DeleteValidator validator = new DeleteValidator();
            var validationResult = validator.Validate(dto);

            if (!validationResult.IsValid)
            {
                throw new BadRequestException(JsonSerializer.Serialize(validationResult.Errors, new JsonSerializerOptions
                {
                    WriteIndented = true
                }));
            }

            RequestHandlerAsync<bool> requestHandler = new RequestHandlerAsync<bool>();
            return await requestHandler.getResultAsync(() => _classService.Delete(dto));
        }
    }
}
