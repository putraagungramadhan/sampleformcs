﻿using NP_API.Common;
using NP_API.Dto;
using NP_API.Models;
using NP_API.Service.Interface;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using NP_API.Validation.Company;
using NP_API.Exceptions;
using System.Text.Json;
using NP_API.Dto.Request.Company;
// using NP_API.Dto.Request.CompanyTitle;

namespace NP_API.Controllers
{
    [ApiController]
    [Route("api/v1/company/"), Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class CompanyController : ControllerBase
    {
        private readonly ICompanyService _companyService;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly string? _token;
        public CompanyController(ICompanyService companyService, IHttpContextAccessor httpContextAccessor)
        {
            this._companyService = companyService;
            this._httpContextAccessor = httpContextAccessor;
            this._token = _httpContextAccessor.HttpContext.Request.Headers["authorization"];
        }
        [HttpGet("")]
        public async Task<ResultDto> Get([FromQuery] GetDto dto)
        {
            RequestHandlerAsync<PagedList<CompanyResultDto>> requestHandler = new RequestHandlerAsync<PagedList<CompanyResultDto>>();
            return await requestHandler.getResultAsync(() => _companyService.GetData(dto));
        }
        [HttpPost("register")]
        public async Task<ResultDto> register([FromForm] NewCompanyDto dto)
        {
            SaveValidator validator = new SaveValidator();
            var validationResult = validator.Validate(dto);

            if (!validationResult.IsValid)
            {
                throw new BadRequestException(JsonSerializer.Serialize(validationResult.Errors, new JsonSerializerOptions
                {
                    WriteIndented = true
                }));
            }

            RequestHandlerAsync<Company> requestHandler = new RequestHandlerAsync<Company>();
            return await requestHandler.getResultAsync(() => _companyService.RegisterCompany(dto));
        }

        [HttpPost("update")]
        public async Task<ResultDto> Update([FromForm] UpdateCompanyDto dto)
        {
            UpdateValidator validator = new UpdateValidator();
            var saveResult = validator.Validate(dto);

            if (!saveResult.IsValid)
            {
                throw new BadRequestException(JsonSerializer.Serialize(saveResult.Errors, new JsonSerializerOptions
                {
                    WriteIndented = true
                }));
            }

            RequestHandlerAsync<CompanyResultDto> requestHandler = new RequestHandlerAsync<CompanyResultDto>();
            return await requestHandler.getResultAsync(() => _companyService.Update(dto), "Berhasil update data");

        }
    }
}
