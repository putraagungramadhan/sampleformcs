﻿using NP_API.Common;
using NP_API.Dto;
using NP_API.Exceptions;
using NP_API.Models;
using NP_API.Service.Interface;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Text.Json;
using NP_API.Dto.Request.CompanyDocument;
using NP_API.Validation.CompanyDocument;
using NP_API.Dto.Custom;
using NP_API.Service;
using NP_API.Validation;

namespace NP_API.Controllers
{
    [ApiController]
    [Route("api/v1/company-document/"), Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class CompanyDocumentController : ControllerBase
    {

        private readonly ICompanyDocumentService _companyDocumentService;
        public CompanyDocumentController(ICompanyDocumentService companyDocumentService)
        {
            this._companyDocumentService = companyDocumentService;
        }

        [HttpGet("")]
        public async Task<ResultDto> Get([FromQuery] GetDto dto)
        {
            RequestHandlerAsync<PagedList<CompanyDocumentResultDto>> requestHandler = new RequestHandlerAsync<PagedList<CompanyDocumentResultDto>>();
            return await requestHandler.getResultAsync(() => _companyDocumentService.GetData(dto));
        }
        [HttpPost("create")]
        public async Task<ResultDto> Create([FromForm] NewCompanyDocumentDto dto)
        {
            CreateValidator validator = new CreateValidator();
            var validationResult = validator.Validate(dto);

            if (!validationResult.IsValid)
            {
                throw new BadRequestException(JsonSerializer.Serialize(validationResult.Errors, new JsonSerializerOptions
                {
                    WriteIndented = true
                }));
            }

            RequestHandlerAsync<CompanyDocumentResultDto> requestHandler = new RequestHandlerAsync<CompanyDocumentResultDto>();
            return await requestHandler.getResultAsync(() => _companyDocumentService.Create(dto), "Berhasil simpan dokumen perusahaan");
        }

        [HttpPost("update")]
        public async Task<ResultDto> Update([FromForm] UpdateCompanyDocumentDto dto)
        {
            UpdateValidator validator = new UpdateValidator();
            var validationResult = validator.Validate(dto);

            if (!validationResult.IsValid)
            {
                throw new BadRequestException(JsonSerializer.Serialize(validationResult.Errors, new JsonSerializerOptions
                {
                    WriteIndented = true
                }));
            }

            RequestHandlerAsync<CompanyDocumentResultDto> requestHandler = new RequestHandlerAsync<CompanyDocumentResultDto>();
            return await requestHandler.getResultAsync(() => _companyDocumentService.Update(dto), "Berhasil update dokumen perusahaan");
        }

        [HttpPost("delete")]
        public async Task<ResultDto> Delete(DeleteDto dto)
        {
            DeleteValidator validator = new DeleteValidator();
            var validationResult = validator.Validate(dto);

            if (!validationResult.IsValid)
            {
                throw new BadRequestException(JsonSerializer.Serialize(validationResult.Errors, new JsonSerializerOptions
                {
                    WriteIndented = true
                }));
            }

            RequestHandlerAsync<bool> requestHandler = new RequestHandlerAsync<bool>();
            return await requestHandler.getResultAsync(() => _companyDocumentService.Delete(dto));
        }
    }
}
