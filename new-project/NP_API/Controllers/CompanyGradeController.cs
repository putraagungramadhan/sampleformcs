﻿using NP_API.Common;
using NP_API.Dto;
using NP_API.Exceptions;
using NP_API.Models;
using NP_API.Service.Interface;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Mvc;
using System.Text.Json;
using Microsoft.AspNetCore.Authorization;
using NP_API.Validation.CompanyGrade;
using NP_API.Dto.Request.CompanyGrade;
using NP_API.Dto.Custom;
using NP_API.Validation;
using NP_API.Dto.Request;

namespace NP_API.Controllers
{
    [ApiController]
    [Route("api/v1/company/grade")]
    public class CompanyGradeController : ControllerBase
    {
        private readonly ICompanyGradeService _gradeService;
        public CompanyGradeController(ICompanyGradeService gradeService)
        {
            this._gradeService = gradeService;
        }
        [HttpGet(""), Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<ResultDto> Get([FromQuery]GetDto dto)
        {
            RequestHandlerAsync<PagedList<CompanyGradeResultDto>> requestHandler = new RequestHandlerAsync<PagedList<CompanyGradeResultDto>>();
            return await requestHandler.getResultAsync(() => _gradeService.GetData(dto));
        }
        [HttpPost("create"), Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<ResultDto> Create(CompanyGradeDto dto)
        {
            CreateValidator validator = new CreateValidator();
            var validationResult = validator.Validate(dto);

            if (!validationResult.IsValid)
            {
                throw new BadRequestException(JsonSerializer.Serialize(validationResult.Errors, new JsonSerializerOptions
                {
                    WriteIndented = true
                }));
            }

            RequestHandlerAsync<CompanyGrade> requestHandler = new RequestHandlerAsync<CompanyGrade>();
            return await requestHandler.getResultAsync(() => _gradeService.Create(dto));
        }
        [HttpPost("update"), Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<ResultDto> Update(UpdateGradeDto dto)
        {
            UpdateValidator validator = new UpdateValidator();
            var validationResult = validator.Validate(dto);

            if (!validationResult.IsValid)
            {
                throw new BadRequestException(JsonSerializer.Serialize(validationResult.Errors, new JsonSerializerOptions
                {
                    WriteIndented = true
                }));
            }

            RequestHandlerAsync<CompanyGrade> requestHandler = new RequestHandlerAsync<CompanyGrade>();
            return await requestHandler.getResultAsync(() => _gradeService.Update(dto));
        }
        [HttpPost("delete"), Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<ResultDto> Delete(DeleteDto dto)
        {
            DeleteValidator validator = new DeleteValidator();
            var validationResult = validator.Validate(dto);

            if (!validationResult.IsValid)
            {
                throw new BadRequestException(JsonSerializer.Serialize(validationResult.Errors, new JsonSerializerOptions
                {
                    WriteIndented = true
                }));
            }

            RequestHandlerAsync<bool> requestHandler = new RequestHandlerAsync<bool>();
            return await requestHandler.getResultAsync(() => _gradeService.Delete(dto));
        }
    }
}
