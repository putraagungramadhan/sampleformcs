﻿using NP_API.Common;
using NP_API.Dto.Request.CompanyGroupEmployee;
using NP_API.Dto;
using NP_API.Service.Interface;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NP_API.Exceptions;
using NP_API.Models;
using System.Text.Json;
using NP_API.Validation.CompanyGroupEmployee;
using NP_API.Dto.Custom;
using NP_API.Validation;

namespace NP_API.Controllers
{
    [ApiController]
    [Route("api/v1/company/group"), Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class CompanyGroupEmployeeController : ControllerBase
    {
        private readonly ICompanyGroupEmployeeService _groupService;
        public CompanyGroupEmployeeController(ICompanyGroupEmployeeService groupService)
        {
            this._groupService = groupService;
        }
        [HttpGet("")]
        public async Task<ResultDto> Get([FromQuery] GetDto dto)
        {
            RequestHandlerAsync<PagedList<CompanyGroupEmployeeResultDto>> requestHandler = new RequestHandlerAsync<PagedList<CompanyGroupEmployeeResultDto>>();
            return await requestHandler.getResultAsync(() => _groupService.GetData(dto));
        }
        [HttpPost("create")]
        public async Task<ResultDto> Create(CompanyGroupEmployeeDto dto)
        {
            RequestHandlerAsync<CompanyGroupEmployee> requestHandler = new RequestHandlerAsync<CompanyGroupEmployee>();
            return await requestHandler.getResultAsync(() => _groupService.Create(dto));
        }
        [HttpPost("update")]
        public async Task<ResultDto> Update(UpdateGroupEmployeeDto dto)
        {
            UpdateValidator validator = new UpdateValidator();
            var validationResult = validator.Validate(dto);

            if (!validationResult.IsValid)
            {
                throw new BadRequestException(JsonSerializer.Serialize(validationResult.Errors, new JsonSerializerOptions
                {
                    WriteIndented = true
                }));
            }

            RequestHandlerAsync<CompanyGroupEmployee> requestHandler = new RequestHandlerAsync<CompanyGroupEmployee>();
            return await requestHandler.getResultAsync(() => _groupService.Update(dto));
        }
        [HttpPost("delete")]
        public async Task<ResultDto> Delete(DeleteDto dto)
        {
            DeleteValidator validator = new DeleteValidator();
            var validationResult = validator.Validate(dto);

            if (!validationResult.IsValid)
            {
                throw new BadRequestException(JsonSerializer.Serialize(validationResult.Errors, new JsonSerializerOptions
                {
                    WriteIndented = true
                }));
            }

            RequestHandlerAsync<bool> requestHandler = new RequestHandlerAsync<bool>();
            return await requestHandler.getResultAsync(() => _groupService.Delete(dto));
        }
    }
}
