﻿using NP_API.Common;
using NP_API.Dto.Request.CompanyLevel;
using NP_API.Dto;
using NP_API.Service.Interface;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NP_API.Exceptions;
using NP_API.Models;
using Microsoft.AspNetCore.Cors.Infrastructure;
using System.Text.Json;
using NP_API.Validation.CompanyLevel;
using NP_API.Dto.Custom;
using NP_API.Validation;

namespace NP_API.Controllers
{
    [ApiController]
    [Route("api/v1/company/level"), Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class CompanyLevelController : ControllerBase
    {
        private readonly ICompanyLevelService _levelService;
        public CompanyLevelController(ICompanyLevelService levelService)
        {
            this._levelService = levelService;
        }
        [HttpGet("")]
        public async Task<ResultDto> Get([FromQuery] GetDto dto)
        {
            RequestHandlerAsync<PagedList<CompanyLevelResultDto>> requestHandler = new RequestHandlerAsync<PagedList<CompanyLevelResultDto>>();
            return await requestHandler.getResultAsync(() => _levelService.GetData(dto));
        }
        [HttpPost("create")]
        public async Task<ResultDto> Create(CompanyLevelDto dto)
        {
            CreateValidator validator = new CreateValidator();
            var validationResult = validator.Validate(dto);

            if (!validationResult.IsValid)
            {
                throw new BadRequestException(JsonSerializer.Serialize(validationResult.Errors, new JsonSerializerOptions
                {
                    WriteIndented = true
                }));
            }

            RequestHandlerAsync<CompanyLevel> requestHandler = new RequestHandlerAsync<CompanyLevel>();
            return await requestHandler.getResultAsync(() => _levelService.Create(dto));
        }
        [HttpPost("update")]
        public async Task<ResultDto> Update(UpdateLevelDto dto)
        {
            UpdateValidator validator = new UpdateValidator();
            var validationResult = validator.Validate(dto);

            if (!validationResult.IsValid)
            {
                throw new BadRequestException(JsonSerializer.Serialize(validationResult.Errors, new JsonSerializerOptions
                {
                    WriteIndented = true
                }));
            }

            RequestHandlerAsync<CompanyLevel> requestHandler = new RequestHandlerAsync<CompanyLevel>();
            return await requestHandler.getResultAsync(() => _levelService.Update(dto));
        }
        [HttpPost("delete")]
        public async Task<ResultDto> Delete(DeleteDto dto)
        {
            DeleteValidator validator = new DeleteValidator();
            var validationResult = validator.Validate(dto);

            if (!validationResult.IsValid)
            {
                throw new BadRequestException(JsonSerializer.Serialize(validationResult.Errors, new JsonSerializerOptions
                {
                    WriteIndented = true
                }));
            }

            RequestHandlerAsync<bool> requestHandler = new RequestHandlerAsync<bool>();
            return await requestHandler.getResultAsync(() => _levelService.Delete(dto));
        }
    }
}
