﻿using NP_API.Common;
using NP_API.Dto.Request.CompanyOrganization;
using NP_API.Dto;
using NP_API.Service.Interface;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NP_API.Exceptions;
using NP_API.Models;
using System.Text.Json;
using NP_API.Validation.CompanyOrganization;
using NP_API.Dto.Custom;
using NP_API.Validation;

namespace NP_API.Controllers
{
    [ApiController]
    [Route("api/v1/company/orgs"), Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class CompanyOrganizationController : ControllerBase
    {
        private readonly ICompanyOrganizationService _orgsService;
        public CompanyOrganizationController(ICompanyOrganizationService orgsService)
        {
            this._orgsService = orgsService;
        }
        [HttpGet("")]
        public async Task<ResultDto> Get([FromQuery] GetDto dto)
        {
            RequestHandlerAsync<PagedList<CompanyOrganizationResultDto>> requestHandler = new RequestHandlerAsync<PagedList<CompanyOrganizationResultDto>>();
            return await requestHandler.getResultAsync(() => _orgsService.GetData(dto));
        }
        [HttpGet("structure")]
        public async Task<ResultDto> GetStructure()
        {
            RequestHandlerAsync<JsonResult> requestHandler = new RequestHandlerAsync<JsonResult>();
            return await requestHandler.getResultAsync(() => _orgsService.GetStructure());
        }
        [HttpPost("create")]
        public async Task<ResultDto> Create(CompanyOrganizationDto dto)
        {
            CreateValidator validator = new CreateValidator();
            var validationResult = validator.Validate(dto);

            if (!validationResult.IsValid)
            {
                throw new BadRequestException(JsonSerializer.Serialize(validationResult.Errors, new JsonSerializerOptions
                {
                    WriteIndented = true
                }));
            }

            RequestHandlerAsync<CompanyOrganization> requestHandler = new RequestHandlerAsync<CompanyOrganization>();
            return await requestHandler.getResultAsync(() => _orgsService.Create(dto));
        }
        [HttpPost("update")]
        public async Task<ResultDto> Update(UpdateOrganizationDto dto)
        {
            UpdateValidator validator = new UpdateValidator();
            var validationResult = validator.Validate(dto);

            if (!validationResult.IsValid)
            {
                throw new BadRequestException(JsonSerializer.Serialize(validationResult.Errors, new JsonSerializerOptions
                {
                    WriteIndented = true
                }));
            }

            RequestHandlerAsync<CompanyOrganization> requestHandler = new RequestHandlerAsync<CompanyOrganization>();
            return await requestHandler.getResultAsync(() => _orgsService.Update(dto));
        }
        [HttpPost("delete")]
        public async Task<ResultDto> Delete(DeleteDto dto)
        {
            DeleteValidator validator = new DeleteValidator();
            var validationResult = validator.Validate(dto);

            if (!validationResult.IsValid)
            {
                throw new BadRequestException(JsonSerializer.Serialize(validationResult.Errors, new JsonSerializerOptions
                {
                    WriteIndented = true
                }));
            }

            RequestHandlerAsync<bool> requestHandler = new RequestHandlerAsync<bool>();
            return await requestHandler.getResultAsync(() => _orgsService.Delete(dto));
        }
    }
}
