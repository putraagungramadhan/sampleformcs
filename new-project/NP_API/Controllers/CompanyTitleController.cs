﻿using NP_API.Common;
using NP_API.Dto.Request.CompanyTitle;
using NP_API.Dto;
using NP_API.Service.Interface;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NP_API.Exceptions;
using NP_API.Models;
using Microsoft.AspNetCore.Cors.Infrastructure;
using System.Text.Json;
using NP_API.Validation.CompanyTitle;
using NP_API.Dto.Custom;
using NP_API.Validation;

namespace NP_API.Controllers
{
    [ApiController]
    [Route("api/v1/company/title"), Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class CompanyTitleController : ControllerBase
    {
        private readonly ICompanyTitleService _titleService;
        public CompanyTitleController(ICompanyTitleService titleService)
        {
            this._titleService = titleService;
        }
        [HttpGet("")]
        public async Task<ResultDto> Get([FromQuery] GetDto dto)
        {
            RequestHandlerAsync<PagedList<CompanyTitleResultDto>> requestHandler = new RequestHandlerAsync<PagedList<CompanyTitleResultDto>>();
            return await requestHandler.getResultAsync(() => _titleService.GetData(dto), "Berhasil ambil data");
        }
        [HttpPost("create")]
        public async Task<ResultDto> Create(CompanyTitleDto dto)
        {
            CreateValidator validator = new CreateValidator();
            var validationResult = validator.Validate(dto);

            if (!validationResult.IsValid)
            {
                throw new BadRequestException(JsonSerializer.Serialize(validationResult.Errors, new JsonSerializerOptions
                {
                    WriteIndented = true
                }));
            }

            RequestHandlerAsync<CompanyTitle> requestHandler = new RequestHandlerAsync<CompanyTitle>();
            return await requestHandler.getResultAsync(() => _titleService.Create(dto), "Berhasil insert data");
        }
        [HttpPost("update")]
        public async Task<ResultDto> Update(UpdateCompanyTitleDto dto)
        {
            UpdateValidator validator = new UpdateValidator();
            var validationResult = validator.Validate(dto);

            if (!validationResult.IsValid)
            {
                throw new BadRequestException(JsonSerializer.Serialize(validationResult.Errors, new JsonSerializerOptions
                {
                    WriteIndented = true
                }));
            }

            RequestHandlerAsync<CompanyTitle> requestHandler = new RequestHandlerAsync<CompanyTitle>();
            return await requestHandler.getResultAsync(() => _titleService.Update(dto), "Berhasil update data");
        }
        [HttpPost("delete")]
        public async Task<ResultDto> Delete(DeleteDto dto)
        {
            DeleteValidator validator = new DeleteValidator();
            var validationResult = validator.Validate(dto);

            if (!validationResult.IsValid)
            {
                throw new BadRequestException(JsonSerializer.Serialize(validationResult.Errors, new JsonSerializerOptions
                {
                    WriteIndented = true
                }));
            }

            RequestHandlerAsync<bool> requestHandler = new RequestHandlerAsync<bool>();
            return await requestHandler.getResultAsync(() => _titleService.Delete(dto), "Berhasil hapus data");
        }
    }
}
