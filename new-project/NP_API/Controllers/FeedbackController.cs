﻿using NP_API.Common;
using NP_API.Dto;
using NP_API.Dto.Custom;
using NP_API.Dto.Request.Feedback;
using NP_API.Exceptions;
using NP_API.Service;
using NP_API.Service.Interface;
using NP_API.Validation;
using NP_API.Validation.Feedback;
using Microsoft.AspNetCore.Mvc;
using System.Text.Json;

namespace NP_API.Controllers
{
    [ApiController]
    [Route("api/v1/feedback/")]
    public class FeedbackController : ControllerBase
    {
        private readonly IFeedbackService _feedbackService;
        public FeedbackController(IFeedbackService feedbackService)
        {
            this._feedbackService = feedbackService; 
        }
        [HttpGet("")]
        public async Task<ResultDto> Get([FromQuery] GetDto dto)
        {
            RequestHandlerAsync<PagedList<FeedbackResultDto>> requestHandler = new RequestHandlerAsync<PagedList<FeedbackResultDto>>();
            return await requestHandler.getResultAsync(() => _feedbackService.GetData(dto), "Berhasil menampilkan data feedback");
        }

        [HttpPost("create")]
        public async Task<ResultDto> Create([FromBody] NewFeedbackDto dto)
        {
            CreateValidator validator = new CreateValidator();
            var validationResult = validator.Validate(dto);

            if (!validationResult.IsValid)
            {
                throw new BadRequestException(JsonSerializer.Serialize(validationResult.Errors, new JsonSerializerOptions
                {
                    WriteIndented = true
                }));
            }

            RequestHandlerAsync<FeedbackResultDto> requestHandler = new RequestHandlerAsync<FeedbackResultDto>();
            return await requestHandler.getResultAsync(() => _feedbackService.Create(dto), "Berhasil simpan feedback");
        }

        [HttpPost("update")]
        public async Task<ResultDto> Update([FromBody] UpdateFeedbackDto dto)
        {
            UpdateValidator validator = new UpdateValidator();
            var validationResult = validator.Validate(dto);

            if (!validationResult.IsValid)
            {
                throw new BadRequestException(JsonSerializer.Serialize(validationResult.Errors, new JsonSerializerOptions
                {
                    WriteIndented = true
                }));
            }

            RequestHandlerAsync<FeedbackResultDto> requestHandler = new RequestHandlerAsync<FeedbackResultDto>();
            return await requestHandler.getResultAsync(() => _feedbackService.Update(dto), "Berhasil update feedback");
        }

        [HttpPost("delete")]
        public async Task<ResultDto> Delete(DeleteDto dto)
        {
            DeleteValidator validator = new DeleteValidator();
            var validationResult = validator.Validate(dto);

            if (!validationResult.IsValid)
            {
                throw new BadRequestException(JsonSerializer.Serialize(validationResult.Errors, new JsonSerializerOptions
                {
                    WriteIndented = true
                }));
            }

            RequestHandlerAsync<bool> requestHandler = new RequestHandlerAsync<bool>();
            return await requestHandler.getResultAsync(() => _feedbackService.Delete(dto), "Berhasil hapus feedback");
        }
    }
}
