﻿using System;
using NP_API.Service;
using NP_API.Common;
using NP_API.Dto;
using NP_API.Models;
using NP_API.Service.Interface;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using NP_API.Validation.NewsCategory;
using NP_API.Exceptions;
using System.Text.Json;
using NP_API.Dto.Request.NewsCategory;
using NP_API.Validation;
using NP_API.Dto.Custom;
using FluentValidation;

namespace NP_API.Controllers {
    [ApiController]
    [Route("api/v1/newscategory/")]
    public class NewsCategoryController : ControllerBase
    {
        private readonly INewsCategoryService _newsCategoryService;
        public NewsCategoryController(INewsCategoryService newsCategoryService)
        {
            this._newsCategoryService = newsCategoryService;
            
        }
        [HttpGet("")]
        public async Task<ResultDto> Get([FromQuery] GetDto dto)
        {
            RequestHandlerAsync<PagedList<NewsCategoryResultDto>> requestHandler = new RequestHandlerAsync<PagedList<NewsCategoryResultDto>>();
            return await requestHandler.getResultAsync(() => _newsCategoryService.GetData(dto));
        }
        [HttpPost("create")]
        public async Task<ResultDto> Create([FromBody] NewNewsCategoryDto dto)
        {
            CreateValidator validator = new CreateValidator();
            var validationResult = validator.Validate(dto);

            if (!validationResult.IsValid)
            {
                throw new BadRequestException(JsonSerializer.Serialize(validationResult.Errors, new JsonSerializerOptions
                {
                    WriteIndented = true
                }));
            }

            RequestHandlerAsync<NewsCategoryResultDto> requestHandler = new RequestHandlerAsync<NewsCategoryResultDto>();
            return await requestHandler.getResultAsync(() => _newsCategoryService.Create(dto), "Berhasil simpan kategori berita");
        }

        [HttpPost("update")]
        public async Task<ResultDto> Update([FromBody] UpdateNewsCategoryDto dto)
        {
            UpdateValidator validator = new UpdateValidator();
            var validationResult = validator.Validate(dto);

            if (!validationResult.IsValid)
            {
                throw new BadRequestException(JsonSerializer.Serialize(validationResult.Errors, new JsonSerializerOptions
                {
                    WriteIndented = true
                }));
            }

            RequestHandlerAsync<NewsCategoryResultDto> requestHandler = new RequestHandlerAsync<NewsCategoryResultDto>();
            return await requestHandler.getResultAsync(() => _newsCategoryService.Update(dto), "Berhasil update kategori berita");
        }
        [HttpPost("delete")]
        public async Task<ResultDto> Delete(DeleteDto dto)
        {
            DeleteValidator validator = new DeleteValidator();
            var validationResult = validator.Validate(dto);

            if (!validationResult.IsValid)
            {
                throw new BadRequestException(JsonSerializer.Serialize(validationResult.Errors, new JsonSerializerOptions
                {
                    WriteIndented = true
                }));
            }

            RequestHandlerAsync<bool> requestHandler = new RequestHandlerAsync<bool>();
            return await requestHandler.getResultAsync(() => _newsCategoryService.Delete(dto), "Berhasil hapus kategori berita");
        }
    }
}

