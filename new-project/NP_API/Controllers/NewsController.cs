﻿using NP_API.Common;
using NP_API.Dto.Request.News;
using NP_API.Dto;
using NP_API.Service.Interface;
using Microsoft.AspNetCore.Mvc;
using NP_API.Exceptions;
using NP_API.Service;
using System.Text.Json;
using NP_API.Validation.News;
using NP_API.Dto.Custom;
using NP_API.Validation;

namespace NP_API.Controllers
{
    [ApiController]
    [Route("api/v1/news/")]
    public class NewsController : ControllerBase
    {
        private readonly INewsService _newsService;
        public NewsController(INewsService newsService)
        {
            this._newsService = newsService;

        }
        [HttpGet("")]
        public async Task<ResultDto> Get([FromQuery] GetDto dto)
        {
            RequestHandlerAsync<PagedList<NewsResultDto>> requestHandler = new RequestHandlerAsync<PagedList<NewsResultDto>>();
            return await requestHandler.getResultAsync(() => _newsService.GetData(dto));
        }

        [HttpPost("create")]
        public async Task<ResultDto> Create([FromForm] NewNewsDto dto)
        {
            CreateValidator validator = new CreateValidator();
            var validationResult = validator.Validate(dto);

            if (!validationResult.IsValid)
            {
                throw new BadRequestException(JsonSerializer.Serialize(validationResult.Errors, new JsonSerializerOptions
                {
                    WriteIndented = true
                }));
            }

            RequestHandlerAsync<NewsResultDto> requestHandler = new RequestHandlerAsync<NewsResultDto>();
            return await requestHandler.getResultAsync(() => _newsService.Create(dto), "Berhasil simpan berita");
        }


        [HttpPost("update")]
        public async Task<ResultDto> Update([FromForm] UpdateNewsDto dto)
        {
            UpdateValidator validator = new UpdateValidator();
            var validationResult = validator.Validate(dto);

            if (!validationResult.IsValid)
            {
                throw new BadRequestException(JsonSerializer.Serialize(validationResult.Errors, new JsonSerializerOptions
                {
                    WriteIndented = true
                }));
            }

            RequestHandlerAsync<NewsResultDto> requestHandler = new RequestHandlerAsync<NewsResultDto>();
            return await requestHandler.getResultAsync(() => _newsService.Update(dto), "Berhasil update berita");
        }

        [HttpPost("delete")]
        public async Task<ResultDto> Delete(DeleteDto dto)
        {
            DeleteValidator validator = new DeleteValidator();
            var validationResult = validator.Validate(dto);

            if (!validationResult.IsValid)
            {
                throw new BadRequestException(JsonSerializer.Serialize(validationResult.Errors, new JsonSerializerOptions
                {
                    WriteIndented = true
                }));
            }

            RequestHandlerAsync<bool> requestHandler = new RequestHandlerAsync<bool>();
            return await requestHandler.getResultAsync(() => _newsService.Delete(dto));
        }
    }
}