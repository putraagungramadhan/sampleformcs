﻿using NP_API.Common;
using NP_API.Dto;
using NP_API.Service.Interface;
using Microsoft.AspNetCore.Mvc;
using NP_API.Exceptions;
using System.Text.Json;
using NP_API.Dto.Custom;
using NP_API.Validation;
using NP_API.Validation.SetupConfiguration;
using NP_API.Dto.Request.SetupConfigurations;

namespace NP_API.Controllers
{
    [ApiController]
    [Route("api/v1/setupconfiguration/")]
    public class SetupConfigurationController : ControllerBase
    {
        private readonly ISetupConfigurationService _thisService;

        public SetupConfigurationController(ISetupConfigurationService newsService)
        {
            this._thisService = newsService;
        }

        [HttpGet("")]
        public async Task<ResultDto> Get([FromQuery] GetDto dto)
        {
            RequestHandlerAsync<PagedList<SetupConfigurationResultDto>> requestHandler = new RequestHandlerAsync<PagedList<SetupConfigurationResultDto>>();
            return await requestHandler.getResultAsync(() => _thisService.GetData(dto));
        }

        [HttpPost("create")]
        public async Task<ResultDto> Create([FromForm] NewSetupConfigurationDto dto)
        {
            CreateValidator validator = new CreateValidator();
            var validationResult = validator.Validate(dto);

            if (!validationResult.IsValid)
            {
                throw new BadRequestException(JsonSerializer.Serialize(validationResult.Errors, new JsonSerializerOptions
                {
                    WriteIndented = true
                }));
            }

            RequestHandlerAsync<SetupConfigurationResultDto> requestHandler = new RequestHandlerAsync<SetupConfigurationResultDto>();
            return await requestHandler.getResultAsync(() => _thisService.Create(dto), "Berhasil simpan berita");
        }


        [HttpPost("update")]
        public async Task<ResultDto> Update([FromForm] UpdateSetupConfigurationDto dto)
        {
            UpdateValidator validator = new UpdateValidator();
            var validationResult = validator.Validate(dto);

            if (!validationResult.IsValid)
            {
                throw new BadRequestException(JsonSerializer.Serialize(validationResult.Errors, new JsonSerializerOptions
                {
                    WriteIndented = true
                }));
            }

            RequestHandlerAsync<SetupConfigurationResultDto> requestHandler = new RequestHandlerAsync<SetupConfigurationResultDto>();
            return await requestHandler.getResultAsync(() => _thisService.Update(dto), "Berhasil update setup configuration");
        }

        [HttpPost("delete")]
        public async Task<ResultDto> Delete(DeleteDto dto)
        {
            DeleteValidator validator = new DeleteValidator();
            var validationResult = validator.Validate(dto);

            if (!validationResult.IsValid)
            {
                throw new BadRequestException(JsonSerializer.Serialize(validationResult.Errors, new JsonSerializerOptions
                {
                    WriteIndented = true
                }));
            }

            RequestHandlerAsync<bool> requestHandler = new RequestHandlerAsync<bool>();
            return await requestHandler.getResultAsync(() => _thisService.Delete(dto));
        }
    }
}