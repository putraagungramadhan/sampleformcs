﻿using NP_API.Common;
using NP_API.Dto;
using NP_API.Exceptions;
using NP_API.Service.Interface;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Text.Json;
using NP_API.Validation.User;
using NP_API.Dto.Request.User;

namespace NP_API.Controllers
{
    [ApiController]
    [Route("api/v1/profile/"), Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;
        public UserController(IUserService userService)
        {
            this._userService = userService;
        }
        [HttpGet("list")]
        public async Task<ResultDto> GetData([FromQuery]GetDto dto)
        {
            RequestHandlerAsync<PagedList<UserResultDto>> requestHandler = new RequestHandlerAsync<PagedList<UserResultDto>>();
            return await requestHandler.getResultAsync(() => _userService.GetData(dto), "Berhasil ambil data");
        }
        [HttpPost("create")]
        public async Task<ResultDto> Create([FromBody] NewUserDto dto)
        {
            CreateValidator validator = new CreateValidator();
            var validationResult = validator.Validate(dto);

            if (!validationResult.IsValid)
            {
                throw new BadRequestException(JsonSerializer.Serialize(validationResult.Errors, new JsonSerializerOptions
                {
                    WriteIndented = true
                }));
            }

            RequestHandlerAsync<UserResultDto> requestHandler = new RequestHandlerAsync<UserResultDto>();
            return await requestHandler.getResultAsync(() => _userService.Create(dto), "Berhasil simpan data");
        }
        [HttpPost("update-password")]
        public async Task<ResultDto> Update([FromForm] UpdateUserPasswordDto dto)
        {
            UpdateUserPasswordValidation validator = new UpdateUserPasswordValidation();
            var validationResult = validator.Validate(dto);

            if (!validationResult.IsValid)
            {
                throw new BadRequestException(JsonSerializer.Serialize(validationResult.Errors, new JsonSerializerOptions
                {
                    WriteIndented = true
                }));
            }

            RequestHandlerAsync<bool> requestHandler = new RequestHandlerAsync<bool>();
            return await requestHandler.getResultAsync(() => _userService.UpdatePassword(dto), "Berhasil update password");
        }
    }
}
