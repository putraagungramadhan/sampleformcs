﻿using NP_API.Common;
using NP_API.Dto;
using NP_API.Dto.Custom;
using NP_API.Dto.Request.UserPermission;
using NP_API.Exceptions;
using NP_API.Models;
using NP_API.Service;
using NP_API.Service.Interface;
using NP_API.Validation;
using NP_API.Validation.UserPermission;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Text.Json;

namespace NP_API.Controllers
{
    [ApiController]
    [Route("api/v1/permission/"), Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class UserPermissionController : ControllerBase
    {
        private readonly IUserPermissionService _userPermissionService;
        public UserPermissionController(IUserPermissionService userPermissionService)
        {
            this._userPermissionService = userPermissionService;
        }
        [HttpGet("")]
        public async Task<ResultDto> GetPermission([FromQuery] Guid userGuid)
        {
            RequestHandlerAsync<object> requestHandler = new RequestHandlerAsync<object>();
            return await requestHandler.getResultAsync(() => _userPermissionService.GetByUser(userGuid), "Berhasil ambil data");
        }
        [HttpGet("module")]
        public async Task<ResultDto> GetModule([FromQuery]bool canEmployee = false)
        {
            RequestHandlerAsync<List<Module>> requestHandler = new RequestHandlerAsync<List<Module>>();
            return await requestHandler.getResultAsync(() => _userPermissionService.GetModule(canEmployee), "Berhasil ambil data");
        }
        [HttpGet("user")]
        public async Task<ResultDto> Get([FromQuery] GetDto dto)
        {
            RequestHandlerAsync<PagedList<UserPermissionResultDto>> requestHandler = new RequestHandlerAsync<PagedList<UserPermissionResultDto>>();
            return await requestHandler.getResultAsync(() => _userPermissionService.GetData(dto), "Berhasil ambil data");
        }
        [HttpPost("user/create")]
        public async Task<ResultDto> Create([FromBody] UserPermissionDto dto)
        {
            CreateValidator validator = new CreateValidator();
            var validationResult = validator.Validate(dto);

            if (!validationResult.IsValid)
            {
                throw new BadRequestException(JsonSerializer.Serialize(validationResult.Errors, new JsonSerializerOptions
                {
                    WriteIndented = true
                }));
            }

            RequestHandlerAsync<UserPermissionResultDto> requestHandler = new RequestHandlerAsync<UserPermissionResultDto>();
            return await requestHandler.getResultAsync(() => _userPermissionService.Save(dto), "Berhasil simpan data");
        }
        [HttpPost("user/update")]
        public async Task<ResultDto> Update([FromBody] UpdateUserPermissionDto dto)
        {
            UpdateValidator validator = new UpdateValidator();
            var validationResult = validator.Validate(dto);

            if (!validationResult.IsValid)
            {
                throw new BadRequestException(JsonSerializer.Serialize(validationResult.Errors, new JsonSerializerOptions
                {
                    WriteIndented = true
                }));
            }

            RequestHandlerAsync<UserPermissionResultDto> requestHandler = new RequestHandlerAsync<UserPermissionResultDto>();
            return await requestHandler.getResultAsync(() => _userPermissionService.Update(dto), "Berhasil simpan data");
        }
        [HttpPost("user/delete")]
        public async Task<ResultDto> Delete([FromBody] DeleteDto dto)
        {
            DeleteValidator validator = new DeleteValidator();
            var validationResult = validator.Validate(dto);

            if (!validationResult.IsValid)
            {
                throw new BadRequestException(JsonSerializer.Serialize(validationResult.Errors, new JsonSerializerOptions
                {
                    WriteIndented = true
                }));
            }

            RequestHandlerAsync<bool> requestHandler = new RequestHandlerAsync<bool>();
            return await requestHandler.getResultAsync(() => _userPermissionService.Delete(dto), "Berhasil hapus data");
        }
    }
}
