﻿using NP_API.Models;
using Microsoft.EntityFrameworkCore;

namespace NP_API.Data.Configuration
{
    public static class ModelBuilderCompanyBranch
    {
        public static void CompanyBranchBuilder(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CompanyBranch>()
                .HasOne(c => c.Company)
                .WithMany(ct => ct.CompanyBranch)
                .HasForeignKey(c => c.CompanyGuid)
                .HasPrincipalKey(c => c.Guid)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
