﻿using NP_API.Models;
using Microsoft.EntityFrameworkCore;

namespace NP_API.Data.Configuration
{
    public static class ModelBuilderCompanyClass
    {
        public static void CompanyClassBuilder(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CompanyClass>()
                .HasOne(c => c.Company)
                .WithMany(ct => ct.CompanyClass)
                .HasForeignKey(c => c.CompanyGuid)
                .HasPrincipalKey(c => c.Guid)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<CompanyClass>()
                .HasOne(c => c.CompanyGrade)
                .WithMany(ct => ct.CompanyClass)
                .HasForeignKey(c => c.CompanyGradeGuid)
                .HasPrincipalKey(c => c.Guid)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
