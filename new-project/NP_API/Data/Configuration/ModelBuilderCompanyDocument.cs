﻿using NP_API.Models;
using Microsoft.EntityFrameworkCore;

namespace NP_API.Data.Configuration
{
    public static class ModelBuilderCompanyDocument
    {
        public static void CompanyDocumentBuilder(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CompanyDocument>()
                .HasOne(c => c.Company)
                .WithMany(ct => ct.CompanyDocument)
                .HasForeignKey(c => c.CompanyGuid)
                .HasPrincipalKey(c => c.Guid)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
