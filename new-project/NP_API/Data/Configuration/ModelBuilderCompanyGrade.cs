﻿using NP_API.Models;
using Microsoft.EntityFrameworkCore;

namespace NP_API.Data.Configuration
{
    public static class ModelBuilderCompanyGrade
    {
        public static void CompanyGradeBuilder(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CompanyGrade>()
                .HasOne(c => c.Company)
                .WithMany(ct => ct.CompanyGrade)
                .HasForeignKey(c => c.CompanyGuid)
                .HasPrincipalKey(c => c.Guid)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
