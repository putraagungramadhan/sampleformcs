﻿using NP_API.Models;
using Microsoft.EntityFrameworkCore;

namespace NP_API.Data.Configuration
{
    public static class ModelBuilderCompanyGroupEmployee
    {
        public static void CompanyGroupEmployeeBuilder(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CompanyGroupEmployee>()
                .HasOne(c => c.Company)
                .WithMany(ct => ct.CompanyGroupEmployee)
                .HasForeignKey(c => c.CompanyGuid)
                .HasPrincipalKey(c => c.Guid)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<CompanyGroupEmployee>()
                .HasOne(c => c.CompanyOrganization)
                .WithMany(ct => ct.CompanyGroupEmployee)
                .HasForeignKey(c => c.CompanyOrganizationGuid)
                .HasPrincipalKey(c => c.Guid)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<CompanyGroupEmployee>()
                .HasOne(c => c.CompanyLevel)
                .WithMany(ct => ct.CompanyGroupEmployee)
                .HasForeignKey(c => c.CompanyLevelGuid)
                .HasPrincipalKey(c => c.Guid)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<CompanyGroupEmployee>()
                .HasOne(c => c.CompanyTitle)
                .WithMany(ct => ct.CompanyGroupEmployee)
                .HasForeignKey(c => c.CompanyTitleGuid)
                .HasPrincipalKey(c => c.Guid)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<CompanyGroupEmployee>()
                .HasOne(c => c.CompanyGrade)
                .WithMany(ct => ct.CompanyGroupEmployee)
                .HasForeignKey(c => c.CompanyGradeGuid)
                .HasPrincipalKey(c => c.Guid)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<CompanyGroupEmployee>()
                .HasOne(c => c.CompanyClass)
                .WithMany(ct => ct.CompanyGroupEmployee)
                .HasForeignKey(c => c.CompanyClassGuid)
                .HasPrincipalKey(c => c.Guid)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
