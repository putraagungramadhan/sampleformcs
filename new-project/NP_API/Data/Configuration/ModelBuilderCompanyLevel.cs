﻿using NP_API.Models;
using Microsoft.EntityFrameworkCore;

namespace NP_API.Data.Configuration
{
    public static class ModelBuilderCompanyLevel
    {
        public static void CompanyLevelBuilder(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CompanyLevel>()
                .HasOne(c => c.Company)
                .WithMany(ct => ct.CompanyLevel)
                .HasForeignKey(c => c.CompanyGuid)
                .HasPrincipalKey(c => c.Guid)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
