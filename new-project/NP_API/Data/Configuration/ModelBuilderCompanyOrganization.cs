﻿using NP_API.Models;
using Microsoft.EntityFrameworkCore;

namespace NP_API.Data.Configuration
{
    public static class ModelBuilderCompanyOrganization
    {
        public static void CompanyOrganizationBuilder(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CompanyOrganization>()
                .HasOne(c => c.Company)
                .WithMany(ct => ct.CompanyOrganization)
                .HasForeignKey(c => c.CompanyGuid)
                .HasPrincipalKey(c => c.Guid)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
