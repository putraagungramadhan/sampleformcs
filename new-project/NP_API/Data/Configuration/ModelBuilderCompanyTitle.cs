﻿using NP_API.Models;
using Microsoft.EntityFrameworkCore;

namespace NP_API.Data.Configuration
{
    public static class ModelBuilderCompanyTitle
    {
        public static void CompanyTitleBuilder(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CompanyTitle>()
                .HasOne(c => c.Company)
                .WithMany(ct => ct.CompanyTitle)
                .HasForeignKey(c => c.CompanyGuid)
                .HasPrincipalKey(c => c.Guid)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
