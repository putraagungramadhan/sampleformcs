﻿using NP_API.Models;
using Microsoft.EntityFrameworkCore;

namespace NP_API.Data.Configuration
{
    public static class ModelBuilderFeedback
    {
        public static void FeedbackBuilder(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Feedback>()
                .HasOne(c => c.Company)
                .WithMany(ct => ct.Feedback)
                .HasForeignKey(c => c.CompanyGuid)
                .HasPrincipalKey(c => c.Guid)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Feedback>()
                .HasOne(c => c.User)
                .WithMany(ct => ct.Feedback)
                .HasForeignKey(c => c.UserGuid)
                .HasPrincipalKey(c => c.Guid)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
