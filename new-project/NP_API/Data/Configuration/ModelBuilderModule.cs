﻿using NP_API.Models;
using Microsoft.EntityFrameworkCore;

namespace NP_API.Data.Configuration
{
    public static class ModelBuilderModule
    {
        public static void ModuleBuilder(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Module>().HasData(
                new Module
                {
                    Id = 1,
                    Guid = new Guid("d1031b46-dca9-499b-a8f4-23fbfa20cbf4"),
                    Label = "Persetujuan Mutasi",
                    Detail = "",
                    Name = "approval_mutation",
                    CanEmployee = true
                },
                new Module
                {
                    Id = 2,
                    Guid = new Guid("d1131b46-dca9-499b-a8f4-23fbfa20cbf4"),
                    Label = "Persetujuan Eksternal Mutasi",
                    Detail = "",
                    Name = "approval_external_mutation",
                    CanEmployee = true
                },
                new Module
                {
                    Id = 3,
                    Label = "Persetujuan lowongan pekerjaan baru",
                    Detail = "",
                    Guid = new Guid("d1231b46-dca9-499b-a8f4-23fbfa20cbf4"),
                    Name = "approval_job_request",
                    CanEmployee = false
                },
                new Module
                {
                    Id = 4,
                    Guid = new Guid("d1331b46-dca9-499b-a8f4-23fbfa20cbf4"),
                    Label = "Kelola hak akses",
                    Detail = "",
                    Name = "manage_permission",
                    CanEmployee = false
                },
                new Module
                {
                    Id = 5,
                    Label = "Kelola admin",
                    Detail = "",
                    Guid = new Guid("d1431b46-dca9-499b-a8f4-23fbfa20cbf4"),
                    Name = "manage_admin",
                    CanEmployee = false
                },
                new Module
                {
                    Id = 6,
                    Guid = new Guid("d1531b46-dca9-499b-a8f4-23fbfa20cbf4"),
                    Label = "Kelola karyawan",
                    Detail = "",
                    Name = "manage_employee",
                    CanEmployee = false
                }
            );
        }
    }
}
