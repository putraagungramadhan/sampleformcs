﻿using NP_API.Models;
using Microsoft.EntityFrameworkCore;

namespace NP_API.Data.Configuration
{
    public static class ModelBuilderNews
    {
        public static void NewsBuilder(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<News>()
                .HasOne(c => c.Company)
                .WithMany(ct => ct.News)
                .HasForeignKey(c => c.CompanyGuid)
                .HasPrincipalKey(c => c.Guid)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<News>()
                .HasOne(c => c.NewsCategory)
                .WithMany(ct => ct.News)
                .HasForeignKey(c => c.NewsCategoryGuid)
                .HasPrincipalKey(c => c.Guid)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
