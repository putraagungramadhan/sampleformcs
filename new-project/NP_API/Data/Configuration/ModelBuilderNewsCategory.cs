﻿using NP_API.Models;
using Microsoft.EntityFrameworkCore;

namespace NP_API.Data.Configuration
{
    public static class ModelBuilderNewsCategory
    {
        public static void NewsCategoryBuilder(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<NewsCategory>()
                .HasOne(c => c.Company)
                .WithMany(ct => ct.NewsCategory)
                .HasForeignKey(c => c.CompanyGuid)
                .HasPrincipalKey(c => c.Guid)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
