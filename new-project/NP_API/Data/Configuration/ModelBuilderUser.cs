﻿using NP_API.Models;
using Microsoft.EntityFrameworkCore;

namespace NP_API.Data.Configuration
{
    public static class ModelBuilderUser
    {
        public static void UserBuilder(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>()
                .HasOne(c => c.Company)
                .WithMany(ct => ct.Users)
                .HasForeignKey(c => c.CompanyGuid)
                .HasPrincipalKey(c => c.Guid)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<User>()
                .HasIndex(p => p.Email)
                .IsUnique();

            modelBuilder.Entity<User>()
                .HasIndex(p => p.Username)
                .IsUnique();
        }
    }
}
