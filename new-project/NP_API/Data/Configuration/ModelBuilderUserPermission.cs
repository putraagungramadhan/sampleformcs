﻿using NP_API.Models;
using Microsoft.EntityFrameworkCore;

namespace NP_API.Data.Configuration
{
    public static class ModelBuilderUserPermission
    {
        public static void UserPermissionBuilder(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<UserPermission>()
                .HasOne(c => c.User)
                .WithMany(ct => ct.UserPermission)
                .HasForeignKey(c => c.UserGuid)
                .HasPrincipalKey(c => c.Guid)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<UserPermission>()
                .HasOne(c => c.Company)
                .WithMany(ct => ct.UserPermission)
                .HasForeignKey(c => c.CompanyGuid)
                .HasPrincipalKey(c => c.Guid)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<UserPermission>()
                .HasOne(c => c.Module)
                .WithMany(ct => ct.UserPermission)
                .HasForeignKey(c => c.ModuleGuid)
                .HasPrincipalKey(c => c.Guid)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
