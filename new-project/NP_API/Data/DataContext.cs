﻿using NP_API.Models;
using Microsoft.EntityFrameworkCore;
using NP_API.Interceptor;
using NP_API.Data.Configuration;


namespace NP_API.Data
{
    public class DataContext : DbContext
    {
        private SoftDeleteInterceptor _interceptor;
        public DataContext(DbContextOptions<DataContext> options, SoftDeleteInterceptor interceptor) : base(options)
        {
            this._interceptor = interceptor;
            this.ChangeTracker.LazyLoadingEnabled = true;
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Company> Companies { get; set; }
        public DbSet<CompanyBranch> CompanyBranches { get; set; }
        public DbSet<CompanyGrade> CompanyGrades { get; set; }
        public DbSet<CompanyClass> CompanyClasses { get; set; }
        public DbSet<CompanyOrganization> CompanyOrganizations { get; set; }
        public DbSet<CompanyLevel> CompanyLevels { get; set; }
        public DbSet<CompanyTitle> CompanyTitles { get; set; }
        public DbSet<CompanyGroupEmployee> CompanyGroupEmployees { get; set; }
        public DbSet<NewsCategory> NewsCategories { get; set; }
        public DbSet<News> News { get; set; }
        public DbSet<Feedback> Feedbacks { get; set; }
        public DbSet<CompanyDocument> CompanyDocuments { get; set; }
        public DbSet<Survey> Surveys { get; set; }
        public DbSet<SurveyDetail> SurveyDetails { get; set; }
        public DbSet<SurveyAnswer> SurveyAnswers { get; set; }
        public DbSet<SurveyAnswerDetail> SurveyAnswerDetails { get; set; }
        public DbSet<UserPermission> UserPermissions { get; set; }
        public DbSet<Module> Modules { get; set; }
        public DbSet<CompanyGroup> CompanyGroups { get; set; }
        public DbSet<SetupConfigurations> SetupConfigurations { get; set; }

        public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            OnBeforeSaving();
            return await base.SaveChangesAsync(cancellationToken);
        }

        private void OnBeforeSaving()
        {
            foreach (var entry in ChangeTracker.Entries())
            {
                if (entry.State == EntityState.Modified)
                {
                    // Exclude specific properties from being updated
                    entry.Property("CreatedAt").IsModified = false;
                    entry.Property("CreatedBy").IsModified = false;
                }
            }
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        => optionsBuilder
            .AddInterceptors(_interceptor);

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.SoftDelete();
            modelBuilder.CompanyBuilder();
            modelBuilder.CompanyBranchBuilder();
            modelBuilder.CompanyClassBuilder();
            modelBuilder.CompanyLevelBuilder();
            modelBuilder.CompanyGradeBuilder();
            modelBuilder.CompanyTitleBuilder();
            modelBuilder.CompanyOrganizationBuilder();
            modelBuilder.CompanyGroupEmployeeBuilder();
            modelBuilder.UserBuilder();
            modelBuilder.NewsCategoryBuilder();
            modelBuilder.NewsBuilder();
            modelBuilder.FeedbackBuilder();
            modelBuilder.CompanyDocumentBuilder();
            modelBuilder.UserPermissionBuilder();
            modelBuilder.ModuleBuilder();
            modelBuilder.Seed();
        }
    }
}
