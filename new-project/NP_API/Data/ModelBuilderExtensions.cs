﻿using NP_API.Models;
using Microsoft.EntityFrameworkCore;

namespace NP_API.Data
{
    public static class ModelBuilderExtensions
    {
        public static void Seed(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>().HasData(
                new User
                {
                    Id = 1,
                    Guid = new Guid("afed2ab7-a361-49a5-b3af-697d926edc3b"),
                    CompanyGuid = null,
                    EmployeeMasterGuid = null,
                    Username = "superadmin",
                    Name = "Super Admin",
                    Email = "superadmin@mail.com",
                    Password = BCrypt.Net.BCrypt.HashPassword("Tes123tes@"),
                    Role = "SUPERADMIN",
                    RoleType = Models.Enum.EnRole.SUPERADMIN,
                    CreatedAt = 1,
                    UpdatedAt = 1,
                    CreatedBy = "1",
                    UpdatedBy = "1"
                }
            );
            
            modelBuilder.Entity<SetupConfigurations>().HasData(
                new SetupConfigurations
                {
                    Id = 1,
                    Guid = new Guid("d4331b46-dca9-412b-a8f4-23fbfa20cbf5"),
                    SubTypeName = "MailSettings",
                    TypeName = "Server",
                    TypeData = Models.Enum.EnType.String,
                    TypeValue = "smtp.gmail.com",
                    IsActive = true,
                    CreatedAt = 1,
                    UpdatedAt = 1,
                    CreatedBy = "1",
                    UpdatedBy = "1"
                },
                new SetupConfigurations
                {
                    Id = 2,
                    Guid = new Guid("d4331b46-dca9-412b-a8f4-23fbfa20cbf6"),
                    SubTypeName = "MailSettings",
                    TypeName = "Port",
                    TypeData = Models.Enum.EnType.Integer,
                    TypeValue = "587",
                    IsActive = true,
                    CreatedAt = 1,
                    UpdatedAt = 1,
                    CreatedBy = "1",
                    UpdatedBy = "1"
                },
                new SetupConfigurations
                {
                    Id = 3,
                    Guid = new Guid("d4331b46-dca9-412b-a8f4-23fbfa20cbf7"),
                    SubTypeName = "MailSettings",
                    TypeName = "UserName",
                    TypeData = Models.Enum.EnType.String,
                    TypeValue = "cvbinakarir@gmail.com",
                    IsActive = true,
                    CreatedAt = 1,
                    UpdatedAt = 1,
                    CreatedBy = "1",
                    UpdatedBy = "1"
                },
                new SetupConfigurations
                {
                    Id = 4,
                    Guid = new Guid("d4331b46-dca9-412b-a8f4-23fbfa20cbf8"),
                    SubTypeName = "MailSettings",
                    TypeName = "Password",
                    TypeData = Models.Enum.EnType.String,
                    TypeValue = "uixe gccu oksv bbiw",
                    IsActive = true,
                    CreatedAt = 1,
                    UpdatedAt = 1,
                    CreatedBy = "1",
                    UpdatedBy = "1"
                },
                new SetupConfigurations
                {
                    Id = 5,
                    Guid = new Guid("d4331b46-dca9-412b-a8f4-23fbfa20cbf9"),
                    SubTypeName = "MailSettings",
                    TypeName = "SenderName",
                    TypeData = Models.Enum.EnType.String,
                    TypeValue = "Testing",
                    IsActive = true,
                    CreatedAt = 1,
                    UpdatedAt = 1,
                    CreatedBy = "1",
                    UpdatedBy = "1"
                },
                new SetupConfigurations
                {
                    Id = 6,
                    Guid = new Guid("d4331b46-dca9-412b-a8f4-23fbfa20cbf1"),
                    SubTypeName = "MailSettings",
                    TypeName = "SenderEmail",
                    TypeData = Models.Enum.EnType.String,
                    TypeValue = "septian.r.maulidar@gmail.com",
                    IsActive = true,
                    CreatedAt = 1,
                    UpdatedAt = 1,
                    CreatedBy = "1",
                    UpdatedBy = "1"
                }
            );
        }
    }
}
