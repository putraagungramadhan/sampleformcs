﻿using NP_API.Models;
using Microsoft.EntityFrameworkCore;

namespace NP_API.Data
{
    public static class ModelBuilderSoftDelete
    {
        public static void SoftDelete(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>()
            .HasQueryFilter(x => x.IsDeleted == false);

            modelBuilder.Entity<Company>()
            .HasQueryFilter(x => x.IsDeleted == false);

            modelBuilder.Entity<CompanyGrade>()
            .HasQueryFilter(x => x.IsDeleted == false);

            modelBuilder.Entity<CompanyClass>()
            .HasQueryFilter(x => x.IsDeleted == false);

            modelBuilder.Entity<CompanyOrganization>()
            .HasQueryFilter(x => x.IsDeleted == false);

            modelBuilder.Entity<CompanyLevel>()
            .HasQueryFilter(x => x.IsDeleted == false);

            modelBuilder.Entity<CompanyGroupEmployee>()
            .HasQueryFilter(x => x.IsDeleted == false);

            modelBuilder.Entity<CompanyTitle>()
            .HasQueryFilter(x => x.IsDeleted == false);

            modelBuilder.Entity<NewsCategory>()
            .HasQueryFilter(x => x.IsDeleted == false);

            modelBuilder.Entity<News>()
            .HasQueryFilter(x => x.IsDeleted == false);

            modelBuilder.Entity<Feedback>()
            .HasQueryFilter(x => x.IsDeleted == false);

            modelBuilder.Entity<CompanyBranch>()
            .HasQueryFilter(x => x.IsDeleted == false);

            modelBuilder.Entity<CompanyDocument>()
            .HasQueryFilter(x => x.IsDeleted == false);

            modelBuilder.Entity<Survey>()
            .HasQueryFilter(x => x.IsDeleted == false);

            modelBuilder.Entity<SurveyDetail>()
            .HasQueryFilter(x => x.IsDeleted == false);

            modelBuilder.Entity<SurveyAnswer>()
            .HasQueryFilter(x => x.IsDeleted == false);

            modelBuilder.Entity<SurveyAnswerDetail>()
            .HasQueryFilter(x => x.IsDeleted == false);

            modelBuilder.Entity<UserPermission>()
            .HasQueryFilter(x => x.IsDeleted == false);

            modelBuilder.Entity<CompanyGroup>()
            .HasQueryFilter(x => x.IsDeleted == false);
        }
    }
}