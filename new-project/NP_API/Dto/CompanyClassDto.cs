﻿namespace NP_API.Dto
{
    public class CompanyClassDto
    {
        public Guid? CompanyGradeGuid { get; set; }
        public string Name { get; set; }
    }
}
