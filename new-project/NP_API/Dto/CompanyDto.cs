﻿using FluentValidation;
using System;
using System.ComponentModel.DataAnnotations;

namespace NP_API.Dto
{
    public class CompanyDto
    {
        public string Logo { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string PhoneNo { get; set; }
        public string Field { get; set; }
        public string Website { get; set; }
        public string Theme { get; set; }
        public int ProvinceId { get; set; }
        public int CityId { get; set; }
        public string Address { get; set; }
        public int PostCode { get; set; }
    }
}
