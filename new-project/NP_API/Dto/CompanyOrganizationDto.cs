﻿namespace NP_API.Dto
{
    public class CompanyOrganizationDto
    {
        public int ParentId { get; set; }
        public string Name { get; set; }
    }
}
