﻿namespace NP_API.Dto
{
    public class CompanyTitleDto
    {
        public string Name { get; set; }
    }
}
