﻿using System.ComponentModel.DataAnnotations;

namespace NP_API.Dto.Custom
{
    public class RegistrationDto
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Role { get; set; }
    }

    public class LoginDto
    {
        [Required(ErrorMessage = "Email/username boleh kosong.")]
        public string Key { get; set; }
        [Required(ErrorMessage = "Password tidak boleh kosong.")]
        public string Password { get; set; }
    }
    public class UserFilterDto
    {
        public string? Name { get; set; }
        public string? Username { get; set; }
        public string? CompanyId { get; set; }
        public string? Email { get; set; }
        public string? sortOrder { get; set; }
    }
    public class DeleteDto
    {
        public Guid Guid { get; set; }
    }
}