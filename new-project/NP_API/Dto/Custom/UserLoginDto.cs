﻿using NP_API.Models.Enum;

namespace NP_API.Dto.Custom
{
    public class UserLoginDto
    {
        public Guid Guid { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Token { get; set; }
        public string Role { get; set; }
        public EnRole RoleType { get; set; }
        public Object Details { get; set; }
        public Object CompanyDetails { get; set; }
    }
}
