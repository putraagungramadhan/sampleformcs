﻿using System.ComponentModel.DataAnnotations.Schema;

namespace NP_API.Dto
{
    public class EmployeeDto
    {
        public Guid? CompanyOrganizationGuid { get; set; }
        public Guid? CompanyLevelGuid { get; set; }
        public Guid? CompanyTitleGuid { get; set; }
        public Guid? CompanyGradeGuid { get; set; }
        public Guid? CompanyClassGuid { get; set; }
        public Guid? EmployeeStatusGuid { get; set; }
        public string Name { get; set; }
        public IFormFile Image { get; set; }
        public string? NickName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string MobileNumber { get; set; }
        public Guid BirthPlaceProvinceGuid { get; set; }
        public Guid BirthPlaceCityGuid { get; set; }
        public DateTime BirthDate { get; set; }
        public string Gender { get; set; }
        public string Religion { get; set; }
        public int? PaidLeave { get; set; }
        public DateTime? StartWorking { get; set; }
        public DateTime? EndWorking { get; set; }
        public DateTime? EndContract { get; set; }
        public string IdentityNo { get; set; }
        public string? IdentityAddress { get; set; }
        public Guid IdentityProvinceGuid { get; set; }
        public Guid IdentityCityGuid { get; set; }
        public string IdentityVillage { get; set; }
        public string IdentityRtRw { get; set; }
        public string? IdentityPostalCode { get; set; }
        public string? IdentityAddressStatus { get; set; }
        public bool IsSameAsIdentity { get; set; }
        public string? CurrentAddress { get; set; }
        public Guid CurrentProvinceGuid { get; set; }
        public Guid CurrentCityGuid { get; set; }
        public string CurrentVillage { get; set; }
        public string CurrentRtRw { get; set; }
        public string? CurrentPostalCode { get; set; }
        public string? MaritalStatus { get; set; }
        public DateTime? MaritalDate { get; set; }
        public string? MarriedYear { get; set; }
        public string? SpouseName { get; set; }
        public string? EmergencyName { get; set; }
        public string? EmergencyPhone { get; set; }
        public string? EmergencyRelation { get; set; }
        public string? AccountBank { get; set; }
        public string? AccountName { get; set; }
        public string? BankName { get; set; }
        public string? TaxNo { get; set; }
        [Column(TypeName = "decimal(23, 10)")]
        public decimal BasicSalary { get; set; }
        public bool IsProRata { get; set; }
        public string Notes { get; set; }
        public string? AdditionalIdentityNo { get; set; }
        public string? NpwpNo { get; set; }
        public string? BPJSKesehatanNo { get; set; }
        public string? BPJSTenagaKerjaNo { get; set; }
        public IFormFile? KtpFile { get; set; }
        public IFormFile? NpwpFile { get; set; }
        public IFormFile? EducationCertificateFile { get; set; }
        public IFormFile? FamilyCertificateFile { get; set; }
        public IFormFile? KesehatanFile { get; set; }
        public IFormFile? TenagaKerjaFile { get; set; }
    }
}
