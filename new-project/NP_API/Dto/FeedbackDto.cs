﻿namespace NP_API.Dto
{
    public class FeedbackDto
    {
        public Guid? CompanyGuid { get; set; }
        public Guid UserGuid { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public bool IsRead { get; set; } = false;
    }
}
