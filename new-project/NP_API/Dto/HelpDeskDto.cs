﻿namespace NP_API.Dto
{
    public class HelpDeskDto
    {
        public Guid? CompanyGuid { get; set; }
        public Guid UserSubmitterGuid { get; set; }
        public string HelpDeskSeverity { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public string HelpDeskStatus { get; set; }
        public string? Attachment { get; set; }
    }
}
