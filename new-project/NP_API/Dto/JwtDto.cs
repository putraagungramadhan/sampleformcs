﻿namespace NP_API.Dto
{
    public class JwtDto
    {
        public string? Id { get; set; }
        public string? UserGuid { get; set; }
        public string? CompanyGuid { get; set; }
        public string? Name { get; set; }
        public string? Email { get; set; }
        public string? Role { get; set; }
    }
}
