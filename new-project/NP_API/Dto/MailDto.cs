﻿namespace NP_API.Dto
{
    public class MailDto
    {
        public string EmailToId { get; set; }
        public string EmailToName { get; set; }
        public string EmailCcId { get; set; }
        public string EmailCcName { get; set; }
        public string EmailSubject { get; set; }
        public string EmailBody { get; set; }
    }
}
