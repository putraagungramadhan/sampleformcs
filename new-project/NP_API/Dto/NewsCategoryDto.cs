﻿namespace NP_API.Dto
{
    public class NewsCategoryDto
    {
        public Guid? CompanyGuid { get; set; }
        public string Name { get; set; }
        public int Status { get; set; } = 1;
    }
}
