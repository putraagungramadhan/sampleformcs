﻿namespace NP_API.Dto
{
    public class NewsDto
    {
        public Guid? CompanyGuid { get; set; }
        public Guid NewsCategoryGuid { get; set; }
        public string Title { get; set; }
        public string? Slug { get; set; }
        public string? CoverImage { get; set; }
        public string Content { get; set; }
        public string Status { get; set; } = "publish";
    }
}
