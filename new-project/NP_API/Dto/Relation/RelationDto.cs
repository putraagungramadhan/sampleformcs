﻿using System.ComponentModel.DataAnnotations.Schema;

namespace NP_API.Dto.Relation
{
    public class RelationDto
    {
    }
    public class CompanyResultOnRelationDto
    {
        public Guid Guid { get; set; }
        public Guid? CompanyGroupGuid { get; set; }
        public string Name { get; set; }
        public string LogoDecode { get; set; }
        public string Logo { get; set; }
    }

    public class CompanyOrganizationResultOnRelationDto
    {
        public Guid Guid { get; set; }
        public string Name { get; set; }
    }
    public class CompanyLevelResultOnRelationDto
    {
        public Guid Guid { get; set; }
        public string Name { get; set; }
    }
    public class CompanyTitleResultOnRelationDto
    {
        public Guid Guid { get; set; }
        public string Name { get; set; }
    }
    public class CompanyClassResultOnRelationDto
    {
        public Guid Guid { get; set; }
        public string Name { get; set; }
    }
    public class CompanyGradeResultOnRelationDto
    {
        public Guid Guid { get; set; }
        public string Name { get; set; }
    }
    public class CompanyBranchResultOnRelationDto
    {
        public Guid Guid { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string PhoneNo { get; set; }
        public Guid ProvinceGuid { get; set; }
        public Guid CityGuid { get; set; }
        public string Address { get; set; }
        public int PostCode { get; set; }
    }
    public class EmployeeStatusResultOnRelationDto
    {
        public Guid Guid { get; set; }
        public string Name { get; set; }
    }
    public class ProvinceResultOnRelationDto
    {
        public Guid Guid { get; set; }
        public string Name { get; set; }
    }
    public class CityResultOnRelationDto
    {
        public Guid Guid { get; set; }
        public string Name { get; set; }
    }
    public class TempCandidateEducationOnRelationDto
    {
        public Guid Guid { get; set; }
        public Guid TempCandidateGuid { get; set; }
        public string EducationLevel { get; set; }
        public string Name { get; set; }
        public int YearStart { get; set; }
        public int? YearEnd { get; set; }
        public string? Major { get; set; }
        public string Score { get; set; }
    }
    public class TempCandidateExperienceOnRelationDto
    {
        public Guid? Guid { get; set; }
        public Guid TempCandidateGuid { get; set; }
        public string Name { get; set; }
        public int YearStart { get; set; }
        public int? YearEnd { get; set; }
        public string Title { get; set; }
        public long Salary { get; set; }
        public string ResignReason { get; set; }
    }
    public class TempCandidateFamilyOnRelationDto
    {
        public Guid? Guid { get; set; }
        public Guid TempCandidateGuid { get; set; }
        public string Name { get; set; }
        public string Gender { get; set; }
        public string Relation { get; set; }
        public string BirthPlace { get; set; }
        public DateTime BirthDate { get; set; }
        public string Education { get; set; }
        public string Occupation { get; set; }
    }
    public class TempCandidateLanguageOnRelationDto
    {
        public Guid? Guid { get; set; }
        public Guid TempCandidateGuid { get; set; }
        public string Name { get; set; }
        public string ReadingScore { get; set; }
        public string ListeningScore { get; set; }
        public string SpeakingScore { get; set; }
        public string WritingScore { get; set; }
    }
    public class TempCandidateOrganizationOnRelationDto
    {
        public Guid? Guid { get; set; }
        public Guid TempCandidateGuid { get; set; }
        public string Name { get; set; }
        public string Title { get; set; }
        public int YearStart { get; set; }
        public int? YearEnd { get; set; }
    }
    public class TempCandidateOnRelationDto
    {
        public Guid? Guid { get; set; }
        public string? Email { get; set; }
        public string? Name { get; set; }
        public string? IdentityNo { get; set; }
        public IFormFile? Image { get; set; }
        public string? PhoneNo { get; set; }
        public string? BirthPlace { get; set; }
        public DateTime? BirthDate { get; set; }
        public string? Gender { get; set; }
        public Guid? ProvinceGuid { get; set; }
        public Guid? CityGuid { get; set; }
        public string? Address { get; set; }
        public string? BloodType { get; set; }
        public string? MaritalStatus { get; set; }
        public string? Hobby { get; set; }
        public string? Religion { get; set; }
        public string? AboutSelf { get; set; }
        public string? AboutFutureSelf { get; set; }
    }
    public class CandidateQuestionnaireOnRelationDto
    {
        public Guid? Guid { get; set; }
        public string Question { get; set; }
    }
    public class TrackingRecruitmentDetailOnRelationDto
    {
        public Guid Guid { get; set; }
        public string? Icon { get; set; }
        public string Name { get; set; }
        public string Details { get; set; }
        public int Weight { get; set; }
    }
    public class CandidateOnRelationDto
    {
        public string? ImageDecode { get; set; }
        public string? Image { get; set; }
        public string? Name { get; set; }
        public string Email { get; set; }
        public string IdentityNo { get; set; }
        public string? PhoneNo { get; set; }
        public string? BirthPlace { get; set; }
        public DateTime? BirthDate { get; set; }
        public string? Gender { get; set; }
        public Guid? ProvinceGuid { get; set; }
        public Guid? CityGuid { get; set; }
        public string? Address { get; set; }
        public string? BloodType { get; set; }
        public string? MaritalStatus { get; set; }
        public string? Hobby { get; set; }
        public string? Religion { get; set; }
        public string? AboutSelf { get; set; }
        public string? AboutFutureSelf { get; set; }
        public ProvinceResultOnRelationDto Province { get; set; }
        public CityResultOnRelationDto City { get; set; }
    }
    public class VacancyCandidateOnRelationDto
    {
        public string VacancyNo { get; set; }
        public Guid VacancyGuid { get; set; }
        public Guid CandidateGuid { get; set; }
        public long ApplyDate { get; set; }
        public string? TrackingRecruimentStatus { get; set; } = "On Progress";
        public CandidateOnRelationDto Candidate { get; set; }
    }
    public class VacancyCandidateTrackingOnRelationDto
    {
        public Guid Guid { get; set; }
        public Guid VacancyCandidateGuid { get; set; }
        public Guid TrackingRecruitmentDetailGuid { get; set; }
        public string? TrackingRecruimentStatus { get; set; }
        public string? Notes { get; set; }
        public TrackingRecruitmentDetailOnRelationDto TrackingRecruitmentDetail { get; set; }
    }
    public class EmployeePlacementResultOnRelationDto
    {
        public Guid EmployeeGuid { get; set; }
        public bool IsOnBranch { get; set; }
        public Guid? CompanyBranchGuid { get; set; }
        public DateTime EffDate { get; set; }
        public string? Attachment { get; set; }
        public string? AttachmentDecode { get; set; }
        public CompanyBranchResultOnRelationDto CompanyBranch { get; set; }
    }
    public class EmployeePlacementProposalApprovalResultOnRelationDto
    {
        public Guid Guid { get; set; }
        public Guid UserGuid { get; set; }
        public string Approval { get; set; } = "On Progress";
        public string Notes { get; set; }
        public UserResultOnRelationDto User { get; set; }
    }
    public class EmployeeCompanyProposalApprovalResultOnRelationDto
    {
        public Guid Guid { get; set; }
        public Guid UserGuid { get; set; }
        public string Approval { get; set; } = "On Progress";
        public string Notes { get; set; }
        public UserResultOnRelationDto User { get; set; }
    }
    public class UserResultOnRelationDto
    {
        public Guid Guid { get; set; }
        public string Username { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Role { get; set; }
        public ICollection<EmployeePlacementResultOnRelationDto>? EmployeePlacement { get; set; }
        public EmployeeOnRelationDto? Employee { get; set; }
        public CompanyResultOnRelationDto Company { get; set; }

    }
    public class EmployeeOnRelationDto
    {
        public Guid Guid { get; set; }
        public Guid UserGuid { get; set; }
        public Guid? CompanyOrganizationGuid { get; set; }
        public Guid? CompanyLevelGuid { get; set; }
        public Guid? CompanyTitleGuid { get; set; }
        public Guid? CompanyGradeGuid { get; set; }
        public Guid? CompanyClassGuid { get; set; }
        public Guid? EmployeeStatusGuid { get; set; }
        public Guid? UserApprovalGuid { get; set; }
        public Guid? CompanyGroupEmployeeGuid { get; set; }
        public string Nik { get; set; }
        public string Name { get; set; }
        public string? ImageDecode { get; set; }
        public string? Image { get; set; }
        public string? NickName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string MobileNumber { get; set; }
        public Guid BirthPlaceProvinceGuid { get; set; }
        public Guid BirthPlaceCityGuid { get; set; }
        public DateTime BirthDate { get; set; }
        public string Gender { get; set; }
        public string Religion { get; set; }
        public int? PaidLeave { get; set; }
        public DateTime? StartWorking { get; set; }
        public DateTime? EndWorking { get; set; }
        public DateTime? EndContract { get; set; }
        public string IdentityNo { get; set; }
        public string? IdentityAddress { get; set; }
        public Guid IdentityProvinceGuid { get; set; }
        public Guid IdentityCityGuid { get; set; }
        public string IdentityVillage { get; set; }
        public string IdentityRtRw { get; set; }
        public string? IdentityPostalCode { get; set; }
        public string? IdentityAddressStatus { get; set; }
        public bool IsSameAsIdentity { get; set; }
        public string? CurrentAddress { get; set; }
        public Guid CurrentProvinceGuid { get; set; }
        public Guid CurrentCityGuid { get; set; }
        public string CurrentVillage { get; set; }
        public string CurrentRtRw { get; set; }
        public string? CurrentPostalCode { get; set; }
        public string? MaritalStatus { get; set; }
        public DateTime? MaritalDate { get; set; }
        public string? MarriedYear { get; set; }
        public string? SpouseName { get; set; }
        public string? EmergencyName { get; set; }
        public string? EmergencyPhone { get; set; }
        public string? EmergencyRelation { get; set; }
        public string? AccountBank { get; set; }
        public string? AccountName { get; set; }
        public string? BankName { get; set; }
        public string? TaxNo { get; set; }
        [Column(TypeName = "decimal(23, 10)")]
        public decimal BasicSalary { get; set; }
        public bool IsProRata { get; set; }
        public string Notes { get; set; }
        public string? AdditionalIdentityNo { get; set; }
        public string? NpwpNo { get; set; }
        public string? BpjsKesehatanNo { get; set; }
        public string? BpjsTenagaKerjaNo { get; set; }
        public string? KtpFile { get; set; }
        public string? KtpFileDecode { get; set; }
        public string? NpwpFile { get; set; }
        public string? NpwpFileDecode { get; set; }
        public string? EducationCertificateFile { get; set; }
        public string? EducationCertificateFileDecode { get; set; }
        public string? FamilyCertificateFile { get; set; }
        public string? FamilyCertificateFileDecode { get; set; }
        public string? KesehatanFile { get; set; }
        public string? KesehatanFileDecode { get; set; }
        public string? TenagaKerjaFile { get; set; }
        public string? TenagaKerjaFileDecode { get; set; }
        public CompanyOrganizationResultOnRelationDto? CompanyOrganization { get; set; }
        public CompanyLevelResultOnRelationDto? CompanyLevel { get; set; }
        public CompanyTitleResultOnRelationDto? CompanyTitle { get; set; }
        public CompanyGradeResultOnRelationDto? CompanyGrade { get; set; }
        public CompanyClassResultOnRelationDto? CompanyClass { get; set; }
        public EmployeeStatusResultOnRelationDto? EmployeeStatus { get; set; }
        public Object EmployeePlacement { get; set; }
    }

    public class GaStationeryItemOnRelationDto { 
        public string Name { get; set; }
        public string Type { get; set; }
    }
    public class NewsCategoryOnRelationDto
    {
        public string Name { get; set; }
    }
    public class ModuleOnRelationDto
    {
        public string Name { get; set; }
        public bool CanWrite { get; set; }
    }
    public class CompanyGroupOnRelationDto
    {
        public string Name { get; set; }
        public string Detail { get; set; }
    }

    public class SurveyOnRelationDto 
    {
        public string Title { get; set; }
        public string Description { get; set; }
    }

    public class SurveyDetailOnRelationDto 
    {
        public Guid Guid { get; set; }
        public string Type { get; set; }
        public string Question { get; set; }
        public string? Choice { get; set; }
    }
    public class AttendanceBreakDetailOnRelationDto
    {
        public Guid Guid { get; set; }
        public Guid AttendanceBreakGuid { get; set; }
        public string BreakIn { get; set; }
        public string BreakOut { get; set; }
    }
    public class AttendanceBreakOnRelationDto
    {
        public Guid Guid { get; set; }
        public string Name { get; set; }
        public string Details { get; set; }
        public ICollection<AttendanceBreakDetailOnRelationDto> AttendanceBreakDetail { get; set; }
    }
    public class AttendanceShiftOnRelationDto
    {
        public string Name { get; set; }
        public string Detail { get; set; }
        public string ShiftIn { get; set; }
        public string ShiftOut { get; set; }
        public bool CanOvertime { get; set; }
        public string? OvertimeMinimum { get; set; }
        public string? OvertimeMaximum { get; set; }
        public AttendanceBreakOnRelationDto? AttendanceBreak { get; set; }
    }
    public class VacancyRequestDetailOnRelationDto
    {
        public Guid Guid { get; set; }
        public Guid VacancyRequestGuid { get; set; }
        public Guid? CompanyGroupEmployeeGuid { get; set; }
        public Guid CompanyOrganizationGuid { get; set; }
        public Guid CompanyLevelGuid { get; set; }
        public Guid CompanyTitleGuid { get; set; }
        public Guid CompanyGradeGuid { get; set; }
        public Guid EmployeeStatusGuid { get; set; }
        public Guid? TrackingRecruitmentGuid { get; set; }
        public string? Name { get; set; }
        public string? Poster { get; set; }
        public int TotalCandidate { get; set; } = 0;
        public string Description { get; set; }
        public int IsAssignment { get; set; } = 0;
        public DateTime? InterviewDateEstimation { get; set; }
        public string Spesification { get; set; }
        public CompanyGroupEmployeeOnRelationDto? CompanyGroupEmployee { get; set; }
        public CompanyOrganizationResultOnRelationDto? CompanyOrganization { get; set; }
        public CompanyLevelResultOnRelationDto? CompanyLevel { get; set; }
        public CompanyTitleResultOnRelationDto? CompanyTitle { get; set; }
        public CompanyGradeResultOnRelationDto? CompanyGrade { get; set; }
        public CompanyClassResultOnRelationDto? CompanyClass { get; set; }
        public EmployeeStatusResultOnRelationDto? EmployeeStatus { get; set; }
    }
    public class CompanyGroupEmployeeOnRelationDto
    {
        public string? Name { get; set; }
        public Guid? CompanyOrganizationGuid { get; set; }
        public Guid? CompanyLevelGuid { get; set; }
        public Guid? CompanyTitleGuid { get; set; }
        public Guid? CompanyGradeGuid { get; set; }
        public Guid? CompanyClassGuid { get; set; }
        public Guid? EmployeeStatusGuid { get; set; }
        public long? BasicSalary { get; set; }
        public int? PaidLeave { get; set; }
    }
    public class EmployeeLeaveProposalApprovalOnRelationDto
    {
        public Guid Guid { get; set; }
        public Guid UserGuid { get; set; }
        public string Approval { get; set; } = "On Progress";
        public string Notes { get; set; }
        public UserResultOnRelationDto User { get; set; }
    }
    public class AttendanceOnRelationDto
    {
        public Guid Guid { get; set; }
        public Guid CompanyGuid { get; set; }
        public Guid UserGuid { get; set; }
        public Guid AttendanceScheduleDetailGuid { get; set; }
        public int AttendanceShiftIndex { get; set; }
        public DateTime AttendanceDate { get; set; }
        public bool IsHoliday { get; set; } = false;
        public bool IsPresent { get; set; } = false;
        public bool IsClockIn { get; set; } = false;
        public bool IsClockOut { get; set; } = false;
        public bool IsLate { get; set; } = false;
        public bool IsEarly { get; set; } = false;
        public string? Notes { get; set; }
        public int? MinutesOfWork { get; set; }
    }
}
