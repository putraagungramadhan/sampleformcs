﻿using NP_API.Config;
using NP_API.Dto.Relation;
using NP_API.Models;
using Microsoft.Extensions.Options;
using Minio;
using Newtonsoft.Json.Linq;

namespace NP_API.Dto.Request.Company
{
    public class CompanyResultDto
    {
        public Guid Guid { get; set; }
        public string LogoDecode { get; set; }
        public string Logo { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string PhoneNo { get; set; }
        public string Field { get; set; }
        public string Website { get; set; }
        public string Theme { get; set; }
        public string CompanyCode { get; set; }
        public bool IsHolding { get; set; }
        public Guid CompanyGroupGuid { get; set; }
        public Guid ProvinceGuid { get; set; }
        public Guid CityGuid { get; set; }
        public string Address { get; set; }
        public int PostCode { get; set; }
        public CompanyGroupOnRelationDto CompanyGroup { get; set; }
    }
}
