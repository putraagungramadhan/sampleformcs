namespace NP_API.Dto.Request.Company
{
    public class GetDto : PaginationDto
    {
        public string Name { get; set; } = "";
    }
}