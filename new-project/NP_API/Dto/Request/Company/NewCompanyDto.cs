﻿using System.ComponentModel.DataAnnotations;

namespace NP_API.Dto.Request.Company
{
    public class NewCompanyDto
    {
        public IFormFile Logo { get; set; }
        public CompanyRegisterDto CompanyDto { get; set; }
        public AdminRegisterDto UserDto { get; set; }
    }
    public class CompanyRegisterDto
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string PhoneNo { get; set; }
        public string Field { get; set; }
        public string Website { get; set; }
        public string Theme { get; set; }
        public string CompanyCode { get; set; }
        public Guid ProvinceGuid { get; set; }
        public Guid CityGuid { get; set; }
        public string Address { get; set; }
        public int PostCode { get; set; }
        public Guid? CompanyGroupGuid { get; set; }
        public bool IsHolding { get; set; } = false;
    }
    public class AdminRegisterDto
    {
        public string Username { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Role { get; set; }
    }
}
