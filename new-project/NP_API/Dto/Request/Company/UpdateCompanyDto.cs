namespace NP_API.Dto.Request.Company
{
    public class UpdateCompanyDto
    {
  
        public Guid Guid { get; set; }
        public IFormFile Logo { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string PhoneNo { get; set; }
        public string Field { get; set; }
        public string Website { get; set; }
        public string Theme { get; set; }
        public Guid ProvinceGuid { get; set; }
        public Guid CityGuid { get; set; }
        public string Address { get; set; }
        public int PostCode { get; set; }
        public bool IsHolding { get; set; }
        public string CompanyCode { get; set; }
        public Guid CompanyGroupGuid { get; set; }
    }

}