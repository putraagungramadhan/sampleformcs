﻿namespace NP_API.Dto.Request.CompanyBranch
{
    public class GetDto : PaginationDto
    {
        public string Name { get; set; }
    }
}
