﻿namespace NP_API.Dto.Request.CompanyBranch
{
    public class UpdateCompanyBranchDto
    {
        public Guid Guid { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string PhoneNo { get; set; }
        public Guid ProvinceGuid { get; set; }
        public Guid CityGuid { get; set; }
        public string Address { get; set; }
        public int PostCode { get; set; }
    }
}
