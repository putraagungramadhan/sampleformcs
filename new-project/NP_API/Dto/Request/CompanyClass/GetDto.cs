﻿namespace NP_API.Dto.Request.CompanyClass
{
    public class GetDto : PaginationDto
    {
        public string Name { get; set; } = "";
        public Guid? CompanyGradeGuid { get; set; }
    }
}
