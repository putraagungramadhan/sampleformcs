﻿namespace NP_API.Dto.Request.CompanyClass
{
    public class UpdateClassDto
    {
        public Guid Guid { get; set; }
        public Guid? CompanyGuid { get; set; }
        public Guid? CompanyGradeGuid { get; set; }
        public string Name { get; set; }
    }
}
