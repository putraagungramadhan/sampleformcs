﻿namespace NP_API.Dto.Request.CompanyDocument
{
    public class CompanyDocumentDto
    {
        public Guid CompanyGuid { get; set; }
        public string Name { get; set; }
        public string file { get; set; }
        public long CreatedAt { get; set; }
    }
}
