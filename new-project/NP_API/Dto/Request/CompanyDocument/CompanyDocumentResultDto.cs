﻿namespace NP_API.Dto.Request.CompanyDocument
{
    public class CompanyDocumentResultDto
    {
        public Guid? Guid { get; set; }
        public Guid CompanyGuid { get; set; }
        public string Name { get; set; }
        public string file { get; set; }
        public long CreatedAt { get; set; }
    }
}
