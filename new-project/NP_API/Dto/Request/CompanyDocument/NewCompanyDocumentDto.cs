﻿namespace NP_API.Dto.Request.CompanyDocument
{
    public class NewCompanyDocumentDto
    {
        public string Name { get; set; }
        public IFormFile File { get; set; }
    }
}
