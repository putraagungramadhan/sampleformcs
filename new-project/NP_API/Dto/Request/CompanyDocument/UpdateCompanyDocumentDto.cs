﻿namespace NP_API.Dto.Request.CompanyDocument
{
    public class UpdateCompanyDocumentDto
    {
        public Guid Guid { get; set; }
        public string Name { get; set; }
        public IFormFile File { get; set; }
      
    }
}
