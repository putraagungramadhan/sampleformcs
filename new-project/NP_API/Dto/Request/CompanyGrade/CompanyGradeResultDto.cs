﻿using NP_API.Dto.Request.Company;

namespace NP_API.Dto.Request.CompanyGrade
{
    public class CompanyGradeResultDto
    {
        public Guid Guid { get; set; }
        public Guid CompanyGuid { get; set; }
        public string Name { get; set; }
    }
    public class CompanyResultOnGradeDto
    {
        public string Name { get; set; }
        public string Email { get; set; }
    }


}
