﻿namespace NP_API.Dto.Request.CompanyGrade
{
    public class UpdateGradeDto
    {
        public Guid Guid { get; set; }
        public Guid CompanyGuid { get; set; }
        public string Name { get; set; }
    }
}
