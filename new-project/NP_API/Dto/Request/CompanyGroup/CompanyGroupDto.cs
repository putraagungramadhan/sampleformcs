﻿namespace NP_API.Dto.Request.CompanyGroup
{
    public class CompanyGroupDto
    {
        public string Name { get; set; }
        public string Detail { get; set; }
    }
}
