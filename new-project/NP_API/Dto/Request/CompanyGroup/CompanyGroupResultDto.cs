﻿namespace NP_API.Dto.Request.CompanyGroup
{
    public class CompanyGroupResultDto
    {
        public Guid Guid { get; set; }
        public string Name { get; set; }
        public string Detail { get; set; }
    }
}
