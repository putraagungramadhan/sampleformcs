﻿namespace NP_API.Dto.Request.CompanyGroup
{
    public class GetDto : PaginationDto
    {
        public string Name { get; set; }
    }
}
