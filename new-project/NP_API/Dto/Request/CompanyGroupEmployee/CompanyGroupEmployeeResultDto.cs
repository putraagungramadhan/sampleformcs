﻿using NP_API.Models;
using NP_API.Dto.Relation;

namespace NP_API.Dto.Request.CompanyGroupEmployee
{
    public class CompanyGroupEmployeeResultDto
    {
        public Guid Guid { get; set; }
        public Guid CompanyGuid { get; set; }
        public string Name { get; set; }
        public CompanyOrganizationResultOnRelationDto CompanyOrganization { get; set; }
        public CompanyLevelResultOnRelationDto CompanyLevel { get; set; }
        public CompanyTitleResultOnRelationDto CompanyTitle { get; set; }
        public CompanyClassResultOnRelationDto CompanyClass { get; set; }
        public CompanyGradeResultOnRelationDto CompanyGrade { get; set; }
        public EmployeeStatusResultOnRelationDto EmployeeStatus { get; set; }
    }
}
