﻿namespace NP_API.Dto.Request.CompanyGroupEmployee
{
    public class GetDto : PaginationDto
    {
        public int Id { get; set; }
        public string Name { get; set; } = "";
    }
}
