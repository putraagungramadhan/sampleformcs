﻿namespace NP_API.Dto.Request.CompanyGroupEmployee
{
    public class UpdateGroupEmployeeDto
    {
        public Guid Guid { get; set; }
        public string? Name { get; set; }
        public Guid? CompanyOrganizationGuid { get; set; }
        public Guid? CompanyLevelGuid { get; set; }
        public Guid? CompanyGradeGuid { get; set; }
        public Guid? CompanyClassGuid { get; set; }
        public Guid? EmployeeStatusGuid { get; set; }
        public long? BasicSalary { get; set; }
        public int? PaidLeave { get; set; }
    }
}
