﻿namespace NP_API.Dto.Request.CompanyLevel
{
    public class GetDto : PaginationDto
    {
        public string Name { get; set; } = "";
    }
}
