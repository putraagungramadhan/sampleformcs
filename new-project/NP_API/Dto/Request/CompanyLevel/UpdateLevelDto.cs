﻿namespace NP_API.Dto.Request.CompanyLevel
{
    public class UpdateLevelDto
    {
        public Guid Guid { get; set; }
        public Guid CompanyGuid { get; set; }
        public string Name { get; set; }
    }
}
