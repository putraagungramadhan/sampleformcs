﻿namespace NP_API.Dto.Request.CompanyOrganization
{
    public class CompanyOrganizationResultDto
    {
        public int Id { get; set; }
        public Guid Guid { get; set; }
        public Guid CompanyGuid { get; set; }
        public int ParentId { get; set; }
        public string Name { get; set; }
    }
}
