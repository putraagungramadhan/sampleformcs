﻿namespace NP_API.Dto.Request.CompanyOrganization
{
    public class GetDto : PaginationDto
    {
        public string Name { get; set; } = "";
    }
}
