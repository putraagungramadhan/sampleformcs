﻿namespace NP_API.Dto.Request.CompanyOrganization
{
    public class UpdateOrganizationDto
    {
        public Guid Guid { get; set; }
        public Guid CompanyGuid { get; set; }
        public int ParentId { get; set; }
        public string Name { get; set; }
    }
}
