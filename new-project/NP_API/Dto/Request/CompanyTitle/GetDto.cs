﻿namespace NP_API.Dto.Request.CompanyTitle
{
    public class GetDto : PaginationDto
    {
        public string Name { get; set; } = "";
    }
}
