﻿namespace NP_API.Dto.Request.CompanyTitle
{
    public class UpdateCompanyTitleDto
    {
        public Guid Guid { get; set; }
        public Guid CompanyGuid { get; set; }
        public string Name { get; set; }
    }
}
