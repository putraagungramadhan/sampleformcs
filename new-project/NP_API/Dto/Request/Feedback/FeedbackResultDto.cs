﻿using NP_API.Dto.Relation;

namespace NP_API.Dto.Request.Feedback
{
    public class FeedbackResultDto
    {
        public Guid? Guid { get; set; }
        public Guid? CompanyGuid { get; set; }
        public Guid UserGuid { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public bool IsRead { get; set; } = false;
        public long CreatedAt { get; set; }
        public UserResultOnRelationDto User { get; set; }

    }

   
}
