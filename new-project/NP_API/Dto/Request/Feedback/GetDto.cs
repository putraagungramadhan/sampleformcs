﻿namespace NP_API.Dto.Request.Feedback
{
    public class GetDto : PaginationDto
    {
        public string Title { get; set; }
    
    }
}
