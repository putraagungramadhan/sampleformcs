﻿namespace NP_API.Dto.Request.Feedback
{
    public class UpdateFeedbackDto
    {
        public Guid Guid { get; set; }
        public Guid UserGuid { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public bool IsRead { get; set; } = false;
    }
}
