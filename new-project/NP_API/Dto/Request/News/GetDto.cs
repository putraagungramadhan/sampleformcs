﻿namespace NP_API.Dto.Request.News
{
    public class GetDto : PaginationDto
    {
        public string Title { get; set; }
        public Guid NewsCategoryGuid { get; set; }
    }
}
