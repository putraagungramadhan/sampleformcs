﻿using NP_API.Dto.Relation;

namespace NP_API.Dto.Request.News
{
    public class NewsResultDto
    {
        public Guid? Guid { get; set; }
        public Guid? CompanyGuid { get; set; }
        public Guid NewsCategoryGuid { get; set; }
        public string Title { get; set; }
        public string? Slug { get; set; }
        public string? CoverImage { get; set; }
        public string Content { get; set; }
        public string Status { get; set; } = "publish";
        public long CreatedAt { get; set; }
        //public NewsCategoryOnRelationDto NewsCategory { get; set; }
    }
}
