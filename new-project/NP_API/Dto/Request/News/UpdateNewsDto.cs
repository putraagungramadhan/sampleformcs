﻿namespace NP_API.Dto.Request.News
{
    public class UpdateNewsDto
    {
        public Guid Guid { get; set; }
        public Guid NewsCategoryGuid { get; set; }
        public string Title { get; set; }
        public string? Slug { get; set; }
        public IFormFile? CoverImage { get; set; }
        public string Content { get; set; }
        public string Status { get; set; } = "publish";
    }
}
