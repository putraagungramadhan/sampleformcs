
namespace NP_API.Dto.Request.NewsCategory {

    public class NewNewsCategoryDto {
     
        public string Name { get; set; }
        public int Status { get; set; } = 1;
    }
}