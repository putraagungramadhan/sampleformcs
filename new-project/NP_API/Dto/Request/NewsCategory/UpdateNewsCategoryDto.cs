﻿namespace NP_API.Dto.Request.NewsCategory {
    public class UpdateNewsCategoryDto {
        public Guid Guid { get; set; }
        public Guid CompanyGuid { get; set; }
        public string Name { get; set; }
        public int Status { get; set; } = 1;
    }
}


