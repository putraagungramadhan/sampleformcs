﻿using NP_API.Config;
using Microsoft.Extensions.Options;

namespace NP_API.Dto.Request
{
    public class PaginationDto
    {
        public Guid Guid { get; set; }
        public int Page { get; set; } = 1;
        public int PageSize { get; set; } = 10;
        public string OrderBy { get; set; } = "";
        public bool Ascending { get; set; } = true;
    }
}
