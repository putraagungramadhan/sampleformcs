﻿using NP_API.Models.Enum;

namespace NP_API.Dto.Request.SetupConfigurations
{
    public class NewSetupConfigurationDto
    {
        public string SubTypeName { get; set; }
        public string TypeName { get; set; }
        public EnType TypeData { get; set; }
        public string TypeValue { get; set; }
    }
}
