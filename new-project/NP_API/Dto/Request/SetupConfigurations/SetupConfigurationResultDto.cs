﻿using NP_API.Dto.Relation;
using NP_API.Models.Enum;

namespace NP_API.Dto.Request.SetupConfigurations
{
    public class SetupConfigurationResultDto
    {
        public Guid Guid { get; set; }
        public string TypeName { get; set; }
        public EnType TypeData { get; set; }
        public string TypeValue { get; set; }
        public bool IsActive { get; set; }
    }
}
