﻿using NP_API.Models.Enum;

namespace NP_API.Dto.Request.User
{
    public class GetDto : PaginationDto
    {
        public string Username { get; set; }
        public string Role { get; set; }
        public EnRole RoleType { get; set; }
    }
}
