﻿namespace NP_API.Dto.Request.User
{
    public class NewUserDto
    {
        public Guid CompanyGuid { get; set; }
        public string Username { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Role { get; set; }
        public List<PermissionOnUserDto> Permissions { get; set; }
    }
    public class PermissionOnUserDto
    {
        public Guid ModuleGuid { get; set; }
        public bool CanWrite { get; set; }
    }
}
