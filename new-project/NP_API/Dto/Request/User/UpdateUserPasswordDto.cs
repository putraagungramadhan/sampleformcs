﻿namespace NP_API.Dto.Request.User
{
    public class UpdateUserPasswordDto
    {
        public Guid Guid { get; set; }
        public string Password { get; set; }
    }
}
