﻿using NP_API.Dto.Relation;

namespace NP_API.Dto.Request.User
{
    public class UserResultDto
    {
        public Guid Guid { get; set; }
        public Guid? CompanyGuid { get; set; }
        public string Username { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Role { get; set; }
        public Object Details { get; set; }
        public CompanyResultOnRelationDto Company { get; set; }
    }
}
