﻿using NP_API.Models.Enum;

namespace NP_API.Dto.Request.UserPermission
{
    public class GetDto : PaginationDto
    {
        public Guid UserGuid { get; set; }
        public Guid ModuleGuid { get; set; }
        public string Role { get; set; }
        public EnRole RoleType { get; set; }
    }
}
