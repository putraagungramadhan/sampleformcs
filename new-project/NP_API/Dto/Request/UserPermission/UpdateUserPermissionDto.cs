﻿namespace NP_API.Dto.Request.UserPermission
{
    public class UpdateUserPermissionDto
    {
        public Guid Guid { get; set; }
        public bool CanWrite { get; set; }
    }
}
