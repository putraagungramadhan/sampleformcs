﻿namespace NP_API.Dto.Request.UserPermission
{
    public class UserPermissionDto
    {
        public Guid UserGuid { get; set; }
        public Guid ModuleGuid { get; set; }
        public bool CanWrite { get; set; }
    }
}
