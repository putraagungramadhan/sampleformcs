﻿using NP_API.Dto.Relation;

namespace NP_API.Dto.Request.UserPermission
{
    public class UserPermissionResultDto
    {
        public Guid UserGuid { get; set; }
        public Guid ModuleGuid { get; set; }
        public bool CanWrite { get; set; }
        public UserResultOnRelationDto User { get; set; }
        public ModuleOnRelationDto Module { get; set; }
    }
}
