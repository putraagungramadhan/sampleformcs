﻿namespace NP_API.Dto
{
    public class ResultDto
    {
        public bool isSuccess { get; set; }
        public string error { get; set; }
        public string title { get; set; }
        public Object data { get; set; }
    }
}
