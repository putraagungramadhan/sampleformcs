﻿using NP_API.Models.Enum;

namespace NP_API.Dto
{
    public class SetupConfigurationDto
    {
        public string? TypeName { get; set; }
        public EnType? TypeData { get; set; }
        public string? TypeValue { get; set; }
        public bool? IsActive { get; set; }
    }
}
