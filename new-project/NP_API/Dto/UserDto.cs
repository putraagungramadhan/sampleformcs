﻿namespace NP_API.Dto
{
    public class UserDto
    {
        public int? EmployeeMasterId { get; set; }
        public int? CompanyId { get; set; }
        public string Username { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Role { get; set; }
        
    }
}
