﻿using NP_API.Exceptions;
using NP_API.Models;

namespace NP_API.Helper
{
    public static class GeneralHelper
    {
        private static readonly DateTime UnixEpoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
        public static Guid ConvertToGuid(string input)
        {
            if (Guid.TryParse(input, out Guid guidValue))
            {
                return guidValue;
            }
            else
            {
                throw new ForbiddenException("Invalid GUID format");
            }
        }
        public static string HashPassword(string password)
        {
            return BCrypt.Net.BCrypt.HashPassword(password);
        }
        public static string GenerateNikEmployee(string companyCode,string orderNumber)
        {
            string currentYear = DateTime.Now.Year.ToString();
            return companyCode + currentYear + orderNumber;
        }
        public static string GenerateOrderNumber(string orderNumber)
        {
            int number = int.Parse(orderNumber);
            number = number + 1;
            return number.ToString().PadLeft(4, '0');
        }
        public static long ToUnixTimestamp(this DateTime dateTime)
        {
            return (long)(dateTime.ToUniversalTime() - UnixEpoch).TotalSeconds;
        }
        public static DateTime GetDateNowByTimezone(string timezone)
        {
            DateTime utcNow = DateTime.UtcNow;
            TimeZoneInfo targetTimeZone = TimeZoneInfo.FindSystemTimeZoneById(timezone);
            DateTime utcPlus7Time = TimeZoneInfo.ConvertTimeFromUtc(utcNow, targetTimeZone);            
            DateTime dateOnly = utcPlus7Time.Date;

            return utcPlus7Time;
        }
        public static DateTime GetDateByTimezoneFromUnixTimestamp(long unixTimestamp, string timezone)
        {
            DateTime epochStart = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            DateTime utcDateTime = epochStart.AddSeconds(unixTimestamp);
            TimeZoneInfo targetTimeZone = TimeZoneInfo.FindSystemTimeZoneById(timezone);
            DateTime targetDateTime = TimeZoneInfo.ConvertTimeFromUtc(utcDateTime, targetTimeZone);

            return targetDateTime;
        }
    }
}
