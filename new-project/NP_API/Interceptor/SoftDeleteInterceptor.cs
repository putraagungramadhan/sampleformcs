﻿using NP_API.Dto;
using NP_API.Models.Interface;
using NP_API.Repository.Interface;
using NP_API.Service;
using NP_API.Service.Interface;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;

namespace NP_API.Interceptor
{
    public class SoftDeleteInterceptor : SaveChangesInterceptor
    {
        private readonly IJwtService _jwtService;
        private readonly IHttpContextAccessor _httpContextAccessor;
        public SoftDeleteInterceptor(IJwtService jwtService, IHttpContextAccessor httpContextAccessor)
        {
            this._jwtService = jwtService;
            this._httpContextAccessor = httpContextAccessor;
        }
        public override async ValueTask<InterceptionResult<int>> SavingChangesAsync(
        DbContextEventData eventData,
        InterceptionResult<int> result,
        CancellationToken cancellationToken = default)
        {
            if (eventData.Context is null) return result;

            JwtDto dto = _jwtService.decode(_httpContextAccessor.HttpContext.Request.Headers["authorization"]);

            foreach (var entry in eventData.Context.ChangeTracker.Entries())
            {
                if (entry is not { State: EntityState.Deleted, Entity: ISoftDelete delete }) continue;
                entry.State = EntityState.Modified;
                delete.IsDeleted = true;
                delete.DeletedBy = dto.Id;
                delete.DeletedAt = (long)DateTime.UtcNow.ToUniversalTime().Subtract(DateTime.UnixEpoch).TotalSeconds;
            }

            return result;
        }
    }
}
