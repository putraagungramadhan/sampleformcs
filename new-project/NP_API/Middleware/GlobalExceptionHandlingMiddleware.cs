﻿using NP_API.Exceptions;
using NP_API.Service;
using NP_API.Validation;
using Microsoft.AspNetCore.Mvc;
using Org.BouncyCastle.Utilities.Encoders;
using Serilog;
using System.Net;
using System.Reflection;
using System.Text.Json;

namespace NP_API.Middleware
{
    public class GlobalExceptionHandlingMiddleware : IMiddleware
    {
        //private readonly ILogger _logger;
        private readonly FactoryService _factory;

        public GlobalExceptionHandlingMiddleware(FactoryService factory)
        {
            this._factory = factory;
        }

        public async Task InvokeAsync(HttpContext context, RequestDelegate next)
        {
            try
            {
                await next(context);
            }
            catch (NotFoundException e)
            {
                context.Response.StatusCode = (int)HttpStatusCode.NotFound;
                ProblemDetails problem = new()
                {
                    Status = (int)HttpStatusCode.NotFound,
                    Type = "not_found",
                    Title = "Data Not Found",
                    Detail = e.Message
                };
                problem.Extensions.Add("errors", e.Message);
                string json = JsonSerializer.Serialize(problem);
                context.Response.ContentType = "application/json";
                await context.Response.WriteAsync(json);

            }
            catch (ForbiddenException e)
            {
                context.Response.StatusCode = (int)HttpStatusCode.Forbidden;
                string[] errors = e.Message.Split("-");
                ProblemDetails problem = new()
                {
                    Status = (int)HttpStatusCode.Forbidden,
                    Type = "forbidden",
                    Title = errors[0],
                    Detail = errors[0]
                };
                Dictionary<string, object> datas = new Dictionary<string, object>();
                if (errors[1] != "")
                {
                    string[] strings = errors[1].Split(":");
                    datas = new Dictionary<string, object>
                    {
                        { strings[0], strings[1] },
                    };
                }
                
                problem.Extensions.Add("errors", errors[0]);
                problem.Extensions.Add("data", datas);
                string json = JsonSerializer.Serialize(problem);
                context.Response.ContentType = "application/json";
                await context.Response.WriteAsync(json);

            }
            catch (BadRequestException e)
            {
                context.Response.StatusCode = (int)HttpStatusCode.BadRequest;
                ProblemDetails problem = new()
                {
                    Status = (int)HttpStatusCode.BadRequest,
                    Type = "bad_request",
                    Title = "Ada kesalahan dalam form input, silakan cek ulang isian form Anda",
                    Detail = "Ada kesalahan dalam form input, silakan cek ulang isian form Anda",
                };
                problem.Extensions.Add("errors", e.Message);
                string json = JsonSerializer.Serialize(problem);
                context.Response.ContentType = "application/json";
                await context.Response.WriteAsync(json);

            }
            catch (Exception e)
            {
                Log.Error(_factory.GetExceptionDetails(e));
                context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                ProblemDetails problem = new()
                {
                    Status = (int)HttpStatusCode.InternalServerError,
                    Type = "server_error",
                    Title = "Internal Server Error",
                    Detail = e.Message
                };
                string json = JsonSerializer.Serialize(problem);
                context.Response.ContentType = "application/json";
                await context.Response.WriteAsync(json);
                
            }
            
        }
        
    }
}
