﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace NP_API.Migrations
{
    /// <inheritdoc />
    public partial class InitialCreate : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "EmployeeMasters",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Email = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    IdentityType = table.Column<int>(type: "int", nullable: false),
                    IdentityNo = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Guid = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    CreatedAt = table.Column<long>(type: "bigint", nullable: false),
                    UpdatedAt = table.Column<long>(type: "bigint", nullable: false),
                    CreatedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UpdatedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    DeletedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DeletedAt = table.Column<long>(type: "bigint", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EmployeeMasters", x => x.Id);
                    table.UniqueConstraint("AK_EmployeeMasters_Guid", x => x.Guid);
                });

            migrationBuilder.CreateTable(
                name: "EmployeeStatus",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Guid = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    CreatedAt = table.Column<long>(type: "bigint", nullable: false),
                    UpdatedAt = table.Column<long>(type: "bigint", nullable: false),
                    CreatedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UpdatedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    DeletedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DeletedAt = table.Column<long>(type: "bigint", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EmployeeStatus", x => x.Id);
                    table.UniqueConstraint("AK_EmployeeStatus_Guid", x => x.Guid);
                });

            migrationBuilder.CreateTable(
                name: "Provinces",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Guid = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    CreatedAt = table.Column<long>(type: "bigint", nullable: false),
                    UpdatedAt = table.Column<long>(type: "bigint", nullable: false),
                    CreatedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UpdatedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    DeletedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DeletedAt = table.Column<long>(type: "bigint", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Provinces", x => x.Id);
                    table.UniqueConstraint("AK_Provinces_Guid", x => x.Guid);
                });

            migrationBuilder.CreateTable(
                name: "Cities",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ProvinceGuid = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Guid = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    CreatedAt = table.Column<long>(type: "bigint", nullable: false),
                    UpdatedAt = table.Column<long>(type: "bigint", nullable: false),
                    CreatedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UpdatedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    DeletedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DeletedAt = table.Column<long>(type: "bigint", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Cities", x => x.Id);
                    table.UniqueConstraint("AK_Cities_Guid", x => x.Guid);
                    table.ForeignKey(
                        name: "FK_Cities_Provinces_ProvinceGuid",
                        column: x => x.ProvinceGuid,
                        principalTable: "Provinces",
                        principalColumn: "Guid",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Companies",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Logo = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Email = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    PhoneNo = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Field = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Website = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Theme = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    ProvinceGuid = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    CityGuid = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Address = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    PostCode = table.Column<int>(type: "int", nullable: false),
                    Guid = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    CreatedAt = table.Column<long>(type: "bigint", nullable: false),
                    UpdatedAt = table.Column<long>(type: "bigint", nullable: false),
                    CreatedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UpdatedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    DeletedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DeletedAt = table.Column<long>(type: "bigint", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Companies", x => x.Id);
                    table.UniqueConstraint("AK_Companies_Guid", x => x.Guid);
                    table.ForeignKey(
                        name: "FK_Companies_Cities_CityGuid",
                        column: x => x.CityGuid,
                        principalTable: "Cities",
                        principalColumn: "Guid",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Companies_Provinces_ProvinceGuid",
                        column: x => x.ProvinceGuid,
                        principalTable: "Provinces",
                        principalColumn: "Guid",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CompanyGrades",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CompanyGuid = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Guid = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    CreatedAt = table.Column<long>(type: "bigint", nullable: false),
                    UpdatedAt = table.Column<long>(type: "bigint", nullable: false),
                    CreatedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UpdatedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    DeletedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DeletedAt = table.Column<long>(type: "bigint", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CompanyGrades", x => x.Id);
                    table.UniqueConstraint("AK_CompanyGrades_Guid", x => x.Guid);
                    table.ForeignKey(
                        name: "FK_CompanyGrades_Companies_CompanyGuid",
                        column: x => x.CompanyGuid,
                        principalTable: "Companies",
                        principalColumn: "Guid",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CompanyLevels",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CompanyGuid = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Guid = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    CreatedAt = table.Column<long>(type: "bigint", nullable: false),
                    UpdatedAt = table.Column<long>(type: "bigint", nullable: false),
                    CreatedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UpdatedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    DeletedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DeletedAt = table.Column<long>(type: "bigint", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CompanyLevels", x => x.Id);
                    table.UniqueConstraint("AK_CompanyLevels_Guid", x => x.Guid);
                    table.ForeignKey(
                        name: "FK_CompanyLevels_Companies_CompanyGuid",
                        column: x => x.CompanyGuid,
                        principalTable: "Companies",
                        principalColumn: "Guid",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CompanyOrganizations",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CompanyGuid = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    ParentId = table.Column<int>(type: "int", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Guid = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    CreatedAt = table.Column<long>(type: "bigint", nullable: false),
                    UpdatedAt = table.Column<long>(type: "bigint", nullable: false),
                    CreatedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UpdatedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    DeletedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DeletedAt = table.Column<long>(type: "bigint", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CompanyOrganizations", x => x.Id);
                    table.UniqueConstraint("AK_CompanyOrganizations_Guid", x => x.Guid);
                    table.ForeignKey(
                        name: "FK_CompanyOrganizations_Companies_CompanyGuid",
                        column: x => x.CompanyGuid,
                        principalTable: "Companies",
                        principalColumn: "Guid",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CompanyTitles",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CompanyGuid = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Guid = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    CreatedAt = table.Column<long>(type: "bigint", nullable: false),
                    UpdatedAt = table.Column<long>(type: "bigint", nullable: false),
                    CreatedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UpdatedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    DeletedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DeletedAt = table.Column<long>(type: "bigint", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CompanyTitles", x => x.Id);
                    table.UniqueConstraint("AK_CompanyTitles_Guid", x => x.Guid);
                    table.ForeignKey(
                        name: "FK_CompanyTitles_Companies_CompanyGuid",
                        column: x => x.CompanyGuid,
                        principalTable: "Companies",
                        principalColumn: "Guid",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    EmployeeMasterGuid = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    CompanyGuid = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    Username = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Email = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Password = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Role = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Guid = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    CreatedAt = table.Column<long>(type: "bigint", nullable: false),
                    UpdatedAt = table.Column<long>(type: "bigint", nullable: false),
                    CreatedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UpdatedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    DeletedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DeletedAt = table.Column<long>(type: "bigint", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Users_Companies_CompanyGuid",
                        column: x => x.CompanyGuid,
                        principalTable: "Companies",
                        principalColumn: "Guid",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Users_EmployeeMasters_EmployeeMasterGuid",
                        column: x => x.EmployeeMasterGuid,
                        principalTable: "EmployeeMasters",
                        principalColumn: "Guid",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CompanyClasses",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CompanyGuid = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    CompanyGradeGuid = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Guid = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    CreatedAt = table.Column<long>(type: "bigint", nullable: false),
                    UpdatedAt = table.Column<long>(type: "bigint", nullable: false),
                    CreatedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UpdatedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    DeletedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DeletedAt = table.Column<long>(type: "bigint", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CompanyClasses", x => x.Id);
                    table.UniqueConstraint("AK_CompanyClasses_Guid", x => x.Guid);
                    table.ForeignKey(
                        name: "FK_CompanyClasses_Companies_CompanyGuid",
                        column: x => x.CompanyGuid,
                        principalTable: "Companies",
                        principalColumn: "Guid",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CompanyClasses_CompanyGrades_CompanyGradeGuid",
                        column: x => x.CompanyGradeGuid,
                        principalTable: "CompanyGrades",
                        principalColumn: "Guid",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CompanyGroupEmployees",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CompanyGuid = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CompanyOrganizationGuid = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    CompanyLevelGuid = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    CompanyTitleGuid = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    CompanyGradeGuid = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    CompanyClassGuid = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    EmployeeStatusGuid = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    BasicSalary = table.Column<long>(type: "bigint", nullable: true),
                    PaidLeave = table.Column<int>(type: "int", nullable: true),
                    Guid = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    CreatedAt = table.Column<long>(type: "bigint", nullable: false),
                    UpdatedAt = table.Column<long>(type: "bigint", nullable: false),
                    CreatedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UpdatedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    DeletedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DeletedAt = table.Column<long>(type: "bigint", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CompanyGroupEmployees", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CompanyGroupEmployees_Companies_CompanyGuid",
                        column: x => x.CompanyGuid,
                        principalTable: "Companies",
                        principalColumn: "Guid",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CompanyGroupEmployees_CompanyClasses_CompanyClassGuid",
                        column: x => x.CompanyClassGuid,
                        principalTable: "CompanyClasses",
                        principalColumn: "Guid",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CompanyGroupEmployees_CompanyGrades_CompanyGradeGuid",
                        column: x => x.CompanyGradeGuid,
                        principalTable: "CompanyGrades",
                        principalColumn: "Guid",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CompanyGroupEmployees_CompanyLevels_CompanyLevelGuid",
                        column: x => x.CompanyLevelGuid,
                        principalTable: "CompanyLevels",
                        principalColumn: "Guid",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CompanyGroupEmployees_CompanyOrganizations_CompanyOrganizationGuid",
                        column: x => x.CompanyOrganizationGuid,
                        principalTable: "CompanyOrganizations",
                        principalColumn: "Guid",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CompanyGroupEmployees_CompanyTitles_CompanyTitleGuid",
                        column: x => x.CompanyTitleGuid,
                        principalTable: "CompanyTitles",
                        principalColumn: "Guid",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CompanyGroupEmployees_EmployeeStatus_EmployeeStatusGuid",
                        column: x => x.EmployeeStatusGuid,
                        principalTable: "EmployeeStatus",
                        principalColumn: "Guid",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.InsertData(
                table: "EmployeeStatus",
                columns: new[] { "Id", "CreatedAt", "CreatedBy", "DeletedAt", "DeletedBy", "Guid", "IsDeleted", "Name", "UpdatedAt", "UpdatedBy" },
                values: new object[,]
                {
                    { 1, 1698929078L, "1", null, null, new Guid("31ed7253-93fd-4d72-ab5b-23deeff8257d"), false, "Permanent", 1698929078L, "1" },
                    { 2, 1698929078L, "1", null, null, new Guid("c10f3340-5aa4-40ef-9a3a-523039ca4514"), false, "Contract", 1698929078L, "1" },
                    { 3, 1698929078L, "1", null, null, new Guid("4a2f030d-44e1-4a38-ac0b-479b51fb9488"), false, "Probation", 1698929078L, "1" },
                    { 4, 1698929078L, "1", null, null, new Guid("9f4ed2d9-bb78-4f74-83b1-4ab934ce61c7"), false, "Internship", 1698929078L, "1" }
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "CompanyGuid", "CreatedAt", "CreatedBy", "DeletedAt", "DeletedBy", "Email", "EmployeeMasterGuid", "Guid", "IsDeleted", "Name", "Password", "Role", "UpdatedAt", "UpdatedBy", "Username" },
                values: new object[] { 1, null, 1698929078L, "1", null, null, "superadmin@mail.com", null, new Guid("049aae54-f725-44d7-befd-6d2063cefb28"), false, "Super Admin", "$2a$11$GFzVjM2QgDUHeF/48aqCduI3kRzEG1d.zXgBriWSN/lmzupgPVGTa", "SUPERADMIN", 1698929078L, "1", "superadmin" });

            migrationBuilder.CreateIndex(
                name: "IX_Cities_ProvinceGuid",
                table: "Cities",
                column: "ProvinceGuid");

            migrationBuilder.CreateIndex(
                name: "IX_Companies_CityGuid",
                table: "Companies",
                column: "CityGuid");

            migrationBuilder.CreateIndex(
                name: "IX_Companies_ProvinceGuid",
                table: "Companies",
                column: "ProvinceGuid");

            migrationBuilder.CreateIndex(
                name: "IX_CompanyClasses_CompanyGradeGuid",
                table: "CompanyClasses",
                column: "CompanyGradeGuid");

            migrationBuilder.CreateIndex(
                name: "IX_CompanyClasses_CompanyGuid",
                table: "CompanyClasses",
                column: "CompanyGuid");

            migrationBuilder.CreateIndex(
                name: "IX_CompanyGrades_CompanyGuid",
                table: "CompanyGrades",
                column: "CompanyGuid");

            migrationBuilder.CreateIndex(
                name: "IX_CompanyGroupEmployees_CompanyClassGuid",
                table: "CompanyGroupEmployees",
                column: "CompanyClassGuid");

            migrationBuilder.CreateIndex(
                name: "IX_CompanyGroupEmployees_CompanyGradeGuid",
                table: "CompanyGroupEmployees",
                column: "CompanyGradeGuid");

            migrationBuilder.CreateIndex(
                name: "IX_CompanyGroupEmployees_CompanyGuid",
                table: "CompanyGroupEmployees",
                column: "CompanyGuid");

            migrationBuilder.CreateIndex(
                name: "IX_CompanyGroupEmployees_CompanyLevelGuid",
                table: "CompanyGroupEmployees",
                column: "CompanyLevelGuid");

            migrationBuilder.CreateIndex(
                name: "IX_CompanyGroupEmployees_CompanyOrganizationGuid",
                table: "CompanyGroupEmployees",
                column: "CompanyOrganizationGuid");

            migrationBuilder.CreateIndex(
                name: "IX_CompanyGroupEmployees_CompanyTitleGuid",
                table: "CompanyGroupEmployees",
                column: "CompanyTitleGuid");

            migrationBuilder.CreateIndex(
                name: "IX_CompanyGroupEmployees_EmployeeStatusGuid",
                table: "CompanyGroupEmployees",
                column: "EmployeeStatusGuid");

            migrationBuilder.CreateIndex(
                name: "IX_CompanyLevels_CompanyGuid",
                table: "CompanyLevels",
                column: "CompanyGuid");

            migrationBuilder.CreateIndex(
                name: "IX_CompanyOrganizations_CompanyGuid",
                table: "CompanyOrganizations",
                column: "CompanyGuid");

            migrationBuilder.CreateIndex(
                name: "IX_CompanyTitles_CompanyGuid",
                table: "CompanyTitles",
                column: "CompanyGuid");

            migrationBuilder.CreateIndex(
                name: "IX_EmployeeMasters_Email",
                table: "EmployeeMasters",
                column: "Email",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_EmployeeMasters_IdentityNo",
                table: "EmployeeMasters",
                column: "IdentityNo",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Users_CompanyGuid",
                table: "Users",
                column: "CompanyGuid");

            migrationBuilder.CreateIndex(
                name: "IX_Users_Email",
                table: "Users",
                column: "Email",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Users_EmployeeMasterGuid",
                table: "Users",
                column: "EmployeeMasterGuid");

            migrationBuilder.CreateIndex(
                name: "IX_Users_Username",
                table: "Users",
                column: "Username",
                unique: true);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CompanyGroupEmployees");

            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.DropTable(
                name: "CompanyClasses");

            migrationBuilder.DropTable(
                name: "CompanyLevels");

            migrationBuilder.DropTable(
                name: "CompanyOrganizations");

            migrationBuilder.DropTable(
                name: "CompanyTitles");

            migrationBuilder.DropTable(
                name: "EmployeeStatus");

            migrationBuilder.DropTable(
                name: "EmployeeMasters");

            migrationBuilder.DropTable(
                name: "CompanyGrades");

            migrationBuilder.DropTable(
                name: "Companies");

            migrationBuilder.DropTable(
                name: "Cities");

            migrationBuilder.DropTable(
                name: "Provinces");
        }
    }
}
