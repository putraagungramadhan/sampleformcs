﻿using NP_API.Models.Interface;
using System.ComponentModel.DataAnnotations;

namespace NP_API.Models
{
    public abstract class BaseModel : ISoftDelete
    {
        [Key]
        public int Id { get; set; }
        public Guid? Guid { get; set; }
        public long CreatedAt { get; set; } = (long)DateTime.UtcNow.ToUniversalTime().Subtract(DateTime.UnixEpoch).TotalSeconds;
        public long UpdatedAt { get; set; } = (long)DateTime.UtcNow.ToUniversalTime().Subtract(DateTime.UnixEpoch).TotalSeconds;
        public string? CreatedBy { get; set; } 
        public string? UpdatedBy { get; set; }
        public bool IsDeleted { get; set; } = false;
        public string? DeletedBy { get; set; }
        public long? DeletedAt { get; set; }
    }
}
