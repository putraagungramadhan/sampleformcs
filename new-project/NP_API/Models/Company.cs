﻿using System.ComponentModel.DataAnnotations.Schema;

namespace NP_API.Models
{
    public class Company : BaseModel
    {
        public Guid? CompanyGroupGuid { get; set; }
        public string? Logo { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string PhoneNo { get; set; }
        public string Field { get; set; }
        public string Website { get; set; }
        public string Theme { get; set; }
        public string? CompanyCode { get; set; }
        public Guid ProvinceGuid { get; set; }
        public Guid CityGuid { get; set; }
        public string Address { get; set; }
        public bool IsHolding { get; set; } = false;
        public int PostCode { get; set; }
        [ForeignKey("CompanyGroupGuid")]
        public CompanyGroup? CompanyGroup { get; set; }
        public ICollection<User>? Users { get; set; }
        public ICollection<CompanyGrade>? CompanyGrade { get; set; }
        public ICollection<CompanyClass>? CompanyClass { get; set; }
        public ICollection<CompanyOrganization>? CompanyOrganization { get; set; }
        public ICollection<CompanyLevel>? CompanyLevel { get; set; }
        public ICollection<CompanyGroupEmployee>? CompanyGroupEmployee { get; set; }
        public ICollection<CompanyTitle>? CompanyTitle { get; set; }
        public ICollection<News>? News { get; set; }
        public ICollection<NewsCategory>? NewsCategory { get; set; }
        public ICollection<Feedback>? Feedback { get; set; }
        public ICollection<CompanyBranch>? CompanyBranch { get; set; }
        public ICollection<CompanyDocument> CompanyDocument { get; set; }
        public ICollection<Survey> Survey { get; set; }
        public ICollection<UserPermission> UserPermission { get; set; }
    }
}
