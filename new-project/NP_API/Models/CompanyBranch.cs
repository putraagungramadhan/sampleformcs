﻿using System.ComponentModel.DataAnnotations.Schema;

namespace NP_API.Models
{
    public class CompanyBranch : BaseModel
    {
        public Guid CompanyGuid { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string PhoneNo { get; set; }
        public Guid ProvinceGuid { get; set; }
        public Guid CityGuid { get; set; }
        public string Address { get; set; }
        public int PostCode { get; set; }
        [ForeignKey("CompanyGuid")]
        public Company Company { get; set; }
    }
}
