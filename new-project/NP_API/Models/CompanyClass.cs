﻿using System.ComponentModel.DataAnnotations.Schema;

namespace NP_API.Models
{
    public class CompanyClass : BaseModel
    {
        public Guid? CompanyGuid { get; set; }
        public Guid? CompanyGradeGuid { get; set; }
        public string Name { get; set; }
        [ForeignKey("CompanyGuid")]
        public Company Company { get; set; }
        [ForeignKey("CompanyGradeGuid")]
        public CompanyGrade? CompanyGrade { get; set; }
        public ICollection<CompanyGroupEmployee>? CompanyGroupEmployee { get; set; }
    }
}
