﻿using System.ComponentModel.DataAnnotations.Schema;

namespace NP_API.Models
{
    public class CompanyDocument : BaseModel
    {
        public Guid CompanyGuid { get; set; }
        public string Name { get; set; }
        public string file { get; set; }
        [ForeignKey("CompanyGuid")]
        public Company Company { get; set; }
    }
}
