﻿using System.ComponentModel.DataAnnotations.Schema;

namespace NP_API.Models
{
    public class CompanyGrade : BaseModel
    {
        public Guid CompanyGuid { get; set; }
        public string Name { get; set; }
        [ForeignKey("CompanyGuid")]
        public Company Company { get; set; }
        public ICollection<CompanyGroupEmployee>? CompanyGroupEmployee { get; set; }
        public ICollection<CompanyClass>? CompanyClass { get; set; }
    }
}
