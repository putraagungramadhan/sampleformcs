﻿namespace NP_API.Models
{
    public class CompanyGroup : BaseModel
    {
        public string Name { get; set; }
        public string Detail { get; set; }
        public ICollection<Company>? Company { get; set; }
    }
}
