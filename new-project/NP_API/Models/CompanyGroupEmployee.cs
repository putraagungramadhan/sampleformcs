﻿using System.ComponentModel.DataAnnotations.Schema;

namespace NP_API.Models
{
    public class CompanyGroupEmployee : BaseModel
    {
        public Guid CompanyGuid { get; set; }
        public string? Name { get; set; }
        public Guid? CompanyOrganizationGuid { get; set; }
        public Guid? CompanyLevelGuid { get; set; }
        public Guid? CompanyTitleGuid { get; set; }
        public Guid? CompanyGradeGuid { get; set; }
        public Guid? CompanyClassGuid { get; set; }
        public Guid? EmployeeStatusGuid { get; set; }
        public long? BasicSalary { get; set; }
        public int? PaidLeave { get; set; }
        [ForeignKey("CompanyGuid")]
        public Company? Company { get; set; }
        [ForeignKey("CompanyOrganizationGuid")]
        public CompanyOrganization? CompanyOrganization { get; set; }
        [ForeignKey("CompanyLevelGuid")]
        public CompanyLevel? CompanyLevel { get; set; }
        [ForeignKey("CompanyTitleGuid")]
        public CompanyTitle? CompanyTitle { get; set; }
        [ForeignKey("CompanyGradeGuid")]
        public CompanyGrade? CompanyGrade { get; set; }
        [ForeignKey("CompanyClassGuid")]
        public CompanyClass? CompanyClass { get; set; }
    }
}
