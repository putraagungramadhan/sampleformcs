﻿namespace NP_API.Models.Enum
{
    public enum EnType
    {
        Integer = 1,
        String = 2,
        Bool = 3,
        Decimal = 4
    }
}
