﻿namespace NP_API.Models.Enum
{
    public enum EnNotifyType
    {
        Information = 1,
        Warning = 2,
        Error = 3
    }
}
