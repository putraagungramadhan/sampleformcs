﻿namespace NP_API.Models.Enum
{
    public enum EnRole
    {
        SUPERADMIN = 1,
        ADMIN = 2,
        EMPLOYEE = 3
    }
}
