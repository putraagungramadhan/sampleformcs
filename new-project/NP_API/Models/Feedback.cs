﻿using System.ComponentModel.DataAnnotations.Schema;

namespace NP_API.Models
{
    public class Feedback : BaseModel
    {
        public Guid? CompanyGuid { get; set; }
        public Guid UserGuid { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public bool IsRead { get; set; } = false;
        [ForeignKey("CompanyGuid")]
        public Company? Company { get; set; }
        [ForeignKey("UserGuid")]
        public User User { get; set; }
    }
}
