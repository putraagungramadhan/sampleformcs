﻿namespace NP_API.Models.Interface
{
    public interface ISoftDelete
    {
        public bool IsDeleted { get; set; }
        public string? DeletedBy { get; set; }
        public long? DeletedAt { get; set; }
        public void Undo()
        {
            IsDeleted = false;
            DeletedBy = null;
            DeletedAt = null;
        }
    }
}
