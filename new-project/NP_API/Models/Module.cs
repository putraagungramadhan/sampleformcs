﻿using System.ComponentModel.DataAnnotations;

namespace NP_API.Models
{
    public class Module
    {
        [Key]
        public int Id { get; set; }
        public Guid? Guid { get; set; }
        public string Label { get; set; }
        public string Name { get; set; }
        public string Detail { get; set; }
        public bool CanEmployee { get; set; }
        public ICollection<UserPermission> UserPermission { get; set; }
    }
}
