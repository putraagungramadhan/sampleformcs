﻿using System.ComponentModel.DataAnnotations.Schema;

namespace NP_API.Models
{
    public class News : BaseModel
    {
        public Guid? CompanyGuid { get; set; }
        public Guid NewsCategoryGuid { get; set; }
        public string Title { get; set; }
        public string? Slug { get; set; }
        public string? CoverImage { get; set; }
        public string Content { get; set; }
        public string Status { get; set; } = "publish";
        [ForeignKey("CompanyGuid")]
        public Company? Company { get; set; }
        [ForeignKey("NewsCategoryGuid")]
        public NewsCategory NewsCategory { get; set; }
    }
}
