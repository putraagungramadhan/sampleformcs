﻿using System.ComponentModel.DataAnnotations.Schema;

namespace NP_API.Models
{
    public class NewsCategory : BaseModel 
    {
        public Guid? CompanyGuid { get; set; }
        public string Name { get; set; }
        public int Status { get; set; } = 1;
        [ForeignKey("CompanyGuid")]
        public Company? Company { get; set; }
        public ICollection<News> News { get; set; }
    }
}
