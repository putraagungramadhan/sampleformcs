﻿using NP_API.Models.Enum;

namespace NP_API.Models
{
    public class SetupConfigurations : BaseModel
    {
        public string? SubTypeName { get; set; }
        public string TypeName { get; set; }
        public EnType TypeData { get; set; }
        public string TypeValue { get; set; }
        public bool IsActive { get; set; }
    }
}
