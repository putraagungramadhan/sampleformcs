﻿using System.ComponentModel.DataAnnotations.Schema;

namespace NP_API.Models
{
    public class Survey : BaseModel
    {
        public Guid CompanyGuid { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Status { get; set; } = "publish";
        [ForeignKey("CompanyGuid")]
        public Company Company { get; set; }
        public ICollection<SurveyDetail> SurveyDetail { get; set; }
        public ICollection<SurveyAnswer> SurveyAnswer { get; set; }
    }
}
