﻿using System.ComponentModel.DataAnnotations.Schema;

namespace NP_API.Models
{
    public class SurveyAnswer : BaseModel
    {
        public Guid SurveyGuid { get; set; }
        public Guid UserGuid { get; set; }
        [ForeignKey("SurveyGuid")]
        public Survey Survey { get; set; }
        [ForeignKey("UserGuid")]
        public User User { get; set; }
        public ICollection<SurveyAnswerDetail> SurveyAnswerDetail { get; set; }
    }
}
