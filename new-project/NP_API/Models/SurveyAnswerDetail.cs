﻿using System.ComponentModel.DataAnnotations.Schema;

namespace NP_API.Models
{
    public class SurveyAnswerDetail : BaseModel
    {
        public Guid SurveyAnswerGuid { get; set; }
        public Guid SurveyDetailGuid { get; set; }
        public string Answer { get; set; }
        [ForeignKey("SurveyAnswerGuid")]
        public SurveyAnswer SurveyAnswer { get; set; }
        [ForeignKey("SurveyDetailGuid")]
        public SurveyDetail SurveyDetail { get; set; }
    }
}
