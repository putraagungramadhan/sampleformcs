﻿using System.ComponentModel.DataAnnotations.Schema;

namespace NP_API.Models
{
    public class SurveyDetail : BaseModel
    {
        public Guid SurveyGuid { get; set; }
        public string Type { get; set; }
        public string Question { get; set; }
        public string? Choice { get; set; }
        public int Weight { get; set; }
        [ForeignKey("SurveyGuid")]
        public Survey Survey { get; set; }
        public ICollection<SurveyAnswerDetail> SurveyAnswerDetail { get; set; }
    }
}
