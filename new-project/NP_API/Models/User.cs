﻿using NP_API.Models.Enum;
using System.ComponentModel.DataAnnotations.Schema;

namespace NP_API.Models
{
    public class User : BaseModel
    {
        public Guid? EmployeeMasterGuid { get; set; }
        public Guid? CompanyGuid { get; set; }
        public string Username { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Role { get; set; }
        public EnRole RoleType { get; set; }
        [ForeignKey("CompanyGuid")]
        public Company? Company { get; set; }
        public ICollection<Feedback> Feedback { get; set; }
        public ICollection<SurveyAnswer>? SurveyAnswer { get; set; }
        public ICollection<UserPermission>? UserPermission { get; set; }
    }
}