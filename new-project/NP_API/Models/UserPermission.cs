﻿using System.ComponentModel.DataAnnotations.Schema;

namespace NP_API.Models
{
    public class UserPermission : BaseModel
    {
        public Guid CompanyGuid { get; set; }
        public Guid UserGuid { get; set; }
        public Guid ModuleGuid { get; set; }
        public bool CanWrite { get; set; }
        [ForeignKey("UserGuid")]
        public User User { get; set; }
        [ForeignKey("CompanyGuid")]
        public Company Company { get; set; }
        [ForeignKey("ModuleGuid")]
        public Module Module { get; set; }
    }
}
