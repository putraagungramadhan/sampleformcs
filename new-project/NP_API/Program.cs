using Microsoft.AspNetCore.Authentication.JwtBearer;
using NP_API.Config;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using NP_API.Data;
using Microsoft.EntityFrameworkCore;
using NP_API.Repository.Interface;
using NP_API.Repository;
using NP_API.Service.Interface;
using NP_API.Service;
using NP_API.Middleware;
using Serilog;
using Microsoft.OpenApi.Models;
using Microsoft.AspNetCore.Localization;
using System.Reflection;
using System.Globalization;
using Microsoft.Extensions.Options;
using NP_API.Interceptor;
using FluentValidation.AspNetCore;
using FluentValidation;
using SharpGrip.FluentValidation.AutoValidation.Mvc.Extensions;
using SharpGrip.FluentValidation.AutoValidation.Mvc.Enums;
using Microsoft.AspNetCore.HttpOverrides;
using System.Text.Json.Serialization;
using Microsoft.Extensions.FileProviders;
using Minio;
using NP_API.Common.Resolver;
using Slugify;

var builder = WebApplication.CreateBuilder(args);
// Add services to the container.

builder.Services.AddControllers().AddJsonOptions(options =>
{
    options.JsonSerializerOptions.ReferenceHandler = ReferenceHandler.IgnoreCycles;
});
builder.Services.AddAutoMapper(typeof(Program).Assembly);

builder.Services.AddFluentValidationAutoValidation(configuration =>
{
    // Disable the built-in .NET model (data annotations) validation.
    configuration.DisableBuiltInModelValidation = true;

    // Only validate controllers decorated with the `FluentValidationAutoValidation` attributeasd.
    configuration.ValidationStrategy = ValidationStrategy.Annotations;
}).AddFluentValidationClientsideAdapters();
ValidatorOptions.Global.PropertyNameResolver = CamelCasePropertyNameResolver.ResolvePropertyName;;
builder.Services.AddValidatorsFromAssembly(Assembly.GetExecutingAssembly());
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.Configure<JwtConfig>(builder.Configuration.GetSection("JwtConfig"));
builder.Services.Configure<MailConfig>(builder.Configuration.GetSection("MailSettings"));
builder.Services.Configure<S3Config>(builder.Configuration.GetSection("S3Setting"));
builder.Services.AddMinio(configureClient => configureClient
            .WithEndpoint(builder.Configuration["S3Setting:Endpoint"])
            .WithCredentials(builder.Configuration["S3Setting:AccessKey"], builder.Configuration["S3Setting:SecretAccessKey"]));
builder.Services.AddDbContextFactory<DataContext>(options => options.UseSqlServer(builder.Configuration.GetConnectionString("DefaultConnection")));
builder.Services.AddDirectoryBrowser();

builder.Services.AddAuthentication(options =>
{
    options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
    options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
    options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
})
      .AddJwtBearer(jwt =>
      {
          var key = Encoding.ASCII.GetBytes(builder.Configuration["JwtConfig:Secret"]);
          jwt.SaveToken = true;
          jwt.TokenValidationParameters = new TokenValidationParameters
          {
              ValidateIssuerSigningKey = true,
              IssuerSigningKey = new SymmetricSecurityKey(key),
              ValidateIssuer = false,
              ValidateAudience = false,
              ValidateLifetime = true,
              RequireExpirationTime = true
          };
      });
builder.Services.AddSwaggerGen(options =>
{
    options.SwaggerDoc("v1", new OpenApiInfo { Title = "NP_API", Version = "v1" });
    options.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
    {
        In = ParameterLocation.Header,
        Description = "Please enter a valid token",
        Name = "Authorization",
        Type = SecuritySchemeType.Http,
        BearerFormat = "JWT",
        Scheme = "Bearer"
    });
    options.AddSecurityRequirement(new OpenApiSecurityRequirement
    {
        {
            new OpenApiSecurityScheme
            {
                Reference = new OpenApiReference
                {
                    Type=ReferenceType.SecurityScheme,
                    Id="Bearer"
                }
            },
            new string[]{}
        }
    });
});
builder.Services.AddCors(p => p.AddPolicy("corsapp", builder =>
{
    builder.WithOrigins("*").AllowAnyMethod().AllowAnyHeader();
}));
builder.Services.AddAuthorization();
builder.Services.AddDistributedMemoryCache();
builder.Services.AddScoped<IAuthService, AuthService>();
builder.Services.AddScoped<IUserRepository, UserRepository>();
builder.Services.AddScoped<ICompanyRepository, CompanyRepository>();
builder.Services.AddScoped<ICompanyGradeRepository, CompanyGradeRepository>();
builder.Services.AddScoped<ICompanyClassRepository, CompanyClassRepository>();
builder.Services.AddScoped<ICompanyOrganizationRepository, CompanyOrganizationRepository>();
builder.Services.AddScoped<ICompanyLevelRepository, CompanyLevelRepository>();
builder.Services.AddScoped<ICompanyGroupEmployeeRepository, CompanyGroupEmployeeRepository>();
builder.Services.AddScoped<ICompanyTitleRepository, CompanyTitleRepository>();
builder.Services.AddScoped<ICompanyBranchRepository, CompanyBranchRepository>();
builder.Services.AddScoped<ICompanyDocumentRepository, CompanyDocumentRepository>();
builder.Services.AddScoped<INewsCategoryRepository, NewsCategoryRepository>();
builder.Services.AddScoped<INewsRepository, NewsRepository>();
builder.Services.AddScoped<IFeedbackRepository, FeedbackRepository>();
builder.Services.AddScoped<IModuleRepository, ModuleRepository>();
builder.Services.AddScoped<IUserPermissionRepository, UserPermissionRepository>();
builder.Services.AddScoped<ICompanyGroupRepository, CompanyGroupRepository>();
builder.Services.AddScoped<ISetupConfigurationRepository, SetupConfigurationRepository>();

builder.Services.AddScoped<ICompanyService, CompanyService>();
builder.Services.AddScoped<ICompanyGradeService, CompanyGradeService>();
builder.Services.AddScoped<ICompanyClassService, CompanyClassService>();
builder.Services.AddScoped<ICompanyOrganizationService, CompanyOrganizationService>();
builder.Services.AddScoped<ICompanyLevelService, CompanyLevelService>();
builder.Services.AddScoped<ICompanyGroupEmployeeService, CompanyGroupEmployeeService>();
builder.Services.AddScoped<ICompanyTitleService, CompanyTitleService>();
builder.Services.AddScoped<ICompanyBranchService, CompanyBranchService>();
builder.Services.AddScoped<ICompanyDocumentService, CompanyDocumentService>();
builder.Services.AddScoped<INewsCategoryService, NewsCategoryService>();
builder.Services.AddScoped<INewsService, NewsService>();
builder.Services.AddScoped<IFeedbackService, FeedbackService>();
builder.Services.AddScoped<IUserService, UserService>();
builder.Services.AddScoped<IUserPermissionService, UserPermissionService>();
builder.Services.AddScoped<ICompanyGroupService, CompanyGroupService>();
builder.Services.AddScoped<ISetupConfigurationService, SetupConfigurationService>();

builder.Services.AddSingleton<IJwtService, JwtService>();
builder.Services.AddTransient<IMailService, MailService>();
builder.Services.AddTransient<IFileUploadService, S3CompatibleService>();
builder.Services.AddSingleton<FactoryService>();
builder.Services.AddTransient<ITokenManager, TokenManager>();
builder.Services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
builder.Services.AddTransient<CheckTokenActiveMiddleware>();
builder.Services.AddTransient<GlobalExceptionHandlingMiddleware>();
builder.Services.AddSingleton<LanguageService>();
builder.Services.AddSingleton<SoftDeleteInterceptor>();
builder.Services.AddScoped<ISlugHelper, SlugHelper>();
builder.Services.AddStackExchangeRedisCache(redisOptions =>
{
    string connection = builder.Configuration
        .GetConnectionString("Redis");
    redisOptions.Configuration = connection;
});
builder.Services.AddLocalization(options => options.ResourcesPath = "Resources");
builder.Services.AddMvc().AddViewLocalization().AddDataAnnotationsLocalization(options => {
    options.DataAnnotationLocalizerProvider = (type, factory) => {
        var assemblyName = new AssemblyName(typeof(SharedResources).GetTypeInfo().Assembly.FullName);
        return factory.Create("ShareResources", assemblyName.Name);
    };
});
builder.Services.Configure<RequestLocalizationOptions>(options => {
    var supportedCultures = new List<CultureInfo> {
        new CultureInfo("id-ID"),
        new CultureInfo("en-US")
    };
    options.DefaultRequestCulture = new RequestCulture("id-ID");
    options.SupportedCultures = supportedCultures;
    options.SupportedUICultures = supportedCultures;
});

Log.Logger = new LoggerConfiguration().ReadFrom.Configuration(builder.Configuration).CreateLogger();
var app = builder.Build();
app.UseForwardedHeaders(new ForwardedHeadersOptions
{
    ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
});
// Configure the HTTP request pipeline.
//if (app.Environment.IsDevelopment())
//{
//    app.UseSwagger();
//    app.UseSwaggerUI();
//}
app.UseStaticFiles();
app.UseDirectoryBrowser(new DirectoryBrowserOptions
{
    FileProvider = new PhysicalFileProvider(
        Path.Combine(Directory.GetCurrentDirectory(), "Storage")),
    RequestPath = "/storage"
});
app.UseCors("corsapp");
app.UseSwagger();
app.UseSwaggerUI();
app.UseRequestLocalization(app.Services.GetRequiredService<IOptions<RequestLocalizationOptions>>().Value);
app.UseMiddleware<CheckTokenActiveMiddleware>();
app.UseMiddleware<GlobalExceptionHandlingMiddleware>();
app.UseHttpsRedirection();
app.UseAuthentication();
app.UseAuthorization();
app.MapControllers();

app.Run();

