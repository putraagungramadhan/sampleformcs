﻿using AutoMapper;
using NP_API.Common;
using NP_API.Data;
using NP_API.Dto.Request.CompanyBranch;
using NP_API.Models;
using NP_API.Repository.Interface;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;

namespace NP_API.Repository
{
    public class CompanyBranchRepository : ICompanyBranchRepository
    {
        private readonly DataContext context;
        private readonly IMapper _mapper;

        public CompanyBranchRepository(IDbContextFactory<DataContext> dataContextFactory, IMapper mapper)
        {
            context = dataContextFactory.CreateDbContext();
            _mapper = mapper;
        }
        public async Task<PagedList<CompanyBranchResultDto>> GetData(GetDto dto, Guid companyGuid)
        {
            List<Expression<Func<CompanyBranch, bool>>> predicates = new List<Expression<Func<CompanyBranch, bool>>>();
            Expression<Func<CompanyBranch, bool>> guidPredicate = x => x.Guid == dto.Guid;
            Expression<Func<CompanyBranch, bool>> nameTermPredicate = x => x.Name.Contains(dto.Name);
            Expression<Func<CompanyBranch, bool>> companyPredicate = x => x.CompanyGuid == companyGuid;
            if (dto.Guid != null && dto.Guid != Guid.Empty)
            {
                predicates.Add(guidPredicate);
            }
            if (dto.Name != null && dto.Name != "")
            {
                predicates.Add(nameTermPredicate);
            }
            predicates.Add(companyPredicate);
            var query = context.CompanyBranches.AsQueryable();
            return await query.PaginateAsync<CompanyBranch, CompanyBranchResultDto>(_mapper, dto.Page, dto.PageSize, predicates.ToArray(), dto.OrderBy, dto.Ascending);
        }
        public async Task<CompanyBranch> FindById(int id)
        {
            return await context.CompanyBranches.FindAsync(id);
        }
        public async Task<CompanyBranch> FindByGuid(Guid guid)
        {
            return await context.CompanyBranches.SingleOrDefaultAsync(x => x.Guid == guid);
        }
        public async Task<CompanyBranchResultDto> Save(CompanyBranch model)
        {
            context.CompanyBranches.Add(model);
            await context.SaveChangesAsync();
            CompanyBranchResultDto resultDto = _mapper.Map<CompanyBranchResultDto>(model);
            return resultDto;
        }
        public async Task<CompanyBranchResultDto> Update(CompanyBranch company)
        {
            CompanyBranch model = await FindByGuid((Guid)company.Guid);

            context.Entry(model).CurrentValues.SetValues(company);
            await context.SaveChangesAsync();
            CompanyBranchResultDto resultDto = _mapper.Map<CompanyBranchResultDto>(model);
            return resultDto;
        }
        public async Task<bool> Delete(Guid guid)
        {
            CompanyBranch model = await FindByGuid((Guid)guid);

            context.CompanyBranches.Remove(model);
            await context.SaveChangesAsync();
            return true;
        }
    }
}
