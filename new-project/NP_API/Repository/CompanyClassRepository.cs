﻿using AutoMapper;
using NP_API.Common;
using NP_API.Data;
using NP_API.Dto.Request.CompanyClass;
using NP_API.Models;
using NP_API.Repository.Interface;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq.Expressions;

namespace NP_API.Repository
{
    public class CompanyClassRepository : ICompanyClassRepository
    {
        private readonly DataContext context;
        private readonly IMapper _mapper;
        public CompanyClassRepository(IDbContextFactory<DataContext> dataContextFactory, IMapper mapper)
        {
            context = dataContextFactory.CreateDbContext();
            _mapper = mapper;
        }
        public async Task<PagedList<CompanyClassResultDto>> GetData(GetDto dto, Guid companyGuid)
        {
            List<Expression<Func<CompanyClass, bool>>> predicates = new List<Expression<Func<CompanyClass, bool>>>();
            Expression<Func<CompanyClass, bool>> searchTermPredicate = x => x.Name.Contains(dto.Name);
            Expression<Func<CompanyClass, bool>> companyPredicate = x => x.CompanyGuid == companyGuid;
            Expression<Func<CompanyClass, bool>> gradePredicate = x => x.CompanyGradeGuid == dto.CompanyGradeGuid;
            if (dto.Name != null && dto.Name != "")
            {
                predicates.Add(searchTermPredicate);
            }
            if (dto.CompanyGradeGuid != null)
            {
                predicates.Add(gradePredicate);
            }

            predicates.Add(companyPredicate);
            var query = context.CompanyClasses.AsQueryable();
            return await query.PaginateAsync<CompanyClass, CompanyClassResultDto>(_mapper, dto.Page, dto.PageSize, predicates.ToArray(), dto.OrderBy, dto.Ascending);
        }
        public async Task<CompanyClass> FindById(int id)
        {
            return await context.CompanyClasses.FindAsync(id);
        }
        public async Task<CompanyClass> FindByGuid(Guid guid)
        {
            return await context.CompanyClasses.SingleOrDefaultAsync(x => x.Guid == guid);
        }
        public async Task<CompanyClass> Save(CompanyClass classes)
        {
            context.CompanyClasses.Add(classes);
            await context.SaveChangesAsync();
            return classes;
        }
        public async Task<CompanyClass> Update(CompanyClass classes)
        {
            CompanyClass model = await context.CompanyClasses.SingleOrDefaultAsync(x => x.Guid == classes.Guid);

            context.Entry(model).CurrentValues.SetValues(classes);
            await context.SaveChangesAsync();
            return model;
        }
        public async Task<bool> Delete(Guid guid)
        {
            CompanyClass model = await context.CompanyClasses.SingleOrDefaultAsync(x => x.Guid == guid);

            context.CompanyClasses.Remove(model);
            await context.SaveChangesAsync();
            return true;
        }
    }
}
