﻿using AutoMapper;
using NP_API.Common;
using NP_API.Data;
using NP_API.Dto.Custom;
using NP_API.Dto.Request.CompanyDocument;
using NP_API.Models;
using NP_API.Repository.Interface;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;

namespace NP_API.Repository
{
    public class CompanyDocumentRepository : ICompanyDocumentRepository
    {
        private readonly DataContext context;
        private readonly IMapper _mapper;

        public CompanyDocumentRepository(IDbContextFactory<DataContext> dataContextFactory, IMapper mapper)
        {
            context = dataContextFactory.CreateDbContext();
            _mapper = mapper;
        }


        public async Task<PagedList<CompanyDocumentResultDto>> GetData(GetDto dto, Guid companyGuid)
        {
            List<Expression<Func<CompanyDocument, bool>>> predicates = new List<Expression<Func<CompanyDocument, bool>>>();
            Expression<Func<CompanyDocument, bool>> nameTermPredicate = x => x.Name == dto.Name;
            Expression<Func<CompanyDocument, bool>> companyGuidTermPredicate = x => x.CompanyGuid == companyGuid;
            

            if (dto.Name != null && dto.Name != string.Empty)
            {
                predicates.Add(nameTermPredicate);
            }
            predicates.Add(companyGuidTermPredicate);
            var query = context.CompanyDocuments
                .Include(ga => ga.Company)
                .AsQueryable();
            return await query.PaginateAsync<CompanyDocument, CompanyDocumentResultDto>(_mapper, dto.Page, dto.PageSize, predicates.ToArray(), dto.OrderBy, dto.Ascending);
        }

        public async Task<CompanyDocument> FindById(int id) {
            return await context.CompanyDocuments.FindAsync(id);
        }

        public async Task<CompanyDocument> FindByGuid(Guid guid)
        {
            return await context.CompanyDocuments.SingleOrDefaultAsync(x => x.Guid == guid);
        }

        public async Task<CompanyDocumentResultDto> Save(CompanyDocument model)
        {
            context.CompanyDocuments.Add(model);
            await context.SaveChangesAsync();
            CompanyDocumentResultDto resultDto = _mapper.Map<CompanyDocumentResultDto>(model);
            return resultDto;
        }

        public async Task<CompanyDocumentResultDto> Update(CompanyDocument companyDocument)
        {
            CompanyDocument model = await context.CompanyDocuments.SingleOrDefaultAsync(x => x.Guid == companyDocument.Guid);

            context.Entry(model).CurrentValues.SetValues(companyDocument);
            await context.SaveChangesAsync();
            CompanyDocumentResultDto resultDto = _mapper.Map<CompanyDocumentResultDto>(model);
            return resultDto;
        }

        public async Task<CompanyDocumentResultDto> UpdateFile(CompanyDocument companyDocument)
        {
            CompanyDocument model = await context.CompanyDocuments.SingleOrDefaultAsync(x => x.Guid == companyDocument.Guid);

            model.file = companyDocument.file;

            await context.SaveChangesAsync();
            CompanyDocumentResultDto resultDto = _mapper.Map<CompanyDocumentResultDto>(model);
            return resultDto;
        }

        public async Task<bool> Delete(Guid guid)
        {
            CompanyDocument model = await context.CompanyDocuments.SingleOrDefaultAsync(x => x.Guid == guid);

            context.CompanyDocuments.Remove(model);
            await context.SaveChangesAsync();
            return true;
        }

    }
}
