﻿using AutoMapper;
using NP_API.Common;
using NP_API.Data;
using NP_API.Dto;
using NP_API.Dto.Request.CompanyGrade;
using NP_API.Models;
using NP_API.Repository.Interface;
using Microsoft.EntityFrameworkCore;
using System;
using System.ComponentModel.Design;
using System.Linq.Expressions;

namespace NP_API.Repository
{
    public class CompanyGradeRepository : ICompanyGradeRepository
    {
        private readonly DataContext context;
        private readonly IMapper _mapper;
        public CompanyGradeRepository(IDbContextFactory<DataContext> dataContextFactory, IMapper mapper)
        {
            context = dataContextFactory.CreateDbContext();
            _mapper = mapper;
        }
        public async Task<PagedList<CompanyGradeResultDto>> GetData(GetDto dto, Guid companyGuid)
        {
            List<Expression<Func<CompanyGrade, bool>>> predicates = new List<Expression<Func<CompanyGrade, bool>>>();
            Expression<Func<CompanyGrade, bool>> searchTermPredicate = x => x.Name.Contains(dto.Name);
            Expression<Func<CompanyGrade, bool>> companyPredicate = x => x.CompanyGuid == companyGuid;
            if (dto.Name != null && dto.Name != "")
            {
                predicates.Add(searchTermPredicate);
            }
            
            predicates.Add(companyPredicate);

            var query = context.CompanyGrades.Include(cg => cg.Company).AsQueryable();
            return await query.PaginateAsync<CompanyGrade, CompanyGradeResultDto>(_mapper,dto.Page, dto.PageSize, predicates.ToArray(), dto.OrderBy, dto.Ascending);
        }
        public async Task<CompanyGrade> FindById(int id)
        {
            return await context.CompanyGrades.FindAsync(id);
        }
        public async Task<CompanyGrade> FindByGuid(Guid guid)
        {
            return await context.CompanyGrades.SingleOrDefaultAsync(x => x.Guid == guid);
        }
        public async Task<CompanyGrade> Save(CompanyGrade grade)
        {
            context.CompanyGrades.Add(grade);
            await context.SaveChangesAsync();
            return grade;
        }
        public async Task<CompanyGrade> Update(CompanyGrade grade)
        {
            CompanyGrade model = await context.CompanyGrades.SingleOrDefaultAsync(x => x.Guid == grade.Guid);

            context.Entry(model).CurrentValues.SetValues(grade);
            await context.SaveChangesAsync();
            return model;
        }
        public async Task<bool> Delete(Guid guid)
        {
            CompanyGrade model = await context.CompanyGrades.SingleOrDefaultAsync(x => x.Guid == guid);

            context.CompanyGrades.Remove(model);
            await context.SaveChangesAsync();
            return true;
        }
    }
}
