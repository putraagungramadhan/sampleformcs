﻿using AutoMapper;
using NP_API.Common;
using NP_API.Data;
using NP_API.Dto.Request.CompanyGroupEmployee;
using NP_API.Models;
using NP_API.Repository.Interface;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;

namespace NP_API.Repository
{
    public class CompanyGroupEmployeeRepository : ICompanyGroupEmployeeRepository
    {
        private readonly DataContext context;
        private readonly IMapper _mapper;
        public CompanyGroupEmployeeRepository(IDbContextFactory<DataContext> dataContextFactory, IMapper mapper)
        {
            context = dataContextFactory.CreateDbContext();
            _mapper = mapper;
        }
        public async Task<PagedList<CompanyGroupEmployeeResultDto>> GetData(GetDto dto, Guid companyId)
        {
            List<Expression<Func<CompanyGroupEmployee, bool>>> predicates = new List<Expression<Func<CompanyGroupEmployee, bool>>>();
            Expression<Func<CompanyGroupEmployee, bool>> searchTermPredicate = x => x.Name.Contains(dto.Name);
            Expression<Func<CompanyGroupEmployee, bool>> companyPredicate = x => x.CompanyGuid == companyId;
            predicates.Add(searchTermPredicate);
            predicates.Add(companyPredicate);
            if (dto.Id != 0)
            {
                predicates.Add(x => x.Id == dto.Id);
            }

            var query = context.CompanyGroupEmployees
                .Include(cgo => cgo.CompanyOrganization)
                .Include(cl => cl.CompanyLevel)
                .Include(cg => cg.CompanyTitle)
                .Include(cc => cc.CompanyClass)
                .Include(cg => cg.CompanyGrade)
                .AsQueryable();
            return await query.PaginateAsync<CompanyGroupEmployee, CompanyGroupEmployeeResultDto>(_mapper, dto.Page, dto.PageSize, predicates.ToArray(), dto.OrderBy, dto.Ascending);
        }
        public async Task<CompanyGroupEmployee> FindById(int id)
        {
            return await context.CompanyGroupEmployees.FindAsync(id);
        }
        public async Task<CompanyGroupEmployee> FindByGuid(Guid guid)
        {
            return await context.CompanyGroupEmployees.SingleOrDefaultAsync(x => x.Guid == guid);
        }
        public async Task<CompanyGroupEmployee> Save(CompanyGroupEmployee model)
        {
            context.CompanyGroupEmployees.Add(model);
            await context.SaveChangesAsync();
            return model;
        }
        public async Task<CompanyGroupEmployee> Update(CompanyGroupEmployee group)
        {
            CompanyGroupEmployee model = await context.CompanyGroupEmployees.SingleOrDefaultAsync(x => x.Guid == group.Guid);

            context.Entry(model).CurrentValues.SetValues(group);
            await context.SaveChangesAsync();
            return model;
        }
        public async Task<bool> Delete(Guid guid)
        {
            CompanyGroupEmployee model = await context.CompanyGroupEmployees.SingleOrDefaultAsync(x => x.Guid == guid);

            context.CompanyGroupEmployees.Remove(model);
            await context.SaveChangesAsync();
            return true;
        }
    }
}
