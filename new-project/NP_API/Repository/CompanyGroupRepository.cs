﻿using AutoMapper;
using NP_API.Common;
using NP_API.Data;
using NP_API.Dto.Request.CompanyGroup;
using NP_API.Models;
using NP_API.Repository.Interface;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;

namespace NP_API.Repository
{
    public class CompanyGroupRepository : ICompanyGroupRepository
    {
        private readonly DataContext context;
        private readonly IMapper _mapper;
        public CompanyGroupRepository(IDbContextFactory<DataContext> dataContextFactory, IMapper mapper)
        {
            context = dataContextFactory.CreateDbContext();
            _mapper = mapper;
        }
        public async Task<PagedList<CompanyGroupResultDto>> GetData(GetDto dto)
        {
            List<Expression<Func<CompanyGroup, bool>>> predicates = new List<Expression<Func<CompanyGroup, bool>>>();
            Expression<Func<CompanyGroup, bool>> searchTermPredicate = x => x.Name.Contains(dto.Name);
            Expression<Func<CompanyGroup, bool>> guidPredicate = x => x.Guid == dto.Guid;

            if (dto.Guid != null && dto.Guid != Guid.Empty)
            {
                predicates.Add(guidPredicate);
            }
            if (!string.IsNullOrEmpty(dto.Name))
            {
                predicates.Add(searchTermPredicate);
            }

            var query = context.CompanyGroups
                .AsQueryable();
            return await query.PaginateAsync<CompanyGroup, CompanyGroupResultDto>(_mapper, dto.Page, dto.PageSize, predicates.ToArray(), dto.OrderBy, dto.Ascending);
        }
        public async Task<CompanyGroup> FindByGuid(Guid guid)
        {
            return await context.CompanyGroups.SingleOrDefaultAsync(x => x.Guid == guid);
        }
        public async Task<CompanyGroupResultDto> Save(CompanyGroup model)
        {
            context.CompanyGroups.Add(model);
            await context.SaveChangesAsync();
            CompanyGroupResultDto resultDto = _mapper.Map<CompanyGroupResultDto>(model);
            return resultDto;
        }
        public async Task<CompanyGroupResultDto> Update(CompanyGroup group)
        {
            CompanyGroup model = await FindByGuid((Guid)group.Guid);

            context.Entry(model).CurrentValues.SetValues(group);
            await context.SaveChangesAsync();
            CompanyGroupResultDto resultDto = _mapper.Map<CompanyGroupResultDto>(model);
            return resultDto;
        }
        public async Task<bool> Delete(Guid guid)
        {
            CompanyGroup model = await FindByGuid((Guid)guid);

            context.CompanyGroups.Remove(model);
            await context.SaveChangesAsync();
            return true;
        }
    }
}
