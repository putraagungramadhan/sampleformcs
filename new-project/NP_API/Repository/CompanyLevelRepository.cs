﻿using AutoMapper;
using NP_API.Common;
using NP_API.Data;
using NP_API.Dto.Request.CompanyLevel;
using NP_API.Models;
using NP_API.Repository.Interface;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq.Expressions;

namespace NP_API.Repository
{
    public class CompanyLevelRepository : ICompanyLevelRepository
    {
        private readonly DataContext context;
        private readonly IMapper _mapper;
        public CompanyLevelRepository(IDbContextFactory<DataContext> dataContextFactory, IMapper mapper)
        {
            context = dataContextFactory.CreateDbContext();
            _mapper = mapper;
        }
        public async Task<PagedList<CompanyLevelResultDto>> GetData(GetDto dto, Guid companyGuid)
        {
            List<Expression<Func<CompanyLevel, bool>>> predicates = new List<Expression<Func<CompanyLevel, bool>>>();
            Expression<Func<CompanyLevel, bool>> searchTermPredicate = x => x.Name.Contains(dto.Name);
            Expression<Func<CompanyLevel, bool>> companyPredicate = x => x.CompanyGuid == companyGuid;
            if (dto.Name != null && dto.Name != "")
            {
                predicates.Add(searchTermPredicate);
            }

            predicates.Add(companyPredicate);
            var query = context.CompanyLevels.AsQueryable();
            return await query.PaginateAsync<CompanyLevel, CompanyLevelResultDto>(_mapper, dto.Page, dto.PageSize, predicates.ToArray(), dto.OrderBy, dto.Ascending);
        }
        public async Task<CompanyLevel> FindById(int id)
        {
            return await context.CompanyLevels.FindAsync(id);
        }
        public async Task<CompanyLevel> FindByGuid(Guid guid)
        {
            return await context.CompanyLevels.SingleOrDefaultAsync(x => x.Guid == guid);
        }
        public async Task<CompanyLevel> Save(CompanyLevel model)
        {
            context.CompanyLevels.Add(model);
            await context.SaveChangesAsync();
            return model;
        }
        public async Task<CompanyLevel> Update(CompanyLevel level)
        {
            CompanyLevel model = await context.CompanyLevels.SingleOrDefaultAsync(x => x.Guid == level.Guid);

            context.Entry(model).CurrentValues.SetValues(level);
            await context.SaveChangesAsync();
            return model;
        }
        public async Task<bool> Delete(Guid guid)
        {
            CompanyLevel model = await context.CompanyLevels.SingleOrDefaultAsync(x => x.Guid == guid);

            context.CompanyLevels.Remove(model);
            await context.SaveChangesAsync();
            return true;
        }
    }
}
