﻿using AutoMapper;
using NP_API.Common;
using NP_API.Data;
using NP_API.Dto.Request.CompanyOrganization;
using NP_API.Models;
using NP_API.Repository.Interface;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.ComponentModel.Design;
using System.Linq.Expressions;

namespace NP_API.Repository
{
    public class CompanyOrganizationRepository : ICompanyOrganizationRepository
    {
        private readonly DataContext context;
        private readonly IMapper _mapper;
        public CompanyOrganizationRepository(IDbContextFactory<DataContext> dataContextFactory, IMapper mapper)
        {
            context = dataContextFactory.CreateDbContext();
            _mapper = mapper;
        }
        public async Task<PagedList<CompanyOrganizationResultDto>> GetData(GetDto dto, Guid companyGuid)
        {
            List<Expression<Func<CompanyOrganization, bool>>> predicates = new List<Expression<Func<CompanyOrganization, bool>>>();
            Expression<Func<CompanyOrganization, bool>> searchTermPredicate = x => x.Name.Contains(dto.Name);
            Expression<Func<CompanyOrganization, bool>> companyPredicate = x => x.CompanyGuid == companyGuid;
            if (dto.Name != null && dto.Name != "")
            {
                predicates.Add(searchTermPredicate);
            }

            predicates.Add(companyPredicate);
            var query = context.CompanyOrganizations.Include(cg => cg.Company).AsQueryable();
            return await query.PaginateAsync<CompanyOrganization, CompanyOrganizationResultDto>(_mapper, dto.Page, dto.PageSize, predicates.ToArray(), dto.OrderBy, dto.Ascending);
        }
        public async Task<CompanyOrganization> FindById(int id)
        {
            return await context.CompanyOrganizations.FindAsync(id);
        }
        public async Task<CompanyOrganization> FindByGuid(Guid guid)
        {
            return await context.CompanyOrganizations.SingleOrDefaultAsync(x => x.Guid == guid);
        }
        public async Task<JsonResult> GetStructure(Guid companyGuid, int parentId = 0)
        {

            var nodes = await context.CompanyOrganizations.Where(x => x.CompanyGuid == companyGuid).ToListAsync();
            var result = BuildHierarchy(nodes, parentId);
            return new JsonResult(result);
        }
        public async Task<CompanyOrganization> Save(CompanyOrganization model)
        {
            context.CompanyOrganizations.Add(model);
            await context.SaveChangesAsync();
            return model;
        }
        public async Task<CompanyOrganization> Update(CompanyOrganization orgs)
        {
            CompanyOrganization model = await context.CompanyOrganizations.SingleOrDefaultAsync(x => x.Guid == orgs.Guid);

            context.Entry(model).CurrentValues.SetValues(orgs);
            await context.SaveChangesAsync();
            return model;
        }
        public async Task<bool> Delete(Guid guid)
        {
            CompanyOrganization model = await context.CompanyOrganizations.SingleOrDefaultAsync(x => x.Guid == guid);

            context.CompanyOrganizations.Remove(model);
            await context.SaveChangesAsync();
            return true;
        }
        public static List<object> BuildHierarchy(List<CompanyOrganization> nodes, int parentId = 0)
        {
            var children = nodes
                .Where(node => node.ParentId == parentId)
                .Select(node => new
                {
                    Id = node.Id,
                    Guid = node.Guid,
                    Name = node.Name,
                    Children = BuildHierarchy(nodes, node.Id)
                })
                .ToList<object>();

            return children;
        }
    }
}
