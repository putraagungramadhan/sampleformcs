﻿using NP_API.Data;
using NP_API.Models;
using NP_API.Repository.Interface;
using Microsoft.EntityFrameworkCore;
using Org.BouncyCastle.Math.EC;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory.Database;
using System;
using AutoMapper;
using static StackExchange.Redis.Role;
using NP_API.Common;
using System.Linq.Expressions;
using NP_API.Dto.Request.Company;

namespace NP_API.Repository
{
    public class CompanyRepository : ICompanyRepository
    {
        private readonly DataContext context;
        private readonly IMapper _mapper;

        public CompanyRepository(IDbContextFactory<DataContext> dataContextFactory, IMapper mapper)
        {
            context = dataContextFactory.CreateDbContext();
            _mapper = mapper;
        }
        public async Task<Company> FindById(int id)
        {
            return await context.Companies.FindAsync(id);
        }

        public async Task<Company> FindByGuid(Guid guid)
        {
            return await context.Companies.SingleOrDefaultAsync(x => x.Guid == guid);
        }
        public async Task<bool> IsAnyChecklist()
        {
            Company model = await context.Companies.Where(x => x.IsHolding == true).FirstOrDefaultAsync();
            if (model == null)
            {
                return false;
            }
            return true;
        }
        public async Task<PagedList<CompanyResultDto>> GetData(GetDto dto)
        {
            List<Expression<Func<Company, bool>>> predicates = new List<Expression<Func<Company, bool>>>();
            Expression<Func<Company, bool>> guidPredicate = x => x.Guid == dto.Guid;
            Expression<Func<Company, bool>> namePredicate = x => x.Name.Contains(dto.Name);
            if (dto.Guid != null && dto.Guid != Guid.Empty)
            {
                predicates.Add(guidPredicate);
            }
            if (dto.Name != null && dto.Name != "")
            {
                predicates.Add(namePredicate);
            }
            var query = context.Companies
                .Include(x => x.CompanyGroup)
                .AsQueryable();
            return await query.PaginateAsync<Company, CompanyResultDto>(_mapper, dto.Page, dto.PageSize, predicates.ToArray(), dto.OrderBy, dto.Ascending);
        }
        public async Task<Company> Save(Company company)
        {
            context.Companies.Add(company);
            await context.SaveChangesAsync();
            return company;
        }
      
        public async Task<CompanyResultDto> Update(Company company)
        {
            Company model = await context.Companies.SingleOrDefaultAsync(x => x.Guid == company.Guid);

            context.Entry(model).CurrentValues.SetValues(company);

            await context.SaveChangesAsync();
            CompanyResultDto resultDto = _mapper.Map<CompanyResultDto>(model);

            return resultDto;
        }
        public async Task<CompanyResultDto> UpdateLogo(Company company)
        {
            Company model = await context.Companies.FindAsync(company.Id);

            model.Logo = company.Logo;

            await context.SaveChangesAsync();
            CompanyResultDto resultDto = _mapper.Map<CompanyResultDto>(model);
            return resultDto;
        }
        public async Task<bool> Delete(int id)
        {
            Company model = await context.Companies.FindAsync(id);

            context.Companies.Remove(model);
            await context.SaveChangesAsync();
            return true;
        }
    }
}
