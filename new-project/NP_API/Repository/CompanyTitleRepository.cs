﻿using AutoMapper;
using NP_API.Common;
using NP_API.Data;
using NP_API.Dto.Request.CompanyTitle;
using NP_API.Models;
using NP_API.Repository.Interface;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;

namespace NP_API.Repository
{
    public class CompanyTitleRepository : ICompanyTitleRepository
    {
        private readonly DataContext context;
        private readonly IMapper _mapper;
        public CompanyTitleRepository(IDbContextFactory<DataContext> dataContextFactory, IMapper mapper)
        {
            context = dataContextFactory.CreateDbContext();
            _mapper = mapper;
        }
        public async Task<PagedList<CompanyTitleResultDto>> GetData(GetDto dto, Guid companyGuid)
        {
            List<Expression<Func<CompanyTitle, bool>>> predicates = new List<Expression<Func<CompanyTitle, bool>>>();
            Expression<Func<CompanyTitle, bool>> searchTermPredicate = x => x.Name.Contains(dto.Name);
            Expression<Func<CompanyTitle, bool>> companyPredicate = x => x.CompanyGuid == companyGuid;
            if (dto.Name != null && dto.Name != "")
            {
                predicates.Add(searchTermPredicate);
            }

            predicates.Add(companyPredicate);
            var query = context.CompanyTitles.AsQueryable();
            return await query.PaginateAsync<CompanyTitle, CompanyTitleResultDto>(_mapper, dto.Page, dto.PageSize, predicates.ToArray(), dto.OrderBy, dto.Ascending);
        }
        public async Task<CompanyTitle> FindById(int id)
        {
            return await context.CompanyTitles.FindAsync(id);
        }
        public async Task<CompanyTitle> FindByGuid(Guid guid)
        {
            return await context.CompanyTitles.SingleOrDefaultAsync(x => x.Guid == guid);
        }
        public async Task<CompanyTitle> Save(CompanyTitle model)
        {
            context.CompanyTitles.Add(model);
            await context.SaveChangesAsync();
            return model;
        }
        public async Task<CompanyTitle> Update(CompanyTitle title)
        {
            CompanyTitle model = await context.CompanyTitles.SingleOrDefaultAsync(x => x.Guid == title.Guid);

            context.Entry(model).CurrentValues.SetValues(title);
            await context.SaveChangesAsync();
            return model;
        }
        public async Task<bool> Delete(Guid guid)
        {
            CompanyTitle model = await context.CompanyTitles.SingleOrDefaultAsync(x => x.Guid == guid);

            context.CompanyTitles.Remove(model);
            await context.SaveChangesAsync();
            return true;
        }
    }
}
