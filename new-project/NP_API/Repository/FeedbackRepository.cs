﻿using AutoMapper;
using NP_API.Common;
using NP_API.Data;
using NP_API.Dto.Request.Feedback;
using NP_API.Models;
using NP_API.Repository.Interface;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;

namespace NP_API.Repository
{
    public class FeedbackRepository : IFeedbackRepository
    {
        private readonly DataContext context;
        private readonly IMapper _mapper;
        public FeedbackRepository(IDbContextFactory<DataContext> dataContextFactory, IMapper mapper)
        {
            context = dataContextFactory.CreateDbContext();
            _mapper = mapper;
        }
        public async Task<PagedList<FeedbackResultDto>> GetData(GetDto dto)
        {
            List<Expression<Func<Feedback, bool>>> predicates = new List<Expression<Func<Feedback, bool>>>();
            Expression<Func<Feedback, bool>> guidPredicate = x => x.Guid == dto.Guid;
            Expression<Func<Feedback, bool>> titlePredicate = x => x.Title == dto.Title;

            if (dto.Guid != null && dto.Guid != Guid.Empty)
            {
                predicates.Add(guidPredicate);
            }
            if (dto.Title != null && dto.Title != "")
            {
                predicates.Add(titlePredicate);
            }

            var query = context.Feedbacks
                .Include(cgo => cgo.User)
                .AsQueryable();
            return await query.PaginateAsync<Feedback, FeedbackResultDto>(_mapper, dto.Page, dto.PageSize, predicates.ToArray(), dto.OrderBy, dto.Ascending);
        }

        public async Task<Feedback> FindById(int id)
        {
            return await context.Feedbacks.FindAsync(id);
        }
        public async Task<Feedback> FindByGuid(Guid guid)
        {
            return await context.Feedbacks.SingleOrDefaultAsync(x => x.Guid == guid);
        }

        public async Task<FeedbackResultDto> Save(Feedback model)
        {
            context.Feedbacks.Add(model);
            await context.SaveChangesAsync();
            FeedbackResultDto resultDto = _mapper.Map<FeedbackResultDto>(model);
            return resultDto;
        }
        public async Task<FeedbackResultDto> Update(Feedback feedback)
        {
            Feedback model = await context.Feedbacks.SingleOrDefaultAsync(x => x.Guid == feedback.Guid);

            context.Entry(model).CurrentValues.SetValues(feedback);
            await context.SaveChangesAsync();
            FeedbackResultDto resultDto = _mapper.Map<FeedbackResultDto>(model);
            return resultDto;
        }
        public async Task<bool> Delete(Guid guid)
        {
            Feedback model = await context.Feedbacks.SingleOrDefaultAsync(x => x.Guid == guid);
            context.Feedbacks.Remove(model);
            await context.SaveChangesAsync();
            return true;
        }


    }
}
