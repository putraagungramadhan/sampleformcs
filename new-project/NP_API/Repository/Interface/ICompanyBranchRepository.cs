﻿using NP_API.Common;
using NP_API.Dto.Request.CompanyBranch;
using NP_API.Models;

namespace NP_API.Repository.Interface
{
    public interface ICompanyBranchRepository
    {
        public Task<CompanyBranch> FindById(int id);
        public Task<CompanyBranch> FindByGuid(Guid guid);
        public Task<PagedList<CompanyBranchResultDto>> GetData(GetDto dto, Guid companyGuid);
        public Task<CompanyBranchResultDto> Save(CompanyBranch company);
        public Task<CompanyBranchResultDto> Update(CompanyBranch company);
        public Task<bool> Delete(Guid guid);
    }
}
