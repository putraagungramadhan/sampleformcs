﻿using NP_API.Common;
using NP_API.Dto.Request.CompanyClass;
using NP_API.Models;

namespace NP_API.Repository.Interface
{
    public interface ICompanyClassRepository
    {
        public Task<PagedList<CompanyClassResultDto>> GetData(GetDto dto, Guid companyId);
        public Task<CompanyClass> FindById(int id);
        public Task<CompanyClass> FindByGuid(Guid guid);
        public Task<CompanyClass> Save(CompanyClass company);
        public Task<CompanyClass> Update(CompanyClass company);
        public Task<bool> Delete(Guid Guid);
    }
}
