﻿using NP_API.Common;
using NP_API.Dto.Custom;
using NP_API.Dto.Request.CompanyDocument;
using NP_API.Models;

namespace NP_API.Repository.Interface
{
    public interface ICompanyDocumentRepository
    {
        public Task<PagedList<CompanyDocumentResultDto>> GetData(GetDto dto, Guid companyId);
        public Task<CompanyDocument> FindById(int id);
        public Task<CompanyDocument> FindByGuid(Guid guid);
        public Task<CompanyDocumentResultDto> Save(CompanyDocument companyDocument);
        public Task<CompanyDocumentResultDto> Update(CompanyDocument companyDocument);
        public Task<CompanyDocumentResultDto> UpdateFile(CompanyDocument companyDocument);
        public Task<bool> Delete(Guid guid);
    }
}
