﻿using NP_API.Common;
using NP_API.Dto.Request.CompanyGrade;
using NP_API.Models;

namespace NP_API.Repository.Interface
{
    public interface ICompanyGradeRepository
    {
        public Task<PagedList<CompanyGradeResultDto>> GetData(GetDto dto, Guid companyGuid);
        public Task<CompanyGrade> FindById(int id);
        public Task<CompanyGrade> FindByGuid(Guid guid);
        public Task<CompanyGrade> Save(CompanyGrade company);
        public Task<CompanyGrade> Update(CompanyGrade company);
        public Task<bool> Delete(Guid guid);
    }
}
