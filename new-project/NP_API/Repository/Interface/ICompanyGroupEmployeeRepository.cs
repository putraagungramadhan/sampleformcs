﻿using NP_API.Common;
using NP_API.Dto.Request.CompanyGroupEmployee;
using NP_API.Models;

namespace NP_API.Repository.Interface
{
    public interface ICompanyGroupEmployeeRepository
    {
        public Task<PagedList<CompanyGroupEmployeeResultDto>> GetData(GetDto dto, Guid companyGuid);
        public Task<CompanyGroupEmployee> FindById(int id);
        public Task<CompanyGroupEmployee> FindByGuid(Guid guid);
        public Task<CompanyGroupEmployee> Save(CompanyGroupEmployee company);
        public Task<CompanyGroupEmployee> Update(CompanyGroupEmployee company);
        public Task<bool> Delete(Guid guid);
    }
}
