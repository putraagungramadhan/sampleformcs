﻿using NP_API.Common;
using NP_API.Dto.Request.CompanyGroup;
using NP_API.Models;

namespace NP_API.Repository.Interface
{
    public interface ICompanyGroupRepository
    {
        public Task<PagedList<CompanyGroupResultDto>> GetData(GetDto dto);
        public Task<CompanyGroup> FindByGuid(Guid guid);
        public Task<CompanyGroupResultDto> Save(CompanyGroup group);
        public Task<CompanyGroupResultDto> Update(CompanyGroup group);
        public Task<bool> Delete(Guid guid);
    }
}
