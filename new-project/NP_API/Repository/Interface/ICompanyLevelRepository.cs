﻿using NP_API.Common;
using NP_API.Dto.Request.CompanyLevel;
using NP_API.Models;

namespace NP_API.Repository.Interface
{
    public interface ICompanyLevelRepository
    {
        public Task<PagedList<CompanyLevelResultDto>> GetData(GetDto dto, Guid companyGuid);
        public Task<CompanyLevel> FindById(int id);
        public Task<CompanyLevel> FindByGuid(Guid guid);
        public Task<CompanyLevel> Save(CompanyLevel company);
        public Task<CompanyLevel> Update(CompanyLevel company);
        public Task<bool> Delete(Guid guid);
    }
}
