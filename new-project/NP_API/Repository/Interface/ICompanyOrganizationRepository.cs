﻿using NP_API.Common;
using NP_API.Dto.Request.CompanyOrganization;
using NP_API.Models;
using Microsoft.AspNetCore.Mvc;

namespace NP_API.Repository.Interface
{
    public interface ICompanyOrganizationRepository
    {
        public Task<PagedList<CompanyOrganizationResultDto>> GetData(GetDto dto, Guid companyGuid);
        public Task<CompanyOrganization> FindById(int id);
        public Task<CompanyOrganization> FindByGuid(Guid guid);
        public Task<JsonResult> GetStructure(Guid companyGuid, int parentId);
        public Task<CompanyOrganization> Save(CompanyOrganization company);
        public Task<CompanyOrganization> Update(CompanyOrganization company);
        public Task<bool> Delete(Guid guid);
    }
}
