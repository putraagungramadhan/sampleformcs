﻿using NP_API.Common;
using NP_API.Dto.Request.Company;

using NP_API.Models;

namespace NP_API.Repository.Interface
{
    public interface ICompanyRepository
    {
        public Task<Company> FindById(int id);
        public Task<Company> FindByGuid(Guid guid);
        public Task<bool> IsAnyChecklist();
        public Task<PagedList<CompanyResultDto>> GetData(GetDto dto);
        public Task<Company> Save(Company company);
        public Task<CompanyResultDto> Update(Company company);
        public Task<CompanyResultDto> UpdateLogo(Company company);
        public Task<bool> Delete(int id);
    }
}
