﻿using NP_API.Common;
using NP_API.Dto.Request.CompanyTitle;
using NP_API.Models;

namespace NP_API.Repository.Interface
{
    public interface ICompanyTitleRepository
    {
        public Task<PagedList<CompanyTitleResultDto>> GetData(GetDto dto, Guid companyGuid);
        public Task<CompanyTitle> FindById(int id);
        public Task<CompanyTitle> FindByGuid(Guid guid);
        public Task<CompanyTitle> Save(CompanyTitle company);
        public Task<CompanyTitle> Update(CompanyTitle company);
        public Task<bool> Delete(Guid guid);
    }
}
