﻿using NP_API.Common;
using NP_API.Dto.Request.Feedback;
using NP_API.Models;

namespace NP_API.Repository.Interface
{
    public interface IFeedbackRepository
    {
        public Task<Feedback> FindById(int Id);
        public Task<Feedback> FindByGuid(Guid Guid);
        public Task<FeedbackResultDto> Save(Feedback feedback);
        public Task<PagedList<FeedbackResultDto>> GetData(GetDto dto);
        public Task<FeedbackResultDto> Update(Feedback feedback);
        public Task<bool> Delete(Guid guid);
    }
}
