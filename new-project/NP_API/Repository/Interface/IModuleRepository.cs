﻿using NP_API.Models;

namespace NP_API.Repository.Interface
{
    public interface IModuleRepository
    {
        public Task<List<Module>> GetAll(bool canEmployee);
    }
}
