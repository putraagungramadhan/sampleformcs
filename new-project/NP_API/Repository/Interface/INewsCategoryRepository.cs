﻿using NP_API.Common;
using NP_API.Dto.Request.NewsCategory;
using NP_API.Models;

namespace NP_API.Repository.Interface 
{
    public interface INewsCategoryRepository
    {
        public Task<NewsCategory> FindById(int Id);
        public Task<NewsCategory> FindByGuid(Guid Guid);
        public Task<NewsCategoryResultDto> Save(NewsCategory newsCategory);
        public Task<PagedList<NewsCategoryResultDto>> GetData(GetDto dto);
        public Task<NewsCategoryResultDto> Update(NewsCategory newsCategory);
        public Task<bool> Delete(Guid guid);
    }
}


