﻿using NP_API.Common;
using NP_API.Dto.Request.News;
using NP_API.Models;

namespace NP_API.Repository.Interface
{
    public interface INewsRepository
    {
        public Task<News> FindById(int Id);
        public Task<News> FindByGuid(Guid Guid);
        public Task<NewsResultDto> Save(News news);
        public Task<PagedList<NewsResultDto>> GetData(GetDto dto);
        public Task<NewsResultDto> Update(News news);
        public Task<NewsResultDto> UpdateCoverImage(News news);
        public Task<bool> Delete(Guid guid);
    }
}
