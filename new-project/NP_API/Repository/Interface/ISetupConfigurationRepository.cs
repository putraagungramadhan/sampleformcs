﻿using NP_API.Common;
using NP_API.Dto.Request.SetupConfigurations;
using NP_API.Models;

namespace NP_API.Repository.Interface
{
    public interface ISetupConfigurationRepository
    {
        public Task<SetupConfigurations> FindById(int id);
        public Task<SetupConfigurations> FindByGuid(Guid guid);
        //public Task<PagedList<SetupConfigurationResultDto>> FindAll(GetDto dto);
        public Task<PagedList<SetupConfigurationResultDto>> GetData(GetDto dto);
        public Task<SetupConfigurationResultDto> Save(SetupConfigurations model);
        public Task<SetupConfigurationResultDto> Update(SetupConfigurations param);
        public Task<bool> Delete(Guid guid);
    }
}
