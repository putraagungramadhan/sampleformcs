﻿using NP_API.Common;
using NP_API.Dto.Request.UserPermission;
using NP_API.Models;

namespace NP_API.Repository.Interface
{
    public interface IUserPermissionRepository
    {
        public Task<UserPermission> FindByGuid(Guid Guid);
        public Task<PagedList<UserPermissionResultDto>> GetData(GetDto dto, Guid companyGuid);
        public Task<Object> GetByUser(Guid UserGuid);
        public Task<UserPermissionResultDto> Save(UserPermission permission);
        public Task<UserPermissionResultDto> Update(UserPermission permission);
        public Task<bool> Delete(Guid guid);
    }
}
