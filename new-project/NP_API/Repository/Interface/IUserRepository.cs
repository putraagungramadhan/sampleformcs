﻿using NP_API.Common;
using NP_API.Dto.Request.User;
using NP_API.Models;

namespace NP_API.Repository.Interface
{
    public interface IUserRepository
    {
        public Task<PagedList<UserResultDto>> GetData(GetDto dto, Guid companyGuid);
        public Task<PagedList<UserResultDto>> GetDataByRole(GetDto dto);
        public Task<User> Save(User user);
        public Task<User> SaveOnly(User user);
        public Task<User> FindByEmail(string email);
        public Task<User> FindByGuid(Guid guid);
        public Task<User> UpdatePassword(User user);
        public Task<User> UpdateEmail(User user);
        public Task<bool> Delete(Guid guid);
        public Task<bool> DeleteByWorker(Guid guid);
    }
}
