﻿using AutoMapper;
using NP_API.Data;
using NP_API.Models;
using NP_API.Repository.Interface;
using Microsoft.EntityFrameworkCore;

namespace NP_API.Repository
{
    public class ModuleRepository : IModuleRepository
    {
        private readonly DataContext context;
        private readonly IMapper _mapper;

        public ModuleRepository(IDbContextFactory<DataContext> dataContextFactory, IMapper mapper)
        {
            context = dataContextFactory.CreateDbContext();
            _mapper = mapper;
        }
        public async Task<List<Module>> GetAll(bool canEmployee)
        {
            return await context.Modules.Where(x => x.CanEmployee == canEmployee).ToListAsync();
        }
    }
}
