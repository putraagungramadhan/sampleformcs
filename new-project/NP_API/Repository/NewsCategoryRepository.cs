﻿using AutoMapper;
using NP_API.Common;
using NP_API.Data;
using NP_API.Dto.Request.NewsCategory;
using NP_API.Models;
using NP_API.Repository.Interface;
using Microsoft.AspNetCore.Routing.Matching;
using Microsoft.EntityFrameworkCore;
using Org.BouncyCastle.Bcpg.OpenPgp;
using System.Linq.Expressions;

namespace NP_API.Repository {
    public class NewsCategoryRepository : INewsCategoryRepository
    {
        private readonly DataContext context;
        private readonly IMapper _mapper;

        public NewsCategoryRepository(IDbContextFactory<DataContext> dataContextFactory, IMapper mapper)
        {
            context = dataContextFactory.CreateDbContext();
            _mapper = mapper;
        }
        public async Task<PagedList<NewsCategoryResultDto>> GetData(GetDto dto)
        {
            List<Expression<Func<NewsCategory, bool>>> predicates = new List<Expression<Func<NewsCategory, bool>>>();
            Expression<Func<NewsCategory, bool>> guidPredicate = x => x.Guid == dto.Guid;
            Expression<Func<NewsCategory, bool>> namePredicate = x => x.Name == dto.Name;

            if (dto.Guid != null && dto.Guid != Guid.Empty)
            {
                predicates.Add(guidPredicate);
            }
            if (dto.Name != null && dto.Name != "")
            {
                predicates.Add(namePredicate);
            }

            var query = context.NewsCategories
                .Include(cgo => cgo.Company)
                .AsQueryable();
            return await query.PaginateAsync<NewsCategory, NewsCategoryResultDto>(_mapper, dto.Page, dto.PageSize, predicates.ToArray(), dto.OrderBy, dto.Ascending);
        }

        public async Task<NewsCategory> FindById(int id)
        {
            return await context.NewsCategories.FindAsync(id);
        }
        public async Task<NewsCategory> FindByGuid(Guid guid)
        {
            return await context.NewsCategories.SingleOrDefaultAsync(x => x.Guid == guid);
        }
        public async Task<NewsCategoryResultDto> Save(NewsCategory model)
        {
            context.NewsCategories.Add(model);
            await context.SaveChangesAsync();
            NewsCategoryResultDto resultDto = _mapper.Map<NewsCategoryResultDto>(model);
            return resultDto;
        }
        public async Task<NewsCategoryResultDto> Update(NewsCategory newsCategory)
        {
            NewsCategory model = await context.NewsCategories.SingleOrDefaultAsync(x => x.Guid == newsCategory.Guid);

            context.Entry(model).CurrentValues.SetValues(newsCategory);
            await context.SaveChangesAsync();
            NewsCategoryResultDto resultDto = _mapper.Map<NewsCategoryResultDto>(model);
            return resultDto;
        }

        public async Task<bool> Delete(Guid guid)
        {
            NewsCategory model = await context.NewsCategories.SingleOrDefaultAsync(x => x.Guid == guid);

            context.NewsCategories.Remove(model);
            await context.SaveChangesAsync();
            return true;
        }

    }
}


