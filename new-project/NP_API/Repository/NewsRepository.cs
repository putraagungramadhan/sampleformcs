﻿using AutoMapper;
using NP_API.Common;
using NP_API.Data;
using NP_API.Dto.Request.News;
using NP_API.Models;
using NP_API.Repository.Interface;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;


namespace NP_API.Repository
{
    public class NewsRepository : INewsRepository
    {
        private readonly DataContext context;
        private readonly IMapper _mapper;
        public NewsRepository(IDbContextFactory<DataContext> dataContextFactory, IMapper mapper)
        {
            context = dataContextFactory.CreateDbContext();
            _mapper = mapper;
        }
        public async Task<PagedList<NewsResultDto>> GetData(GetDto dto)
        {
            List<Expression<Func<News, bool>>> predicates = new List<Expression<Func<News, bool>>>();
            Expression<Func<News, bool>> guidPredicate = x => x.Guid == dto.Guid;
            Expression<Func<News, bool>> titlePredicate = x => x.Title == dto.Title;
            Expression<Func<News, bool>> categoryPredicate = x => x.NewsCategoryGuid == dto.NewsCategoryGuid;


            if (dto.Guid != null && dto.Guid != Guid.Empty)
            {
                predicates.Add(guidPredicate);
            }
            if (dto.NewsCategoryGuid != null && dto.NewsCategoryGuid != Guid.Empty)
            {
                predicates.Add(categoryPredicate);
            }
            if (dto.Title != null && dto.Title != "")
            {
                predicates.Add(titlePredicate);
            }

            var query = context.News
                .Include(cgo => cgo.NewsCategory)
                .AsQueryable();
            return await query.PaginateAsync<News, NewsResultDto>(_mapper, dto.Page, dto.PageSize, predicates.ToArray(), dto.OrderBy, dto.Ascending);
        }

        public async Task<News> FindById(int id)
        {
            return await context.News.FindAsync(id);
        }
        public async Task<News> FindByGuid(Guid guid)
        {
            return await context.News.SingleOrDefaultAsync(x => x.Guid == guid);
        }

        public async Task<NewsResultDto> Save(News model)
        {
            context.News.Add(model);
            await context.SaveChangesAsync();
            NewsResultDto resultDto = _mapper.Map<NewsResultDto>(model);
            return resultDto;
        }
        public async Task<NewsResultDto> Update(News news)
        {
            News model = await context.News.SingleOrDefaultAsync(x => x.Guid == news.Guid);

            context.Entry(model).CurrentValues.SetValues(news);
            await context.SaveChangesAsync();
            NewsResultDto resultDto = _mapper.Map<NewsResultDto>(model);
            return resultDto;
        }

        public async Task<NewsResultDto> UpdateCoverImage(News news)
        {
            News model = await context.News.SingleOrDefaultAsync(x => x.Guid == news.Guid);

            model.CoverImage = news.CoverImage;

            await context.SaveChangesAsync();
            NewsResultDto resultDto = _mapper.Map<NewsResultDto>(model);
            return resultDto;
        }
        public async Task<bool> Delete(Guid guid)
        {
            News model = await context.News.SingleOrDefaultAsync(x => x.Guid == guid);

            context.News.Remove(model);
            await context.SaveChangesAsync();
            return true;
        }
    }
}
