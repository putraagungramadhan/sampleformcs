﻿using AutoMapper;
using NP_API.Common;
using NP_API.Data;
using NP_API.Dto.Request.SetupConfigurations;
using NP_API.Models;
using NP_API.Repository.Interface;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;

namespace NP_API.Repository
{
    public class SetupConfigurationRepository : ISetupConfigurationRepository
    {
        private readonly DataContext context;
        private readonly IMapper _mapper;

        public SetupConfigurationRepository(IDbContextFactory<DataContext> dataContextFactory, IMapper mapper)
        {
            context = dataContextFactory.CreateDbContext();
            _mapper = mapper;
        }

        public async Task<SetupConfigurations> FindById(int id)
        {
            return await context.SetupConfigurations.FindAsync(id);
        }

        public async Task<SetupConfigurations> FindByGuid(Guid guid)
        {
            return await context.SetupConfigurations.SingleOrDefaultAsync(x => x.Guid == guid);
        }

        public async Task<PagedList<SetupConfigurationResultDto>> GetData(GetDto dto)
        {
            List<Expression<Func<SetupConfigurations, bool>>> predicates = new List<Expression<Func<SetupConfigurations, bool>>>();
            Expression<Func<SetupConfigurations, bool>> guidPredicate = x => x.Guid == dto.Guid;
            Expression<Func<SetupConfigurations, bool>> namePredicate = x => x.TypeName.Contains(dto.TypeName);
            Expression<Func<SetupConfigurations, bool>> subPredicate = x => x.SubTypeName.Contains(dto.SubTypeName);

            if (dto.Guid != null && dto.Guid != Guid.Empty)
            {
                predicates.Add(guidPredicate);
            }
            if (dto.TypeName != null && dto.TypeName != "")
            {
                predicates.Add(namePredicate);
            }
            if (dto.SubTypeName != null && dto.SubTypeName != "")
            {
                predicates.Add(subPredicate);
            }

            var query = context.SetupConfigurations.AsQueryable();
            return await query.PaginateAsync<SetupConfigurations, SetupConfigurationResultDto>(_mapper, dto.Page, dto.PageSize, predicates.ToArray(), dto.OrderBy, dto.Ascending);
        }

        public async Task<SetupConfigurationResultDto> Save(SetupConfigurations model)
        {
            context.SetupConfigurations.Add(model);

            await context.SaveChangesAsync();
            SetupConfigurationResultDto resultDto = _mapper.Map<SetupConfigurationResultDto>(model);

            return resultDto;
        }

        public async Task<SetupConfigurationResultDto> Update(SetupConfigurations param)
        {
            SetupConfigurations model = await context.SetupConfigurations.SingleOrDefaultAsync(x => x.Guid == param.Guid);
            context.Entry(model).CurrentValues.SetValues(param);

            await context.SaveChangesAsync();
            SetupConfigurationResultDto resultDto = _mapper.Map<SetupConfigurationResultDto>(model);

            return resultDto;
        }

        public async Task<bool> Delete(Guid guid)
        {
            SetupConfigurations model = await context.SetupConfigurations.SingleOrDefaultAsync(x => x.Guid == guid);

            context.SetupConfigurations.Remove(model);
            await context.SaveChangesAsync();
            return true;
        }
    }
}
