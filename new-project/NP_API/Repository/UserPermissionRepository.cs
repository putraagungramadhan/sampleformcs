﻿using AutoMapper;
using NP_API.Common;
using NP_API.Data;
using NP_API.Dto.Request.UserPermission;
using NP_API.Models;
using NP_API.Repository.Interface;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;

namespace NP_API.Repository
{
    public class UserPermissionRepository : IUserPermissionRepository
    {
        private readonly DataContext context;
        private readonly IMapper _mapper;
        public UserPermissionRepository(IDbContextFactory<DataContext> dataContextFactory, IMapper mapper)
        {
            context = dataContextFactory.CreateDbContext();
            _mapper = mapper;
        }
        public async Task<UserPermission> FindByGuid(Guid guid)
        {
            return await context.UserPermissions.SingleOrDefaultAsync(x => x.Guid == guid);
        }
        public async Task<Object> GetByUser(Guid UserGuid)
        {
            List<UserPermission> permissionList = await context.UserPermissions
                .Include(x => x.Module)
                .Where(x => x.UserGuid == UserGuid).ToListAsync();
            List<UserPermissionResultDto> resultDto = _mapper.Map<List<UserPermissionResultDto>>(permissionList);

            var permissions = resultDto.ToDictionary(item => item.Module.Name, item => item.CanWrite == true?"write":"read");
            return permissions;
        }
        public async Task<PagedList<UserPermissionResultDto>> GetData(GetDto dto, Guid companyGuid)
        {
            List<Expression<Func<UserPermission, bool>>> predicates = new List<Expression<Func<UserPermission, bool>>>();
            Expression<Func<UserPermission, bool>> guidTermPredicate = x => x.Guid == dto.Guid;
            Expression<Func<UserPermission, bool>> userTermPredicate = x => x.UserGuid == dto.UserGuid;
            Expression<Func<UserPermission, bool>> moduleTermPredicate = x => x.ModuleGuid == dto.ModuleGuid;
            Expression<Func<UserPermission, bool>> companyPredicate = x => x.CompanyGuid == companyGuid;
            if (dto.Guid != null && dto.Guid != Guid.Empty)
            {
                predicates.Add(guidTermPredicate);
            }
            if (dto.UserGuid != null && dto.UserGuid != Guid.Empty)
            {
                predicates.Add(userTermPredicate);
            }
            if (dto.ModuleGuid != null && dto.ModuleGuid != Guid.Empty)
            {
                predicates.Add(moduleTermPredicate);
            }

            predicates.Add(companyPredicate);
            var query = context.UserPermissions
                .Include(c => c.User)
                .Include(cgo => cgo.Module)
                .AsQueryable();
            if (!string.IsNullOrEmpty(dto.Role))
            {
                query = query.Where(vc => vc.User.Role == dto.Role);
            }
            if (dto.RoleType != null)
            {
                query = query.Where(vc => vc.User.RoleType == dto.RoleType);
            }
            return await query.PaginateAsync<UserPermission, UserPermissionResultDto>(_mapper, dto.Page, dto.PageSize, predicates.ToArray(), dto.OrderBy, dto.Ascending);
        }
        public async Task<UserPermissionResultDto> Save(UserPermission model)
        {
            context.UserPermissions.Add(model);
            await context.SaveChangesAsync();
            UserPermissionResultDto resultDto = _mapper.Map<UserPermissionResultDto>(model);
            return resultDto;
        }
        public async Task<UserPermissionResultDto> Update(UserPermission permission)
        {
            UserPermission model = await FindByGuid((Guid)permission.Guid);

            context.Entry(model).CurrentValues.SetValues(permission);
            await context.SaveChangesAsync();
            UserPermissionResultDto resultDto = _mapper.Map<UserPermissionResultDto>(model);
            return resultDto;
        }
        public async Task<bool> Delete(Guid guid)
        {
            UserPermission model = await FindByGuid(guid);

            context.UserPermissions.Remove(model);
            await context.SaveChangesAsync();
            return true;
        }
    }
}
