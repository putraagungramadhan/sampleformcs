﻿using NP_API.Data;
using NP_API.Models;
using NP_API.Repository.Interface;
using Microsoft.EntityFrameworkCore;
using NP_API.Common;
using System.Linq.Expressions;
using NP_API.Dto.Request.User;
using AutoMapper;

namespace NP_API.Repository
{
    public class UserRepository : IUserRepository
    {
        private readonly DataContext context;
        private readonly IMapper _mapper;

        public UserRepository(IDbContextFactory<DataContext> dataContextFactory, IMapper mapper)
        {
            context = dataContextFactory.CreateDbContext();
            _mapper = mapper;
        }
        public async Task<PagedList<UserResultDto>> GetData(GetDto dto, Guid companyGuid)
        {
            List<Expression<Func<User, bool>>> predicates = new List<Expression<Func<User, bool>>>();
            Expression<Func<User, bool>> guidTermPredicate = x => x.Guid == dto.Guid;
            Expression<Func<User, bool>> usernameTermPredicate = x => x.Username.Contains(dto.Username);
            Expression<Func<User, bool>> roleTermPredicate = x => x.Role.Contains(dto.Role);
            Expression<Func<User, bool>> roleTypeTermPredicate = x => x.RoleType == dto.RoleType;
            Expression<Func<User, bool>> companyPredicate = x => x.CompanyGuid == companyGuid;
            //=== for more check
            //Expression<Func<User, bool>> namePredicate = x => x.Name == dto.Name;
            //Expression<Func<User, bool>> mailPredicate = x => x.Email == dto.Email;
            if (dto.Guid != null && dto.Guid != Guid.Empty)
            {
                predicates.Add(guidTermPredicate);
            }
            if (!string.IsNullOrEmpty(dto.Username))
            {
                predicates.Add(usernameTermPredicate);
            }
            if (!string.IsNullOrEmpty(dto.Role))
            {
                predicates.Add(roleTermPredicate);
            }
            if (dto.RoleType != null)
            {
                predicates.Add(roleTypeTermPredicate);
            }

            predicates.Add(companyPredicate);
            var query = context.Users
                .Include(x => x.Company)
                .AsQueryable();
            return await query.PaginateAsync<User, UserResultDto>(_mapper, dto.Page, dto.PageSize, predicates.ToArray(), dto.OrderBy, dto.Ascending);
        }

        public async Task<PagedList<UserResultDto>> GetDataByRole(GetDto dto)
        {
            List<Expression<Func<User, bool>>> predicates = new List<Expression<Func<User, bool>>>();
            Expression<Func<User, bool>> roleTermPredicate = x => x.RoleType == dto.RoleType;
            if (dto.RoleType != null) predicates.Add(roleTermPredicate);

            var query = context.Users
                .Include(x => x.Company)
                .AsQueryable();
            return await query.PaginateAsync<User, UserResultDto>(_mapper, dto.Page, dto.PageSize, predicates.ToArray(), dto.OrderBy, dto.Ascending);
        }

        public async Task<User> FindByEmail(string key)
        {
            return await context.Users.SingleOrDefaultAsync(user => user.Email == key || user.Username == key);
        }
        public async Task<User> FindByGuid(Guid guid)
        {
            return await context.Users
                .Include(x => x.Company)
                .SingleOrDefaultAsync(user => user.Guid == guid);
        }
        public async Task<User> Save(User user)
        {
            context.Users.Add(user);
            await context.SaveChangesAsync();
            return user;
        }
        public async Task<User> UpdatePassword(User user)
        {
            User model = await FindByGuid((Guid)user.Guid);

            model.Password = user.Password;

            await context.SaveChangesAsync();
            return model;
        }
        public async Task<User> UpdateEmail(User user)
        {
            User model = await FindByGuid((Guid)user.Guid);

            model.Email = user.Email;

            await context.SaveChangesAsync();
            return model;
        }

        public async Task<bool> Delete(Guid guid)
        {
            User model = await FindByGuid(guid);

            context.Users.Remove(model);
            await context.SaveChangesAsync();
            return true;
        }

        public async Task<bool> DeleteByWorker(Guid guid)
        {
            try
            {
                User model = await FindByGuid(guid);
                var customModel = new User
                {
                    IsDeleted = true,
                    DeletedAt = (long)DateTime.UtcNow.ToUniversalTime().Subtract(DateTime.UnixEpoch).TotalSeconds,
                    DeletedBy = model.CreatedBy
                };

                context.Entry(model).CurrentValues.SetValues(customModel);
                await context.SaveChangesAsync();
                return true;
            }
            catch (Exception ex)
            {
                var error = ex.InnerException;
                return false;
            }
        }

        public async Task<User> SaveOnly(User user)
        {
            context.Users.Add(user);
            context.SaveChanges();
            return user;
        }
    }
}
