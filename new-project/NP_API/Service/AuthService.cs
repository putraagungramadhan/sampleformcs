﻿using NP_API.Dto;
using NP_API.Models;
using NP_API.Repository.Interface;
using NP_API.Service.Interface;
using NP_API.Exceptions;
using NP_API.Dto.Custom;
using AutoMapper;
using NP_API.Dto.Request.Company;
using NP_API.Models.Enum;

namespace NP_API.Service
{
    public class AuthService : IAuthService
    {
        private readonly IUserRepository _userRepository;
        private readonly ICompanyRepository _companyRepository;
        private readonly IJwtService _jwtService;
        private readonly ITokenManager _tokenManager;
        private readonly IMailService _mailService;
        private LanguageService _localization;
        private FactoryService _factory;
        private IMapper _mapper;
        public AuthService(IUserRepository userRepository,
                      ICompanyRepository companyRepository,
                      IJwtService jwtService,
                      ITokenManager tokenManager,
                      LanguageService localization, FactoryService factory, IMailService mailService,
                      IMapper mapper)
        {
            _userRepository = userRepository;
            _jwtService = jwtService;
            _tokenManager = tokenManager;
            _localization = localization;
            _factory = factory;
            _mailService = mailService;
            _mapper = mapper;
            _companyRepository = companyRepository;
        }
        public async Task<UserLoginDto> login(LoginDto dto)
        {
            User user = await _userRepository.FindByEmail(dto.Key);
            if (user == null)
            {
                throw new NotFoundException(_localization.Getkey("auth_login_key_notfound"));
            }
            else if(!BCrypt.Net.BCrypt.Verify(dto.Password, user.Password))
            {
                throw new NotFoundException(_localization.Getkey("auth_login_key_notfound"));
            }

            UserLoginDto result = _mapper.Map<UserLoginDto>(user);
            //get company
            if (result.Role != "SUPERADMIN" || result.RoleType == EnRole.SUPERADMIN) //if ()
            {
                Company company = await _companyRepository.FindByGuid((Guid)user.CompanyGuid);
                CompanyResultDto retCompany = _mapper.Map<CompanyResultDto>(company);
                result.CompanyDetails = retCompany;
            }
            
            var jwtToken = _jwtService.generateToken(user);
            result.Token = jwtToken;
            //cek email
            MailDto mailDto = new MailDto();
            mailDto.EmailSubject = "Tes Subject";
            mailDto.EmailToId = "septianrizky23@gmail.com";
            mailDto.EmailToName = "Bambang";
            mailDto.EmailBody = string.Format(_factory.GetEmailTemplate("hello.html"), mailDto.EmailToName, DateTime.Today.Date.ToShortDateString());
            
            var retEmail = await _mailService.SendMail(mailDto);
            return result;
            
            
        }
        public async Task<ResultDto> logout(string token)
        {
            JwtDto decode = _jwtService.decode(token);
            await _tokenManager.DeactivateCurrentToken();
            ResultDto resultDto = new ResultDto();
            resultDto.isSuccess = true;
            resultDto.error = null;
            resultDto.data = null;
            return resultDto;
        }
    }    
}