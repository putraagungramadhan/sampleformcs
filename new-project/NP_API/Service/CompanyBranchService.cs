﻿using AutoMapper;
using NP_API.Common;
using NP_API.Dto;
using NP_API.Dto.Custom;
using NP_API.Dto.Request.CompanyBranch;
using NP_API.Exceptions;
using NP_API.Helper;
using NP_API.Models;
using NP_API.Repository;
using NP_API.Repository.Interface;
using NP_API.Service.Interface;

namespace NP_API.Service
{
    public class CompanyBranchService : ICompanyBranchService
    {
        private readonly ICompanyBranchRepository _companyBranchRepository;
        private readonly IJwtService _jwtService;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly JwtDto? _token;
        private LanguageService _localization;
        private IMapper _mapper;

        public CompanyBranchService(ICompanyBranchRepository companyBranchRepository,
                                IJwtService jwtService, IHttpContextAccessor httpContextAccessor, LanguageService localization,
                                IMapper mapper)
        {
            _companyBranchRepository = companyBranchRepository;
            _jwtService = jwtService;
            _httpContextAccessor = httpContextAccessor;
            _localization = localization;
            _mapper = mapper;
            _token = _jwtService.decode(_httpContextAccessor.HttpContext.Request.Headers["authorization"]);
        }
        public async Task<PagedList<CompanyBranchResultDto>> GetData(GetDto dto)
        {
            return await _companyBranchRepository.GetData(dto, GeneralHelper.ConvertToGuid(_token.CompanyGuid));
        }
        public async Task<CompanyBranchResultDto> Save(CompanyBranchDto dto)
        {
            CompanyBranch model = _mapper.Map<CompanyBranch>(dto);
            model.Guid = Guid.NewGuid();
            model.CompanyGuid = GeneralHelper.ConvertToGuid(_token.CompanyGuid);
            model.CreatedBy = _token.Id;
            model.UpdatedBy = _token.Id;
            CompanyBranchResultDto retModel = await _companyBranchRepository.Save(model);

            return retModel;
        }
        public async Task<CompanyBranchResultDto> Update(UpdateCompanyBranchDto dto)
        {
            CompanyBranch model = await _companyBranchRepository.FindByGuid(dto.Guid);
            if (model == null)
            {
                throw new NotFoundException(_localization.Getkey("company_branch_id_not_found"));
            }

            CompanyBranch modelModified = _mapper.Map<CompanyBranch>(dto);
            modelModified.Id = model.Id;
            modelModified.CompanyGuid = GeneralHelper.ConvertToGuid(_token.CompanyGuid);
            modelModified.UpdatedAt = (long)DateTime.UtcNow.ToUniversalTime().Subtract(DateTime.UnixEpoch).TotalSeconds;
            modelModified.UpdatedBy = _token.Id;
            CompanyBranchResultDto retModel = await _companyBranchRepository.Update(modelModified);

            return retModel;
        }
        public async Task<bool> Delete(DeleteDto dto)
        {
            CompanyBranch model = await _companyBranchRepository.FindByGuid(dto.Guid);
            if (model == null)
            {
                throw new NotFoundException(_localization.Getkey("company_branch_id_not_found"));
            }
            return await _companyBranchRepository.Delete(dto.Guid);
        }
    }
}
