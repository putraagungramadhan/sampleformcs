﻿using AutoMapper;
using NP_API.Common;
using NP_API.Dto;
using NP_API.Dto.Custom;
using NP_API.Dto.Request.CompanyClass;
using NP_API.Exceptions;
using NP_API.Models;
using NP_API.Repository.Interface;
using NP_API.Service.Interface;
using NP_API.Helper;
using System.Diagnostics;

namespace NP_API.Service
{
    public class CompanyClassService : ICompanyClassService
    {
        private readonly ICompanyClassRepository _classRepository;
        private readonly IJwtService _jwtService;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly JwtDto? _token;
        private LanguageService _localization;
        private IMapper _mapper;
        public CompanyClassService(ICompanyClassRepository classRepository,
            IJwtService jwtService, IHttpContextAccessor httpContextAccessor, LanguageService languageService, IMapper mapper)
        {
            this._classRepository = classRepository;
            this._jwtService = jwtService;
            this._httpContextAccessor = httpContextAccessor;
            this._localization = languageService;
            this._mapper = mapper;  
            this._token = _jwtService.decode(_httpContextAccessor.HttpContext.Request.Headers["authorization"]);
        }
        public async Task<PagedList<CompanyClassResultDto>> GetData(GetDto dto)
        {
            return await _classRepository.GetData(dto, GeneralHelper.ConvertToGuid(_token.CompanyGuid));
        }
        public async Task<CompanyClass> Create(CompanyClassDto dto)
        {
            CompanyClass classes = _mapper.Map<CompanyClass>(dto);
            classes.Guid = Guid.NewGuid();
            classes.CompanyGuid = GeneralHelper.ConvertToGuid(_token.CompanyGuid);
            classes.CreatedBy = _token.Id;
            classes.UpdatedBy = _token.Id;
            classes = await _classRepository.Save(classes);

            return classes;
        }
        public async Task<CompanyClass> Update(UpdateClassDto dto)
        {
            CompanyClass model = await _classRepository.FindByGuid(dto.Guid);
            if (model == null)
            {
                throw new NotFoundException(_localization.Getkey("class_id_not_found"));
            }
            CompanyClass modelModified = _mapper.Map<CompanyClass>(dto);
            modelModified.Id = model.Id;
            modelModified.CompanyGuid = GeneralHelper.ConvertToGuid(_token.CompanyGuid);
            modelModified.UpdatedAt = (long)DateTime.UtcNow.ToUniversalTime().Subtract(DateTime.UnixEpoch).TotalSeconds;
            modelModified.UpdatedBy = _token.Id;
            modelModified = await _classRepository.Update(modelModified);

            return modelModified;
        }
        public async Task<bool> Delete(DeleteDto dto)
        {
            CompanyClass grade = await _classRepository.FindByGuid(dto.Guid);
            if (grade == null)
            {
                throw new NotFoundException(_localization.Getkey("class_id_not_found"));
            }
            return await _classRepository.Delete(dto.Guid);
        }
    }
}
