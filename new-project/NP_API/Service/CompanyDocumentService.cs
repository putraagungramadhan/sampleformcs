﻿using AutoMapper;
using NP_API.Common;
using NP_API.Dto;
using NP_API.Dto.Request.CompanyDocument;
using NP_API.Repository.Interface;
using NP_API.Service.Interface;
using NP_API.Helper;
using System.Transactions;
using NP_API.Models;
using NP_API.Exceptions;
using NP_API.Dto.Custom;

namespace NP_API.Service
{
    public class CompanyDocumentService : ICompanyDocumentService
    {
        private readonly ICompanyDocumentRepository _companyDocumentRepository;
        private readonly ICompanyRepository _companyRepository;
        private readonly IJwtService _jwtService;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly JwtDto? _token;
        private LanguageService _localization;
        private IMapper _mapper;
        private readonly IFileUploadService _uploadService;
        public CompanyDocumentService(ICompanyDocumentRepository companyDocumentRepository, IUserRepository userRepository, ICompanyRepository companyRepository,
           IJwtService jwtService, IHttpContextAccessor httpContextAccessor, LanguageService localization,
           IMapper mapper, IFileUploadService fileUploadService)
        {
            this._companyDocumentRepository = companyDocumentRepository;
            this._companyRepository = companyRepository;
            this._jwtService = jwtService;
            this._localization = localization;
            this._httpContextAccessor = httpContextAccessor;
            this._mapper = mapper;
            this._uploadService = fileUploadService;
            this._token = _jwtService.decode(_httpContextAccessor.HttpContext.Request.Headers["authorization"]);
        }

        public async Task<PagedList<CompanyDocumentResultDto>> GetData(GetDto dto)
        {
            return await _companyDocumentRepository.GetData(dto, GeneralHelper.ConvertToGuid(_token.CompanyGuid));
        }

        public async Task<CompanyDocumentResultDto> Create(NewCompanyDocumentDto dto)
        {

            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    CompanyDocument model = _mapper.Map<CompanyDocument>(dto);

                    model.Guid = Guid.NewGuid();
                    model.CompanyGuid = GeneralHelper.ConvertToGuid(_token.CompanyGuid);
                    model.CreatedBy = _token.Id;
                    model.UpdatedBy = _token.Id;
                    CompanyDocumentResultDto retModel = await _companyDocumentRepository.Save(model);
                    //upload coverImage
                    if (dto.File != null && dto.File.Length > 0)
                    {
                       
                        model.file = await _uploadService.UploadFileAsync(dto.File, "company/document/" + retModel.Guid.ToString() + "/file/");
                        CompanyDocumentResultDto retFile = await _companyDocumentRepository.UpdateFile(model);
                    }
                    scope.Complete();
                    return retModel;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public async Task<CompanyDocumentResultDto> Update(UpdateCompanyDocumentDto dto)
        {

            CompanyDocument model = await _companyDocumentRepository.FindByGuid(dto.Guid);
            if (model == null)
            {
                throw new NotFoundException(_localization.Getkey("company_document_id_not_found"));
            }


            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    CompanyDocument modelModified = _mapper.Map<CompanyDocument>(dto);
                    modelModified.CompanyGuid = GeneralHelper.ConvertToGuid(_token.CompanyGuid);
                    modelModified.file = model.file;
                    modelModified.Id = model.Id;
                    modelModified.UpdatedAt = (long)DateTime.UtcNow.ToUniversalTime().Subtract(DateTime.UnixEpoch).TotalSeconds;
                    modelModified.UpdatedBy = _token.Id;
                    CompanyDocumentResultDto retModel = await _companyDocumentRepository.Update(modelModified);
                    //upload file
                    if (dto.File != null && dto.File.Length > 0)
                    {
                        modelModified.file = await _uploadService.UploadFileAsync(dto.File, "company/document/" + modelModified.Guid.ToString() + "/file/");
                        CompanyDocumentResultDto retPoster = await _companyDocumentRepository.UpdateFile(modelModified);
                    }
                    scope.Complete();
                    return retModel;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public async Task<bool> Delete(DeleteDto dto)
        {

            CompanyDocument model = await _companyDocumentRepository.FindByGuid(dto.Guid);
            if (model == null)
            {
                throw new NotFoundException(_localization.Getkey("company_document_id_not_found"));
            }
            return await _companyDocumentRepository.Delete(dto.Guid);
        }

    }
}
