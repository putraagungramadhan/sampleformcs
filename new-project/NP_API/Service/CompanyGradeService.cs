﻿using AutoMapper;
using NP_API.Common;
using NP_API.Data;
using NP_API.Dto;
using NP_API.Dto.Custom;
using NP_API.Dto.Request.CompanyGrade;
using NP_API.Exceptions;
using NP_API.Helper;
using NP_API.Models;
using NP_API.Repository;
using NP_API.Repository.Interface;
using NP_API.Service.Interface;
using Microsoft.EntityFrameworkCore;

namespace NP_API.Service
{
    public class CompanyGradeService : ICompanyGradeService
    {
        private readonly ICompanyGradeRepository _gradeRepository;
        private readonly IJwtService _jwtService;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly JwtDto? _token;
        private LanguageService _localization;
        private readonly IMapper _mapper;
        public CompanyGradeService(ICompanyGradeRepository gradeRepository, 
            IJwtService jwtService, IHttpContextAccessor httpContextAccessor, LanguageService languageService,IMapper mapper)
        {
            this._gradeRepository = gradeRepository;
            this._jwtService = jwtService;
            this._httpContextAccessor = httpContextAccessor;
            this._localization = languageService;
            this._token = _jwtService.decode(_httpContextAccessor.HttpContext.Request.Headers["authorization"]);
            this._mapper = mapper;
        }
        public async Task<PagedList<CompanyGradeResultDto>> GetData(GetDto dto)
        {
            return await _gradeRepository.GetData(dto, GeneralHelper.ConvertToGuid(_token.CompanyGuid));
        }
        public async Task<CompanyGrade> FindById(int Id)
        {
            CompanyGrade grade = await _gradeRepository.FindById(Id);
            if (grade == null)
            {
                throw new NotFoundException(_localization.Getkey("grade_id_not_found"));
            }
            return grade;
        }
        public async Task<CompanyGrade> Create(CompanyGradeDto dto)
        {
            CompanyGrade grade = _mapper.Map<CompanyGrade>(dto);
            grade.Guid = Guid.NewGuid();
            grade.CompanyGuid = GeneralHelper.ConvertToGuid(_token.CompanyGuid);
            grade.CreatedBy = _token.Id;
            grade.UpdatedBy = _token.Id;
            grade = await _gradeRepository.Save(grade);

            return grade;
        }
        public async Task<CompanyGrade> Update(UpdateGradeDto dto)
        {
            CompanyGrade grade = await _gradeRepository.FindByGuid(dto.Guid);
            if (grade == null)
            {
                throw new NotFoundException(_localization.Getkey("grade_id_not_found"));
            }

            CompanyGrade modelModified = _mapper.Map<CompanyGrade>(dto);
            modelModified.CompanyGuid = GeneralHelper.ConvertToGuid(_token.CompanyGuid);
            modelModified.Id = grade.Id;
            modelModified.UpdatedAt = (long)DateTime.UtcNow.ToUniversalTime().Subtract(DateTime.UnixEpoch).TotalSeconds;
            modelModified.UpdatedBy = _token.Id;
            modelModified = await _gradeRepository.Update(modelModified);

            return modelModified;
        }
        public async Task<bool> Delete(DeleteDto dto)
        {
            CompanyGrade grade = await _gradeRepository.FindByGuid(dto.Guid);
            if (grade == null)
            {
                throw new NotFoundException(_localization.Getkey("grade_id_not_found"));
            }
            return await _gradeRepository.Delete(dto.Guid);
        }
    }
}
