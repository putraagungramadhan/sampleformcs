﻿using AutoMapper;
using NP_API.Common;
using NP_API.Dto;
using NP_API.Dto.Custom;
using NP_API.Dto.Request.CompanyGroupEmployee;
using NP_API.Exceptions;
using NP_API.Helper;
using NP_API.Models;
using NP_API.Repository.Interface;
using NP_API.Service.Interface;
using System.Diagnostics;

namespace NP_API.Service
{
    public class CompanyGroupEmployeeService : ICompanyGroupEmployeeService
    {
        private readonly ICompanyGradeRepository _gradeRepository;
        private readonly ICompanyOrganizationRepository _organizationRepository;
        private readonly ICompanyLevelRepository _levelRepository;
        private readonly ICompanyClassRepository _classRepository;
        private readonly ICompanyGroupEmployeeRepository _groupRepository;
        private readonly IJwtService _jwtService;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly JwtDto? _token;
        private LanguageService _localization;
        private IMapper _mapper;
        public CompanyGroupEmployeeService(ICompanyGradeRepository gradeRepository,
            ICompanyOrganizationRepository organizationRepository, ICompanyLevelRepository companyLevelRepository, 
            ICompanyClassRepository classRepository, 
            ICompanyGroupEmployeeRepository groupRepository,   
            IJwtService jwtService, IHttpContextAccessor httpContextAccessor, LanguageService languageService,
            IMapper mapper)
        {
            this._gradeRepository = gradeRepository;
            this._organizationRepository = organizationRepository;
            this._levelRepository = companyLevelRepository;
            this._classRepository = classRepository;
            this._groupRepository = groupRepository;
            this._jwtService = jwtService;
            this._httpContextAccessor = httpContextAccessor;
            this._localization = languageService;
            this._token = _jwtService.decode(_httpContextAccessor.HttpContext.Request.Headers["authorization"]);
            this._mapper = mapper;
        }
        public async Task<PagedList<CompanyGroupEmployeeResultDto>> GetData(GetDto dto)
        {
            return await _groupRepository.GetData(dto, GeneralHelper.ConvertToGuid(_token.CompanyGuid));
        }
        public async Task<CompanyGroupEmployee> Create(CompanyGroupEmployeeDto dto)
        {
            string name = await ValidateInput(dto);
            CompanyGroupEmployee model = _mapper.Map<CompanyGroupEmployee>(dto);
            model.Guid = Guid.NewGuid();
            model.Name = name;
            model.CompanyGuid = GeneralHelper.ConvertToGuid(_token.CompanyGuid);
            model.CreatedBy = _token.Id;
            model.UpdatedBy = _token.Id;
            model = await _groupRepository.Save(model);

            return model;
        }
        public async Task<CompanyGroupEmployee> Update(UpdateGroupEmployeeDto dto)
        {
            CompanyGroupEmployee model = await _groupRepository.FindByGuid(dto.Guid);
            if (model == null)
            {
                throw new NotFoundException(_localization.Getkey("group_id_not_found"));
            }
            CompanyGroupEmployeeDto validateDto = _mapper.Map<CompanyGroupEmployeeDto>(dto);
            string name = await ValidateInput(validateDto);

            CompanyGroupEmployee modelModified = _mapper.Map<CompanyGroupEmployee>(dto);
            modelModified.CompanyGuid = GeneralHelper.ConvertToGuid(_token.CompanyGuid);
            modelModified.Id = model.Id;
            modelModified.UpdatedAt = (long)DateTime.UtcNow.ToUniversalTime().Subtract(DateTime.UnixEpoch).TotalSeconds;
            modelModified.UpdatedBy = _token.Id;
            modelModified = await _groupRepository.Update(modelModified);

            return modelModified;
        }
        public async Task<bool> Delete(DeleteDto dto)
        {
            CompanyGroupEmployee model = await _groupRepository.FindByGuid(dto.Guid);
            if (model == null)
            {
                throw new NotFoundException(_localization.Getkey("group_id_not_found"));
            }
            return await _groupRepository.Delete(dto.Guid);
        }
        public async Task<string> ValidateInput(CompanyGroupEmployeeDto dto)
        {
            string name = DateTime.UtcNow.ToUniversalTime().Subtract(DateTime.UnixEpoch).TotalSeconds.ToString();
            //cek orgs company
            if (dto.CompanyOrganizationGuid.HasValue)
            {
                CompanyOrganization orgs = await _organizationRepository.FindByGuid((Guid)dto.CompanyOrganizationGuid);
                if (orgs == null)
                {
                    throw new NotFoundException(_localization.Getkey("orgs_id_not_found"));
                }
                name = name + "_" + orgs.Name;
            }
            //cek level company
            if (dto.CompanyLevelGuid.HasValue)
            {
                CompanyLevel level = await _levelRepository.FindByGuid((Guid)dto.CompanyLevelGuid);
                if (level == null)
                {
                    throw new NotFoundException(_localization.Getkey("level_id_not_found"));
                }
                name = name + "_" + level.Name;
            }
            //cek grade company
            if (dto.CompanyGradeGuid.HasValue)
            {
                CompanyGrade grade = await _gradeRepository.FindByGuid((Guid)dto.CompanyGradeGuid);
                if (grade == null)
                {
                    throw new NotFoundException(_localization.Getkey("grade_id_not_found"));
                }
                name = name + "_" + grade.Name;
            }
            //cek class company
            if (dto.CompanyClassGuid.HasValue)
            {
                CompanyClass classes = await _classRepository.FindByGuid((Guid)dto.CompanyClassGuid);
                if (classes == null)
                {
                    throw new NotFoundException(_localization.Getkey("class_id_not_found"));
                }
                name = name + "_" + classes.Name;
            }
            return name;
        }
    }
}
