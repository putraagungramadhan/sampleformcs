﻿using AutoMapper;
using NP_API.Common;
using NP_API.Dto;
using NP_API.Dto.Custom;
using NP_API.Dto.Request.CompanyGroup;
using NP_API.Exceptions;
using NP_API.Helper;
using NP_API.Models;
using NP_API.Repository.Interface;
using NP_API.Service.Interface;

namespace NP_API.Service
{
    public class CompanyGroupService : ICompanyGroupService
    {
        private readonly ICompanyGroupRepository _companyGroupRepository;
        private readonly IJwtService _jwtService;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly JwtDto? _token;
        private LanguageService _localization;
        private readonly IMapper _mapper;
        public CompanyGroupService(ICompanyGroupRepository companyGroupRepository,
            IJwtService jwtService, IHttpContextAccessor httpContextAccessor, LanguageService languageService, IMapper mapper)
        {
            this._companyGroupRepository = companyGroupRepository;
            this._jwtService = jwtService;
            this._httpContextAccessor = httpContextAccessor;
            this._localization = languageService;
            this._token = _jwtService.decode(_httpContextAccessor.HttpContext.Request.Headers["authorization"]);
            this._mapper = mapper;
        }
        public async Task<PagedList<CompanyGroupResultDto>> GetData(GetDto dto)
        {
            return await _companyGroupRepository.GetData(dto);
        }
        public async Task<CompanyGroupResultDto> Create(CompanyGroupDto dto)
        {
            CompanyGroup model = _mapper.Map<CompanyGroup>(dto);
            model.Guid = Guid.NewGuid();
            model.CreatedBy = _token.Id;
            model.UpdatedBy = _token.Id;
            CompanyGroupResultDto retModel = await _companyGroupRepository.Save(model);

            return retModel;
        }
        public async Task<CompanyGroupResultDto> Update(UpdateCompanyGroupDto dto)
        {
            CompanyGroup group = await _companyGroupRepository.FindByGuid(dto.Guid);
            if (group == null)
            {
                throw new NotFoundException(_localization.Getkey("company_group_id_not_found"));
            }

            CompanyGroup modelModified = _mapper.Map<CompanyGroup>(dto);
            modelModified.Id = group.Id;
            modelModified.UpdatedAt = (long)DateTime.UtcNow.ToUniversalTime().Subtract(DateTime.UnixEpoch).TotalSeconds;
            modelModified.UpdatedBy = _token.Id;
            CompanyGroupResultDto retModel = await _companyGroupRepository.Update(modelModified);

            return retModel;
        }
        public async Task<bool> Delete(DeleteDto dto)
        {
            CompanyGroup model = await _companyGroupRepository.FindByGuid(dto.Guid);
            if (model == null)
            {
                throw new NotFoundException(_localization.Getkey("company_group_id_not_found"));
            }
            return await _companyGroupRepository.Delete(dto.Guid);
        }
    }
}
