﻿using AutoMapper;
using NP_API.Common;
using NP_API.Dto;
using NP_API.Dto.Custom;
using NP_API.Dto.Request.CompanyLevel;
using NP_API.Exceptions;
using NP_API.Helper;
using NP_API.Models;
using NP_API.Repository.Interface;
using NP_API.Service.Interface;
using Microsoft.Extensions.Primitives;
using System.Diagnostics;

namespace NP_API.Service
{
    public class CompanyLevelService : ICompanyLevelService
    {
        private readonly ICompanyLevelRepository _levelRepository;
        private readonly IJwtService _jwtService;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly JwtDto? _token;
        private LanguageService _localization;
        private readonly IMapper _mapper;
        public CompanyLevelService(ICompanyLevelRepository levelRepository,
            IJwtService jwtService, IHttpContextAccessor httpContextAccessor, LanguageService languageService, IMapper mapper)
        {
            this._levelRepository = levelRepository;
            this._jwtService = jwtService;
            this._httpContextAccessor = httpContextAccessor;
            this._localization = languageService;
            this._token = _jwtService.decode(_httpContextAccessor.HttpContext.Request.Headers["authorization"]);
            this._mapper = mapper;
        }
        public async Task<PagedList<CompanyLevelResultDto>> GetData(GetDto dto)
        {
            return await _levelRepository.GetData(dto, GeneralHelper.ConvertToGuid(_token.CompanyGuid));
        }
        public async Task<CompanyLevel> Create(CompanyLevelDto dto)
        {
            CompanyLevel model = _mapper.Map<CompanyLevel>(dto);
            model.Guid = Guid.NewGuid();
            model.CompanyGuid = GeneralHelper.ConvertToGuid(_token.CompanyGuid);
            model.CreatedBy = _token.Id;
            model.UpdatedBy = _token.Id;
            model = await _levelRepository.Save(model);

            return model;
        }
        public async Task<CompanyLevel> Update(UpdateLevelDto dto)
        {
            CompanyLevel model = await _levelRepository.FindByGuid(dto.Guid);
            if (model == null)
            {
                throw new NotFoundException(_localization.Getkey("level_id_not_found"));
            }

            CompanyLevel modelModified = _mapper.Map<CompanyLevel>(dto);
            modelModified.Id = model.Id;
            modelModified.Guid = dto.Guid;
            modelModified.CompanyGuid = GeneralHelper.ConvertToGuid(_token.CompanyGuid);
            modelModified.UpdatedAt = (long)DateTime.UtcNow.ToUniversalTime().Subtract(DateTime.UnixEpoch).TotalSeconds;
            modelModified.UpdatedBy = _token.Id;
            modelModified = await _levelRepository.Update(modelModified);

            return modelModified;
        }
        public async Task<bool> Delete(DeleteDto dto)
        {
            CompanyLevel model = await _levelRepository.FindByGuid(dto.Guid);
            if (model == null)
            {
                throw new NotFoundException(_localization.Getkey("level_id_not_found"));
            }
            return await _levelRepository.Delete(dto.Guid);
        }
    }
}
