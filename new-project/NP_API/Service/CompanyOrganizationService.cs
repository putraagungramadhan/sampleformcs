﻿using AutoMapper;
using NP_API.Common;
using NP_API.Dto;
using NP_API.Dto.Custom;
using NP_API.Dto.Request.CompanyOrganization;
using NP_API.Exceptions;
using NP_API.Helper;
using NP_API.Models;
using NP_API.Repository.Interface;
using NP_API.Service.Interface;
using Microsoft.AspNetCore.Mvc;

namespace NP_API.Service
{
    public class CompanyOrganizationService : ICompanyOrganizationService
    {
        private readonly ICompanyOrganizationRepository _orgsRepository;
        private readonly IJwtService _jwtService;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly JwtDto? _token;
        private LanguageService _localization;
        private readonly IMapper _mapper;
        public CompanyOrganizationService(ICompanyOrganizationRepository orgsRepository,
            IJwtService jwtService, IHttpContextAccessor httpContextAccessor, LanguageService languageService, IMapper mapper)
        {
            this._orgsRepository = orgsRepository;
            this._jwtService = jwtService;
            this._httpContextAccessor = httpContextAccessor;
            this._localization = languageService;
            this._token = _jwtService.decode(_httpContextAccessor.HttpContext.Request.Headers["authorization"]);
            this._mapper = mapper;
        }
        public async Task<PagedList<CompanyOrganizationResultDto>> GetData(GetDto dto)
        {
            return await _orgsRepository.GetData(dto, GeneralHelper.ConvertToGuid(_token.CompanyGuid));
        }
        public async Task<JsonResult> GetStructure()
        {
            return await _orgsRepository.GetStructure(GeneralHelper.ConvertToGuid(_token.CompanyGuid), 0);
        }
        public async Task<CompanyOrganization> Create(CompanyOrganizationDto dto)
        {
            CompanyOrganization model = _mapper.Map<CompanyOrganization>(dto);
            //cek parent idnya apakah ada
            if (model.ParentId != 0)
            {
                CompanyOrganization orgs = await _orgsRepository.FindById(model.ParentId);
                if (orgs == null)
                {
                    throw new NotFoundException(_localization.Getkey("orgs_id_not_found"));
                }
            }
            model.Guid = Guid.NewGuid();
            model.CompanyGuid = GeneralHelper.ConvertToGuid(_token.CompanyGuid);
            model.CreatedBy = _token.Id;
            model.UpdatedBy = _token.Id;
            model = await _orgsRepository.Save(model);

            return model;
        }
        public async Task<CompanyOrganization> Update(UpdateOrganizationDto dto)
        {
            CompanyOrganization model = await _orgsRepository.FindByGuid(dto.Guid);
            if (model == null)
            {
                throw new NotFoundException(_localization.Getkey("orgs_id_not_found"));
            }
            //cek parent idnya apakah ada
            if (model.ParentId != 0)
            {
                CompanyOrganization orgs = await _orgsRepository.FindById(model.ParentId);
                if (orgs == null)
                {
                    throw new NotFoundException(_localization.Getkey("orgs_id_not_found"));
                }
            }
            CompanyOrganization modelModified = _mapper.Map<CompanyOrganization>(dto);
            modelModified.Id = model.Id;
            modelModified.Guid = dto.Guid;
            modelModified.CompanyGuid = GeneralHelper.ConvertToGuid(_token.CompanyGuid);
            modelModified.UpdatedAt = (long)DateTime.UtcNow.ToUniversalTime().Subtract(DateTime.UnixEpoch).TotalSeconds;
            modelModified.UpdatedBy = _token.Id;
            modelModified = await _orgsRepository.Update(modelModified);

            return modelModified;
        }
        public async Task<bool> Delete(DeleteDto dto)
        {
            CompanyOrganization model = await _orgsRepository.FindByGuid(dto.Guid);
            if (model == null)
            {
                throw new NotFoundException(_localization.Getkey("orgs_id_not_found"));
            }
            return await _orgsRepository.Delete(dto.Guid);
        }
    }
}
