﻿using AutoMapper;
using NP_API.Common;
using NP_API.Data;
using NP_API.Dto;
using NP_API.Dto.Request.Company;
using NP_API.Exceptions;
using NP_API.Helper;
using NP_API.Models;
using NP_API.Repository.Interface;
using NP_API.Service.Interface;
using Microsoft.EntityFrameworkCore;
using System.Transactions;

namespace NP_API.Service
{
    public class CompanyService : ICompanyService
    {
        private readonly IUserRepository _userRepository;
        private readonly ICompanyRepository _companyRepository;
        private readonly IJwtService _jwtService;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly string? _token;
        private LanguageService _localization;
        private readonly IMapper _mapper;
        private readonly IFileUploadService _uploadService;
        public CompanyService(IUserRepository userRepository, ICompanyRepository companyRepository,
                      IJwtService jwtService, IHttpContextAccessor httpContextAccessor,
                      LanguageService localization, FactoryService factory, IMailService mailService,
                      IDbContextFactory<DataContext> dataContextFactory, IMapper mapper,
                      IFileUploadService uploadService)
        {
            this._userRepository = userRepository;
            this._companyRepository = companyRepository;
            this._jwtService = jwtService;
            this._localization = localization;
            this._httpContextAccessor = httpContextAccessor;
            this._token = _httpContextAccessor.HttpContext.Request.Headers["authorization"];
            this._mapper = mapper;
            this._uploadService = uploadService;
        }
        public async Task<PagedList<CompanyResultDto>> GetData(GetDto dto)
        {
            return await _companyRepository.GetData(dto);
        }
        public async Task<Company> RegisterCompany(NewCompanyDto dto)
        {
            JwtDto token = _jwtService.decode(_token);
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    //check apakah ada company yg ditandai holding
                    bool IsHolding = await _companyRepository.IsAnyChecklist();
                    if (IsHolding == dto.CompanyDto.IsHolding)
                    {
                        throw new BadRequestException("company_exist_holding");
                    }
                    Company company = _mapper.Map<Company>(dto.CompanyDto);
                    company.Guid = Guid.NewGuid();
                    company = await _companyRepository.Save(company);
                    //upload logo
                    if (dto.Logo != null && dto.Logo.Length > 0)
                    {
                        company.Logo = await _uploadService.UploadFileAsync(dto.Logo, "logo/" + company.Id.ToString());
                        CompanyResultDto retCompany = await _companyRepository.UpdateLogo(company);
                    }

                    User user = _mapper.Map<User>(dto.UserDto);
                    user.Guid = Guid.NewGuid();
                    user.EmployeeMasterGuid = null;
                    user.CompanyGuid = company.Guid;
                    user.Password = GeneralHelper.HashPassword(user.Password);
                    user.CreatedBy = token.Id;
                    user.UpdatedBy = token.Id;
                    user = await _userRepository.Save(user);
                    scope.Complete();
                    return company;
                }
                catch (Exception ex)
                {
                    throw;
                }
            }
        }

        public async Task<CompanyResultDto> Update(UpdateCompanyDto dto)
        {
            Company model = await _companyRepository.FindByGuid(dto.Guid);
            if (model == null)
            {
                throw new NotFoundException(_localization.Getkey("company_id_not_found"));
            }
            bool IsHolding = await _companyRepository.IsAnyChecklist();
            if (model.IsHolding == false)
            {
                if (IsHolding == dto.IsHolding)
                {
                    throw new BadRequestException("company_exist_holding");
                }
            }
            
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    Company modelModified = _mapper.Map<Company>(dto);
                    modelModified.Logo = model.Logo;
                    modelModified.Id = model.Id;
                    modelModified.UpdatedAt = (long)DateTime.UtcNow.ToUniversalTime().Subtract(DateTime.UnixEpoch).TotalSeconds;
                    modelModified.UpdatedBy = dto.Email;
                    CompanyResultDto retModel = await _companyRepository.Update(modelModified);
                    //upload image
                    if (dto.Logo != null && dto.Logo.Length > 0)
                    {
                        model.Logo = await _uploadService.UploadFileAsync(dto.Logo, "candidate/" + model.Guid.ToString() + "/image/");
                        CompanyResultDto retPoster = await _companyRepository.UpdateLogo(model);
                    }
                    scope.Complete();
                    return retModel;


                }
                catch (Exception)
                {
                    throw;
                }
            }


        }
    }
}
