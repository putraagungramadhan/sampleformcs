﻿using AutoMapper;
using NP_API.Common;
using NP_API.Dto;
using NP_API.Dto.Custom;
using NP_API.Dto.Request.CompanyTitle;
using NP_API.Exceptions;
using NP_API.Helper;
using NP_API.Models;
using NP_API.Repository.Interface;
using NP_API.Service.Interface;

namespace NP_API.Service
{
    public class CompanyTitleService : ICompanyTitleService
    {
        private readonly ICompanyTitleRepository _titleRepository;
        private readonly IJwtService _jwtService;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly JwtDto? _token;
        private LanguageService _localization;
        private readonly IMapper _mapper;
        public CompanyTitleService(ICompanyTitleRepository titleRepository,
            IJwtService jwtService, IHttpContextAccessor httpContextAccessor, LanguageService languageService, IMapper mapper)
        {
            this._titleRepository = titleRepository;
            this._jwtService = jwtService;
            this._httpContextAccessor = httpContextAccessor;
            this._localization = languageService;
            this._token = _jwtService.decode(_httpContextAccessor.HttpContext.Request.Headers["authorization"]);
            this._mapper = mapper;
        }
        public async Task<PagedList<CompanyTitleResultDto>> GetData(GetDto dto)
        {
            return await _titleRepository.GetData(dto, GeneralHelper.ConvertToGuid(_token.CompanyGuid));
        }
        public async Task<CompanyTitle> Create(CompanyTitleDto dto)
        {
            CompanyTitle model = _mapper.Map<CompanyTitle>(dto);
            
            model.Guid = Guid.NewGuid();
            model.CompanyGuid = GeneralHelper.ConvertToGuid(_token.CompanyGuid);
            model.CreatedBy = _token.Id;
            model.UpdatedBy = _token.Id;
            model = await _titleRepository.Save(model);

            return model;
        }
        public async Task<CompanyTitle> Update(UpdateCompanyTitleDto dto)
        {
            CompanyTitle model = await _titleRepository.FindByGuid(dto.Guid);
            if (model == null)
            {
                throw new NotFoundException(_localization.Getkey("company_title_id_not_found"));
            }

            CompanyTitle modelModified = _mapper.Map<CompanyTitle>(dto);
            modelModified.Id = model.Id;
            modelModified.Guid = dto.Guid;
            modelModified.CompanyGuid = GeneralHelper.ConvertToGuid(_token.CompanyGuid);
            modelModified.UpdatedAt = (long)DateTime.UtcNow.ToUniversalTime().Subtract(DateTime.UnixEpoch).TotalSeconds;
            modelModified.UpdatedBy = _token.Id;
            modelModified = await _titleRepository.Update(modelModified);

            return modelModified;
        }
        public async Task<bool> Delete(DeleteDto dto)
        {
            CompanyTitle model = await _titleRepository.FindByGuid(dto.Guid);
            if (model == null)
            {
                throw new NotFoundException(_localization.Getkey("company_title_id_not_found"));
            }
            return await _titleRepository.Delete(dto.Guid);
        }
    }
}
