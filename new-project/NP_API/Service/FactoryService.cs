﻿using System.Reflection;

namespace NP_API.Service
{
    public class FactoryService
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        public FactoryService(IHttpContextAccessor httpContextAccessor)
        {
            this._httpContextAccessor = httpContextAccessor;
            JwtToken = _httpContextAccessor.HttpContext.Request.Headers["authorization"];
        }

        public string JwtToken { get; private set; }

        public string GetEmailTemplate(string template)
        {
            string filePath = Directory.GetCurrentDirectory() + "/Templates/Email/" + template;
            string emailTemplateText = File.ReadAllText(filePath);
            return emailTemplateText;
        }

        public string GetNotificationTemplate(string template)
        {
            string filePath = Directory.GetCurrentDirectory() + "/Templates/Notification/" + template;
            var str = new StreamReader(filePath);
            var result = str.ReadToEnd();
            str.Close();

            return result;
        }

        public string GetExceptionDetails(Exception exception)
        {
            PropertyInfo[] properties = exception.GetType()
                                    .GetProperties();
            List<string> fields = new List<string>();
            foreach (PropertyInfo property in properties)
            {
                object value = property.GetValue(exception, null);
                fields.Add(String.Format(
                                 "{0} = {1}",
                                 property.Name,
                                 value != null ? value.ToString() : String.Empty
                ));
            }
            return String.Join("\n", fields.ToArray());
        }
    }
}
