﻿using AutoMapper;
using NP_API.Common;
using NP_API.Dto;
using NP_API.Dto.Custom;
using NP_API.Dto.Request.Feedback;
using NP_API.Exceptions;
using NP_API.Helper;
using NP_API.Models;
using NP_API.Repository;
using NP_API.Repository.Interface;
using NP_API.Service.Interface;
using System.Transactions;

namespace NP_API.Service
{
    public class FeedbackService : IFeedbackService
    {
        private readonly IFeedbackRepository _feedbackRepository;
        private readonly IUserRepository _userRepository;
        private readonly ICompanyRepository _companyRepository;
        private readonly IJwtService _jwtService;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly JwtDto? _token;
        private LanguageService _localization;
        private IMapper _mapper;
        public FeedbackService(IFeedbackRepository feedbackRepository, IUserRepository userRepository, ICompanyRepository companyRepository,
           IJwtService jwtService, IHttpContextAccessor httpContextAccessor, LanguageService localization,
           IMapper mapper)
        {
            this._feedbackRepository = feedbackRepository;
            this._userRepository = userRepository;
            this._companyRepository = companyRepository;
            this._jwtService = jwtService;
            this._localization = localization;
            this._httpContextAccessor = httpContextAccessor;
            this._mapper = mapper;
            this._token = _jwtService.decode(_httpContextAccessor.HttpContext.Request.Headers["authorization"]);
        }

        public async Task<PagedList<FeedbackResultDto>> GetData(GetDto dto)
        {
            return await _feedbackRepository.GetData(dto);
        }

        public async Task<FeedbackResultDto> Create(NewFeedbackDto dto)
        {
            JwtDto _token = _jwtService.decode(_httpContextAccessor.HttpContext.Request.Headers["authorization"]);

            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    Feedback model = _mapper.Map<Feedback>(dto);

                    model.Guid = Guid.NewGuid();
                    model.CompanyGuid = GeneralHelper.ConvertToGuid(_token.CompanyGuid);
                    model.CreatedBy = _token.Id;
                    model.UpdatedBy = _token.Id;
                    FeedbackResultDto retModel = await _feedbackRepository.Save(model);
                   
                    scope.Complete();
                    return retModel;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public async Task<FeedbackResultDto> Update(UpdateFeedbackDto dto)
        {
            JwtDto _token = _jwtService.decode(_httpContextAccessor.HttpContext.Request.Headers["authorization"]);
            Feedback model = await _feedbackRepository.FindByGuid(dto.Guid);
            if (model == null)
            {
                throw new NotFoundException(_localization.Getkey("feedback_id_not_found"));
            }


            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    Feedback modelModified = _mapper.Map<Feedback>(dto);
                    modelModified.CompanyGuid = GeneralHelper.ConvertToGuid(_token.CompanyGuid);
                    modelModified.UserGuid = GeneralHelper.ConvertToGuid(_token.UserGuid);
                    modelModified.Id = model.Id;
                    modelModified.UpdatedAt = (long)DateTime.UtcNow.ToUniversalTime().Subtract(DateTime.UnixEpoch).TotalSeconds;
                    modelModified.UpdatedBy = _token.Id;
                    FeedbackResultDto retModel = await _feedbackRepository.Update(modelModified);
                   
                    scope.Complete();
                    return retModel;
                }
                catch (Exception)
                {
                    throw;
                }
            }

        }
        public async Task<bool> Delete(DeleteDto dto)
        {
            JwtDto _token = _jwtService.decode(_httpContextAccessor.HttpContext.Request.Headers["authorization"]);
            Feedback model = await _feedbackRepository.FindByGuid(dto.Guid);
            if (model == null)
            {
                throw new NotFoundException(_localization.Getkey("feedback_id_not_found"));
            }
            return await _feedbackRepository.Delete(dto.Guid);
        }
    }
}
