﻿using NP_API.Dto;
using NP_API.Dto.Custom;
using NP_API.Models;

namespace NP_API.Service.Interface
{
    public interface IAuthService
    {
        public Task<UserLoginDto> login(LoginDto dto);
        public Task<ResultDto> logout(string token);
    }
}
