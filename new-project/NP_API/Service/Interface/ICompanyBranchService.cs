﻿using NP_API.Common;
using NP_API.Dto.Custom;
using NP_API.Dto.Request.CompanyBranch;

namespace NP_API.Service.Interface
{
    public interface ICompanyBranchService
    {
        public Task<PagedList<CompanyBranchResultDto>> GetData(GetDto dto);
        public Task<CompanyBranchResultDto> Save(CompanyBranchDto dto);
        public Task<CompanyBranchResultDto> Update(UpdateCompanyBranchDto dto);
        public Task<bool> Delete(DeleteDto Id);
    }
}
