﻿using NP_API.Common;
using NP_API.Dto.Custom;
using NP_API.Dto.Request.CompanyClass;
using NP_API.Dto;
using NP_API.Models;

namespace NP_API.Service.Interface
{
    public interface ICompanyClassService
    {
        public Task<PagedList<CompanyClassResultDto>> GetData(GetDto dto);
        public Task<CompanyClass> Create(CompanyClassDto dto);
        public Task<CompanyClass> Update(UpdateClassDto dto);
        public Task<bool> Delete(DeleteDto Id);
    }
}
