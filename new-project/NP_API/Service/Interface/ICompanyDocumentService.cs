﻿using NP_API.Common;
using NP_API.Dto.Custom;
using NP_API.Dto.Request.CompanyDocument;
using NP_API.Models;

namespace NP_API.Service.Interface
{
    public interface ICompanyDocumentService
    {
        public Task<PagedList<CompanyDocumentResultDto>> GetData(GetDto dto);
        public Task<CompanyDocumentResultDto> Create(NewCompanyDocumentDto dto);
        public Task<CompanyDocumentResultDto> Update(UpdateCompanyDocumentDto dto);
        public Task<bool> Delete(DeleteDto Id);
    }
}
