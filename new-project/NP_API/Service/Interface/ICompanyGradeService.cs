﻿using NP_API.Common;
using NP_API.Dto;
using NP_API.Dto.Custom;
using NP_API.Dto.Request.CompanyGrade;
using NP_API.Models;

namespace NP_API.Service.Interface
{
    public interface ICompanyGradeService
    {
        public Task<PagedList<CompanyGradeResultDto>> GetData(GetDto dto);
        public Task<CompanyGrade> FindById(int id);
        public Task<CompanyGrade> Create(CompanyGradeDto dto);
        public Task<CompanyGrade> Update(UpdateGradeDto dto);
        public Task<bool> Delete(DeleteDto Id);
    }
}
