﻿using NP_API.Common;
using NP_API.Dto.Custom;
using NP_API.Dto.Request.CompanyGroupEmployee;
using NP_API.Dto;
using NP_API.Models;

namespace NP_API.Service.Interface
{
    public interface ICompanyGroupEmployeeService
    {
        public Task<PagedList<CompanyGroupEmployeeResultDto>> GetData(GetDto dto);
        public Task<CompanyGroupEmployee> Create(CompanyGroupEmployeeDto dto);
        public Task<CompanyGroupEmployee> Update(UpdateGroupEmployeeDto dto);
        public Task<bool> Delete(DeleteDto Id);
    }
}
