﻿using NP_API.Common;
using NP_API.Dto.Custom;
using NP_API.Dto.Request.CompanyGroup;

namespace NP_API.Service.Interface
{
    public interface ICompanyGroupService
    {
        public Task<PagedList<CompanyGroupResultDto>> GetData(GetDto dto);
        public Task<CompanyGroupResultDto> Create(CompanyGroupDto dto);
        public Task<CompanyGroupResultDto> Update(UpdateCompanyGroupDto dto);
        public Task<bool> Delete(DeleteDto dto);
    }
}
