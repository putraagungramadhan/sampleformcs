﻿using NP_API.Common;
using NP_API.Dto.Custom;
using NP_API.Dto.Request.CompanyLevel;
using NP_API.Dto;
using NP_API.Models;

namespace NP_API.Service.Interface
{
    public interface ICompanyLevelService
    {
        public Task<PagedList<CompanyLevelResultDto>> GetData(GetDto dto);
        public Task<CompanyLevel> Create(CompanyLevelDto dto);
        public Task<CompanyLevel> Update(UpdateLevelDto dto);
        public Task<bool> Delete(DeleteDto Id);
    }
}
