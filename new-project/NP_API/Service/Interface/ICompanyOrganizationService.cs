﻿using NP_API.Common;
using NP_API.Dto.Custom;
using NP_API.Dto.Request.CompanyOrganization;
using NP_API.Dto;
using NP_API.Models;
using Microsoft.AspNetCore.Mvc;

namespace NP_API.Service.Interface
{
    public interface ICompanyOrganizationService
    {
        public Task<PagedList<CompanyOrganizationResultDto>> GetData(GetDto dto);
        public Task<JsonResult> GetStructure();
        public Task<CompanyOrganization> Create(CompanyOrganizationDto dto);
        public Task<CompanyOrganization> Update(UpdateOrganizationDto dto);
        public Task<bool> Delete(DeleteDto Id);
    }
}
