﻿using NP_API.Common;
using NP_API.Dto;
using NP_API.Dto.Request.Company;
using NP_API.Models;

namespace NP_API.Service.Interface
{
    public interface ICompanyService
    {
        public Task<PagedList<CompanyResultDto>> GetData(GetDto dto);
        public Task<Company> RegisterCompany(NewCompanyDto dto);
        public Task<CompanyResultDto> Update(UpdateCompanyDto dto);
    }
}
