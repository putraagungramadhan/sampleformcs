﻿using NP_API.Common;
using NP_API.Dto.Custom;
using NP_API.Dto.Request.CompanyTitle;
using NP_API.Dto;
using NP_API.Models;
using Microsoft.AspNetCore.Mvc;

namespace NP_API.Service.Interface
{
    public interface ICompanyTitleService
    {
        public Task<PagedList<CompanyTitleResultDto>> GetData(GetDto dto);
        public Task<CompanyTitle> Create(CompanyTitleDto dto);
        public Task<CompanyTitle> Update(UpdateCompanyTitleDto dto);
        public Task<bool> Delete(DeleteDto Id);
    }
}
