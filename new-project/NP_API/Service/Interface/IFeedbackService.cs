﻿using NP_API.Common;
using NP_API.Dto.Custom;
using NP_API.Dto.Request.Feedback;

namespace NP_API.Service.Interface
{
    public interface IFeedbackService
    {
        public Task<PagedList<FeedbackResultDto>> GetData(GetDto dto);
        public Task<FeedbackResultDto> Create(NewFeedbackDto dto);
        public Task<FeedbackResultDto> Update(UpdateFeedbackDto dto);
        public Task<bool> Delete(DeleteDto Id);
    }
}
