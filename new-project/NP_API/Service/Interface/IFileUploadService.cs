﻿namespace NP_API.Service.Interface
{
    public interface IFileUploadService
    {
        public Task<string> UploadFileAsync(IFormFile file, string? prefix);
        public Task<bool> MoveFileAsync(string sourceObject, string destinationObject);
    }
}
