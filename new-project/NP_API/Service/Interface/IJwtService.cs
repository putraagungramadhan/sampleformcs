﻿using NP_API.Models;
using NP_API.Dto;

namespace NP_API.Service.Interface
{
    public interface IJwtService
    {
        public string generateToken(User user);
        public JwtDto decode(string token);
    }
}
