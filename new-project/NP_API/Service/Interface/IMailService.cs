﻿using NP_API.Dto;

namespace NP_API.Service.Interface
{
    public interface IMailService
    {
        public Task<bool> SendMail(MailDto mailDto);
    }
}
