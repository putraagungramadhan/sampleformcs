﻿using NP_API.Common;
using NP_API.Dto.Custom;
using NP_API.Dto;
using NP_API.Dto.Request.NewsCategory;
using NP_API.Models;

namespace NP_API.Service.Interface {
    public interface INewsCategoryService
    {

        public Task<PagedList<NewsCategoryResultDto>> GetData(GetDto dto);
        public Task<NewsCategoryResultDto>Create (NewNewsCategoryDto dto);
        public Task<NewsCategoryResultDto> Update(UpdateNewsCategoryDto dto);
        public Task<bool> Delete(DeleteDto Id);
    }
}


