﻿using NP_API.Common;
using NP_API.Dto.Custom;
using NP_API.Dto.Request.News;

namespace NP_API.Service.Interface
{
    public interface INewsService
    {
        public Task<PagedList<NewsResultDto>> GetData(GetDto dto);
        public Task<NewsResultDto> Create(NewNewsDto dto);
        public Task<NewsResultDto> Update(UpdateNewsDto dto);
        public Task<bool> Delete(DeleteDto Id);
    }
}
