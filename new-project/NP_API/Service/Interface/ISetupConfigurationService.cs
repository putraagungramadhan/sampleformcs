﻿using NP_API.Common;
using NP_API.Dto.Custom;
using NP_API.Dto.Request.SetupConfigurations;

namespace NP_API.Service.Interface
{
    public interface ISetupConfigurationService
    {
        public Task<PagedList<SetupConfigurationResultDto>> GetData(GetDto dto);
        public Task<SetupConfigurationResultDto> Create(NewSetupConfigurationDto dto);
        public Task<SetupConfigurationResultDto> Update(UpdateSetupConfigurationDto dto);
        public Task<bool> Delete(DeleteDto dto);
    }
}
