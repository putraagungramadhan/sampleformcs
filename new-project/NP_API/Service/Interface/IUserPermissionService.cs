﻿using NP_API.Common;
using NP_API.Dto.Custom;
using NP_API.Dto;
using NP_API.Dto.Request.UserPermission;
using NP_API.Models;

namespace NP_API.Service.Interface
{
    public interface IUserPermissionService
    {
        public Task<PagedList<UserPermissionResultDto>> GetData(GetDto dto);
        public Task<Object> GetByUser(Guid UserGuid);
        public Task<List<Module>> GetModule(bool canEmployee);
        public Task<UserPermissionResultDto> Save(UserPermissionDto dto);
        public Task<UserPermissionResultDto> Update(UpdateUserPermissionDto dto);
        public Task<bool> Delete(DeleteDto dto);
    }
}
