﻿using NP_API.Common;
using NP_API.Dto.Request.User;

namespace NP_API.Service.Interface
{
    public interface IUserService
    {
        public Task<PagedList<UserResultDto>> GetData(GetDto dto);
        public Task<PagedList<UserResultDto>> GetDataByRole(GetDto dto);
        public Task<UserResultDto> Create(NewUserDto dto);
        public Task<bool> UpdatePassword(UpdateUserPasswordDto user);
    }
}
