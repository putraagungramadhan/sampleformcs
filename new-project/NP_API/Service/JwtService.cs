﻿using NP_API.Config;
using NP_API.Dto;
using NP_API.Models;
using NP_API.Service.Interface;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace NP_API.Service
{
    public class JwtService : IJwtService
    {
        private readonly JwtConfig jwtConfig;

        public JwtService(IOptionsMonitor<JwtConfig> optionsMonitor)
        {
            this.jwtConfig = optionsMonitor.CurrentValue;
        }
        public string generateToken(User user)
        {
            var jwtTokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(jwtConfig.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[] {
              new Claim( "id", user.Id.ToString()),
              new Claim( "companyGuid", user.CompanyGuid.ToString()),
              new Claim( "userGuid", user.Guid.ToString()),
              new Claim( "email", user.Email),
              new Claim( "name", user.Name),
              new Claim( "role", user.Role)
              //new Claim( "role", user.RoleType.ToString())
            }),
                Expires = DateTime.UtcNow.AddMinutes(480),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = jwtTokenHandler.CreateToken(tokenDescriptor);
            var jwtToken = jwtTokenHandler.WriteToken(token);
            return jwtToken;
        }

        public JwtDto decode(string tokenString)
        {
            JwtDto dto = new JwtDto();
            if (tokenString != "" && tokenString != null)
            {
                // Trim 'Bearer ' from the start since its just a prefix for the token
                var jwtEncodedString = tokenString.Substring(7);

                // Instantiate a new Jwt Security Token from the Jwt Encoded String
                var token = new JwtSecurityToken(jwtEncodedString);

                dto.Id = token.Claims.First(c => c.Type == "id").Value;
                dto.CompanyGuid = token.Claims.First(c => c.Type == "companyGuid").Value;
                dto.UserGuid = token.Claims.First(c => c.Type == "userGuid").Value;
                dto.Name = token.Claims.First(c => c.Type == "name").Value;
                dto.Email = token.Claims.First(c => c.Type == "email").Value;
                dto.Role = token.Claims.First(c => c.Type == "role").Value;
            }
            
            return dto;
        }
    }
}
