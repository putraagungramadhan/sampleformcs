﻿using Microsoft.Extensions.Localization;
using System.Reflection;

namespace NP_API.Service
{
    public class SharedResources
    {

    }
    public class LanguageService
    {
        private readonly IStringLocalizer _stringLocalizer;
        public LanguageService(IStringLocalizerFactory factory)
        {
            var type = typeof(SharedResources);
            var assemblyName = new AssemblyName(type.GetTypeInfo().Assembly.FullName);
            _stringLocalizer = factory.Create("SharedResources", assemblyName.Name);
        }

        public LocalizedString Getkey(string key)
        {

            return _stringLocalizer[key];
        }
        
    }
}
