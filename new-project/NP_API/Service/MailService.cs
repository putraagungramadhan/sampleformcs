﻿using NP_API.Dto;
using NP_API.Dto.Request.SetupConfigurations;
using NP_API.Exceptions;
using NP_API.Repository.Interface;
using NP_API.Service.Interface;
using MailKit.Net.Smtp;
using MimeKit;

namespace NP_API.Service
{
    public class MailService : IMailService
    {
        private readonly ISetupConfigurationRepository _setupRepository;
        private LanguageService _localization;

        public MailService(ISetupConfigurationRepository setupRepository, LanguageService localization)
        {
            this._setupRepository = setupRepository;
            this._localization = localization;
        }

        public async Task<bool> SendMail(MailDto mailDto)
        {
            var param = new GetDto { SubTypeName = "MailSettings" };
            var setupConfiguration = await _setupRepository.GetData(param);
            if (setupConfiguration == null)
            {
                throw new NotFoundException(_localization.Getkey("setup_config_id_not_found"));
            }

            var mailSetup = setupConfiguration.Items;
            var server = mailSetup.FirstOrDefault(f => f.TypeName == "Server")?.TypeValue;
            var port = int.Parse(mailSetup.FirstOrDefault(f => f.TypeName == "Port")?.TypeValue);
            var userName = mailSetup.FirstOrDefault(f => f.TypeName == "UserName")?.TypeValue;
            var password = mailSetup.FirstOrDefault(f => f.TypeName == "Password")?.TypeValue;
            var senderName = mailSetup.FirstOrDefault(f => f.TypeName == "SenderName")?.TypeValue;
            var senderEmail = mailSetup.FirstOrDefault(f => f.TypeName == "SenderEmail")?.TypeValue;

            using (MimeMessage emailMessage = new MimeMessage())
            {
                emailMessage.From.Add(new MailboxAddress(senderName, senderEmail));
                emailMessage.To.Add(new MailboxAddress(mailDto.EmailToName, mailDto.EmailToId));
                emailMessage.Cc.Add(new MailboxAddress(mailDto.EmailCcName, mailDto.EmailCcId));
                //emailMessage.Cc.Add(new MailboxAddress("Cc Receiver", "maniskntl71@gmail.com"));
                emailMessage.Subject = mailDto.EmailSubject;

                BodyBuilder emailBodyBuilder = new BodyBuilder();
                emailBodyBuilder.HtmlBody = mailDto.EmailBody;
                emailMessage.Body = emailBodyBuilder.ToMessageBody();
                //this is the SmtpClient from the Mailkit.Net.Smtp namespace, not the System.Net.Mail one
                using (SmtpClient mailClient = new SmtpClient())
                {
                    await mailClient.ConnectAsync(server, port, MailKit.Security.SecureSocketOptions.StartTls);
                    await mailClient.AuthenticateAsync(userName, password);
                    await mailClient.SendAsync(emailMessage);
                    await mailClient.DisconnectAsync(true);
                }
            }

            return true;
        }
    }
}
