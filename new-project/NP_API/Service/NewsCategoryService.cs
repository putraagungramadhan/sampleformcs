﻿using NP_API.Dto;
using NP_API.Dto.Custom;
using NP_API.Service.Interface;
using NP_API.Dto.Request.NewsCategory;
using NP_API.Repository.Interface;
using NP_API.Common;
using NP_API.Models;
using NP_API.Helper;
using AutoMapper;
using NP_API.Exceptions;
using System.Transactions;

namespace NP_API.Service { 
 
        public class NewsCategoryService : INewsCategoryService
        {
        private readonly INewsCategoryRepository _newsCategoryRepository;
        private readonly ICompanyRepository _companyRepository;
        private readonly IJwtService _jwtService;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly JwtDto? _token;
        private LanguageService _localization;
        private IMapper _mapper;

        public NewsCategoryService(INewsCategoryRepository newsCategoryRepository, ICompanyRepository companyRepository,
        IJwtService jwtService, IHttpContextAccessor httpContextAccessor, LanguageService localization,
        IMapper mapper)
        {
            this._newsCategoryRepository = newsCategoryRepository;
            this._companyRepository = companyRepository;
            this._jwtService = jwtService;
            this._localization = localization;
            this._httpContextAccessor = httpContextAccessor;
            this._mapper = mapper;
            this._token = _jwtService.decode(_httpContextAccessor.HttpContext.Request.Headers["authorization"]);
        }
        public async Task<PagedList<NewsCategoryResultDto>> GetData(GetDto dto)
        {
            return await _newsCategoryRepository.GetData(dto);
        }
        public async Task<NewsCategoryResultDto> Create(NewNewsCategoryDto dto)
        {
            NewsCategory categories = _mapper.Map<NewsCategory>(dto);
            categories.Guid = Guid.NewGuid();
            categories.CompanyGuid = GeneralHelper.ConvertToGuid(_token.CompanyGuid);
            categories.CreatedBy = _token.Id;
            categories.UpdatedBy = _token.Id;
            NewsCategoryResultDto retModel = await _newsCategoryRepository.Save(categories);

            return retModel;
        }
    public async Task<NewsCategoryResultDto> Update(UpdateNewsCategoryDto dto)
    {
        NewsCategory model = await _newsCategoryRepository.FindByGuid(dto.Guid);
        if (model == null)
        {
            throw new NotFoundException(_localization.Getkey("news_category_id_not_found"));
        }

            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    NewsCategory modelModified = _mapper.Map<NewsCategory>(dto);
                    modelModified.Id = model.Id;
                    modelModified.CompanyGuid = GeneralHelper.ConvertToGuid(_token.CompanyGuid);
                    modelModified.UpdatedAt = (long)DateTime.UtcNow.ToUniversalTime().Subtract(DateTime.UnixEpoch).TotalSeconds;
                    modelModified.UpdatedBy = _token.Id;
                    NewsCategoryResultDto retModel = await _newsCategoryRepository.Update(modelModified);

                    return retModel;
                }
                catch(Exception) {
                    throw;
                }
            }
    }
    public async Task<bool> Delete(DeleteDto dto)
    {
        NewsCategory grade = await _newsCategoryRepository.FindByGuid(dto.Guid);
        if (grade == null)
        {
            throw new NotFoundException(_localization.Getkey("news_category_id_not_found"));
        }
        return await _newsCategoryRepository.Delete(dto.Guid);
    }

}
}


