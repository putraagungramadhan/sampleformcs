﻿using AutoMapper;
using NP_API.Common;
using NP_API.Dto;
using NP_API.Dto.Custom;
using NP_API.Dto.Request.News;
using NP_API.Exceptions;
using NP_API.Helper;
using NP_API.Models;
using NP_API.Repository;
using NP_API.Repository.Interface;
using NP_API.Service.Interface;
using Slugify;
using System.Transactions;

namespace NP_API.Service
{
    public class NewsService : INewsService
    {
        private readonly INewsRepository _newsRepository;
        private readonly INewsCategoryRepository _newsCategoryRepository;
        private readonly ICompanyRepository _companyRepository;
        private readonly IJwtService _jwtService;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly JwtDto? _token;
        private LanguageService _localization;
        private IMapper _mapper;
        private readonly IFileUploadService _uploadService;
        private readonly ISlugHelper _slugHelper;


        public NewsService(INewsRepository newsRepository, INewsCategoryRepository newsCategoryRepository, ICompanyRepository companyRepository,
            IJwtService jwtService, IHttpContextAccessor httpContextAccessor, LanguageService localization,
            IMapper mapper, IFileUploadService fileUploadService, ISlugHelper slugHelper)
        {
            this._newsRepository = newsRepository;
            this._newsCategoryRepository = newsCategoryRepository;
            this._companyRepository = companyRepository;
            this._jwtService = jwtService;
            this._localization = localization;
            this._httpContextAccessor = httpContextAccessor;
            this._mapper = mapper;
            this._token = _jwtService.decode(_httpContextAccessor.HttpContext.Request.Headers["authorization"]);
            this._uploadService = fileUploadService;
            _slugHelper = slugHelper;
        }

        public async Task<PagedList<NewsResultDto>> GetData(GetDto dto)
        {
            return await _newsRepository.GetData(dto);
        }

        public async Task<NewsResultDto> Create(NewNewsDto dto)
        {
           
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    News model = _mapper.Map<News>(dto);

                    model.Guid = Guid.NewGuid();
                    model.Slug = _slugHelper.GenerateSlug(dto.Title);
                    model.CompanyGuid = GeneralHelper.ConvertToGuid(_token.CompanyGuid);
                    model.CreatedBy = _token.Id;
                    model.UpdatedBy = _token.Id;
                    NewsResultDto retModel = await _newsRepository.Save(model);
                    //upload coverImage
                    if (dto.CoverImage != null && dto.CoverImage.Length > 0)
                    {
                        model.CoverImage = await _uploadService.UploadFileAsync(dto.CoverImage, "news/" + model.Guid.ToString() + "/coverimage/");
                        NewsResultDto retCoverImage = await _newsRepository.UpdateCoverImage(model);
                    }
                    scope.Complete();
                    return retModel;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public async Task<NewsResultDto> Update(UpdateNewsDto dto)
        {
           
            News model = await _newsRepository.FindByGuid(dto.Guid);
            if (model == null)
            {
                throw new NotFoundException(_localization.Getkey("news_id_not_found"));
            }
          
          
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    News modelModified = _mapper.Map<News>(dto);
                    modelModified.CompanyGuid = GeneralHelper.ConvertToGuid(_token.CompanyGuid);
                    model.Slug = _slugHelper.GenerateSlug(dto.Title);
                    modelModified.CoverImage = model.CoverImage;
                    modelModified.Id = model.Id;
                    modelModified.UpdatedAt = (long)DateTime.UtcNow.ToUniversalTime().Subtract(DateTime.UnixEpoch).TotalSeconds;
                    modelModified.UpdatedBy = _token.Id;
                    NewsResultDto retModel = await _newsRepository.Update(modelModified);
                    //upload poster
                    if (dto.CoverImage != null && dto.CoverImage.Length > 0)
                    {
                        model.CoverImage = await _uploadService.UploadFileAsync(dto.CoverImage, "news/" + model.Guid.ToString() + "/coverimage/");
                        NewsResultDto retPoster = await _newsRepository.UpdateCoverImage(model);
                    }
                    scope.Complete();
                    return retModel;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public async Task<bool> Delete(DeleteDto dto)
        {
           
            News model = await _newsRepository.FindByGuid(dto.Guid);
            if (model == null)
            {
                throw new NotFoundException(_localization.Getkey("news_id_not_found"));
            }
            return await _newsRepository.Delete(dto.Guid);
        }
    }

}
