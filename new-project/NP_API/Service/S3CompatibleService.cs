﻿using Amazon.S3;
using Amazon.S3.Model;
using Amazon.S3.Transfer;
using NP_API.Config;
using NP_API.Exceptions;
using NP_API.Service.Interface;
using Microsoft.Extensions.Options;
using Minio;
using Minio.DataModel.Args;
using System.Net;

namespace NP_API.Service
{
    public class S3CompatibleService : IFileUploadService
    {
        private readonly S3Config _config;
        private readonly IMinioClient _minioClient;

        public S3CompatibleService(IOptionsMonitor<S3Config> optionsMonitor)
        {
            this._config = optionsMonitor.CurrentValue;
            this._minioClient = new MinioClient()
                                    .WithEndpoint(_config.Endpoint)
                                    .WithCredentials(_config.AccessKey, _config.SecretAccessKey)
                                    .WithSSL()
                                    .Build();
        }
        
        public async Task<string> UploadFileAsync(IFormFile file, string? prefix)
        {
            var beArgs = new BucketExistsArgs()
                    .WithBucket(_config.BucketName);
            bool bucketExists = await _minioClient.BucketExistsAsync(beArgs).ConfigureAwait(false);
            
            if (!bucketExists) throw new NotFoundException($"Bucket {_config.BucketName} tidak ditemukan.");
            using (var stream = file.OpenReadStream())
            {
                var objectName = string.IsNullOrEmpty(prefix) ? file.FileName : $"{prefix?.TrimEnd('/')}/{file.FileName}";
                // Upload the file to Minio using PutObjectAsync with ObjectPutArgs
                var objectPutArgs = new PutObjectArgs()
                    .WithBucket(_config.BucketName)
                    .WithObject(objectName)
                    .WithObjectSize(file.Length)
                    .WithContentType(file.ContentType)
                    .WithStreamData(file.OpenReadStream());
                
                // Upload the file to Minio
                await _minioClient.PutObjectAsync(objectPutArgs).ConfigureAwait(false);

                var policyJson =
                $@"{{""Version"":""2012-10-17"",""Statement"":[{{""Action"":[""s3:GetBucketLocation""],""Effect"":""Allow"",""Principal"":{{""AWS"":[""*""]}},""Resource"":[""arn:aws:s3:::{_config.BucketName}""],""Sid"":""""}},{{""Action"":[""s3:ListBucket""],""Effect"":""Allow"",""Principal"":{{""AWS"":[""*""]}},""Resource"":[""arn:aws:s3:::{_config.BucketName}""],""Sid"":""""}},{{""Action"":[""s3:GetObject""],""Effect"":""Allow"",""Principal"":{{""AWS"":[""*""]}},""Resource"":[""arn:aws:s3:::{_config.BucketName}/*""],""Sid"":""""}}]}}";
                // Change policy type parameter
                var args = new SetPolicyArgs()
                    .WithBucket(_config.BucketName)
                    .WithPolicy(policyJson);
                await _minioClient.SetPolicyAsync(args).ConfigureAwait(false);

            }

            return file.FileName;
        }
        public async Task<bool> MoveFileAsync(string sourceObject, string destinationObject)
        {
            var source = new CopySourceObjectArgs()
                .WithBucket(_config.BucketName)
                .WithObject(sourceObject);
            var copyArgs = new CopyObjectArgs()
                .WithBucket(_config.BucketName)
                .WithObject(destinationObject)
                .WithCopyObjectSource(source);
            Console.WriteLine(_config.BucketName + sourceObject);
            Console.WriteLine(_config.BucketName + destinationObject);
            await _minioClient.CopyObjectAsync(copyArgs);
            // Remove the object from the source folder
            //var remove = new RemoveObjectArgs().WithBucket(_config.BucketName).WithObject(sourceObject);
            //await _minioClient.RemoveObjectAsync(remove);

            return true;
        }
    }
}
