﻿using AutoMapper;
using NP_API.Common;
using NP_API.Dto;
using NP_API.Dto.Custom;
using NP_API.Dto.Request.SetupConfigurations;
using NP_API.Exceptions;
using NP_API.Models;
using NP_API.Repository.Interface;
using NP_API.Service.Interface;
using System.Transactions;

namespace NP_API.Service
{
    public class SetupConfigurationService : ISetupConfigurationService
    {
        private readonly ISetupConfigurationRepository _thisrepo;
        private readonly IJwtService _jwtService;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly JwtDto? _token;
        private LanguageService _localization;
        private IMapper _mapper;

        public SetupConfigurationService(ISetupConfigurationRepository setupConfigurationRepository, IJwtService jwtService, IHttpContextAccessor httpContextAccessor, LanguageService localization, IMapper mapper)
        {
            this._thisrepo = setupConfigurationRepository;
            this._jwtService = jwtService;
            this._localization = localization;
            this._httpContextAccessor = httpContextAccessor;
            this._mapper = mapper;
            this._token = _jwtService.decode(_httpContextAccessor.HttpContext.Request.Headers["authorization"]);
        }

        public async Task<PagedList<SetupConfigurationResultDto>> GetData(GetDto dto)
        {
            return await _thisrepo.GetData(dto);
        }

        public async Task<SetupConfigurationResultDto> Create(NewSetupConfigurationDto dto)
        {

            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    SetupConfigurations model = _mapper.Map<SetupConfigurations>(dto);

                    model.Guid = Guid.NewGuid();
                    model.CreatedBy = _token.Id;
                    model.UpdatedBy = _token.Id;
                    model.IsActive = true;
                    SetupConfigurationResultDto retModel = await _thisrepo.Save(model);
                    
                    scope.Complete();
                    return retModel;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public async Task<SetupConfigurationResultDto> Update(UpdateSetupConfigurationDto dto)
        {

            SetupConfigurations model = await _thisrepo.FindByGuid(dto.Guid);
            if (model == null)
            {
                throw new NotFoundException(_localization.Getkey("setup_config_id_not_found"));
            }

            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    SetupConfigurations modelModified = _mapper.Map<SetupConfigurations>(dto);
                    modelModified.Id = model.Id;
                    modelModified.UpdatedAt = (long)DateTime.UtcNow.ToUniversalTime().Subtract(DateTime.UnixEpoch).TotalSeconds;
                    modelModified.UpdatedBy = _token.Id;
                    SetupConfigurationResultDto retModel = await _thisrepo.Update(modelModified);
                    
                    scope.Complete();
                    return retModel;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public async Task<bool> Delete(DeleteDto dto)
        {

            var model = await _thisrepo.FindByGuid(dto.Guid);
            if (model == null)
            {
                throw new NotFoundException(_localization.Getkey("setup_config_id_not_found"));
            }
            return await _thisrepo.Delete(dto.Guid);
        }
    }
}
