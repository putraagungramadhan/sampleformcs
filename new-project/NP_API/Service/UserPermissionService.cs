﻿using AutoMapper;
using NP_API.Common;
using NP_API.Dto;
using NP_API.Dto.Custom;
using NP_API.Dto.Request.UserPermission;
using NP_API.Exceptions;
using NP_API.Helper;
using NP_API.Models;
using NP_API.Repository;
using NP_API.Repository.Interface;
using NP_API.Service.Interface;

namespace NP_API.Service
{
    public class UserPermissionService : IUserPermissionService
    {
        private readonly IUserPermissionRepository _userPermissionRepository;
        private readonly IModuleRepository _moduleRepository;
        private readonly IJwtService _jwtService;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly JwtDto? _token;
        private LanguageService _localization;
        private IMapper _mapper;
        public UserPermissionService(IUserPermissionRepository userPermissionRepository,
            IFileUploadService uploadService, IModuleRepository moduleRepository,
            IJwtService jwtService, IHttpContextAccessor httpContextAccessor, LanguageService languageService,
            IMapper mapper)
        {
            _userPermissionRepository = userPermissionRepository;
            _moduleRepository = moduleRepository;
            _jwtService = jwtService;
            _httpContextAccessor = httpContextAccessor;
            _localization = languageService;
            _mapper = mapper;
            _token = _jwtService.decode(_httpContextAccessor.HttpContext.Request.Headers["authorization"]);
        }
        public async Task<List<Module>> GetModule(bool canEmployee)
        {
            return await _moduleRepository.GetAll(canEmployee);
        }
        public async Task<PagedList<UserPermissionResultDto>> GetData(GetDto dto)
        {
            return await _userPermissionRepository.GetData(dto,GeneralHelper.ConvertToGuid(_token.CompanyGuid));
        }
        public async Task<Object> GetByUser(Guid UserGuid)
        {
            return await _userPermissionRepository.GetByUser(UserGuid);
        }
        public async Task<UserPermissionResultDto> Save(UserPermissionDto dto)
        {
            UserPermission model = _mapper.Map<UserPermission>(dto);

            model.Guid = Guid.NewGuid();
            model.CompanyGuid = GeneralHelper.ConvertToGuid(_token.CompanyGuid);
            model.CreatedBy = _token.Id;
            model.UpdatedBy = _token.Id;
            UserPermissionResultDto retModel = await _userPermissionRepository.Save(model);

            return retModel;
        }
        public async Task<UserPermissionResultDto> Update(UpdateUserPermissionDto dto)
        {
            UserPermission model = await _userPermissionRepository.FindByGuid(dto.Guid);
            if (model == null)
            {
                throw new NotFoundException("user_permission_not_found");
            }
            UserPermission modelModified = _mapper.Map<UserPermission>(dto);
            modelModified.CompanyGuid = GeneralHelper.ConvertToGuid(_token.CompanyGuid);
            modelModified.Id = model.Id;
            modelModified.ModuleGuid = model.ModuleGuid;
            modelModified.UserGuid = model.UserGuid;
            modelModified.UpdatedAt = (long)DateTime.UtcNow.ToUniversalTime().Subtract(DateTime.UnixEpoch).TotalSeconds;
            modelModified.UpdatedBy = _token.Id;
            UserPermissionResultDto retModel = await _userPermissionRepository.Update(modelModified);
            return retModel;
        }
        public async Task<bool> Delete(DeleteDto dto)
        {
            UserPermission model = await _userPermissionRepository.FindByGuid(dto.Guid);
            if (model == null)
            {
                throw new NotFoundException(_localization.Getkey("user_permission_not_found"));
            }
            return await _userPermissionRepository.Delete(dto.Guid);
        }
    }
}
