﻿using AutoMapper;
using NP_API.Common;
using NP_API.Dto;
using NP_API.Dto.Request.User;
using NP_API.Dto.Request.UserPermission;
using NP_API.Exceptions;
using NP_API.Helper;
using NP_API.Models;
using NP_API.Repository.Interface;
using NP_API.Service.Interface;
using System.Transactions;

namespace NP_API.Service
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        private readonly IUserPermissionRepository _userPermissionRepository;
        private readonly IJwtService _jwtService;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly JwtDto? _token;
        private LanguageService _localization;
        private IMapper _mapper;
        public UserService(IUserRepository userRepository, 
            IFileUploadService uploadService, IUserPermissionRepository userPermissionRepository,
            IJwtService jwtService, IHttpContextAccessor httpContextAccessor, LanguageService languageService,
            IMapper mapper)
        {
            _userPermissionRepository = userPermissionRepository;
            _userRepository = userRepository;
            _jwtService = jwtService;
            _httpContextAccessor = httpContextAccessor;
            _localization = languageService;
            _mapper = mapper;
            _token = _jwtService.decode(_httpContextAccessor.HttpContext.Request.Headers["authorization"]);
        }
        public async Task<PagedList<UserResultDto>> GetData(Dto.Request.User.GetDto dto)
        {
            return await _userRepository.GetData(dto, GeneralHelper.ConvertToGuid(_token.CompanyGuid));
        }

        public async Task<PagedList<UserResultDto>> GetDataByRole(Dto.Request.User.GetDto dto)
        {
            return await _userRepository.GetDataByRole(dto);
        }

        public async Task<UserResultDto> Create(NewUserDto dto)
        {
            
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    User model = _mapper.Map<User>(dto);
                    model.Password = GeneralHelper.HashPassword(dto.Password);
                    model.Guid = Guid.NewGuid();
                    model.CreatedBy = _token.Id;
                    model.UpdatedBy = _token.Id;
                    model = await _userRepository.Save(model);
                    UserResultDto retUser = _mapper.Map<UserResultDto>(model);
                    //add permission
                    foreach (var item in dto.Permissions)
                    {
                        UserPermission permission = new UserPermission();
                        permission.Guid = Guid.NewGuid();
                        permission.CompanyGuid = (Guid)retUser.CompanyGuid;
                        permission.UserGuid = retUser.Guid;
                        permission.ModuleGuid = item.ModuleGuid;
                        permission.CanWrite = item.CanWrite;
                        permission.CreatedBy = _token.Id;
                        permission.UpdatedBy = _token.Id;
                        UserPermissionResultDto retPermission = await _userPermissionRepository.Save(permission);
                    }
                    
                    scope.Complete();
                    return retUser;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }
        public async Task<bool> UpdatePassword(UpdateUserPasswordDto dto)
        {
            User model = await _userRepository.FindByGuid(dto.Guid);
            if (model == null)
            {
                throw new NotFoundException(_localization.Getkey("user_id_not_found"));
            }
            model.Password = GeneralHelper.HashPassword(dto.Password);
            model = await _userRepository.UpdatePassword(model);
            return true;
        }
    }
}
