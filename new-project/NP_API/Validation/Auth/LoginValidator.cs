﻿using FluentValidation;
using NP_API.Dto.Custom;
using NP_API.Dto.Request.Company;

namespace NP_API.Validation.Auth
{
    public class LoginValidator : AbstractValidator<LoginDto>
    {
        public LoginValidator()
        {
            RuleFor(x => x.Key).NotNull().NotEmpty().WithMessage("Username/email tidak boleh kosong");
            RuleFor(x => x.Password).NotNull().NotEmpty().WithMessage("Kata sandi tidak boleh kosong");
        }
    }
}
