﻿using FluentValidation;
using NP_API.Dto;
using NP_API.Dto.Request.Company;

namespace NP_API.Validation.Company
{
    public class SaveValidator : AbstractValidator<NewCompanyDto>
    {
        public SaveValidator()
        {
            //RuleFor(x => x.Logo).NotEmpty().NotNull().WithMessage("Logo tidak boleh kosong");
            RuleFor(x => x.CompanyDto.Name).NotEmpty().NotNull().WithMessage("Nama perusahaan tidak boleh kosong");
            RuleFor(x => x.CompanyDto.Email).NotEmpty().NotNull().WithMessage("Email perusahaan tidak boleh kosong");
            RuleFor(x => x.CompanyDto.PhoneNo).NotEmpty().NotNull().WithMessage("Email perusahaan tidak boleh kosong");
            RuleFor(x => x.CompanyDto.Field).NotEmpty().NotNull().WithMessage("Bidang perusahaan tidak boleh kosong");
            RuleFor(x => x.CompanyDto.Website).NotEmpty().NotNull().WithMessage("Website perusahaan tidak boleh kosong");
            RuleFor(x => x.CompanyDto.Theme).NotEmpty().NotNull().WithMessage("Tema perusahaan tidak boleh kosong");
            RuleFor(x => x.CompanyDto.ProvinceGuid).NotNull().NotEmpty().WithMessage("Provinsi perusahaan tidak boleh kosong");
            RuleFor(x => x.CompanyDto.CityGuid).NotNull().NotEmpty().WithMessage("Kota perusahaan tidak boleh kosong");
            RuleFor(x => x.CompanyDto.Address).NotEmpty().NotNull().WithMessage("Alamat perusahaan tidak boleh kosong");
            RuleFor(x => x.CompanyDto.PostCode).NotNull().NotEmpty().WithMessage("Kode pos perusahaan tidak boleh kosong");
            RuleFor(x => x.CompanyDto.Email).EmailAddress();
            RuleFor(x => x.CompanyDto.CompanyCode).NotEmpty().WithMessage("Kode perusahaan tidak boleh kosong")
                .NotNull().WithMessage("Kode perusahaan tidak boleh kosong")
                .Length(3).WithMessage("Kode perusahaan harus 3 digit.")
                .Must(BeValidDigits).WithMessage("Kode perusahaan harus berupa angka.");
        }
        private bool BeValidDigits(string value)
        {
            return !string.IsNullOrEmpty(value) && value.All(char.IsDigit);
        }
    }
}
