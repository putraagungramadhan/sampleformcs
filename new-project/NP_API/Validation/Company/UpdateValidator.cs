using FluentValidation;
using NP_API.Dto.Request.Company;

namespace NP_API.Validation.Company
{
    public class UpdateValidator : AbstractValidator<UpdateCompanyDto>
    {
        public UpdateValidator()
        {
            RuleFor(x => x.Name).NotEmpty().NotNull().WithMessage("Nama perusahaan tidak boleh kosong");
            RuleFor(x => x.Email).NotEmpty().NotNull().WithMessage("Email perusahaan tidak boleh kosong");
            RuleFor(x => x.PhoneNo).NotEmpty().NotNull().WithMessage("Email perusahaan tidak boleh kosong");
            RuleFor(x => x.Field).NotEmpty().NotNull().WithMessage("Bidang perusahaan tidak boleh kosong");
            RuleFor(x => x.Website).NotEmpty().NotNull().WithMessage("Website perusahaan tidak boleh kosong");
            RuleFor(x => x.Theme).NotEmpty().NotNull().WithMessage("Tema perusahaan tidak boleh kosong");
            RuleFor(x => x.ProvinceGuid).NotNull().NotEmpty().WithMessage("Provinsi perusahaan tidak boleh kosong");
            RuleFor(x => x.CityGuid).NotNull().NotEmpty().WithMessage("Kota perusahaan tidak boleh kosong");
            RuleFor(x => x.Address).NotEmpty().NotNull().WithMessage("Alamat perusahaan tidak boleh kosong");
            RuleFor(x => x.PostCode).NotNull().NotEmpty().WithMessage("Kode pos perusahaan tidak boleh kosong");
            RuleFor(x => x.Email).EmailAddress();
            RuleFor(x => x.IsHolding).NotEmpty().WithMessage("Penanda holding tidak boleh kosong");
        }
    }
}