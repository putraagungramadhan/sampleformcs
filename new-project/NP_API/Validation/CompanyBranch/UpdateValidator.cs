﻿using FluentValidation;
using NP_API.Dto.Request.CompanyBranch;

namespace NP_API.Validation.CompanyBranch
{
    public class UpdateValidator : AbstractValidator<UpdateCompanyBranchDto>
    {
        public UpdateValidator()
        {
            RuleFor(x => x.Guid).NotEmpty().WithMessage("ID cabang tidak boleh kosong")
                .NotNull().WithMessage("ID cabang tidak boleh kosong");
            RuleFor(x => x.Email).NotEmpty().WithMessage("Email tidak boleh kosong")
                .NotNull().WithMessage("Email tidak boleh kosong")
                .EmailAddress().WithMessage("Email format tidak valid. Contoh contoh@gmail.com.");
            RuleFor(x => x.Name).NotEmpty().WithMessage("Nama cabang tidak boleh kosong")
                .NotNull().WithMessage("Nama cabang tidak boleh kosong");
            RuleFor(x => x.PhoneNo).NotEmpty().WithMessage("Nomor telepon tidak boleh kosong")
                .NotNull().WithMessage("Nomor telepon tidak boleh kosong");
            RuleFor(x => x.ProvinceGuid).NotEmpty().WithMessage("Provinsi tidak boleh kosong")
                .NotNull().WithMessage("Provinsi tidak boleh kosong");
            RuleFor(x => x.CityGuid).NotEmpty().WithMessage("Kota tidak boleh kosong")
                .NotNull().WithMessage("Kota tidak boleh kosong");
            RuleFor(x => x.Address).NotEmpty().WithMessage("Alamat tidak boleh kosong")
                .NotNull().WithMessage("Alamat tidak boleh kosong");
            RuleFor(x => x.PostCode).NotEmpty().WithMessage("Kode pos tidak boleh kosong")
                .NotNull().WithMessage("Kode pos tidak boleh kosong");
        }
    }
}
