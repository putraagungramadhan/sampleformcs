﻿using FluentValidation;
using NP_API.Dto;

namespace NP_API.Validation.CompanyClass
{
    public class CreateValidator : AbstractValidator<CompanyClassDto>
    {
        public CreateValidator()
        {
            RuleFor(x => x.CompanyGradeGuid).NotEmpty().NotNull().WithMessage("ID grade perusahaan tidak boleh kosong");
            RuleFor(x => x.Name).NotEmpty().WithMessage("Nama class tidak boleh kosong");
            RuleFor(x => x.Name).NotNull().WithMessage("Nama class tidak boleh kosong");
        }
    }
}
