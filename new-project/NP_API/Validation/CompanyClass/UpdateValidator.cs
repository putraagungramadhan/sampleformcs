﻿using FluentValidation;
using NP_API.Dto.Request.CompanyClass;

namespace NP_API.Validation.CompanyClass
{
    public class UpdateValidator : AbstractValidator<UpdateClassDto>
    {
        public UpdateValidator()
        {
            RuleFor(x => x.Guid).NotEmpty().NotNull().WithMessage("ID grade perusahaan tidak boleh kosong");
            RuleFor(x => x.CompanyGradeGuid).NotEmpty().NotNull().WithMessage("ID Grade perusahaan tidak boleh kosong");
            RuleFor(x => x.Name).NotEmpty().WithMessage("ID perusahaan tidak boleh kosong");
            RuleFor(x => x.Name).NotNull().WithMessage("ID perusahaan tidak boleh kosong");
        }
    }
}
