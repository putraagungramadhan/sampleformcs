﻿using FluentValidation;
using NP_API.Dto.Request.CompanyDocument;

namespace NP_API.Validation.CompanyDocument
{
    public class CreateValidator : AbstractValidator<NewCompanyDocumentDto>
    {
        public CreateValidator() 
        {
            RuleFor(x => x.Name).NotNull().WithMessage("Nama dokumen perusahaan tidak boleh kosong");
            RuleFor(x => x.File).NotNull().WithMessage("File dokumen perusahaan tidak boleh kosong")
                .Must(file => file == null || BeAValidFileSize(file)).WithMessage("Ukuran file lebih besar dari yang diperbolehkan (2 MB).")
                .Must(file => file == null || BeAValidMimeType(file, new[] { "application/pdf" })).WithMessage("File harus berformat pdf");
        }
        private bool BeAValidMimeType(IFormFile file, string[] mimeTypes)
        {
            var contentType = file?.ContentType;
            return contentType != null && mimeTypes.Contains(contentType);
        }
        private bool BeAValidFileSize(IFormFile file)
        {
            var length = file?.Length;
            return length != null && length <= 2 * 1024 * 1024;
        }
    }
}
