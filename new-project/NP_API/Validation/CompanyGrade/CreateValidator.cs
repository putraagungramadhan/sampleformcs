﻿using FluentValidation;
using NP_API.Dto;
using NP_API.Dto.Request.Company;

namespace NP_API.Validation.CompanyGrade
{
    public class CreateValidator : AbstractValidator<CompanyGradeDto>
    {
        public CreateValidator() 
        {
            RuleFor(x => x.Name).NotEmpty().WithMessage("Nama grade tidak boleh kosong");
            RuleFor(x => x.Name).NotNull().WithMessage("Nama grade tidak boleh kosong");
        }
    }
}
