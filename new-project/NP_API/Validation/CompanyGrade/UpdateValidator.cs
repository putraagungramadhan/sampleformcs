﻿using FluentValidation;
using NP_API.Dto;
using NP_API.Dto.Request.CompanyGrade;

namespace NP_API.Validation.CompanyGrade
{
    public class UpdateValidator : AbstractValidator<UpdateGradeDto>
    {
        public UpdateValidator()
        {
            RuleFor(x => x.Guid).NotEmpty().NotNull().WithMessage("ID grade perusahaan tidak boleh kosong");
            RuleFor(x => x.Name).NotEmpty().NotNull().WithMessage("ID perusahaan tidak boleh kosong");
        }
    }
}
