﻿using FluentValidation;
using NP_API.Dto.Request.CompanyGroup;

namespace NP_API.Validation.CompanyGroup
{
    public class CreateValidator : AbstractValidator<CompanyGroupDto>
    {
        public CreateValidator()
        {
            RuleFor(x => x.Name).NotEmpty().WithMessage("Nama tidak boleh kosong")
                .NotNull().WithMessage("Judul tidak boleh kosong");
            RuleFor(x => x.Detail).NotEmpty().WithMessage("Detail tidak boleh kosong")
                .NotNull().WithMessage("Detail tidak boleh kosong");
        }
    }
}
