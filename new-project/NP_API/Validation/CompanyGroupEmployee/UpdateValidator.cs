﻿using FluentValidation;
using NP_API.Dto.Request.CompanyGroupEmployee;

namespace NP_API.Validation.CompanyGroupEmployee
{
    public class UpdateValidator: AbstractValidator<UpdateGroupEmployeeDto>
    {
        public UpdateValidator()
        {
            RuleFor(x => x.Guid).NotEmpty().WithMessage("ID group tidak boleh kosong");
            
        }

    }
}
