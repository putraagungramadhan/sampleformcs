﻿using FluentValidation;
using NP_API.Dto;

namespace NP_API.Validation.CompanyLevel
{
    public class CreateValidator : AbstractValidator<CompanyLevelDto>
    {
        public CreateValidator()
        {
            RuleFor(x => x.Name).NotEmpty().WithMessage("Nama level tidak boleh kosong");
            RuleFor(x => x.Name).NotNull().WithMessage("Nama level tidak boleh kosong");
        }
    }
}
