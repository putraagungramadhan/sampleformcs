﻿using FluentValidation;
using NP_API.Dto.Request.CompanyLevel;

namespace NP_API.Validation.CompanyLevel
{
    public class UpdateValidator : AbstractValidator<UpdateLevelDto>
    {
        public UpdateValidator()
        {
            RuleFor(x => x.Guid).NotEmpty().WithMessage("ID level tidak boleh kosong");
            RuleFor(x => x.Name).NotEmpty().WithMessage("Nama level tidak boleh kosong");
            RuleFor(x => x.Name).NotNull().WithMessage("Nama level tidak boleh kosong");
        }
    }
}
