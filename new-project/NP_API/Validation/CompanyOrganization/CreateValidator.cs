﻿using FluentValidation;
using NP_API.Dto;

namespace NP_API.Validation.CompanyOrganization
{
    public class CreateValidator : AbstractValidator<CompanyOrganizationDto>
    {
        public CreateValidator()
        {
            RuleFor(x => x.Name).NotEmpty().WithMessage("Nama organisasi tidak boleh kosong");
            RuleFor(x => x.Name).NotNull().WithMessage("Nama organisasi tidak boleh kosong");
        }
    }
}
