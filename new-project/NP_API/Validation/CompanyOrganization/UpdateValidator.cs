﻿using FluentValidation;
using NP_API.Dto;
using NP_API.Dto.Request.CompanyOrganization;

namespace NP_API.Validation.CompanyOrganization
{
    public class UpdateValidator : AbstractValidator<UpdateOrganizationDto>
    {
        public UpdateValidator()
        {
            RuleFor(x => x.Guid).NotEmpty().WithMessage("ID organisasi tidak boleh kosong");
            RuleFor(x => x.Name).NotEmpty().WithMessage("Nama organisasi tidak boleh kosong");
            RuleFor(x => x.Name).NotNull().WithMessage("Nama organisasi tidak boleh kosong");
        }
    }
}
