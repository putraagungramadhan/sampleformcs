﻿using FluentValidation;
using NP_API.Dto;

namespace NP_API.Validation.CompanyTitle
{
    public class CreateValidator : AbstractValidator<CompanyTitleDto>
    {
        public CreateValidator()
        {
            RuleFor(x => x.Name).NotEmpty().WithMessage("Nama title tidak boleh kosong");
            RuleFor(x => x.Name).NotNull().WithMessage("Nama title tidak boleh kosong");
        }
    }
}
