﻿using FluentValidation;
using NP_API.Dto.Request.CompanyOrganization;
using NP_API.Dto.Request.CompanyTitle;

namespace NP_API.Validation.CompanyTitle
{
    public class UpdateValidator : AbstractValidator<UpdateCompanyTitleDto>
    {
        public UpdateValidator()
        {
            RuleFor(x => x.Guid).NotEmpty().WithMessage("ID title tidak boleh kosong");
            RuleFor(x => x.Name).NotEmpty().WithMessage("Nama title tidak boleh kosong");
            RuleFor(x => x.Name).NotNull().WithMessage("Nama title tidak boleh kosong");
        }
    }
}
