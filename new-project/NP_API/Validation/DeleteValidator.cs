﻿using FluentValidation;
using NP_API.Dto.Custom;
using NP_API.Dto.Request.Company;

namespace NP_API.Validation
{
    public class DeleteValidator : AbstractValidator<DeleteDto>
    {
        public DeleteValidator()
        {
            RuleFor(x => x.Guid).NotEmpty().NotNull().WithMessage("ID tidak boleh kosong");
        }
    }
}
