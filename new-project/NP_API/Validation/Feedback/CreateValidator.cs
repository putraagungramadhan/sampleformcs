﻿using FluentValidation;
using NP_API.Dto.Request.Feedback;

namespace NP_API.Validation.Feedback
{
    public class CreateValidator : AbstractValidator<NewFeedbackDto>
    {
        public CreateValidator()
        {
            RuleFor(x => x.Title).NotEmpty().WithMessage("Title feedback tidak boleh kosong");
            RuleFor(x => x.Content).NotEmpty().WithMessage("Content feedback tidak boleh kosong");
            RuleFor(x => x.IsRead).NotNull().WithMessage("Status feedback tidak boleh kosong");
        }
    }
}
