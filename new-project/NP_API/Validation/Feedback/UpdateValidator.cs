﻿using FluentValidation;
using NP_API.Dto.Request.Feedback;

namespace NP_API.Validation.Feedback
{
    public class UpdateValidator : AbstractValidator<UpdateFeedbackDto>
    {
        public UpdateValidator()
        {
            RuleFor(x => x.IsRead).NotNull().WithMessage("Status feedback tidak boleh kosong");
        }
    }
}
