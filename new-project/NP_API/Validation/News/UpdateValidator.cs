﻿using FluentValidation;
using NP_API.Dto.Request.News;

namespace NP_API.Validation.News
{
    public class UpdateValidator : AbstractValidator<UpdateNewsDto>
    {
        public UpdateValidator() {
            RuleFor(x => x.Guid).NotEmpty().NotNull().WithMessage("ID berita tidak boleh kosong");
            RuleFor(x => x.Title).NotEmpty().WithMessage("Title berita tidak boleh kosong");
            RuleFor(x => x.CoverImage).NotEmpty().WithMessage("CoverImage berita tidak boleh kosong");
            RuleFor(x => x.Content).NotEmpty().WithMessage("Content berita tidak boleh kosong");
            RuleFor(x => x.Status).NotNull().WithMessage("Status berita tidak boleh kosong");
        }
    }
}
