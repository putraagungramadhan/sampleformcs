﻿using FluentValidation;
using NP_API.Dto;
using NP_API.Dto.Request.NewsCategory;



namespace NP_API.Validation.NewsCategory
{
   
    public class CreateValidator : AbstractValidator<NewNewsCategoryDto>
    {
        public CreateValidator()
        {
          
            RuleFor(x => x.Name).NotEmpty().WithMessage("Nama kategori berita tidak boleh kosong");
            RuleFor(x => x.Status).NotNull().WithMessage("Status kategori berita tidak boleh kosong");
        }
  
    }
}
