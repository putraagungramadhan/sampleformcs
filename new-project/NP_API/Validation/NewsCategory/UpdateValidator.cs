﻿using FluentValidation;
using NP_API.Dto.Request.CompanyClass;
using NP_API.Dto.Request.NewsCategory;
using NP_API.Dto;

namespace NP_API.Validation.NewsCategory
{
    public class UpdateValidator : AbstractValidator<UpdateNewsCategoryDto>
    {
        public UpdateValidator() {
            
            RuleFor(x => x.Guid).NotEmpty().NotNull().WithMessage("ID kategori berita tidak boleh kosong");
            RuleFor(x => x.Name).NotEmpty().WithMessage("Name kategori berita tidak boleh kosong");
            RuleFor(x => x.Status).NotNull().WithMessage("Status kategori berita tidak boleh kosong");

        }
     
    }
}
