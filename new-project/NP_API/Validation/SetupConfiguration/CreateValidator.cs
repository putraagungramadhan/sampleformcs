﻿using FluentValidation;
using NP_API.Dto.Request.SetupConfigurations;

namespace NP_API.Validation.SetupConfiguration
{
    public class CreateValidator : AbstractValidator<NewSetupConfigurationDto>
    {
        public CreateValidator()
        {
            RuleFor(x => x.SubTypeName).NotEmpty().WithMessage("Sub Type tidak boleh kosong");
            RuleFor(x => x.TypeName).NotEmpty().WithMessage("Type tidak boleh kosong");
            RuleFor(x => x.TypeValue).NotEmpty().WithMessage("Value tidak boleh kosong");
        }
    }
}
