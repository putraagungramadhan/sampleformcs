﻿using FluentValidation;
using NP_API.Dto.Request.SetupConfigurations;

namespace NP_API.Validation.SetupConfiguration
{
    public class UpdateValidator : AbstractValidator<UpdateSetupConfigurationDto>
    {
        public UpdateValidator() {
            RuleFor(x => x.Guid).NotEmpty().NotNull().WithMessage("ID tidak boleh kosong");
            RuleFor(x => x.SubTypeName).NotEmpty().WithMessage("Sub Type tidak boleh kosong");
            RuleFor(x => x.TypeName).NotEmpty().WithMessage("Type tidak boleh kosong");
            RuleFor(x => x.TypeValue).NotEmpty().WithMessage("Value tidak boleh kosong");
        }
    }
}
