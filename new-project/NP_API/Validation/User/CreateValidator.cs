﻿using FluentValidation;
using NP_API.Dto.Request.User;
using System.Text.RegularExpressions;

namespace NP_API.Validation.User
{
    public class CreateValidator : AbstractValidator<NewUserDto>
    {
        public CreateValidator()
        {
            RuleFor(x => x.CompanyGuid).NotEmpty().WithMessage("Perusahaan tidak boleh kosong")
                .NotNull().WithMessage("Perusahaan tidak boleh kosong");
            RuleFor(x => x.Name).NotEmpty().WithMessage("Nama tidak boleh kosong")
                .NotNull().WithMessage("Nama tidak boleh kosong");
            RuleFor(x => x.Username).NotEmpty().WithMessage("Username tidak boleh kosong")
                .NotNull().WithMessage("Username tidak boleh kosong");
            RuleFor(x => x.Email).NotEmpty().WithMessage("Email tidak boleh kosong")
                .NotNull().WithMessage("Email tidak boleh kosong")
                .EmailAddress().WithMessage("Email format tidak valid. Contoh contoh@gmail.com.");
            RuleFor(x => x.Password).NotEmpty().WithMessage("Password tidak boleh kosong")
                .NotNull().WithMessage("Password tidak boleh kosong")
                .Must(IsValidPassword).WithMessage("Password setidaknya mengandung 1 Huruf besar, 1 angka dan minimal 8 digit");
            RuleFor(x => x.Role).NotEmpty().WithMessage("Role tidak boleh kosong")
                .NotNull().WithMessage("Role tidak boleh kosong");
            RuleForEach(x => x.Permissions)
                .SetValidator(new PermissionOnUserValidator());
        }
        public static bool IsValidPassword(string password)
        {
            string pattern = @"^(?=.*[0-9])(?=.*[a-zA-Z])(?=.*[A-Z]).{8,}$";
            return Regex.IsMatch(password, pattern);
        }

        public class PermissionOnUserValidator : AbstractValidator<PermissionOnUserDto>
        {
            public PermissionOnUserValidator()
            {
                RuleFor(x => x.ModuleGuid)
                    .NotEmpty().WithMessage("Modul tidak boleh kosong")
                    .NotNull().WithMessage("Modul tidak boleh kosong");

                RuleFor(x => x.CanWrite)
                    .NotEmpty().WithMessage("Permission tidak boleh kosong")
                    .NotNull().WithMessage("Permission tidak boleh kosong");
            }
        }
    }
}
