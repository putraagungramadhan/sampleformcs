﻿using FluentValidation;
using NP_API.Dto.Request.User;
using System.Text.RegularExpressions;

namespace NP_API.Validation.User
{
    public class UpdateUserPasswordValidation : AbstractValidator<UpdateUserPasswordDto>
    {
        public UpdateUserPasswordValidation()
        {
            RuleFor(x => x.Guid).NotEmpty().WithMessage("ID tidak boleh kosong")
                .NotNull().WithMessage("ID tidak boleh kosong");
            RuleFor(x => x.Password).NotEmpty().WithMessage("Password tidak boleh kosong")
                .NotNull().WithMessage("Password tidak boleh kosong")
                .Must(IsValidPassword).WithMessage("Password setidaknya mengandung 1 Huruf besar, 1 angka dan minimal 8 digit");
        }
        public static bool IsValidPassword(string password)
        {
            string pattern = @"^(?=.*[0-9])(?=.*[a-zA-Z])(?=.*[A-Z]).{8,}$";
            return Regex.IsMatch(password, pattern);
        }
    }
}
