﻿using FluentValidation;
using NP_API.Dto.Request.UserPermission;

namespace NP_API.Validation.UserPermission
{
    public class CreateValidator : AbstractValidator<UserPermissionDto>
    {
        public CreateValidator()
        {
            RuleFor(x => x.UserGuid).NotEmpty().WithMessage("User tidak boleh kosong")
                .NotNull().WithMessage("User tidak boleh kosong");
            RuleFor(x => x.ModuleGuid).NotEmpty().WithMessage("Modul tidak boleh kosong")
                .NotNull().WithMessage("Modul tidak boleh kosong");
            RuleFor(x => x.CanWrite)
                .NotNull().WithMessage("Hak akses tidak boleh kosong");
        }
    }
}
