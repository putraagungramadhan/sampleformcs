﻿using FluentValidation;
using NP_API.Dto.Request.UserPermission;

namespace NP_API.Validation.UserPermission
{
    public class UpdateValidator : AbstractValidator<UpdateUserPermissionDto>
    {
        public UpdateValidator()
        {
            RuleFor(x => x.Guid).NotEmpty().WithMessage("ID tidak boleh kosong")
                .NotNull().WithMessage("ID tidak boleh kosong");
            RuleFor(x => x.CanWrite)
                .NotNull().WithMessage("Hak akses tidak boleh kosong");
        }
    }
}
