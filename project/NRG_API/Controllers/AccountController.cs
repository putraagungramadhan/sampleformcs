﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Text;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using NRG_Core.Context;
using NRG_Core.DataModels;
using NRG_Core.Interface;
using NRG_Core.Repository;

namespace NRG_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly ILogin _login;
        private readonly IConfiguration _config;

        public AccountController(IConfiguration iConfig, WebContext db)
        {
            _config = iConfig;
            _login = new RLogin(db);
        }

        private string GenerateJSONWebToken()
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"]));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken(_config["Jwt:Issuer"], _config["Jwt:Issuer"], null, expires: DateTime.Now.AddMinutes(60), signingCredentials: credentials);
            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        [HttpGet]
        public ActionResult<string> Get()
        {
            return "this api running well";
        }

        [HttpGet]
        [Route("[action]/{u}/{p}")]
        public IActionResult TryLogin(string u, string p)
        {
            IActionResult response = Unauthorized();
            try
            {
                var get = _login.Login(u, p);
                var rsp = JsonConvert.DeserializeObject<ResponseEnt>(get);

                response = Ok(new { rsp.code, rsp.message });
                if (rsp.status)
                    response = Ok(new { rsp.code, rsp.message, rsp.data, token = GenerateJSONWebToken() });
            }
            catch (Exception ex)
            {
                response = Ok(new { code = "ERG99", message = ex.Message.ToString() });
            }
            return response;
        }

        //[HttpPost]
        //[Route("[action]")]
        //public IActionResult TryLogin([FromForm]MLogin login)
        //{
        //    IActionResult response = Unauthorized();
        //    try
        //    {
        //        var result = _login.Login(login.Username, login.Passwd);

        //var result = AuthenticateUser(login);
        //var resp = JsonConvert.DeserializeObject<MLogin>(result);
        //if (resp.Success)
        //{
        //    var token = GenerateJSONWebToken(resp);
        //    response = Ok(new { code = "000", data = result, message = token });
        //}
        //else
        //{
        //    response = Ok(new { code = "101", message = "Email or password not found and maybe yout account is single login. please ask your admin." });
        //}
        //}
        //catch (Exception ex)
        //{
        //response = Ok(new { code = "199", message = ex.Message.ToString() });
        //    }
        //    return response;
        //}

        //[HttpPost]
        //[Route("[action]")]
        //public IActionResult TryLogout([FromForm]MLogin login)
        //{
        //    IActionResult response = Unauthorized();
        //    try
        //    {
        //        var dbConn = _config.GetSection("DBCon").GetSection("MainDB").Value;
        //        _login.Logout(login.ID, dbConn);
        //        response = Ok(new { code = "000" });
        //    }
        //    catch (Exception ex)
        //    {
        //        response = Ok(new { code = "199", message = ex.Message.ToString() });
        //    }
        //    return response;
        //}
    }
}