﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using NRG_Core.Context;
using NRG_Core.DataModels;
using NRG_Core.Interface;
using NRG_Core.Repository;

namespace NRG_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ControlController : ControllerBase
    {
        private readonly IConfiguration _config;
        private readonly IControl _ctrl;
        private readonly ILogger _log;

        public ControlController(IConfiguration iConfig, WebContext db)//, LogContext lc)
        {
            _config = iConfig;
            _ctrl = new RControl(db);
            //_log = new RLogger(lc);
        }

        private void addLog(EnLog tipe, string message, string by)
        {
            //_log.Insert(new MLogger { LogType = tipe, Message = message, CreateBy = by, CreateDate = DateTime.Now });
        }

        [HttpGet]
        public ActionResult<string> Get()
        {
            return "this api running well";
        }

        #region Control
        [HttpGet]
        [Route("[action]/{i}")]
        public IActionResult Take(int i)
        {
            IActionResult response = Unauthorized();
            try
            {
                var rsp = JsonConvert.DeserializeObject<ResponseEnt>(_ctrl.All(i));
                response = Ok(new { rsp.code, rsp.message });
                if (rsp.status)
                    response = Ok(new { rsp.code, rsp.message, rsp.data });
                else
                    addLog(EnLog.Error, rsp.code + " - " + rsp.message, "system");
            }
            catch (Exception ex)
            {
                response = Ok(new { code = "ERG99", message = ex.Message.ToString() });
            }
            return response;
        }

        [HttpPost]
        [Route("[action]")]
        public IActionResult Update([FromForm]MControl obj)
        {
            IActionResult response = Unauthorized();
            try
            {
                //var rsp = JsonConvert.DeserializeObject<ResponseEnt>(_ctrl.Update(obj));
                //response = Ok(new { rsp.code, rsp.message });
                //if (rsp.status)
                //    response = Ok(new { rsp.code, rsp.message, rsp.data });
                //else
                //    addLog(EnLog.Error, rsp.code + " - " + rsp.message, obj.CreateBy);
            }
            catch (Exception ex)
            {
                response = Ok(new { code = "ERG99", message = ex.Message.ToString() });
            }
            return response;
        }

        [HttpPost]
        [Route("[action]")]
        public IActionResult UpdateImage([FromForm]PControlImages obj)
        {
            IActionResult response = Unauthorized();
            try
            {
                //var req = _ctrl.UpdateImage(obj.ControlID, obj.Name, obj.Data, obj.Width, obj.Height, obj.CreateBy);
                //var rsp = JsonConvert.DeserializeObject<ResponseEnt>(req);
                //response = Ok(new { rsp.code, rsp.message });
                //if (rsp.status)
                //    response = Ok(new { rsp.code, rsp.message, rsp.data });
                //else
                //    addLog(EnLog.Error, rsp.code + " - " + rsp.message, obj.CreateBy);
            }
            catch (Exception ex)
            {
                response = Ok(new { code = "ERG99", message = ex.Message.ToString() });
            }
            return response;
        }

        [HttpGet]
        [Route("[action]/{i}/{b}")]
        public IActionResult Remove(int i, string b)
        {
            IActionResult response = Unauthorized();
            try
            {
                var rsp = JsonConvert.DeserializeObject<ResponseEnt>(_ctrl.Remove(i, b));
                response = Ok(new { rsp.code, rsp.message });
                if (rsp.status)
                    response = Ok(new { rsp.code, rsp.message, rsp.data });
                else
                    addLog(EnLog.Error, rsp.code + " - " + rsp.message, b);
            }
            catch (Exception ex)
            {
                response = Ok(new { code = "ERG99", message = ex.Message.ToString() });
            }
            return response;
        }
        #endregion
    }
}