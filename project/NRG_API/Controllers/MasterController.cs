﻿using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using NRG_Core.Context;
using NRG_Core.DataModels;
using NRG_Core.Interface;
using NRG_Core.Repository;

namespace NRG_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MasterController : ControllerBase
    {
        private readonly IConfiguration _config;
        private readonly IHospital _hospital;
        private readonly ISurgeon _surgeon;
        private readonly IPatient _patient;
        private readonly ILogger _log;

        public MasterController(IConfiguration iConfig, WebContext db)
        {
            _config = iConfig;
            _hospital = new RHospital(db);
            _surgeon = new RSurgeon(db);
            _patient = new RPatient(db);
            _log = new RLogger(db);
        }

        private void addLog(EnLog tipe, string message, string by)
        {
            _log.Insert(new MLogger { LogType = tipe, Message = message, CreateBy = by, CreateDate = DateTime.Now });
        }

        [HttpGet]
        public ActionResult<string> Get()
        {
            return "this api running well";
        }

        #region Hospital
        [HttpGet]
        [Route("[action]/{i}")]
        public IActionResult GetHospital(int i)
        {
            IActionResult response = Unauthorized();
            try
            {
                var rsp = JsonConvert.DeserializeObject<ResponseEnt>(_hospital.All(i));
                response = Ok(new { rsp.code, rsp.message });
                if (rsp.status)
                    response = Ok(new { rsp.code, rsp.message, rsp.data });
                else
                    addLog(EnLog.Error, rsp.code + " - " + rsp.message, "system");
            }
            catch (Exception ex)
            {
                response = Ok(new { code = "ERG99", message = ex.Message.ToString() });
            }
            return response;
        }

        [HttpPost]
        [Route("[action]")]
        public IActionResult PostHospital([FromForm]MHospital obj)
        {
            IActionResult response = Unauthorized();
            try
            {
                var by = obj.CreateBy;
                var rsp = JsonConvert.DeserializeObject<ResponseEnt>(_hospital.Update(obj));
                response = Ok(new { rsp.code, rsp.message });
                if (rsp.status)
                    response = Ok(new { rsp.code, rsp.message, rsp.data });
                else
                    addLog(EnLog.Error, rsp.code + " - " + rsp.message, by);
            }
            catch (Exception ex)
            {
                response = Ok(new { code = "ERG99", message = ex.Message.ToString() });
            }
            return response;

            //var mdl = new MHospital
            //{
            //    ID = obj.ID,
            //    Code = obj.Code,
            //    Name = obj.Name,
            //    Phone = obj.Phone,
            //    Mail = obj.Mail,
            //    Address = obj.Address,
            //    Description = obj.Description,
            //    Status = obj.Status,
            //    CreateBy = obj.CreateBy,
            //    CreateDate = obj.CreateDate
            //};

            //var r = new ResponseEnt
            //{
            //    code = "000",
            //    message = "success",
            //    data = _hospital.Update(mdl)
            //};
            //return new OkObjectResult(r);
        }

        [HttpGet]
        [Route("[action]/{i}/{b}")]
        public IActionResult RemoveHospital(int i, string b)
        {
            IActionResult response = Unauthorized();
            try
            {
                var rsp = JsonConvert.DeserializeObject<ResponseEnt>(_hospital.Remove(i, b));
                response = Ok(new { rsp.code, rsp.message });
                if (rsp.status)
                    response = Ok(new { rsp.code, rsp.message, rsp.data });
                else
                    addLog(EnLog.Error, rsp.code + " - " + rsp.message, b);
            }
            catch (Exception ex)
            {
                response = Ok(new { code = "ERG99", message = ex.Message.ToString() });
            }
            return response;
        }
        #endregion

        #region Surgeon
        [HttpGet]
        [Route("[action]/{i}")]
        public IActionResult GetSurgeon(int i)
        {
            IActionResult response = Unauthorized();
            try
            {
                var rsp = JsonConvert.DeserializeObject<ResponseEnt>(_surgeon.All(i));
                response = Ok(new { rsp.code, rsp.message });
                if (rsp.status)
                    response = Ok(new { rsp.code, rsp.message, rsp.data });
                else
                    addLog(EnLog.Error, rsp.code + " - " + rsp.message, "system");
            }
            catch (Exception ex)
            {
                response = Ok(new { code = "ERG99", message = ex.Message.ToString() });
            }
            return response;
        }

        [HttpPost]
        [Route("[action]")]
        public IActionResult PostSurgeon([FromForm]MSurgeon obj)
        {
            IActionResult response = Unauthorized();
            try
            {
                var by = obj.CreateBy;
                var rsp = JsonConvert.DeserializeObject<ResponseEnt>(_surgeon.Update(obj));
                response = Ok(new { rsp.code, rsp.message });
                if (rsp.status)
                    response = Ok(new { rsp.code, rsp.message, rsp.data });
                else
                    addLog(EnLog.Error, rsp.code + " - " + rsp.message, by);
            }
            catch (Exception ex)
            {
                response = Ok(new { code = "ERG99", message = ex.Message.ToString() });
            }
            return response;
        }

        [HttpGet]
        [Route("[action]/{i}/{b}")]
        public IActionResult RemoveSurgeon(int i, string b)
        {
            IActionResult response = Unauthorized();
            try
            {
                var rsp = JsonConvert.DeserializeObject<ResponseEnt>(_surgeon.Remove(i, b));
                response = Ok(new { rsp.code, rsp.message });
                if (rsp.status)
                    response = Ok(new { rsp.code, rsp.message, rsp.data });
                else
                    addLog(EnLog.Error, rsp.code + " - " + rsp.message, b);
            }
            catch (Exception ex)
            {
                response = Ok(new { code = "ERG99", message = ex.Message.ToString() });
            }
            return response;
        }
        #endregion

        #region Patient
        [HttpGet]
        [Route("[action]/{i}")]
        public IActionResult GetPatient(int i)
        {
            IActionResult response = Unauthorized();
            try
            {
                var rsp = JsonConvert.DeserializeObject<ResponseEnt>(_patient.All(i));
                response = Ok(new { rsp.code, rsp.message });
                if (rsp.status)
                    response = Ok(new { rsp.code, rsp.message, rsp.data });
                else
                    addLog(EnLog.Error, rsp.code + " - " + rsp.message, "system");
            }
            catch (Exception ex)
            {
                response = Ok(new { code = "ERG99", message = ex.Message.ToString() });
            }
            return response;
        }

        [HttpPost]
        [Route("[action]")]
        public IActionResult PostPatient([FromForm]MPatient obj)
        {
            IActionResult response = Unauthorized();
            try
            {
                var by = obj.CreateBy;
                var rsp = JsonConvert.DeserializeObject<ResponseEnt>(_patient.Update(obj));
                response = Ok(new { rsp.code, rsp.message });
                if (rsp.status)
                    response = Ok(new { rsp.code, rsp.message, rsp.data });
                else
                    addLog(EnLog.Error, rsp.code + " - " + rsp.message, by);
            }
            catch (Exception ex)
            {
                response = Ok(new { code = "ERG99", message = ex.Message.ToString() });
            }
            return response;
        }

        [HttpGet]
        [Route("[action]/{i}/{b}")]
        public IActionResult RemovePatient(int i, string b)
        {
            IActionResult response = Unauthorized();
            try
            {
                var rsp = JsonConvert.DeserializeObject<ResponseEnt>(_patient.Remove(i, b));
                response = Ok(new { rsp.code, rsp.message });
                if (rsp.status)
                    response = Ok(new { rsp.code, rsp.message, rsp.data });
                else
                    addLog(EnLog.Error, rsp.code + " - " + rsp.message, b);
            }
            catch (Exception ex)
            {
                response = Ok(new { code = "ERG99", message = ex.Message.ToString() });
            }
            return response;
        }
        #endregion
    }
}