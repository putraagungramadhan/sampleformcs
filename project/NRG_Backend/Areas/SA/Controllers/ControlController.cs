﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Newtonsoft.Json;
using NRG_Core.Context;
using NRG_Core.DataModels;
using NRG_Core.Interface;
using NRG_Core.Repository;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace NRG_Backend.Areas.SA.Controllers
{
    [Area("SA")]
    [Authorize]
    public class ControlController : Controller
    {
        private readonly IControl _ctrl;
        private readonly IPatient _patient;
        private readonly IHospital _hospital;
        private readonly ISurgeon _surgeon;
        private readonly ILogger _log;
        private static string controllerName = "Control";

        public ControlController(WebContext db)
        {
            _ctrl = new RControl(db);
            _patient = new RPatient(db);
            _hospital = new RHospital(db);
            _surgeon = new RSurgeon(db);
            _log = new RLogger(db);
        }

        #region Combo
        public void ComboPatient()
        {
            ViewBag.ComboPatient = _patient.Combo().Select(c => new SelectListItem { Text = c.Name, Value = c.ID.ToString() }).ToList();
        }

        public void ComboHospital()
        {
            ViewBag.ComboHospital = _hospital.Combo().Select(c => new SelectListItem { Text = c.Name, Value = c.ID.ToString() }).ToList();
        }

        public void ComboSurgeon()
        {
            ViewBag.ComboSurgeon = _surgeon.Combo().Select(c => new SelectListItem { Text = c.Name, Value = c.ID.ToString() }).ToList();
        }
        #endregion

        //private bool UpdateImageData(int controlId, List<IFormFile> images, string by)
        //{
        //    if (images != null)
        //    {
        //        foreach (var img in images)
        //        {
        //            var fileName = Path.GetFileName(img.FileName);
        //            if (img == null || img.ContentType.ToLower().StartsWith("image/"))
        //            {
        //                var ms = new MemoryStream();
        //                img.OpenReadStream().CopyTo(ms);

        //                var prm = new MControlImages
        //                {
        //                    ControlID = controlId,
        //                    ImageType = EnControlImage.CTSE,
        //                    Name = img.Name,
        //                    Data = ms.ToArray(),
        //                    Width = 100,
        //                    Height = 100,
        //                    ContentType = img.ContentType,
        //                    CreateBy = by,
        //                    CreateDate = DateTime.Now
        //                };
        //                _ctrl.UpdateImage(prm);
        //            }
        //        }
        //    }
        //    return true;
        //}

        public JsonResult GetData(int id)
        {
            var data = new Object();

            try
            {
                var result = JsonConvert.DeserializeObject<ResponseEnt>(_ctrl.All(id));
                if (result.status)
                {
                    data = JsonConvert.DeserializeObject<List<VControl>>(result.data);
                }
                else
                {
                    HttpContext.Session.SetString("TmpError", result.message);
                    _log.Insert(new MLogger { LogType = EnLog.Error, Message = "Control.GetData-" + result.message, CreateBy = "system", CreateDate = DateTime.Now });
                }
            }
            catch (Exception ex)
            {
                HttpContext.Session.SetString("TmpError", ex.Message);
                _log.Insert(new MLogger { LogType = EnLog.Error, Message = "Control.GetData.Ex-" + ex.Message, CreateBy = "system", CreateDate = DateTime.Now });
            }
            return Json(new { data });
        }

        public ActionResult Index()
        {
            var login = HttpContext.Session.GetString("uname");
            if (string.IsNullOrEmpty(login))
            {
                login = HttpContext.Session.GetString("nname");
                if (string.IsNullOrEmpty(login)) return Redirect("~/Home");
            }

            ViewBag.Message = string.Empty;
            var msg = HttpContext.Session.GetString("TmpError");
            if (!string.IsNullOrEmpty(msg))
            {
                ViewBag.Message = msg;
                HttpContext.Session.SetString("TmpError", string.Empty);
            }

            ComboPatient();
            ComboHospital();
            ComboSurgeon();
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult UpdateData(PControl prm)
        {
            try
            {
                var username = HttpContext.Session.GetString("uname");
                if (string.IsNullOrEmpty(username))
                {
                    username = HttpContext.Session.GetString("nname");
                    if (string.IsNullOrEmpty(username)) return Redirect("~/Home");
                }

                if (prm.Master.PatientID > 0)
                {
                    if (string.IsNullOrEmpty(prm.Master.CreateBy)) prm.Master.CreateBy = username;

                    if (prm.DeletedID != null)
                    {
                        var del = prm.DeletedID.Split(',').Select(int.Parse).ToList();
                        foreach (var d in del)
                            prm.Images.RemoveAt(d);
                    }

                    var images = new List<MControlImages>();
                    if (prm.Images != null)
                    {
                        foreach (var img in prm.Images)
                        {
                            if (img == null || img.ContentType.ToLower().StartsWith("image/"))
                            {
                                var ms = new MemoryStream();
                                img.OpenReadStream().CopyTo(ms);

                                images.Add(new MControlImages
                                {
                                    ImageType = EnControlImage.CTSE,
                                    Name = Path.GetFileName(img.FileName),
                                    Data = ms.ToArray(),
                                    Width = 100,
                                    Height = 100,
                                    ContentType = img.ContentType,
                                    CreateBy = username,
                                    CreateDate = DateTime.Now
                                });
                            }
                        }
                    }

                    var result = JsonConvert.DeserializeObject<ResponseEnt>(_ctrl.Update(prm.Master, images));
                    if (!result.status)
                    {
                        HttpContext.Session.SetString("TmpError", result.message);
                        _log.Insert(new MLogger { LogType = EnLog.Error, Message = "Master.UpdateHospital-" + result.message, CreateBy = "system", CreateDate = DateTime.Now });
                    }
                }
            }
            catch (Exception ex)
            {
                HttpContext.Session.SetString("TmpError", ex.Message);
                _log.Insert(new MLogger { LogType = EnLog.Error, Message = "Control.UpdateData.Ex-" + ex.Message, CreateBy = "system", CreateDate = DateTime.Now });
            }
            return RedirectToAction(nameof(Index));
        }

        public bool DeleteData(int id)
        {
            var result = false;

            try
            {
                var username = HttpContext.Session.GetString("uname");
                if (string.IsNullOrEmpty(username))
                {
                    username = HttpContext.Session.GetString("nname");
                    if (string.IsNullOrEmpty(username)) return false;
                }

                if (id > 0)
                {
                    var resp = JsonConvert.DeserializeObject<ResponseEnt>(_ctrl.Remove(id, username));
                    if (resp.status)
                    {
                        result = true;
                    }
                    else
                    {
                        HttpContext.Session.SetString("TmpError", resp.message);
                        _log.Insert(new MLogger { LogType = EnLog.Error, Message = "Master.DeleteHospital-" + resp.message, CreateBy = "system", CreateDate = DateTime.Now });
                    }
                }
            }
            catch (Exception ex)
            {
                HttpContext.Session.SetString("TmpError", ex.Message);
                _log.Insert(new MLogger { LogType = EnLog.Error, Message = "Control.DeleteData.Ex-" + ex.Message, CreateBy = "system", CreateDate = DateTime.Now });
            }
            return result;
        }
    }
}