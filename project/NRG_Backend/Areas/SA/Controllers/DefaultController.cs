﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using NRG_Core.Context;
using NRG_Core.DataModels;
using NRG_Core.Interface;
using NRG_Core.Repository;
using System;

namespace NRG_Backend.Areas.SA.Controllers
{
    [Area("SA")]
    [Authorize]
    public class DefaultController : Controller
    {
        private readonly ILogger _log;

        public DefaultController(WebContext db)
        {
            _log = new RLogger(db);
        }

        public JsonResult GetOrder()
        {
            var data = new Object();
            try
            {
                var tgl = DateTime.Now;
                var dt = string.Empty;

                if (!string.IsNullOrEmpty(dt))
                    data = JsonConvert.DeserializeObject(dt);
            }
            catch (Exception ex)
            {
                HttpContext.Session.SetString("TmpError", ex.Message);
                _log.Insert(new MLogger { LogType = EnLog.Error, Message = "Default.GetOrder.Ex-" + ex.Message, CreateBy = "system", CreateDate = DateTime.Now });
            }
            return Json(new { data });
        }

        public ActionResult Index()
        {
            ViewBag.Message = string.Empty;
            var msg = HttpContext.Session.GetString("TmpError");
            if (!string.IsNullOrEmpty(msg))
            {
                ViewBag.Message = msg;
                HttpContext.Session.SetString("TmpError", string.Empty);
            }
            return View();
        }
    }
}