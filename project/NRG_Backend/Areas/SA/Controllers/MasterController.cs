﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using NRG_Core.Context;
using NRG_Core.DataModels;
using NRG_Core.Interface;
using NRG_Core.Repository;
using System;
using System.Collections.Generic;

namespace NRG_Backend.Areas.SA.Controllers
{
    [Area("SA")]
    [Authorize]
    public class MasterController : Controller
    {
        private readonly ILogger _log;
        private readonly IHospital _hospital;
        private readonly ISurgeon _surgeon;
        private readonly IPatient _patient;

        public MasterController(WebContext db)
        {
            _log = new RLogger(db);
            _hospital = new RHospital(db);
            _surgeon = new RSurgeon(db);
            _patient = new RPatient(db);
        }

        #region Hospital
        public JsonResult GetHospital(int id)
        {
            var data = new Object();

            try
            {
                var result = JsonConvert.DeserializeObject<ResponseEnt>(_hospital.All(id));
                if (!string.IsNullOrEmpty(result.data))
                {
                    data = JsonConvert.DeserializeObject<List<MHospital>>(result.data);
                }
                else
                {
                    HttpContext.Session.SetString("TmpError", result.message);
                    _log.Insert(new MLogger { LogType = EnLog.Error, Message = "Master.GetHospital-" + result.message, CreateBy = "system", CreateDate = DateTime.Now });
                }
            }
            catch (Exception ex)
            {
                HttpContext.Session.SetString("TmpError", ex.Message);
                _log.Insert(new MLogger { LogType = EnLog.Error, Message = "Master.GetHospital.Ex-" + ex.Message, CreateBy = "system", CreateDate = DateTime.Now });
            }
            return Json(new { data });
        }

        public ActionResult Hospital()
        {
            var login = HttpContext.Session.GetString("uname");
            if (string.IsNullOrEmpty(login))
            {
                login = HttpContext.Session.GetString("nname");
                if (string.IsNullOrEmpty(login)) return Redirect("~/Home");
            }

            ViewBag.Message = string.Empty;
            var msg = HttpContext.Session.GetString("TmpError");
            if (!string.IsNullOrEmpty(msg))
            {
                ViewBag.Message = msg;
                HttpContext.Session.SetString("TmpError", string.Empty);
            }
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult UpdateHospital(MHospital prm)
        {
            try
            {
                var login = HttpContext.Session.GetString("uname");
                if (string.IsNullOrEmpty(login))
                {
                    login = HttpContext.Session.GetString("nname");
                    if (string.IsNullOrEmpty(login)) return Redirect("~/Home");
                }

                if (!string.IsNullOrEmpty(prm.Name))
                {
                    if (string.IsNullOrEmpty(prm.CreateBy)) prm.CreateBy = login;
                    var result = JsonConvert.DeserializeObject<ResponseEnt>(_hospital.Update(prm));
                    if (!result.status)
                    {
                        HttpContext.Session.SetString("TmpError", result.message);
                        _log.Insert(new MLogger { LogType = EnLog.Error, Message = "Master.UpdateHospital-" + result.message, CreateBy = "system", CreateDate = DateTime.Now });
                    }
                }
            }
            catch (Exception ex)
            {
                HttpContext.Session.SetString("TmpError", ex.Message);
                _log.Insert(new MLogger { LogType = EnLog.Error, Message = "Master.UpdateHospital.Ex-" + ex.Message, CreateBy = "system", CreateDate = DateTime.Now });
            }
            return RedirectToAction(nameof(Hospital));
        }

        public bool DeleteHospital(int id)
        {
            var rets = false;

            try
            {
                var username = HttpContext.Session.GetString("uname");
                if (string.IsNullOrEmpty(username))
                {
                    username = HttpContext.Session.GetString("nname");
                    if (string.IsNullOrEmpty(username)) return false;
                }

                if (id > 0)
                {
                    var result = JsonConvert.DeserializeObject<ResponseEnt>(_hospital.Remove(id, username));
                    if (result.status)
                    {
                        rets = true;
                    }
                    else
                    {
                        HttpContext.Session.SetString("TmpError", result.message);
                        _log.Insert(new MLogger { LogType = EnLog.Error, Message = "Master.DeleteHospital-" + result.message, CreateBy = "system", CreateDate = DateTime.Now });
                    }
                }
            }
            catch (Exception ex)
            {
                HttpContext.Session.SetString("TmpError", ex.Message);
                _log.Insert(new MLogger { LogType = EnLog.Error, Message = "Master.DeleteHospital.Ex-" + ex.Message, CreateBy = "system", CreateDate = DateTime.Now });
            }
            return rets;
        }
        #endregion

        #region Surgeon
        public JsonResult GetSurgeon(int id)
        {
            var data = new Object();

            try
            {
                var result = JsonConvert.DeserializeObject<ResponseEnt>(_surgeon.All(id));
                if (!string.IsNullOrEmpty(result.data))
                {
                    data = JsonConvert.DeserializeObject<List<MSurgeon>>(result.data);
                }
                else
                {
                    HttpContext.Session.SetString("TmpError", result.message);
                    _log.Insert(new MLogger { LogType = EnLog.Error, Message = "Master.GetSurgeon-" + result.message, CreateBy = "system", CreateDate = DateTime.Now });
                }
            }
            catch (Exception ex)
            {
                HttpContext.Session.SetString("TmpError", ex.Message);
                _log.Insert(new MLogger { LogType = EnLog.Error, Message = "Master.GetSurgeon.Ex-" + ex.Message, CreateBy = "system", CreateDate = DateTime.Now });
            }            
            return Json(new { data });
        }

        public ActionResult Surgeon()
        {
            var username = HttpContext.Session.GetString("uname");
            if (string.IsNullOrEmpty(username))
            {
                username = HttpContext.Session.GetString("nname");
                if (string.IsNullOrEmpty(username)) return Redirect("~/Home");
            }
            
            ViewBag.Message = string.Empty;
            var msg = HttpContext.Session.GetString("TmpError");
            if (!string.IsNullOrEmpty(msg))
            {
                ViewBag.Message = msg;
                HttpContext.Session.SetString("TmpError", string.Empty);
            }
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult UpdateSurgeon(MSurgeon prm)
        {
            try
            {
                var username = HttpContext.Session.GetString("uname");
                if (string.IsNullOrEmpty(username))
                {
                    username = HttpContext.Session.GetString("nname");
                    if (string.IsNullOrEmpty(username)) return Redirect("~/Home");
                }

                if (!string.IsNullOrEmpty(prm.Name))
                {
                    if (string.IsNullOrEmpty(prm.CreateBy)) prm.CreateBy = username;

                    var result = JsonConvert.DeserializeObject<ResponseEnt>(_surgeon.Update(prm));
                    if (!result.status)
                    {
                        HttpContext.Session.SetString("TmpError", result.message);
                        _log.Insert(new MLogger { LogType = EnLog.Error, Message = "Master.UpdateSurgeon-" + result.message, CreateBy = "system", CreateDate = DateTime.Now });
                    }
                }
            }
            catch (Exception ex)
            {
                HttpContext.Session.SetString("TmpError", ex.Message);
                _log.Insert(new MLogger { LogType = EnLog.Error, Message = "Master.UpdateSurgeon.Ex-" + ex.Message, CreateBy = "system", CreateDate = DateTime.Now });
            }
            return RedirectToAction(nameof(Surgeon));
        }

        public bool DeleteSurgeon(int id)
        {
            var result = false;

            try
            {
                var username = HttpContext.Session.GetString("uname");
                if (string.IsNullOrEmpty(username))
                {
                    username = HttpContext.Session.GetString("nname");
                    if (string.IsNullOrEmpty(username)) return false;
                }

                if (id > 0)
                {
                    var resp = JsonConvert.DeserializeObject<ResponseEnt>(_surgeon.Remove(id, username));
                    if (resp.status)
                    {
                        result = true;
                    }
                    else
                    {
                        HttpContext.Session.SetString("TmpError", resp.message);
                        _log.Insert(new MLogger { LogType = EnLog.Error, Message = "Master.DeleteSurgeon-" + resp.message, CreateBy = "system", CreateDate = DateTime.Now });
                    }
                }
            }
            catch (Exception ex)
            {
                HttpContext.Session.SetString("TmpError", ex.Message);
                _log.Insert(new MLogger { LogType = EnLog.Error, Message = "Master.DeleteSurgeon.Ex-" + ex.Message, CreateBy = "system", CreateDate = DateTime.Now });
            }
            return result;
        }
        #endregion

        #region Patient
        public JsonResult GetPatient(int id)
        {
            var data = new Object();

            try
            {
                var result = JsonConvert.DeserializeObject<ResponseEnt>(_patient.All(id));
                if (!string.IsNullOrEmpty(result.data))
                {
                    data = JsonConvert.DeserializeObject<List<MPatient>>(result.data);
                }
                else
                {
                    HttpContext.Session.SetString("TmpError", result.message);
                    _log.Insert(new MLogger { LogType = EnLog.Error, Message = "Master.GetPatient-" + result.message, CreateBy = "system", CreateDate = DateTime.Now });
                }
            }
            catch (Exception ex)
            {
                HttpContext.Session.SetString("TmpError", ex.Message);
                _log.Insert(new MLogger { LogType = EnLog.Error, Message = "Master.GetPatient.Ex-" + ex.Message, CreateBy = "system", CreateDate = DateTime.Now });
            }
            return Json(new { data });
        }

        public ActionResult Patient()
        {
            var login = HttpContext.Session.GetString("uname");
            if (string.IsNullOrEmpty(login))
            {
                login = HttpContext.Session.GetString("nname");
                if (string.IsNullOrEmpty(login)) return Redirect("~/Home");
            }

            ViewBag.Message = string.Empty;
            var msg = HttpContext.Session.GetString("TmpError");
            if (!string.IsNullOrEmpty(msg))
            {
                ViewBag.Message = msg;
                HttpContext.Session.SetString("TmpError", string.Empty);
            }
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult UpdatePatient(MPatient prm)
        {
            try
            {
                var username = HttpContext.Session.GetString("uname");
                if (string.IsNullOrEmpty(username))
                {
                    username = HttpContext.Session.GetString("nname");
                    if (string.IsNullOrEmpty(username)) return Redirect("~/Home");
                }

                if (!string.IsNullOrEmpty(prm.Name))
                {
                    if (string.IsNullOrEmpty(prm.CreateBy)) prm.CreateBy = username;
                    var result = JsonConvert.DeserializeObject<ResponseEnt>(_patient.Update(prm));
                    if (!result.status)
                    {
                        HttpContext.Session.SetString("TmpError", result.message);
                        _log.Insert(new MLogger { LogType = EnLog.Error, Message = "Master.UpdatePatient-" + result.message, CreateBy = "system", CreateDate = DateTime.Now });
                    }
                }
            }
            catch (Exception ex)
            {
                HttpContext.Session.SetString("TmpError", ex.Message);
                _log.Insert(new MLogger { LogType = EnLog.Error, Message = "Master.UpdatePatient.Ex-" + ex.Message, CreateBy = "system", CreateDate = DateTime.Now });
            }
            return RedirectToAction(nameof(Patient));
        }

        public bool DeletePatient(int id)
        {
            var result = false;

            try
            {
                var username = HttpContext.Session.GetString("uname");
                if (string.IsNullOrEmpty(username))
                {
                    username = HttpContext.Session.GetString("nname");
                    if (string.IsNullOrEmpty(username)) return false;
                }

                if (id > 0)
                {
                    var jresp = JsonConvert.DeserializeObject<ResponseEnt>(_patient.Remove(id, username));
                    if (jresp.status)
                    {
                        result = true;
                    }
                    else
                    {
                        HttpContext.Session.SetString("TmpError", jresp.message);
                        _log.Insert(new MLogger { LogType = EnLog.Error, Message = "Master.DeletePatient-" + jresp.message, CreateBy = "system", CreateDate = DateTime.Now });
                    }
                }
            }
            catch (Exception ex)
            {
                HttpContext.Session.SetString("TmpError", ex.Message);
                _log.Insert(new MLogger { LogType = EnLog.Error, Message = "Master.DeletePatient.Ex-" + ex.Message, CreateBy = "system", CreateDate = DateTime.Now });
            }
            return result;
        }
        #endregion
    }
}