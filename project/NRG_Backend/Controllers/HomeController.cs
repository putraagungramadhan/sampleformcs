﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using NRG_Core.Context;
using NRG_Core.DataModels;
using NRG_Core.Function;
using NRG_Core.Interface;
using NRG_Core.Repository;

namespace NRG_Backend.Controllers
{
    public class HomeController : Controller
    {
        private readonly IConfiguration _config;
        private IMemoryCache _cache;
        private OFPassword _ofpass = new OFPassword();
        private readonly ILogin _login;
        private readonly ILogger _log;
        
        public HomeController(IConfiguration config, IMemoryCache cache, WebContext db)
        {
            _cache = cache;
            _config = config;
            _log = new RLogger(db);
            _login = new RLogin(db);
        }

        public IActionResult Index()
        {
            ViewBag.Message = string.Empty;
            var msg = HttpContext.Session.GetString("TmpError");
            if (!string.IsNullOrEmpty(msg))
            {
                ViewBag.Message = msg;
                HttpContext.Session.SetString("TmpError", string.Empty);
            }
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult Login(MLogin data)
        {
            if (!ModelState.IsValid)
                return BadRequest("/");

            try
            {
                var conn = _config["ConnectionStrings:DefaultConnection"];
                var result = _login.Login(data.Username, _ofpass.EncryptIT(data.Passwd));
                var srz = JsonConvert.DeserializeObject<ResponseEnt>(result);
                if (srz.status)
                {
                    var mdl = JsonConvert.DeserializeObject<MLogin>(srz.data);

                    var r = new string[3];
                    r[0] = GenerateJSONWebToken(mdl);
                    r[1] = mdl.Username;
                    r[2] = mdl.RoleID.ToString();

                    _cache.Set("MasterLogin", _ofpass.SecureCache(r));
                    var identity = new ClaimsIdentity(new[] { new Claim(ClaimTypes.Name, mdl.Username), new Claim(ClaimTypes.Role, "Register") }, CookieAuthenticationDefaults.AuthenticationScheme);
                    var principal = new ClaimsPrincipal(identity);
                    var login = HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, principal);
                    HttpContext.Session.SetString("uname", mdl.Username);
                    HttpContext.Session.SetString("nname", mdl.Username);
                    HttpContext.Session.SetString("ufullname", mdl.Fullname);
                    HttpContext.Session.SetString("uid", mdl.ID.ToString());

                    return RedirectToAction("Index", "Default", new { area = mdl.strRole });
                }
                else
                {
                    _log.Insert(new MLogger { LogType = EnLog.Error, Message = "HomeController.Login#0-Email or password not found and maybe yout account is single login. please ask your admin.", CreateBy = "system", CreateDate = DateTime.Now });
                }
            }
            catch (Exception ex)
            {
                HttpContext.Session.SetString("TmpError", ex.Message);
                _log.Insert(new MLogger { LogType = EnLog.Error, Message = "HomeController.Login#1-" + ex.Message, CreateBy = "system", CreateDate = DateTime.Now });
            }
            return RedirectToAction(nameof(Index));
        }

        public IActionResult Logout(string id)
        {
            if (!ModelState.IsValid)
                return BadRequest("/");

            var userId = HttpContext.Session.GetString("uid");
            if (string.IsNullOrEmpty(userId) && id != userId)
                return BadRequest("/");

            var username = HttpContext.Session.GetString("uname");
            if (string.IsNullOrEmpty(username))
                return BadRequest("/");

            try
            {
                var conn = _config["ConnectionStrings:DefaultConnection"];
                var result = _login.Logout(username);
                if (string.IsNullOrEmpty(result))
                {
                    HttpContext.SignOutAsync();
                    HttpContext.Session.Clear();
                    _cache.Set("MasterLogin", string.Empty);
                }
                else
                {
                    _log.Insert(new MLogger { LogType = EnLog.Error, Message = "HomeController.Logout#0-" + result, CreateBy = "system", CreateDate = DateTime.Now });
                }

                HttpContext.Session.SetString("TmpError", "logout succeed.");
            }
            catch (Exception ex)
            {
                HttpContext.Session.SetString("TmpError", ex.Message);
                _log.Insert(new MLogger { LogType = EnLog.Error, Message = "HomeController.Logout#1-" + ex.Message, CreateBy = "system", CreateDate = DateTime.Now });
            }
            return RedirectToAction(nameof(Index));
        }

        private string GenerateJSONWebToken(MLogin userInfo)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"]));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);
            var token = new JwtSecurityToken(_config["Jwt:Issuer"], _config["Jwt:Issuer"], null, expires: DateTime.Now.AddMinutes(60), signingCredentials: credentials);
            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
}
