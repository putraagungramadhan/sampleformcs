﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using NRG_Core.DataModels;

namespace NRG_Core.Context
{
    public class WebContext : IdentityDbContext
    {
        public WebContext(DbContextOptions<WebContext> options)
            : base(options)
        {
        }

        public DbSet<MControl> ControlCtx { get; set; }
        public DbSet<MControlImages> CtrlImgeCtx { get; set; }

        public DbSet<MPatient> PatientCtx { get; set; }
        public DbSet<MHospital> HospitalCtx { get; set; }
        public DbSet<MSurgeon> SurgeonCtx { get; set; }
        public DbSet<MLogin> LoginCtx { get; set; }
        public DbSet<MLogger> LoggerCtx { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<MControl>().ToTable("MControl");
            modelBuilder.Entity<MControlImages>().ToTable("MControlImages");

            modelBuilder.Entity<MPatient>().ToTable("MPatient");
            modelBuilder.Entity<MHospital>().ToTable("MHospital");
            modelBuilder.Entity<MSurgeon>().ToTable("MSurgeon");
            modelBuilder.Entity<MLogin>().ToTable("MLogin");
            modelBuilder.Entity<MLogger>().ToTable("Logger");
        }
    }
}
