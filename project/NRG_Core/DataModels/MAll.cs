﻿using System;
using System.Globalization;

namespace NRG_Core.DataModels
{
    public class DetailEnt
    {
        public DetailEnt()
        {
            Status = true;
            CreateDate = DateTime.Now;
        }

        public bool Status { get; set; }
        public string IsActive { get { return Status ? "Yes" : "No"; } }

        public string CreateBy { get; set; }
        public string UpdateBy { get; set; }

        public DateTime CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }

        public string strCreateDate { get { return CreateDate.ToString("dd/MM/yy HH:mm"); } }
        public string strUpdateDate { get { return UpdateDate != null ? UpdateDate.Value.ToString("dd/MM/yy HH:mm") : ""; } }
        public string strCreateDate2 { get { return CreateDate.ToString("dd MMM yyy"); } }
    }

    public class ResponseEnt
    {
        public string code { get; set; }
        public string message { get; set; }
        public string data { get; set; }
        public bool status { get { return code == "00000" ? true : false; } }
    }

    public class ListSelectModel
    {
        public int id { get; set; }
        public string value { get; set; }
    }

    public class SearchModel
    {
        public int tour { get; set; }
        public int region { get; set; }
        public int country { get; set; }
        public string departdate { get; set; }
        public int duration { get; set; }
        public DateTime tanggal { get { return !string.IsNullOrEmpty(departdate) ? DateTime.ParseExact(departdate, "d/M/yyyy", CultureInfo.InvariantCulture) : DateTime.Now; } }
    }
}
