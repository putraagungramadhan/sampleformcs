﻿using Microsoft.AspNetCore.Identity;
using System;

namespace NRG_Core.DataModels
{
    public class MApplicationUser : IdentityUser
    {
        public string Name { get; set; }
        public DateTime UpdatedTime { get; set; }
        public string UpdatedBy_Name { get; set; }
    }
}
