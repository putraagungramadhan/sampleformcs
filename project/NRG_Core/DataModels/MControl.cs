﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace NRG_Core.DataModels
{
    public class MControl : DetailEnt
    {
        [Key]
        public int ID { get; set; }
        public string Code { get; set; }
        public int PatientID { get; set; }
        public DateTime ComeinDate { get; set; }
        public EnControl ControlType { get; set; }
        public string MedicalRecord { get; set; }
        public int HospitalID { get; set; }
        public int SurgeryID { get; set; }
        public string GOS { get; set; } //glasgow outcome scale
        public string Diagnose { get; set; }
        public int HeadDiameter { get; set; } //for VP Shunt & ETV else 0 
        public string Note { get; set; }

        public string strComeinDate { get { return ComeinDate.ToString("dd/MM/yy HH:mm"); } }
    }

    public class MControlImages
    {
        [Key]
        public int ID { get; set; }
        public int ControlID { get; set; }
        public EnControlImage ImageType { get; set; }
        public string Description { get; set; }

        public string Name { get; set; }
        public byte[] Data { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public string ContentType { get; set; }

        public string CreateBy { get; set; }
        public DateTime CreateDate { get; set; }

        public string strData { get { return Convert.ToBase64String(Data); } }
        public string strLoad { get { return "data:image/" + ContentType + ";base64," + strData; } }
        public string strCreateDate { get { return CreateDate.ToString("dd/MM/yy HH:mm"); } }
    }

    public class VControl
    {
        public MControl Master { get; set; }
        public List<MControlImages> Picts { get; set; }
    }

    public class PControl
    {
        public MControl Master { get; set; }
        public List<IFormFile> Images { get; set; }
        public string DeletedID { get; set; }
    }

    public class PControlImages
    {
        public int ControlID { get; set; }
        public string Name { get; set; }
        public string Data { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public string CreateBy { get; set; }
    }
}