﻿using System.ComponentModel.DataAnnotations;

namespace NRG_Core.DataModels
{
    public enum EnRole
    {
        SA = 0
    }

    public enum EnLog
    {
        Ok = 1,
        Error = 2
    }

    public enum EnTask
    {
        New = 0,
        Edit = 1,
        Delete = 2
    }

    public enum EnGender
    {
        Female = 0,
        Male = 1
    }

    public enum EnControl
    {
        [Display(Name = "Pre OP")]
        Pre = 1,
        [Display(Name = "Post OP")]
        Post = 2
    }

    public enum EnControlImage
    {
        [Display(Name = "CT Scan Pre OP")]
        CTSPre = 1,
        [Display(Name = "CT Scan Emergency")]
        CTSE = 2,
        [Display(Name = "CT Scan Post OP")]
        CTSPos = 3,
        [Display(Name = "Patient")]
        Patient = 4
    }
}
