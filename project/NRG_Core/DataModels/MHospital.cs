﻿using System;
using System.ComponentModel.DataAnnotations;

namespace NRG_Core.DataModels
{
    public class MHospital : DetailEnt
    {
        [Key]
        public int ID { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Mail { get; set; }
        public string Address { get; set; }
        public string Description { get; set; }

        public string strName { get { return Code + " - " + Name; } }
        public string strInfo { get { return Address + ". phone: " + Phone + " - email: " + Mail; } }
    }

    public class TmpHospital
    {
        public int ID { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Mail { get; set; }
        public string Address { get; set; }
        public string Description { get; set; }
        public bool Status { get; set; }
        public string CreateBy { get; set; }
        public DateTime CreateDate { get; set; }
    }
}