﻿using System;
using System.ComponentModel.DataAnnotations;

namespace NRG_Core.DataModels
{
    public class MLogger
    {
        [Key]
        public int ID { get; set; }
        public EnLog LogType { get; set; }
        public string Message { get; set; }
        public string CreateBy { get; set; }
        public DateTime CreateDate { get; set; }
    }
}