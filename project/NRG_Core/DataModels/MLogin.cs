﻿using System;
using System.ComponentModel.DataAnnotations;

namespace NRG_Core.DataModels
{
 //   [ID] [int] IDENTITY(1,1) NOT NULL,
 //   [RoleID] [int]
 //   [Username] [varchar] (200) NULL,
	//[Passwd] [varchar] (max) NOT NULL,
	//[CreatedDate][datetime]
 //   [CreatedBy] [varchar] (100) NOT NULL,
 //    [UpdatedDate] [datetime] NULL,
	//[UpdatedBy] [varchar] (100) NULL,
	//[IsActive][bit]
 //   [IsLogin] [bit]
 //   [LastLogin] [datetime]
 //   [SingleLogin] [bit]

    public class MLogin
    {
        [Key]
        public int ID { get; set; }
        public EnRole RoleID { get; set; }
        public string Fullname { get; set; }
        [Required(ErrorMessage = "Username is required")]
        public string Username { get; set; }
        //public string FullName { get; set; }
        //public string Email { get; set; }
        [Required(ErrorMessage = "Password is required")]
        [DataType(DataType.Password)]
        public string Passwd { get; set; }

        //public string Token { get; set; }
        public bool IsActive { get; set; }
        public bool IsLogin { get; set; }
        public bool SingleLogin { get; set; }
        public DateTime LastLogin { get; set; }
        //public bool Success { get; set; }

        public string strRole { get { return RoleID.ToString(); } }
    }

    //public class MResp
    //{
    //    public string code { get; set; }
    //    public string message { get; set; }
    //    public string data { get; set; }
    //}
}
