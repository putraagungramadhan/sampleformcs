﻿using System;
using System.ComponentModel.DataAnnotations;

namespace NRG_Core.DataModels
{
    public class MPatient : DetailEnt
    {
        [Key]
        public int ID { get; set; }
        public string Code { get; set; } //rekam medik
        public string Name { get; set; }
        public DateTime DOB { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public EnGender Gender { get; set; }
        public string Description { get; set; }

        public string strName { get { return Code + " - " + Name; } }
        public string strAge { get { return DateTime.Today.Year == DOB.Year ? (DateTime.Today.Month - DOB.Month).ToString() + " bulan" : (DateTime.Today.Year - DOB.Year).ToString() + " tahun"; } }
        public string strGender { get { return Gender.ToString(); } }
        public string strDOB { get { return DOB.ToString("dd/MM/yyyy"); } }
    }
}