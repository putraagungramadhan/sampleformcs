﻿using System.ComponentModel.DataAnnotations;

namespace NRG_Core.DataModels
{
    public class MSurgeon : DetailEnt
    {
        [Key]
        public int ID { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public bool IsHead { get; set; } //Need Head Diameter
        public string Description { get; set; }

        public string strName { get { return Code + " - " + Name; } }
    }
}