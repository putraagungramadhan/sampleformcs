﻿using Microsoft.Data.SqlClient;
using System;
using System.Collections;
using System.Data;

namespace NRG_Core.Function
{
    public class OFDBAdaptor
    {
        public void ExecSP(string sp, Hashtable hash, string v)
        {
            SqlConnection objCon = new SqlConnection(v);
            SqlCommand objCmd = new SqlCommand(sp, objCon);
            SqlDataAdapter objDa = new SqlDataAdapter();
            DataTable objDt = new DataTable("Data");
            try
            {
                objCmd.CommandType = CommandType.StoredProcedure;
                objCon.Open();
                if ((hash != null))
                {
                    SqlCommandBuilder.DeriveParameters(objCmd);
                    foreach (DictionaryEntry e in hash)
                    {
                        string k = Convert.ToString(e.Key);

                        objCmd.Parameters[k].Value = e.Value;
                    }

                }
                objDt.Clear();
                objDa.SelectCommand = objCmd;
                objCmd.ExecuteNonQuery();
            }
            catch (SqlException sqlEx)
            {
                throw new Exception("EXEC SP: " + sp + " Error: " + sqlEx.Message);
            }
            catch (Exception ex)
            {
                throw new Exception("EXEC SP:" + sp + " Error : " + ex.Message);
            }
            finally
            {
                objDa.Dispose();
                objCmd.Dispose();
                objCon.Close();
                objCon.Dispose();
            }
        }

        public DataTable GetTable(string sp, Hashtable hash, string v)
        {
            SqlConnection objCon = new SqlConnection(v);
            SqlCommand objCmd = new SqlCommand(sp, objCon);
            SqlDataAdapter objDa = new SqlDataAdapter();
            DataTable objDt = new DataTable("Data");

            try
            {
                objCmd.CommandType = CommandType.StoredProcedure;
                objCon.Open();
                if ((hash != null))
                {
                    SqlCommandBuilder.DeriveParameters(objCmd);
                    foreach (DictionaryEntry e in hash)
                    {
                        string k = Convert.ToString(e.Key);
                        objCmd.Parameters[k].Value = e.Value;
                    }
                }
                objDt.Clear();
                objDa.SelectCommand = objCmd;
                objDa.Fill(objDt);
                return objDt;
            }
            catch (SqlException sqlEx)
            {
                throw new Exception("Get Table ERROR SP:" + sp + " Error : " + sqlEx.Message);
            }
            catch (Exception ex)
            {
                throw new Exception("Get Table ERROR SP:" + sp + " Error : " + ex.Message);
            }
            finally
            {
                objDa.Dispose();
                objCmd.Dispose();
                objCon.Close();
                objCon.Dispose();
            }
        }
    }
}