﻿using System;

namespace NRG_Core.Function
{

    public class OFPassword
    {
        static string res = string.Empty;
        OFED ed = new OFED();

        public string EncryptIT(string txt)
        {
            return ed.Encrypt(txt);
        }

        public string DecryptIT(string txt)
        {
            return ed.Decrypt(txt);
        }

        public string SecureCache(string[] data)
        {
            string vals = DateTime.Now.ToString("ss") + "|";
            foreach (string d in data)
            {
                vals = vals + d + "|";
            }
            return EncryptIT(vals);
        }

        public string[] OpenCache(string data)
        {
            string[] vals = DecryptIT(data).Split("|");
            string[] NewVals = new string[3];
            for(int i=1;i <= vals.Length-2; i++)
            {
                NewVals[i-1] = vals[i];
            }
            return NewVals;
        }
    }
}
