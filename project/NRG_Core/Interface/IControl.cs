﻿using NRG_Core.DataModels;
using System.Collections.Generic;

namespace NRG_Core.Interface
{
    public interface IControl
    {
        string All(int id);
        string Update(MControl mdl, List<MControlImages> imgs);
        //bool UpdateBulkImage(MControlImages img);
        //string UpdateImage(int controlId, string imgName, string imgData, string by);
        string Remove(int id, string by);
    }
}