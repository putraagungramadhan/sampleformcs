﻿using NRG_Core.DataModels;
using System.Collections.Generic;

namespace NRG_Core.Interface
{
    public interface IHospital
    {
        List<MHospital> Combo();
        string All(int id);
        string Update(MHospital mdl);
        string Remove(int id, string by);
    }
}