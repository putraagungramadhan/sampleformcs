﻿using NRG_Core.DataModels;

namespace NRG_Core.Interface
{
    public interface ILogger
    {
        string Get();
        void Insert(MLogger mdl);
    }
}