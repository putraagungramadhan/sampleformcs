﻿namespace NRG_Core.Interface
{
    public interface ILogin
    {
        //string Register(MLogin prm, string dbCon);
        string Login(string name, string pswd);
        string Logout(string name);
    }
}
