﻿using NRG_Core.DataModels;
using System.Collections.Generic;

namespace NRG_Core.Interface
{
    public interface IPatient
    {
        List<MPatient> Combo();
        string All(int id);
        string Update(MPatient mdl);
        string Remove(int id, string by);
    }
}