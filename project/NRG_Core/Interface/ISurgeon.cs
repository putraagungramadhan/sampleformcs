﻿using NRG_Core.DataModels;
using System.Collections.Generic;

namespace NRG_Core.Interface
{
    public interface ISurgeon
    {
        List<MSurgeon> Combo();
        string All(int id);
        string Update(MSurgeon mdl);
        string Remove(int id, string by);
    }
}