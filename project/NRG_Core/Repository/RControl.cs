﻿using Newtonsoft.Json;
using NRG_Core.Context;
using NRG_Core.DataModels;
using NRG_Core.Interface;
using System;
using System.Collections.Generic;
using System.Linq;

namespace NRG_Core.Repository
{
    public class RControl : IControl
    {
        private readonly WebContext _db;

        public RControl(WebContext db)
        {
            _db = db;
        }

        private string GenerateCode()
        {
            var tgl = DateTime.Now.ToString("yyyMMddHHmm");
            var num = _db.ControlCtx.Count() + 1;
            return "CT" + tgl + num.ToString("#00");
        }

        public string All(int id)
        {
            var result = new ResponseEnt
            {
                code = "ERG99",
                message = "Error in RControl, All"
            };

            try
            {
                var datas = _db.ControlCtx.Where(w => w.Status).ToList();
                if (datas.Count > 0)
                {
                    if (id > 0)
                        datas = datas.Where(w => w.ID == id).ToList();

                    var temp = new List<VControl>();
                    foreach (var t in datas)
                    {
                        var imgs = _db.CtrlImgeCtx.Where(w => w.ControlID == t.ID).ToList();
                        temp.Add(new VControl
                        {
                            Master = t,
                            Picts = imgs
                        });
                    }

                    //var rw = string.Empty;
                    //if (id > 0)
                    //    rw = JsonConvert.SerializeObject(tbl.Where(w => w.ID == id)).ToString();
                    //else
                    //    rw = JsonConvert.SerializeObject(tbl).ToString();

                    result = new ResponseEnt
                    {
                        code = "00000",
                        message = "Ok",
                        data = JsonConvert.SerializeObject(temp).ToString()
                    };
                }
            }
            catch (Exception ex)
            {
                result.message += ex.InnerException.Message + ", " + ex.Message;
            }
            return JsonConvert.SerializeObject(result).ToString();
        }

        public string Update(MControl mdl, List<MControlImages> imgs)
        {
            var result = new ResponseEnt
            {
                code = "ERG99",
                message = "Error in RControl, Update"
            };

            try
            {
                if (mdl.ID == 0)
                {
                    mdl.Code = GenerateCode();
                    mdl.Status = true;
                    mdl.CreateDate = DateTime.Now;
                }
                else
                {
                    mdl.UpdateBy = mdl.CreateBy;
                    mdl.UpdateDate = DateTime.Now;
                }

                _db.ControlCtx.Update(mdl);
                _db.SaveChanges();
                
                if (mdl.ID > 0)
                {
                    foreach (var i in imgs)
                        i.ControlID = mdl.ID;
                    var x = UpdateBulkImage(imgs);

                    result.code = "00000";
                    result.message = "Ok";
                    result.data = JsonConvert.SerializeObject(mdl);
                }
                return JsonConvert.SerializeObject(result).ToString();
            }
            catch (Exception ex)
            {
                result.message += ex.InnerException.Message + ", " + ex.Message;
                return JsonConvert.SerializeObject(result).ToString();
            }
        }

        private bool UpdateBulkImage(List<MControlImages> imgs)
        {
            try
            {
                _db.CtrlImgeCtx.UpdateRange(imgs);
                _db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                var err = ex.Message.ToString();
                return false;
            }
        }

        public string Remove(int id, string by)
        {
            var result = new ResponseEnt
            {
                code = "ERG99",
                message = "Error in RControl, Remove"
            };

            try
            {
                var data = _db.ControlCtx.FirstOrDefault(f => f.ID == id);
                data.Status = false;
                data.UpdateBy = by;
                data.UpdateDate = DateTime.Now;

                _db.ControlCtx.Update(data);
                _db.SaveChanges();

                result.code = "00000";
                result.message = "Ok";
                return JsonConvert.SerializeObject(result).ToString();
            }
            catch (Exception ex)
            {
                result.message += ex.InnerException.Message + ", " + ex.Message;
                return JsonConvert.SerializeObject(result).ToString();
            }
        }
    }
}
