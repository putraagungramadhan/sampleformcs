﻿using Newtonsoft.Json;
using NRG_Core.Context;
using NRG_Core.DataModels;
using NRG_Core.Interface;
using System;
using System.Collections.Generic;
using System.Linq;

namespace NRG_Core.Repository
{
    public class RHospital : IHospital
    {
        private readonly WebContext _db;

        public RHospital(WebContext db)
        {
            _db = db;
        }

        public List<MHospital> Combo()
        {
            return _db.HospitalCtx.Where(w => w.Status).OrderBy(o => o.Name).ToList();
        }

        private string GenerateCode()
        {
            var num = _db.HospitalCtx.Count() + 1;
            return "HSP" + num.ToString("#0");
        }

        public string All(int id)
        {
            var result = new ResponseEnt
            {
                code = "ERG99",
                message = "Error in RHospital, All"
            };

            try
            {
                var tbl = _db.HospitalCtx.Where(w => w.Status).ToList();
                if (tbl.Count > 0)
                {
                    var rw = string.Empty;
                    if (id > 0)
                        rw = JsonConvert.SerializeObject(tbl.Where(w => w.ID == id)).ToString();
                    else
                        rw = JsonConvert.SerializeObject(tbl).ToString();

                    result = new ResponseEnt
                    {
                        code = "00000",
                        message = "Ok",
                        data = rw
                    };
                }
            }
            catch (Exception ex)
            {
                result.message += ex.InnerException.Message + ", " + ex.Message;
            }
            return JsonConvert.SerializeObject(result).ToString();
        }

        public string Update(MHospital mdl)
        {
            var result = new ResponseEnt
            {
                code = "ERG99",
                message = "Error in RHospital, Update"
            };

            try
            {
                mdl.Phone.Replace("(", "").Replace(")", "");

                if (mdl.ID == 0)
                {
                    mdl.Code = GenerateCode();
                    mdl.Status = true;
                    mdl.CreateDate = DateTime.Now;
                }
                else
                {
                    mdl.UpdateBy = mdl.CreateBy;
                    mdl.UpdateDate = DateTime.Now;
                }

                _db.HospitalCtx.Update(mdl);
                _db.SaveChanges();

                if (mdl.ID > 0)
                {
                    result.code = "00000";
                    result.message = "Ok";
                }
                return JsonConvert.SerializeObject(result).ToString();
            }
            catch (Exception ex)
            {
                result.message += ex.InnerException.Message + ", " + ex.Message;
                return JsonConvert.SerializeObject(result).ToString();
            }
        }

        public string Remove(int id, string by)
        {
            var result = new ResponseEnt
            {
                code = "ERG99",
                message = "Error in RHospital, Remove"
            };

            try
            {
                var data = _db.HospitalCtx.FirstOrDefault(f => f.ID == id);
                data.Status = false;
                data.UpdateBy = by;
                data.UpdateDate = DateTime.Now;

                _db.HospitalCtx.Update(data);
                _db.SaveChanges();

                result.code = "00000";
                result.message = "Ok";
                return JsonConvert.SerializeObject(result).ToString();
            }
            catch (Exception ex)
            {
                result.message += ex.InnerException.Message + ", " + ex.Message;
                return JsonConvert.SerializeObject(result).ToString();
            }
        }
    }
}
