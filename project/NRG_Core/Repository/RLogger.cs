﻿using Newtonsoft.Json;
using NRG_Core.Context;
using NRG_Core.DataModels;
using NRG_Core.Interface;
using System;
using System.Linq;

namespace NRG_Core.Repository
{
    public class RLogger : ILogger
    {
        private readonly WebContext _db;

        public RLogger(WebContext db)
        {
            _db = db;
        }

        public string Get()
        {
            try
            {
                var result = _db.LoggerCtx.OrderByDescending(o => o.CreateDate).ToList();
                return JsonConvert.SerializeObject(result).ToString();
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(ex.Message).ToString();
            }
        }

        public void Insert(MLogger mdl)
        {
            try
            {
                _db.LoggerCtx.Add(mdl);
                _db.SaveChanges();
            }
            catch (Exception ex)
            {
                var errmsg = ex.InnerException.Message + ", " + ex.Message;
            }
        }
    }
}
