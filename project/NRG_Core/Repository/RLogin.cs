﻿using Newtonsoft.Json;
using NRG_Core.Context;
using NRG_Core.DataModels;
using NRG_Core.Interface;
using System;
using System.Linq;

namespace NRG_Core.Repository
{
    public class RLogin: ILogin
    {
        #region SP
        //public string Register(MLogin prm, string dbCon)
        //{
        //    try
        //    {
        //        var hst = new Hashtable();
        //        hst.Add("@Email", prm.Email);
        //        hst.Add("@HashPassword", prm.Passwd);

        //        var dt = _dba.GetTable("[SP_LOGIN]", hst, dbCon);

        //        var result = new MLogin();
        //        foreach (DataRow dr in dt.Rows)
        //        {
        //            result.ID = Convert.ToInt32(dr["CompanyID"].ToString());
        //            result.Username = dr["UserName"].ToString();
        //            result.Email = prm.Email;
        //            result.RoleID = (EnRole)Convert.ToInt16(dr["RoleID"].ToString());
        //        }
        //        return JsonConvert.SerializeObject(result).ToString();
        //    }
        //    catch (Exception ex)
        //    {
        //        return ex.Message.ToString();
        //    }
        //}
        #endregion

        private readonly WebContext _db;

        public RLogin(WebContext db)
        {
            _db = db;
        }

        public string Login(string name, string pswd)
        {
            var response = new ResponseEnt { code = "ERG99" };

            try
            {
                var data = _db.LoginCtx.FirstOrDefault(f => f.Username == name);
                if (data != null)
                {
                    if (data.Passwd != pswd)
                    {
                        response.code = "ERL01";
                        response.message = "Wrong password attemp. Please try again.";
                    }
                    else if (!data.IsActive)
                    {
                        response.code = "ERL02";
                        response.message = "Account is not active. Please contact administrator to login.";
                    }
                    else
                    {
                        response.code = "00000";
                        response.message = "Ok.";
                        response.data = JsonConvert.SerializeObject(data).ToString();
                    }
                }
                else
                {
                    response.code = "ERL03";
                    response.message = "Account not found. Please try another account to login.";
                }
            }
            catch (Exception ex)
            {
                response.message = ex.Message.ToString();
            }

            return JsonConvert.SerializeObject(response).ToString();
        }

        public string Logout(string name)
        {
            var response = new ResponseEnt { code = "ERG99" };

            try
            {
                var data = _db.LoginCtx.FirstOrDefault(f => f.Username == name);
                if (!data.IsLogin)
                {
                    response.code = "ERL11";
                    response.message = "User isn't login now.";
                }
                else
                {
                    data.IsLogin = false;
                    data.LastLogin = DateTime.Now;
                    _db.LoginCtx.Update(data);
                    _db.SaveChanges();

                    response.code = "00000";
                    response.message = "Ok.";
                    response.data = JsonConvert.SerializeObject(data).ToString();
                }
            }
            catch (Exception ex)
            {
                response.message = ex.Message.ToString();
            }

            return JsonConvert.SerializeObject(response).ToString();
        }
    }
}
