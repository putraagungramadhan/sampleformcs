﻿using Newtonsoft.Json;
using NRG_Core.Context;
using NRG_Core.DataModels;
using NRG_Core.Interface;
using System;
using System.Collections.Generic;
using System.Linq;

namespace NRG_Core.Repository
{
    public class RPatient : IPatient
    {
        private readonly WebContext _db;

        public RPatient(WebContext db)
        {
            _db = db;
        }

        private string GenerateCode()
        {
            var num = _db.PatientCtx.Count() + 1;
            return "PTI" + num.ToString();
        }

        public List<MPatient> Combo()
        {
            return _db.PatientCtx.Where(w => w.Status).OrderBy(o => o.Name).ToList();
        }

        public string All(int id)
        {
            var result = new ResponseEnt
            {
                code = "ERG99",
                message = "Error in RPatient, All"
            };

            try
            {
                var tbl = _db.PatientCtx.Where(w => w.Status).ToList();
                if (tbl.Count > 0)
                {
                    var rw = string.Empty;
                    if (id > 0)
                        rw = JsonConvert.SerializeObject(tbl.Where(w => w.ID == id)).ToString();
                    else
                        rw = JsonConvert.SerializeObject(tbl).ToString();

                    result = new ResponseEnt
                    {
                        code = "00000",
                        message = "Ok",
                        data = rw
                    };
                }
            }
            catch (Exception ex)
            {
                result.message += ex.InnerException.Message + ", " + ex.Message;
            }
            return JsonConvert.SerializeObject(result).ToString();
        }

        public string Update(MPatient mdl)
        {
            var result = new ResponseEnt
            {
                code = "ERG99",
                message = "Error in RPatient, Update"
            };

            try
            {
                if (mdl.ID == 0)
                {
                    mdl.Code = GenerateCode();
                    mdl.Status = true;
                    mdl.CreateDate = DateTime.Now;
                }
                else
                {
                    mdl.UpdateBy = mdl.CreateBy;
                    mdl.UpdateDate = DateTime.Now;
                }

                _db.PatientCtx.Update(mdl);
                _db.SaveChanges();

                if (mdl.ID > 0)
                {
                    result.code = "00000";
                    result.message = "Ok";
                }
                return JsonConvert.SerializeObject(result).ToString();
            }
            catch (Exception ex)
            {
                result.message += ex.InnerException.Message + ", " + ex.Message;
                return JsonConvert.SerializeObject(result).ToString();
            }
        }

        public string Remove(int id, string by)
        {
            var result = new ResponseEnt
            {
                code = "ERG99",
                message = "Error in RPatient, Remove"
            };

            try
            {
                var data = _db.PatientCtx.FirstOrDefault(f => f.ID == id);
                data.Status = false;
                data.UpdateBy = by;
                data.UpdateDate = DateTime.Now;

                _db.PatientCtx.Update(data);
                _db.SaveChanges();

                result.code = "00000";
                result.message = "Ok";
                return JsonConvert.SerializeObject(result).ToString();
            }
            catch (Exception ex)
            {
                result.message += ex.InnerException.Message + ", " + ex.Message;
                return JsonConvert.SerializeObject(result).ToString();
            }
        }
    }
}
