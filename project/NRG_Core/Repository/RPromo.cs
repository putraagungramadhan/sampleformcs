﻿using NRG_Core.Context;
using NRG_Core.Interface;

namespace NRG_Core.Repository
{
    public class RPromo : IPromo
    {
        private readonly WebContext _db;

        public RPromo(WebContext db)
        {
            _db = db;
        }

        //public string All(int id)
        //{
        //    try
        //    {
        //        var result = new List<MPromo>();
        //        var master = _db.Promo.Where(w => w.Status).ToList();
        //        if (master.Count > 0)
        //        {
        //            foreach (var m in master)
        //            {
        //                var nama = "";
        //                if (m.PromoType == EnPromo.Tour)
        //                    nama = _db.Tours.FirstOrDefault(f => f.ID == int.Parse(m.ItemID)).Name;
        //                if (m.PromoType == EnPromo.Ticket)
        //                    nama = _db.Products.FirstOrDefault(f => f.Guid == m.ItemID).Name;

        //                if (m.PromoType == EnPromo.Visa)
        //                {
        //                    var ctr = _db.Docs.FirstOrDefault(f => f.ID == int.Parse(m.ItemID)).CountryID;
        //                    nama = _db.Countries.FirstOrDefault(f => f.ID == ctr).Name;
        //                }

        //                if (id == 0)
        //                    m.ItemID = nama;

        //                result.Add(m);
        //            }

        //            if (id > 0)
        //                return JsonConvert.SerializeObject(result.FirstOrDefault(f => f.ID == id)).ToString();
        //            else
        //                return JsonConvert.SerializeObject(result).ToString();
        //        }
        //        else
        //        {
        //            return string.Empty;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        return JsonConvert.SerializeObject(ex.Message.ToString()).ToString();
        //    }
        //}

        //public string PromoOnly()
        //{
        //    try
        //    {
        //        var result = new List<VPromo>();
        //        var master = _db.Promo.Where(w => w.Status).ToList();
        //        if (master.Count > 0)
        //        {
        //            var skrg = DateTime.Now;

        //            foreach (var m in master)
        //            {
        //                if (m.dtUpToDate > skrg)
        //                {
        //                    var link = "/Document/Visa";
        //                    if (m.PromoType == EnPromo.Tour)
        //                        link = "/Tour/Detail?tour=" + m.ItemID;
        //                    if (m.PromoType == EnPromo.Ticket)
        //                        link = "/Ticket/Detail?g=" + m.ItemID;

        //                    result.Add(new VPromo { Tipe = m.PromoType, ImageName = m.ImgName, Link = link, Image = m.strLoad });
        //                }
        //            }
        //            return JsonConvert.SerializeObject(result).ToString();
        //        }
        //        else
        //        {
        //            return string.Empty;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        return JsonConvert.SerializeObject(ex.Message.ToString()).ToString();
        //    }
        //}

        //public string PromoByType(EnPromo tipe)
        //{
        //    try
        //    {
        //        var result = new List<VPromo>();
        //        var master = _db.Promo.Where(w => w.Status && w.PromoType == tipe).ToList();
        //        if (master.Count > 0)
        //        {
        //            var skrg = DateTime.Now;

        //            foreach (var m in master)
        //            {
        //                if (m.dtUpToDate > skrg)
        //                {
        //                    var title = string.Empty;
        //                    var link = string.Empty;

        //                    if (tipe == EnPromo.Tour)
        //                    {
        //                        title = _db.Tours.FirstOrDefault(f => f.ID == int.Parse(m.ItemID)).lblName;
        //                        link = "/Tour/Detail?tour=" + m.ItemID;
        //                    }
        //                    else if (tipe == EnPromo.Ticket)
        //                    {
        //                        title = _db.Products.FirstOrDefault(f => f.ID == int.Parse(m.ItemID)).lblName;
        //                        link = "/Ticket/Detail?g=" + m.ItemID;
        //                    }
        //                    else
        //                    {
        //                        var negara = _db.Docs.FirstOrDefault(f => f.ID == int.Parse(m.ItemID));
        //                        title = "Visa " + _db.Countries.FirstOrDefault(f => f.ID == negara.CountryID).Name;
        //                        link = "/Document/Visa";
        //                    }

        //                    result.Add(new VPromo { Title = title, Info = "", Tipe = m.PromoType, ImageName = m.ImgName, Link = link, Image = m.strLoad });
        //                }
        //            }
        //            return JsonConvert.SerializeObject(result).ToString();
        //        }
        //        else
        //        {
        //            return string.Empty;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        return JsonConvert.SerializeObject(ex.Message.ToString()).ToString();
        //    }
        //}

        //public string Update(MPromo mdl, IFormFile img, string by)
        //{
        //    var result = new ResponseEnt
        //    {
        //        code = "001",
        //        message = "Error in RTours, Update"
        //    };

        //    try
        //    {
        //        if (img != null || img.ContentType.ToLower().StartsWith("image/"))
        //        {
        //            var ms = new MemoryStream();
        //            img.OpenReadStream().CopyTo(ms);
        //            var image = Image.FromStream(ms);

        //            mdl.ImgName = img.Name;
        //            mdl.ImgData = ms.ToArray();
        //            mdl.ImgWidth = image.Width;
        //            mdl.ImgHeight = image.Height;
        //            mdl.ImgContentType = img.ContentType;
        //        }

        //        if (mdl.ID == 0)
        //        {
        //            mdl.Status = true;
        //            mdl.CreateBy = by;
        //            mdl.CreateDate = DateTime.Now;
        //        }
        //        else
        //        {
        //            mdl.UpdateBy = by;
        //            mdl.UpdateDate = DateTime.Now;
        //        }

        //        _db.Promo.Update(mdl);
        //        _db.SaveChanges();
                
        //        if (mdl.ID > 0) {
        //            result.code = "000";
        //            result.message = "Ok";
        //        }
        //        return JsonConvert.SerializeObject(result).ToString();
        //    }
        //    catch (Exception ex)
        //    {
        //        result.message = ex.Message.ToString();
        //        return JsonConvert.SerializeObject(result).ToString();
        //    }
        //}

        //public bool Remove(int id)
        //{
        //    try
        //    {
        //        var data = _db.Promo.FirstOrDefault(f => f.ID == id);
        //        data.Status = false;
                
        //        _db.Promo.Update(data);
        //        _db.SaveChanges();
        //        return true;
        //    }
        //    catch (Exception ex)
        //    {
        //        var err = ex.Message.ToString();
        //        return false;
        //    }
        //}

        //public bool Cloning(int id, string by)
        //{
        //    try
        //    {
        //        var data = _db.Promo.FirstOrDefault(f => f.ID == id);
        //        var mdl = new MPromo
        //        {
        //            PromoType = data.PromoType,
        //            ItemID = data.ItemID,
        //            UpToDate = data.UpToDate,
        //            Visibility = data.Visibility,
        //            ImgName = data.ImgName,
        //            ImgData = data.ImgData,
        //            ImgHeight = data.ImgHeight,
        //            ImgWidth = data.ImgWidth,
        //            ImgContentType = data.ImgContentType,
        //            Status = true,
        //            CreateBy = by,
        //            CreateDate = DateTime.Now
        //        };

        //        _db.Promo.Update(mdl);
        //        _db.SaveChanges();
        //        return true;
        //    }
        //    catch (Exception ex)
        //    {
        //        var err = ex.Message.ToString();
        //        return false;
        //    }
        //}
    }
}
